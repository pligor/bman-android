package receivers

import android.content.{Intent, Context, BroadcastReceiver}
import com.fortumo.android.Fortumo
import myandroid.log
import myfortumo.FortumoPaymentStatus
import preferences.FortumoDeviceReceiptPreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class FortumoPaymentStatusReceiver extends BroadcastReceiver {

  private val PAYMENT_STATUS_CHANGED_ACTION = "com.fortumo.android.PAYMENT_STATUS_CHANGED"

  def onReceive(context: Context, intent: Intent): Unit = {
    implicit val implicitContext = context

    intent.getAction match {
      case PAYMENT_STATUS_CHANGED_ACTION =>

        //do not try this, it is not working as expected
        //val paymentResponse = new PaymentResponse(intent)

        val fortumoPaymentStatus = FortumoPaymentStatus.createFromBundle(intent.getExtras)

        val billingStatusOption = fortumoPaymentStatus.billing_status

        if (billingStatusOption.isDefined) {
          billingStatusOption.get match {
            case Fortumo.MESSAGE_STATUS_BILLED | Fortumo.MESSAGE_STATUS_PENDING =>
              //here we say that right now the timestamp is now, but this will get refreshed on every next run
              //so we can sleep like dogs
              val fortumoDeviceReceiptOption = fortumoPaymentStatus.getFortumoDeviceReceipt
              if (fortumoDeviceReceiptOption.isDefined) {
                FortumoDeviceReceiptPreference.setValue(fortumoDeviceReceiptOption)
              } else {
                log log "this is very bad. billing status is good but payment code or user id are not present"
                log log "we dont even have bugsense available to send this error message"
              }

            case Fortumo.MESSAGE_STATUS_FAILED |
                 Fortumo.MESSAGE_STATUS_USE_ALTERNATIVE_METHOD |
                 Fortumo.MESSAGE_STATUS_NOT_SENT =>

              FortumoDeviceReceiptPreference.clear

            case _ => log log "we do not have a valid billing status therefore we do not know what to do"
          }
        } else {
          //nop
          log log "leave fortumo as it is because we do not know"
        }
    }

  }
}
