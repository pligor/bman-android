package com.pligor.bman

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object CommunicationReason extends Enumeration {
  val PING_PONG = Value;
  val SEND_CARD = Value;
}
