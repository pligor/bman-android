package com.pligor.bman

import android.app.Application
import java.util.{Calendar, UUID}
import android.content.{Context, Intent}
import java.security.Security
import org.spongycastle.jce.provider.BouncyCastleProvider
import generic.FileHelper._
import net.sqlcipher.database.SQLiteDatabase
import services.{SenderImmutableService, ReceiverImmutableService}
import preferences.{FortumoSubscriptionPreference, PreTrialPreference, FirstRunTimestampPreference, InAppUnmanagedSubscriptionPreference}
import simple.Simple
import myAndroidSqlite.MySQLiteOpenHelper

//TODO how to avoid memory leaks:
//http://android-developers.blogspot.de/2009/01/avoiding-memory-leaks.html
//http://stackoverflow.com/questions/1949066/java-lang-outofmemoryerror-bitmap-size-exceeds-vm-budget-android

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 *
 * In most cases, use the Context directly available to you from the enclosing component
 * you’re working within. You can safely hold a reference to it as long as that reference
 * does not extend beyond the lifecycle of that component. As soon as you need to save a
 * reference to a Context from an object that lives beyond your Activity or Service, even
 * temporarily, switch that reference you save over to the application context
 */
object Bman {
  //TODO change that to false before release
  val isDebuggable = true

  val trialPeriod = (6, Calendar.MONTH)

  def isSubscribed(implicit context: Context): Boolean = {
    InAppUnmanagedSubscriptionPreference.isSubscribed || FortumoSubscriptionPreference.isSubscribed
  }

  def isPremium(implicit context: Context): Boolean = {
    PreTrialPreference.getValue ||
      FirstRunTimestampPreference.isOnTrial ||
      isSubscribed
  }

  def getTrialPeriodString(implicit context: Context): String = {
    val months = trialPeriod._1
    months + " " + context.getString(
      if (months > 1) R.string.month_plural
      else R.string.month_singular
    )
  }

  val DIALOG_CONFIRM_RECEIVE_TIMEOUT_SECS = (Simple.REPLY_TIMEOUT_msec / 1000) - 7
  assert(DIALOG_CONFIRM_RECEIVE_TIMEOUT_SECS == 18)

  val FILE_EXT = "json"

  val PREFERENCES_KEY = "ak73pgPe7WK4CvvS"

  //directly taken from manifest
  val FORTUMO_PERMISSION = "com.pligor.bman.PAYMENT_BROADCAST_PERMISSION"

  //REFERENCE: http://www.bluecove.org/bluecove/apidocs/javax/bluetooth/UUID.html
  //pligorBmanInsecure
  val MY_UUID_INSECURE = UUID.fromString("e5959bfe-6201-2644-122f-c8894763c624")

  //pligorBmanSecure
  val MY_UUID_SECURE = UUID.fromString("c6cbe920-fc68-a862-ab01-a0c24ded0758")

  val MY_SERVICE_NAME_SECURE = "BmanSecure"
  
  val MY_SERVICE_NAME_INSECURE = "BmanInsecure"

  val PACKAGE_NAME = "com.pligor.bman"

  //we have a contact form in the website. No need to reveal email address
  //val ADMIN_MAIL = "creator@bman.co"
  val URL = "http://www.bman.co"

  val CARDS_URL = "http://www.bman.co/#!cards/c1dui"

  //fix this before release
  //val SERVER_LINK = "http://10.0.0.29:9000"
  val SERVER_LINK = "http://backend.bman.co"

  def killAllServices(implicit context: Context): Unit = {
    Seq(
      classOf[SenderImmutableService],
      classOf[ReceiverImmutableService]
    ) foreach {
      cl =>
        context.stopService(new Intent(context, cl))
    }
  }

  /*private var curActivity: Option[Activity] = None;
  def setCurActivity(act: Activity) {
    curActivity = Some(act);
  }*/

  private val SUBPATH_DIRECTORY = "DCIM/bman"

  val FULLPATH_DIRECTORY = sdSubpath2fullpath(SUBPATH_DIRECTORY)
}

class Bman extends Application {
  Security.addProvider(new BouncyCastleProvider())

  System.loadLibrary("alljoyn_java")

  //better try to see if bugsense can handle it
  //http://stackoverflow.com/questions/7680568/show-message-on-uncaught-exception
  /*Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler {
    def uncaughtException(thread: Thread, throwable: Throwable) {
      Logger("I am wondering if I could see some sort of something after crash");

      Bman.curActivity.map {
        curAct =>
          AndroidHelper.onUI({

            Toast.makeText(getApplicationContext, "yes we see after crash", Toast.LENGTH_LONG).show();
            val builder = new AlertDialog.Builder(getApplicationContext);
            builder.setTitle("title here");
            builder.setMessage("message here");
            builder.setNeutralButton("button", new OnClickListener {
              def onClick(p1: DialogInterface, p2: Int) {
                Logger("clicked");
              }
            });
            builder.create().show();

          })(curAct);
      }

      defaultUncaughtExceptionHandler.uncaughtException(thread, throwable);
    }
  });
  private lazy val defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler;*/

  override def onCreate(): Unit = {
    super.onCreate()

    MySQLiteOpenHelper.loadSqlcipherLibs(this)
  }

  override def onTerminate(): Unit = {
    super.onTerminate()

    Bman.killAllServices(this)
  }
}
