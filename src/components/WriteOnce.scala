package components

class WriteOnceException extends Exception

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class WriteOnce[T](onWritten: (T) => Unit = (param: T) => {}) {
  private var value: Option[T] = None

  def apply() = value.getOrElse(throw new UninitializedError)

  def isInitialized = value.isDefined

  def setValue(x: T): Unit = {
    if (isInitialized) {
      throw new WriteOnceException
    }
    else {
      value = Some(x)

      onWritten(x)
    }
  }

  def map[R](f: T => R): Option[R] = {
    if (isInitialized) {
      Some(
        f(value.get)
      )
    } else {
      None
    }
  }
}
