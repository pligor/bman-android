package components

import android.bluetooth.{BluetoothDevice, BluetoothAdapter}
import adapters.BluetoothNameListAdapter
import android.widget.{AdapterView, ListView}
import android.os.Bundle
import myandroid.AndroidHelper._
import android.widget.AdapterView.OnItemClickListener
import android.view.View
import models.BluetoothNameListItem
import android.content.{Context, IntentFilter, BroadcastReceiver, Intent}
import bluetooth.BluetoothHelper
import BluetoothHelper._
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait BluetoothList extends BluetoothActivity with BluetoothVerifying {
  //abstract

  protected def bluetoothNameListView: ListView;

  protected def onBluetoothDiscoveryStarted(): Unit;

  protected def onBluetoothDiscoveryFinished(): Unit;

  protected def onBluetoothListItemClick(nameListItem: BluetoothNameListItem): Unit;

  //concrete

  //you will have to refactor if a device is found with two bluetooth devices!! weird but it could happen
  private implicit lazy val bluetoothAdapter = Option(BluetoothAdapter.getDefaultAdapter)

  private lazy val bluetoothNameListAdapter = new BluetoothNameListAdapter;

  protected def isDiscovering: Boolean = bluetoothAdapter.exists(_.isDiscovering);

  protected def isBluetoothAvail = isBluetoothAvailable;

  protected def startBluetoothDiscovery = bluetoothAdapter.get.startDiscovery();

  override def onPostCreate(savedInstanceState: Bundle): Unit = {
    super.onPostCreate(savedInstanceState);

    bluetoothNameListView.setAdapter(bluetoothNameListAdapter);

    bluetoothNameListView.setOnItemClickListener(new OnItemClickListener {
      /**
       * avoid using 'id' at the current implementation
       */
      def onItemClick(parent: AdapterView[_], view: View, position: Int, id: Long): Unit = {
        val item = bluetoothNameListAdapter.getItem(position);
        onBluetoothListItemClick(item);
      }
    });
  }

  override def onStart(): Unit = {
    super.onStart();
    if (isBluetoothAvailable) {
      startupBroadcastReceiver();
    } else {
      log log "bluetooth is not available, bluetooth broadcast receiver is not enabled";
    }
  }

  override def onResume(): Unit = {
    super.onResume();
    if (isBluetoothAvailable) {
      startupDiscovery()
    } else {
      log log "bluetooth is not available, either not supported or disabled"
    }
  }

  override def onPause(): Unit = {
    super.onPause();
    if (isBluetoothAvailable) {
      val isCancelled = safeCancelDiscovery(bluetoothAdapter.get);
    } else {
      log log "bluetooth is disabled therefore we will not have discovery to cancel";
    }
  }

  override def onStop(): Unit = {
    super.onStop();
    safeUnregisterReceiver(broadcastReceiver);
  }

  protected def startupBluetoothList(): Unit = {
    require(isBluetoothAvailable, "call startup bluetooth list only if bluetooth is available");
    startupBroadcastReceiver();
    startupDiscovery();

    bluetoothNameListView.setVisibility(View.VISIBLE);
  }

  protected def shutdownBluetoothList(): Unit = {
    require(isBluetoothSupported, "call shutdown bluetooth list only if bluetooth is supported");

    safeCancelDiscovery(bluetoothAdapter.get);
    safeUnregisterReceiver(broadcastReceiver);

    bluetoothNameListView.setVisibility(View.GONE);
  }

  protected def onFoundTargetedUUID(device: BluetoothDevice): Unit = {
    bluetoothNameListAdapter.verifyItem(device);
    log log "we have found the targeted uuid for the device: " + device;
  }

  private object broadcastReceiver extends BroadcastReceiver {

    def onReceive(context: Context, intent: Intent): Unit = {
      intent.getAction match {
        case BluetoothAdapter.ACTION_DISCOVERY_STARTED => {
          log log "bluetooth discovery just started";
          clearFetchUUIDsTasks();
          bluetoothNameListAdapter.clearItems();
          onBluetoothDiscoveryStarted();
        }
        case BluetoothDevice.ACTION_FOUND => {
          val device: BluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
          val devRSSI = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MinValue);

          //sometimes we get a positive rssi and we need to fix that
          //(-math.abs(devRSSI)).toShort <--- this solution is unfair to other devices :P
          val fixedRSSI = if (devRSSI >= 0) Short.MinValue else devRSSI;

          val model = new BluetoothNameListItem(device, fixedRSSI);
          bluetoothNameListAdapter.addItem(model);

          //discoveredDevices += device -> devRSSI;
          log log "bluetooth discovered device " + device.getName + " (" + device.getAddress + ") with rssi: " + devRSSI;

          //onBluetoothDeviceFound(model);
        }
        case BluetoothAdapter.ACTION_DISCOVERY_FINISHED => {
          log log "bluetooth discovery has finished";
          bluetoothNameListAdapter.getItems.foreach(nameListItem => executeFetchUUID(nameListItem.device));

          if (bluetoothNameListAdapter.isEmpty) {
            onBluetoothDiscoveryFinished();
          } else {
            waitFetchUUIDsTasksToFinish(onBluetoothDiscoveryFinished);
          }
        }
      }
    }
  }

  private def startupDiscovery(): Unit = {
    //safeDeviceDiscovery(bluetoothAdapter.get)
    //bluetoothAdapter.get.cancelDiscovery()

    bluetoothAdapter.get.startDiscovery()
  }

  private def startupBroadcastReceiver() {
    val filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
    filter.addAction(BluetoothDevice.ACTION_FOUND);
    filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
    registerReceiver(broadcastReceiver, filter)
  }
}
