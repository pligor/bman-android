package components

import com.pligor.bman.Bman

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait DebuggableTrait {
  protected val isDebuggable = Bman.isDebuggable
}
