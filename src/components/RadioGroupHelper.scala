package components

import android.widget.{RadioButton, RadioGroup}
import android.content.Context
import android.view.LayoutInflater
import myAndroidSqlite.MyModel

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object RadioGroupHelper {
  val invalidRadioButtonId = -1;

  def inflateRadioGroupWithModels(radioGroup: RadioGroup,
                                  models: Seq[MyModel],
                                  layoutId: Int)
                                 (implicit context: Context) {
    val layoutInflater = LayoutInflater.from(context);
    //we are setting null here because we need to do some stuff before adding it to radiogroup
    models foreach {
      model =>
        val radioButton = layoutInflater.inflate(layoutId, null).asInstanceOf[RadioButton];
        radioButton.setText(model.toString);
        //radioButton.setId(model.id.get.toInt);
        radioButton.setTag(model);
        //buttonId.map(radioButton.setButtonDrawable);
        radioGroup.addView(radioButton);
    }
  }

  def getModelFromRadioGroup[T](radioGroup: RadioGroup): T = {
    val checkedId = radioGroup.getCheckedRadioButtonId;
    radioGroup.findViewById(checkedId).getTag.asInstanceOf[T];
  }
}
