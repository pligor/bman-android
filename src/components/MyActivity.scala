package components

import android.app.Activity
import android.view.ViewGroup
import myandroid.{MyContextWrapper, AndroidHelper}
import AndroidHelper._
import android.os.Bundle
import android.widget.Toast
import com.pligor.bman.R
import models.SoundWrapper

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MyActivity extends Activity with MyContextWrapper {
  protected implicit val activity: Activity = this;

  def getContentLayout = findViewById(android.R.id.content).asInstanceOf[ViewGroup].getChildAt(0);

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);

    if (isSDcardAvailable) {
      //log log "sd card is found";
    } else {
      Toast.makeText(
        getApplicationContext,
        String.format(getResources.getString(R.string.no_sd_card_warning), getResources.getString(R.string.app_name)),
        Toast.LENGTH_LONG
      ).show();
      finish();
    }

    SoundWrapper.init;
  }
}
