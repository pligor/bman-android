package components

import com.pligor.bman.{Bman, TypedViewHolder, TR}
import components.WebviewHelper._
import android.os.Bundle
import android.webkit.{WebView, WebViewClient}
import android.view.View
import myandroid.AndroidHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait TransferringStatement extends MyActivity with TypedViewHolder {
  private lazy val transferringStatementWrapper = findView(TR.transferringStatementWrapper);
  private lazy val transferringStatement = defaultSetupWebview(findView(TR.transferringStatement));

  override def onPostCreate(savedInstanceState: Bundle) {
    super.onPostCreate(savedInstanceState);

    transferringStatement.setWebViewClient(new WebViewClient {
      override def onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        view.setVisibility(View.GONE);
      }
    });
  }

  override def onStart() {
    super.onStart();

    transferringStatementWrapper.setVisibility(if (isAndroidConnectedOnline) View.VISIBLE else View.GONE);

    loadUrlIfConnected(Bman.SERVER_LINK + "/transferringCard", transferringStatement);
  }
}
