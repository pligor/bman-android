package components

import myfortumo.FortumoPaymentActivity
import com.pligor.bman.{R, Bman}
import models.RequestCode

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MyFortumoPaymentActivity extends FortumoPaymentActivity {
  //take that directly from manifest setup, its unique for each project
  protected val FORTUMO_PERMISSION: String = Bman.FORTUMO_PERMISSION

  protected val FORTUMO_REQUEST_CODE: Int = RequestCode.FORTUMO_PAYMENT.id

  protected def fortumo_message_status_billed_id: Int = R.string.fortumo_message_status_billed

  protected def fortumo_message_status_not_sent_id: Int = R.string.fortumo_message_status_not_sent

  protected def fortumo_message_status_pending_id: Int = R.string.fortumo_message_status_pending

  protected def fortumo_message_status_failed_id: Int = R.string.fortumo_message_status_failed

  protected def fortumo_message_status_use_alternative_method_id: Int = R.string.fortumo_message_status_use_alternative_method

  protected def fortumo_result_code_not_ok_id: Int = R.string.fortumo_result_code_not_ok
}
