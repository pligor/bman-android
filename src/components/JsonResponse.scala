package components

import spray.json.JsValue

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * with current implementation we must NOT check if json is valid
 */
case class JsonResponse(code: Int, json: String);
