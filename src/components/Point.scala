package components

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class Point(x: Int,y: Int) extends android.graphics.Point(x,y) with Ordered[Point] {
  override def equals(that: Any) = {
    val thatPoint = that match {
      case p: Point => p;
      case _ => throw new ClassCastException;
    };
    //super.equals(that)
    (this.x == thatPoint.x) && (this.y == thatPoint.y);
  }


  def compare(that: Point): Int = {
    val diffX = this.x - that.x;
    val diffY = this.y - that.y;
    if (diffX < 0 || diffY < 0) {
      -1
    }
    else if (diffX > 0 && diffY > 0) {
      1;
    }
    else {
      diffX*diffY;
    }
  }
}
