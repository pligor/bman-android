package components

import myauth.SingleToken
import android.app.Activity
import java.net.URL
import scalaj.http.{HttpException, HttpOptions, Http}
import spray.json._
import models.{CardInfo, PeopleCard}
import org.apache.http.HttpStatus
import myandroid.{ScalaAsyncTask, log}
import com.pligor.bman.{R, Bman}
import generic.FileReceiver

/*import uk.co.bigbeeconsultants.http.{Config, HttpClient}
import uk.co.bigbeeconsultants.http.request.{Request, RequestBody}
import uk.co.bigbeeconsultants.http.header.{Headers, MediaType}
import uk.co.bigbeeconsultants.http.response.{BufferedResponseBuilder, Status, InputStreamResponseBody}*/

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait DownloadOldestCardTrait extends MyActivity {

  //abstract

  protected def onOldestCardResult(success: Boolean, messageId: Int, args: String*): Unit;

  protected def onSenderNameReceived(senderName: String): Unit;

  protected def onOldestCardProgress(percentage: Float): Unit;

  //concrete

  private var downloadOldestCardTask: Option[DownloadOldestCardTask] = None;

  private var getOldestInfoTask: Option[GetOldestInfoTask] = None;

  protected def executeDownloadCard(token: String): Unit = {
    getOldestInfoTask = Some(new GetOldestInfoTask);
    getOldestInfoTask.get.execute(SingleToken(token));
  }

  override def onStop(): Unit = {
    super.onStop();

    getOldestInfoTask.map(_.cancel({
      val mayInterruptIfRunning = true;
      mayInterruptIfRunning;
    }));

    downloadOldestCardTask.map(_.cancel({
      val mayInterruptIfRunning = true;
      mayInterruptIfRunning;
    }));
  }

  private class GetOldestInfoTask extends ScalaAsyncTask[SingleToken, AnyRef, (SingleToken, Option[JsonResponse])] {
    private val connectionTimeout_msec = 5000;
    private val readTimeout_msec = 2 * connectionTimeout_msec;

    private val url = new URL(Bman.SERVER_LINK + "/card/getOldestInfo");

    protected def doInBackground(singleToken: SingleToken): (SingleToken, Option[JsonResponse]) = {
      try {
        val request = Http.postData(url.toString, singleToken.toJson(SingleToken.itsJsonFormat).toString()).
          header("Content-Type", "application/json").
          option(HttpOptions.connTimeout(connectionTimeout_msec)).
          option(HttpOptions.readTimeout(readTimeout_msec));
        val (responseCode, headersMap, resultString) = request.asHeadersAndParse(Http.readString);
        (singleToken, Some(JsonResponse(code = responseCode, json = resultString)));
      } catch {
        case e: HttpException => {
          log log "http code is not enough but it is: " + e.code;
          (singleToken, None);
        }
        case e: Exception => (singleToken, None);
      }
    }

    override def onPostExecute(tuple: (SingleToken, Option[JsonResponse])) {
      super.onPostExecute(tuple);
      val (singleToken, jsonResponseOption) = tuple;

      if (jsonResponseOption.isDefined) {
        val jsonResponse = jsonResponseOption.get;
        jsonResponse.code match {
          case HttpStatus.SC_OK => {
            val cardInfo = try {
              Some(JsonParser(jsonResponse.json).convertTo[CardInfo](CardInfo.itsJsonFormat));
            } catch {
              case e: org.parboiled.errors.ParsingException => None;
            }

            if (cardInfo.isDefined) {
              onSenderNameReceived(cardInfo.get.senderName);
              downloadOldestCardTask = Some(new DownloadOldestCardTask);
              downloadOldestCardTask.get.execute((singleToken, cardInfo.get.fileSize));
            } else {
              onOldestCardResult(success = false, messageId = R.string.not_json_response);
            }
          }
          case HttpStatus.SC_EXPECTATION_FAILED => {
            onOldestCardResult(success = false, messageId = R.string.downloading_card_failed_no_retry, Bman.URL);
          }
          case HttpStatus.SC_UNAUTHORIZED => {
            onOldestCardResult(success = false, messageId = R.string.unauthorized_server_response);
          }
          case HttpStatus.SC_BAD_REQUEST => {
            onOldestCardResult(success = false, messageId = R.string.bad_request_server_response, Bman.URL);
          }
        }
      } else {
        onOldestCardResult(success = false, messageId = R.string.unexpected_server_response, Bman.URL);
      }
    }
  }

  private class DownloadOldestCardTask extends ScalaAsyncTask[(SingleToken, Long), Float, Int] {
    private val bufferSize = 1024;

    private val connectionTimeout_msec = 5000;
    private val readTimeout_msec = 2 * //minutes
      60 * 1000;

    private val url = new URL(Bman.SERVER_LINK + "/card/popOldest");

    protected def doInBackground(tuple: (SingleToken, Long)): Int = {
      val (singleToken, fileSize) = tuple;

      /*val httpConfig = Config(
        connectTimeout = connectionTimeout_msec,
        readTimeout = readTimeout_msec
      );
      val httpClient = new HttpClient(httpConfig);
      val contentType = MediaType.APPLICATION_JSON;
      val requestBody = RequestBody(string = singleToken.toJson.toString(), contentType = contentType);
      val request = Request.post(url, Some(requestBody));
      val res = new InputStreamResponseBody(request, Status.S200_OK, mediaType = contentType);
      val resBuilder = new BufferedResponseBuilder();
      httpClient.execute(request, resBuilder, httpConfig);*/

      val request = Http.postData(url.toString, singleToken.toJson(SingleToken.itsJsonFormat).toString()).
        header("Content-Type", "application/json").
        option(HttpOptions.connTimeout(connectionTimeout_msec)).
        option(HttpOptions.readTimeout(readTimeout_msec));

      //val tempFile = genRandomTemporaryFile(ext = Some("json"));
      //val tempOutstream = new FileOutputStream(tempFile);

      log log "the file has size: " + fileSize;
      val fileReceiver = new FileReceiver(fileSize, fileExt = Some(Bman.FILE_EXT));

      val (responseCode, headers, dummyBody) = try {
        request.asHeadersAndParse {
          inputStream =>
          /*copyInputToOutputImperative(inputStream, tempOutstream, bufferSize)({
            bufferLenWritten =>
          });*/
            fileReceiver.write(inputStream, bufferSize)({
              bufferLenWritten =>
              //Logger("percent received: " + fileReceiver.getPercentageReceived);
                publishProgress(fileReceiver.getPercentageReceived);
            });

            val fileOption = fileReceiver.safeEnd();
            //Logger("file option: " + fileOption);
            fileOption.map(PeopleCard.create); //remember: slow operation
        }
      } finally {
        fileReceiver.closeFile();
      }

      responseCode;
    }

    protected override def onProgressUpdate(value: Float) {
      super.onProgressUpdate(value);
      onOldestCardProgress(value);
    }

    override def onPostExecute(responseCode: Int) {
      super.onPostExecute(responseCode);

      log log "download oldest card response code is: " + responseCode;
      responseCode match {
        case HttpStatus.SC_OK => {
          onOldestCardResult(success = true, messageId = R.string.downloading_card_success);
        }
        case HttpStatus.SC_BAD_REQUEST => {
          onOldestCardResult(success = false, messageId = R.string.bad_request_server_response, Bman.URL);
        }
        case HttpStatus.SC_EXPECTATION_FAILED => {
          onOldestCardResult(success = false, messageId = R.string.downloading_card_failed_no_retry, Bman.URL);
        }
        case HttpStatus.SC_UNAUTHORIZED => {
          onOldestCardResult(success = false, messageId = R.string.unauthorized_server_response);
        }
        case _ => {
          onOldestCardResult(success = false, messageId = R.string.unexpected_server_response, Bman.URL);
        }
      }
    }
  }

}
