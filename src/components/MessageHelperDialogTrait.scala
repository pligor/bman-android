package components

import android.app.Dialog
import android.view.{View, Window}
import com.pligor.bman.R
import android.widget.{TextView, Button, CheckBox}
import android.view.View.OnClickListener
import preferences.HelperPreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MessageHelperDialogTrait extends MyActivity {
  protected def getMessageHelperDialog(stringId: Int) = {
    val dialog = new Dialog(context);

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
    dialog.setCanceledOnTouchOutside(false);

    dialog.setCancelable(true);

    dialog.setContentView(R.layout.dialog_helper_message);

    val messageHelperText = dialog.findViewById(R.id.messageHelperText).asInstanceOf[TextView];
    val noHelperMessagesCheckBox = dialog.findViewById(R.id.noHelperMessagesCheckBox).asInstanceOf[CheckBox];
    val messageUnderstoodButton = dialog.findViewById(R.id.messageUnderstoodButton).asInstanceOf[Button];

    messageHelperText.setText(stringId);

    messageUnderstoodButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        HelperPreference.setValue(!noHelperMessagesCheckBox.isChecked);
        dialog.dismiss();
      }
    });

    dialog;
  }
}
