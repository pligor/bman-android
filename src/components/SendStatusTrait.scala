package components

import preferences.{PreTrialPreference, FirstRunTimestampPreference}
import com.pligor.bman.{Bman, TR, TypedViewHolder, R}
import android.view.View.OnClickListener
import android.view.View
import myandroid.MyContextWrapper

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait SendStatusTrait extends SubscriptionDialog with MyContextWrapper with TypedViewHolder {
  private lazy val sendStatusButton = findView(TR.sendStatusButton);

  protected def onAfterInAppBillingServiceIsConnected(): Unit = {
    renderImageButton();
  }

  private def renderImageButton(): Unit = {
    val isSubscribed = Bman.isSubscribed

    if (!isSubscribed) {
      val imageResource = if (PreTrialPreference.getValue) {
        R.drawable.pay_status_pre_trial
      } else if (FirstRunTimestampPreference.isOnTrial) {
        R.drawable.pay_status_on_trial
      } else {
        R.drawable.pay_status_post_trial
      }

      sendStatusButton.setImageResource(imageResource)
    }

    sendStatusButton.setOnClickListener(if (isSubscribed) null else sendStatusButtonClickListener);
    sendStatusButton.setVisibility(if (isSubscribed) View.GONE else View.VISIBLE);
  }

  private val sendStatusButtonClickListener: View.OnClickListener = new OnClickListener {
    def onClick(view: View): Unit = {
      getSubscriptionDialog.show()
    }
  }
}
