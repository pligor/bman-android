package components

import android.view.View.OnTouchListener
import android.view.{MotionEvent, View, GestureDetector}
import android.view.GestureDetector.SimpleOnGestureListener
import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class MySwipeListener(
                       val onRight: () => Unit = () => {},
                       val onLeft: () => Unit = () => {},
                       val onDown: () => Unit = () => {},
                       val onUp: () => Unit = () => {}
                       )(implicit context: Context) extends OnTouchListener {

  private val gestureListener = new MyGestureListener(
    onSwipeRight = onRight,
    onSwipeLeft = onLeft,
    onSwipeDown = onDown,
    onSwipeUp = onUp
  );

  private val gestureDetector = new GestureDetector(context, gestureListener);

  def onTouch(view: View, motionEvent: MotionEvent): Boolean = {
    //Logger("raw x: " + motionEvent.getRawX + ", raw y: " + motionEvent.getRawY);
    view.onTouchEvent(motionEvent);
    gestureDetector.onTouchEvent(motionEvent); //currently this always returns false
    true; //we had to set this to true in order to prevent the on click method from being called twice
  }

  class MyGestureListener(val onSwipeRight: () => Unit,
                          val onSwipeLeft: () => Unit,
                          val onSwipeDown: () => Unit,
                          val onSwipeUp: () => Unit
                           ) extends SimpleOnGestureListener {
    private val SWIPE_THRESHOLD = 100;
    private val SWIPE_VELOCITY_THRESHOLD = 100;

    //private var itsView: View = _;
    /*def setView(view: View) {
      itsView = view
    }*/

    //super.onDown(e)
    //override def onDown(motionEvent: MotionEvent): Boolean = true;

    /*{
         val touchPoint = new Point(motionEvent.getX.toInt, motionEvent.getY.toInt);

         //view.getLocationInWindow(windowLocation);
         val viewLocation = Array[Int](0, 0);
         itsView.getLocationInWindow(viewLocation);

         val topLeft = new Point(viewLocation(0), viewLocation(1));

         val bottomRight = new Point(itsView.getMeasuredWidth + viewLocation(0), itsView.getMeasuredHeight + viewLocation(1));

         touchPoint > topLeft && touchPoint < bottomRight
       }*/

    //super.onFling(e1, e2, velocityX, velocityY);
    override def onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean = {
      try {
        val diffY = e2.getY - e1.getY;
        val diffX = e2.getX - e1.getX;
        import scala.math._;
        if (abs(diffX) > abs(diffY)) {
          //actually X has moved
          if (
            abs(diffX) > SWIPE_THRESHOLD
              && abs(velocityX) > SWIPE_VELOCITY_THRESHOLD
          ) {
            if (diffX > 0) {
              onSwipeRight();
            }
            else {
              onSwipeLeft();
            }
          }
          else {
            //nothing
          }
        }
        else {
          //actually Y has moved
          if (
            abs(diffY) > SWIPE_THRESHOLD
              && abs(velocityY) > SWIPE_VELOCITY_THRESHOLD
          ) {
            if (diffY > 0) {
              onSwipeDown();
            }
            else {
              onSwipeUp();
            }
          }
          else {
            //nothing
          }
        }
      }
      /*catch {
        case e: Exception => {
          e.printStackTrace();
          Logger(e.getMessage);
        }
      }*/
      false;
    }
  }

}