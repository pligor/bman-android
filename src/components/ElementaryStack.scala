package components

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class ElementaryStack[T] {
  private var valueOption: Option[T] = None;

  def isFull = valueOption.isDefined;
  def isEmpty = !isFull;

  def put(newValue: T) {
    require(!valueOption.isDefined);
    valueOption = Some(newValue);
  }

  def get: T = {
    require(valueOption.isDefined);
    val value = valueOption.get;
    valueOption = None;
    value;
  }
}
