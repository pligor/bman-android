package components

import generic.PhoneNumberHelper._
import com.pligor.bman.R
import EmailHelper._
import generic.UrlHelper._
import java.net.URL
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import myandroid.AndroidHelper._
import android.content.Context
import myandroid.InternetHelper._
import android.widget.Toast

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MetaDataParser {
  def execute(value: String)(implicit context: Context) {
    parseValue(value,
      onPhoneNumber = {
        phoneNumber: PhoneNumber =>
          if (hasTelephony) {
            phoneCall(phoneNumber);
          } else {
            showToast(R.string.no_telephony);
          }
      },
      onEmail = {
        mail: String =>
          callEmailApps(Some(mail));
      },
      onUrl = {
        url: URL =>
          showWebSite(url.toString);
      }, onOther = {
        other: String =>
          textToClipboard(
            key = other,
            text = other
          );

          Toast.makeText(
            context,
            context.getResources.getString(R.string.copied_to_clipboard).format(value),
            Toast.LENGTH_LONG
          ).show();
      });
  }

  def getImageResource(value: String): Int = {
    parseValue(value,
      onPhoneNumber = {
        phoneNumber: PhoneNumber =>
          R.drawable.cellphone
      },
      onEmail = {
        mail: String =>
          R.drawable.mail
      },
      onUrl = {
        url: URL =>
          R.drawable.world_light_blue
      }, onOther = {
        other: String =>
          R.drawable.clipboard_blue_light
      });
  }

  private def parseValue[A](value: String,
                            onPhoneNumber: (PhoneNumber) => A,
                            onEmail: (String) => A,
                            onUrl: (URL) => A,
                            onOther: (String) => A) = {
    if (isValidPhoneNumber(value)) {
      onPhoneNumber(parsePhoneString(value).get);
    } else if (isValidEmailAddress(value)) {
      //email check must precede the url check
      onEmail(value);
    } else if (isValidUrl(value)) {
      //email check must precede the url check
      onUrl(new URL(value));
    } else {
      onOther(value);
    }
  }
}
