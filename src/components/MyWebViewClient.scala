package components

import android.webkit.{WebView, WebViewClient}
import android.view.View
import myandroid.log
import myandroid.AndroidHelper._
import scala.collection.JavaConverters._
import android.content.Context
import generic.HttpHeaderName

object WebviewHelper {
  def defaultSetupWebview(webView: WebView): WebView = {
    webView.getSettings.setJavaScriptEnabled(false);
    webView.getSettings.setLoadWithOverviewMode(true); // complete zoom out
    //webView.getSettings.setUseWideViewPort(true); //this is to say that we have a wide screen like a desktop
    webView.getSettings.setLoadsImagesAutomatically(true);
    webView.getSettings.setBlockNetworkImage(false);
    webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
    webView;
  }

  def loadUrlIfConnected(url: String, webView: WebView)(implicit context: Context) {
    if (isAndroidConnectedOnline) {
      val locale = getAndroidConfigurationLocale.toString;
      val contentLanguageHeader = HttpHeaderName.CONTENT_LANGUAGE.toString;
      log log "current locale is: " + locale + " and header: " + contentLanguageHeader;
      webView.loadUrl(url, Map(contentLanguageHeader -> locale).asJava);
    }
  }
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class MyWebViewClient extends WebViewClient {
  /*override def shouldOverrideUrlLoading(view: WebView, url: String): Boolean = {
    val check = super.shouldOverrideUrlLoading(view, url);

  }*/
}
