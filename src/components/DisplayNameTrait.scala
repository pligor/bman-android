package components

import myandroid.{log, ProgressDialogActivity}
import android.content.Context
import android.app.{ProgressDialog, Dialog}
import com.pligor.bman.{TR, TypedViewHolder, R}
import preferences.CurrentPgpKeyIdPreference
import myandroid.AndroidHelper._
import android.view.{Window, View}
import components.EmailHelper._
import models._
import generic.FutureHelper._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import models.PgpIdentity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait DisplayNameTrait extends MyActivity with ProgressDialogActivity {
  private val minNameLength = 3
  //official bluetooth max len is 248 (http://msdn.microsoft.com/en-us/library/cc510763.aspx)
  private val maxNameLength = 120

  protected def setDisplayNameIfEmpty(onNewName: (String, => Unit) => Unit)(implicit context: Context): Boolean = {
    val isEmpty = UserInfo.isEmpty

    if (isEmpty) {
      log log "this should be called only once on first time"

      popupDisplayNameDialog(prefilled = false, onNewName = Some(onNewName))
    } else if (CurrentPgpKeyIdPreference.isDefault) {
      log log "if you already have a name, why dont you tell us about it, since we dont know it"

      CurrentPgpKeyIdPreference.setValue(PgpSecret.getFirst.get.id.get)
    } else {
      //nop
    }

    isEmpty
  }

  protected def popupDisplayNameDialog(prefilled: Boolean,
                                       onNewName: Option[(String, => Unit) => Unit])
                                      (implicit context: Context): Unit = {
    val dialog = new Dialog(context) with TypedViewHolder

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background)

    dialog.setCanceledOnTouchOutside(false)

    dialog.setCancelable(false)

    dialog.setContentView(R.layout.dialog_user_info)

    val userNameEditText = dialog.findView(TR.userNameEditText)

    //val userEmailEditText = dialog.findView(TR.userEmailEditText)

    val saveDeviceNameButton = dialog.findView(TR.saveDeviceNameButton)

    if (prefilled) {
      userNameEditText.setText(UserInfo.getName.getOrElse(""))

      //userEmailEditText.setText(UserInfo.getEmail.getOrElse(""))
    } else {
      //nop
    }

    saveDeviceNameButton.setOnClickListener(new View.OnClickListener {
      def onClick(view: View): Unit = {
        val newName = userNameEditText.getText.toString.trim

        val newEmail = newName(0) + "." + newName(1) + "@" + newName(2) + ".co" //a.b@c.co //userEmailEditText.getText.toString.trim

        if (newName.length < minNameLength) {
          showToast(R.string.dialog_user_name_too_short)
        } else if (newName.length > maxNameLength) {
          showToast(R.string.dialog_user_name_too_long)
        } else if (!isValidEmailAddress(newEmail)) {
          showToast(R.string.invalid_mail)
        } else {
          new SetIdentityClass(
            PgpIdentity(name = newName, email = newEmail),
            dialog,
            onNewName.getOrElse({
              (name, callback) =>
                callback
            })
          ).run()
        }
      }
    })

    dialog.show()
  }

  private class SetIdentityClass(identity: PgpIdentity,
                                dialog: Dialog,
                                onNewName: (String, => Unit) => Unit)(implicit context: Context) {

    private val progressDialog = new WriteOnce[ProgressDialog]

    protected def onPreExecute(): Unit = {
      //super.onPreExecute();
      onUI {
        progressDialog setValue {
          registerProgressDialog {
            val indeterminate = true

            val cancelable = false ////////////////////////////////////////////

            ProgressDialog.show(
              DisplayNameTrait.this,
              getResources.getString(R.string.display_name_progress_dialog_title),
              getResources.getString(R.string.display_name_progress_dialog_message),
              indeterminate,
              cancelable
            )
          }
        }
      }
    }

    private def storeKeyId(keyId: Long)(onResult: Boolean => Unit): Option[String] = {
      val result = CurrentPgpKeyIdPreference.setValue(keyId)

      onResult(result)

      if (result) {
        PgpPublic.getById(keyId).map(_.name)
      } else {
        None
      }
    }

    protected def doInBackground(): Option[String] = {
      val newNameOption: Option[String] = {
        log log "find by email"

        val pgpPublicOption = PgpPublic.findByEmail(identity.email)

        if (pgpPublicOption.isDefined) {
          log log "email exists inside pgp public table"

          val pgpPublic = pgpPublicOption.get

          if (pgpPublic.name != identity.name) {
            log log "if name different update the name of pgp public"

            pgpPublic.saveNewName(identity.name).flatMap {
              pgpPublicWithNewName =>
                assert(!pgpPublicWithNewName.isNew, "of course it is not new since we just got it from db above")

                storeKeyId(pgpPublicWithNewName.id.get) {
                  result =>
                  //nop
                }
            }
          } else {
            log log "email and name are exactly the same so do nothing"

            assert(!pgpPublic.isNew, "of course it is not new since we just got it from db above")

            val keyId = pgpPublic.id.get

            storeKeyId(keyId) {
              result =>
              //nop
            }
          }
        } else {
          log log "create a new keypair"

          identity.saveNewKeyPair.flatMap {
            newKeyId =>

              storeKeyId(newKeyId) {
                result =>
                  if (result) {
                    //nop
                  } else {
                    PgpPublic.getById(newKeyId).map(_.delete)
                  }
              }
          }
        }
      }

      newNameOption
    }

    protected def onPostExecute(newNameOption: Option[String]): Unit = {
      onUI {
        def dimissDialogs(): Unit = {
          assert(progressDialog.isInitialized)

          progressDialog().dismiss()

          dialog.dismiss()
        }

        if (newNameOption.isDefined) {
          onNewName(newNameOption.get, dimissDialogs())
        } else {
          dimissDialogs()

          showToast(R.string.failure_setting_identity)
        }
      }
    }


    def run(): () => Boolean = {
      onPreExecute()

      val (f, cancellor) = interruptableFuture({
        future: Future[Unit] =>
          onPostExecute(doInBackground())
      })

      cancellor
    }
  }

}
