package components

import java.io.{OutputStream, InputStream}
import scala.annotation.tailrec

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object JavaStreamHelper {

  //the testing works meaning you need a param like this to pass the partial function correctly
  /*private def testing(myfunc: (InputStream, OutputStream, Int) => ((Int) => Unit) => Unit) {
    myfunc(null,null,0)(_)
  }
  testing(copyInputToOutputImperative);*/

  def copyInputToOutputImperative(inputStream: InputStream,
                                  outputStream: OutputStream,
                                  bufferSize: Int)
                                 (onBufferWritten: (Int) => Unit = (dummy: Int) => {}) {
    val buffer = new Array[Byte](bufferSize);
    var len: Int = 0;
    while ( {
      len = inputStream.read(buffer);
      len;
    } > 0) {
      outputStream.write(buffer.take(len));
      onBufferWritten(len);
    }
  }

  /**
   * Functional is not recommended for android because of stack overflow errors
   */
  def copyInputToOutputFunctional(inputStream: InputStream, outputStream: OutputStream, bufferSize: Int) {
    val buffer = new Array[Byte](bufferSize);
    @tailrec
    def recurse() {
      val len = inputStream.read(buffer);
      if (len > 0) {
        outputStream.write(buffer.take(len));
        recurse();
      }
    }
    recurse();
  }
}
