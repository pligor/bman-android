package components

import android.provider.MediaStore
import android.content.Intent
import generic.FileHelper._
import android.app.Activity
import preferences.UriTemporaryPreference
import android.net.Uri
import java.io.File
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait PhotoCapturing extends MyActivity {
  //RequestCode.CAMERA_DATA.id

  protected def photoCaptureRequestCode: Int

  protected def shootFromCamera(onError: => Unit): Unit = {

    //yes this is true: MediaStore.ACTION_IMAGE_CAPTURE == "android.media.action.IMAGE_CAPTURE"

    val intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE)

    try {
      val photoFile = genRandomTemporaryFile(prefix = Some("picture"), ext = Some("jpg"))

      val isSet = UriTemporaryPreference.setValue(Uri.fromFile(photoFile))

      if (isSet) {
        val photoUri = UriTemporaryPreference.getValue

        if (Option(photoUri).isDefined) {
          intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)

          startActivityForResult(
            intent,
            photoCaptureRequestCode
          )
        } else {
          log log "for some reason uri from file is null"
          onError
        }
      } else {
        log log "issue with uri temporary preference, could not be written"
        onError
      }
    } catch {
      case e: Exception => {
        log log "cannot create file to take picture"
        onError
      }
    }
  }

  protected def onShootFromCameraSuccess(photoUri: Uri, onAfterPhotoUriUsage: (Uri) => Unit): Unit

  protected def onShootFromCameraFailure(): Unit

  override def onActivityResult(requestCode: Int, resultCode: Int, intent: Intent): Unit = {
    if (requestCode == photoCaptureRequestCode) {
      if (resultCode == Activity.RESULT_OK) {
        val intentOption = Option(intent);
        val uriOption: Option[Uri] = if (intentOption.isDefined) {
          Option(intentOption.get.getData);
        } else {
          None;
        }
        /*//old way, to only get thumbnail
          data.getExtras.get("data") match {
            case x: Bitmap => Some(x)
            case _ => throw new ClassCastException;
          }*/

        //http://stackoverflow.com/questions/6448856/android-camera-intent-how-to-get-full-sized-photo

        //I guess this line is not necessary because we do not want to have anything
        //getContentResolver.notifyChange(photoUri, null);
        val photoUri = if (uriOption.isDefined /*&& photoUri == uriOption.get*/ ) {
          log log "you dont need the photo uri preference";
          uriOption.get;
        } else {
          log log "unfortunately you need the photo uri preference";
          UriTemporaryPreference.getAndClear
        }

        onShootFromCameraSuccess(photoUri, {
          uri =>
          //below line does not work, find out why
          //getContentResolver.delete(uri, null, null);

          //this line does not work either
          //val uriDeleted = new File(getRealPathFromURI(uri)).delete();

          /*log log "uri path is: " + uri.getPath;
          log log "real uri path is: " + getRealPathFromURI(uri);*/

            val uriDeleted = new File(uri.getPath).delete()

            log log (if (uriDeleted) "uri deleted" else "uri was not deleted damn it")

          //but actually the picture still remains because the uri we are getting is the temporary file,
          //this file is also getting written inside DCIM folder!
        });
      } else {
        onShootFromCameraFailure()
      }
    } else {
      super.onActivityResult(requestCode, resultCode, intent)
    }
  }
}
