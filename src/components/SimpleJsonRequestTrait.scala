package components

import android.app.ProgressDialog
import android.content.{Context, DialogInterface}
import myandroid.{ScalaAsyncTask, log, ProgressDialogActivity}
import scalaj.http.{HttpException, HttpOptions, Http}
import java.net.URL
import spray.json._
import generic.{HttpHeaderName, ContentType}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait SimpleJsonRequestTrait extends ProgressDialogActivity {
  //abstract

  protected def isProgressDialogShown: Boolean

  //concrete

  protected case class JsonRequestParam(url: URL,
                                        json: JsValue,
                                        connTimeout_msec: Int,
                                        readTimeout_msec: Int)

  protected case class JsonResponseResult(code: Int, body: String);

  protected class SimpleJsonRequestTask(progressDialogTitleResource: Int,
                                        progressDialogMessageResource: Int)(op: Option[JsonResponseResult] => Unit)
                                       (implicit context: Context) extends ScalaAsyncTask[JsonRequestParam, AnyRef, Option[JsonResponseResult]] {

    private val progressDialog = new WriteOnce[ProgressDialog]()

    override def onPreExecute(): Unit = {
      super.onPreExecute()

      if (isProgressDialogShown) {
        progressDialog setValue {
          val progDialog = registerProgressDialog({
            val indeterminate = true

            val cancelable = true

            ProgressDialog.show(
              context,
              context.getString(progressDialogTitleResource),
              context.getString(progressDialogMessageResource),
              indeterminate,
              cancelable
            )
          })

          progDialog.setOnCancelListener(new DialogInterface.OnCancelListener {
            def onCancel(dialog: DialogInterface): Unit = {
              cancel({
                val mayInterruptIfRunning = true

                mayInterruptIfRunning
              })
            }
          })

          progDialog
        }
      }
    }

    protected def doInBackground(jsonRequestParam: JsonRequestParam): Option[JsonResponseResult] = {
      try {
        val request = Http.postData(jsonRequestParam.url.toString, jsonRequestParam.json.toString()).
          header(HttpHeaderName.CONTENT_TYPE.toString, ContentType.APPLICATION_JSON.toString).
          option(HttpOptions.connTimeout(jsonRequestParam.connTimeout_msec)).
          option(HttpOptions.readTimeout(jsonRequestParam.readTimeout_msec))

        val (responseCode, headersMap, resultString) = request.asHeadersAndParse(Http.readString);

        log log "response code: " + responseCode

        Some(JsonResponseResult(responseCode, resultString))
      } catch {
        case e: HttpException => Some(JsonResponseResult(e.code, e.body))
        case e: Exception => None
      }
    }

    override def onPostExecute(jsonResponseOption: Option[JsonResponseResult]): Unit = {
      super.onPostExecute(jsonResponseOption)

      if (isProgressDialogShown) {
        assert(progressDialog.isInitialized);
        progressDialog().dismiss();
      }

      op(jsonResponseOption)
    }
  }

}
