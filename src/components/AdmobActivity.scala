package components

import com.pligor.bman.{Bman, R}
import android.os.Bundle
import com.google.ads.{AdRequest, AdView}
import android.view.View

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * Include this inside manifest:
 * <activity android:name="com.google.ads.AdActivity"
      android:configChanges="keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize"/>
 */
trait AdmobActivity extends MyActivity {

  private lazy val adView = findViewById(R.id.myAdView).asInstanceOf[AdView]

  override def onPostCreate(savedInstanceState: Bundle): Unit = {
    super.onPostCreate(savedInstanceState)

    if (Bman.isSubscribed) {
      adView.setVisibility(View.GONE)
    } else {
      val adRequest = new AdRequest()
      //USE REFINEMENT OF THE AD REQUEST IN THE NEAR FUTURE
      adView.loadAd(adRequest)
    }
  }

  override def onDestroy(): Unit = {
    if (Option(adView).isDefined) {
      adView.destroy()
    }

    super.onDestroy()
  }
}
