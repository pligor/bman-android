package components

import myauth.SingleToken
import scalaj.http.{HttpOptions, Http}
import spray.json._
import org.apache.http.HttpStatus
import android.app.Activity
import models.PendingCounter
import myandroid.{ScalaAsyncTask, log}
import com.pligor.bman.{R, Bman}
import myandroid.AndroidHelper._
import scalaj.http.HttpException
import scala.Some
import java.util.{TimerTask, Timer}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait CheckPendingCardsTrait extends Activity with MyActivity {
  //abstract

  protected def getTokenForCheckPendingCards: Option[SingleToken];

  //concrete

  private var checkPendingCardsTask: Option[CheckPendingCardsTask] = None;

  private var timer: Option[Timer] = None;

  private val connectionTimeout_msec = 5000;
  private val readTimeout_msec = 2 * connectionTimeout_msec;

  protected def getSafePeriod(msecs: Int) = {
    val lowerBoundary = connectionTimeout_msec + readTimeout_msec;
    assert(msecs >= lowerBoundary);
    msecs;
  }

  private val url = Bman.SERVER_LINK + "/card/checkPending";

  override def onStop() {
    super.onStop();

    //to terpnon me ta tou wfelimou (to cancel pending cards task ekteleitai outws h allws)
    cancelPeriodicCheckPendingCards();

    checkPendingCardsTask.map(_.cancel({
      val mayInterruptIfRunning = true;
      mayInterruptIfRunning;
    }));
  }

  /*protected class CheckPendingCardsCircularTask extends CircularTask[Option[JsonResponse]] {
    protected def onBackground: Option[JsonResponse] = {
      val singleTokenOption = getTokenForCheckPendingCards;
      singleTokenOption.flatMap {
        singleToken =>
          checkPendingCards(singleToken);
      }
    }

    protected def onResult(jsonResponseOption: Option[JsonResponse]) {
      onJsonResponse(jsonResponseOption);
    }
  }*/

  protected def checkPendingCardsTaskAsync(singleToken: SingleToken) {
    checkPendingCardsTask = Some(new CheckPendingCardsTask);
    checkPendingCardsTask.get.execute(singleToken);
  }


  protected def repeatPeriodicallyCheckPendingCards(periodMsecs: Long): Boolean = {
    val check = !timer.isDefined;
    if (check) {
      timer = Some(new Timer);
      timer.map {
        t =>
          t.scheduleAtFixedRate(new TimerTask {
            def run() {
              onUI {
                getTokenForCheckPendingCards.map(checkPendingCardsTaskAsync);
              }
            }
          }, {
            val delayMsecs = 0;
            delayMsecs
          }, periodMsecs)
      }
    }
    check;
  }

  protected def cancelPeriodicCheckPendingCards() {
    timer.map {
      t =>
        t.cancel();
        checkPendingCardsTask.map(_.cancel({
          val mayInterruptIfRunning = true;
          mayInterruptIfRunning;
        }));
    }
    timer = None;
  }

  protected class CheckPendingCardsTask extends ScalaAsyncTask[SingleToken, AnyRef, Option[JsonResponse]] {
    protected def doInBackground(singleToken: SingleToken): Option[JsonResponse] = {
      checkPendingCards(singleToken);
    }

    override def onPostExecute(jsonResponseOption: Option[JsonResponse]) {
      super.onPostExecute(jsonResponseOption);
      onJsonResponse(jsonResponseOption);
    }
  }

  private def checkPendingCards(singleToken: SingleToken): Option[JsonResponse] = {
    try {
      val request = Http.postData(url, singleToken.toJson(SingleToken.itsJsonFormat).toString()).
        header("Content-Type", "application/json").
        option(HttpOptions.connTimeout(connectionTimeout_msec)).
        option(HttpOptions.readTimeout(readTimeout_msec));
      val (responseCode, headersMap, resultString) = request.asHeadersAndParse(Http.readString);
      Some(JsonResponse(code = responseCode, json = resultString));
    } catch {
      case e: HttpException => {
        log log "the http code we received was " + e.code;
        None;
      }
      case e: Exception => None;
    }
  }

  private def onJsonResponse(jsonResponseOption: Option[JsonResponse]) {
    if (jsonResponseOption.isDefined) {
      val jsonResponse = jsonResponseOption.get;

      jsonResponse.code match {
        case HttpStatus.SC_OK => {
          val pendingCounter = try {
            Some(JsonParser(jsonResponse.json).convertTo[PendingCounter](PendingCounter.itsJsonFormat));
          } catch {
            case e: org.parboiled.errors.ParsingException => None;
          }

          if (pendingCounter.isDefined) {
            log log "the pending emails are: " + pendingCounter.get.emails;
            log log "the pending cellphones are: " + pendingCounter.get.cellphones;
            renderCheckPendingCardsSuccess(pendingCounter.get);
          } else {
            renderCheckPendingCardsFailure(R.string.not_json_response);
          }
        }
        case HttpStatus.SC_UNAUTHORIZED => {
          log log "user is not authorized";
          renderCheckPendingCardsFailure(R.string.unauthorized_server_response);
        }
        case _ => {
          log log "unexpected response code";
          renderCheckPendingCardsFailure(R.string.unexpected_server_response);
        }
      }
    } else {
      log log "the request was bad or the server crashed for some reason";
      renderCheckPendingCardsFailure(R.string.unexpected_server_response);
    }
  }

  protected def renderCheckPendingCardsSuccess(pendingCounter: PendingCounter): Unit;

  protected def renderCheckPendingCardsFailure(messageId: Int, args: String*): Unit;
}
