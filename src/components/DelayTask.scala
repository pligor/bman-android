package components

import myandroid.{ScalaAsyncTask, log}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class DelayTask extends ScalaAsyncTask[Long, AnyRef, Unit] {
  //private val timestamp = new WriteOnce[Long];
  //private val delayMilliSeconds = new WriteOnce[Long];

  /**
   * To ensure that a task is cancelled as quickly as possible,
   * you should always check the return value of isCancelled()
   * periodically from doInBackground(Object[]), if possible (inside a loop for instance.)
   */
  protected def doInBackground(delayMsecs: Long) {
    //timestamp.setValue(System.currentTimeMillis());
    //delayMilliSeconds.setValue(delayMsecs);
    val startTime = System.currentTimeMillis();
    try {
      Thread.sleep(delayMsecs);
    } catch {
      case e: InterruptedException => {
        val duration = System.currentTimeMillis() - startTime;
        log log ("cancelled and msecs left were: " + (delayMsecs - duration));
      }
    }
  }

  override def onPostExecute(result: Unit) {
    super.onPostExecute(result);
    onAfterDelay();
  }

  protected def onAfterDelay(): Unit;

  /**
   * From documentation:
   * This is invoced AFTER doInBackground returns. Which makes it useless
   * It does NOT execute on actual time of cancelling
   */
  override def onCancelled() {
    super.onCancelled();
    /*if (timestamp.isInitialized && delayMilliSeconds.isInitialized) {
      val duration = System.currentTimeMillis() - timestamp();
      Logger("cancelled and msecs left were: " + (delayMilliSeconds() - duration));
    }*/
  }
}
