package components

import myandroid.MyPreferencesCase
import com.pligor.bman.Bman

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class MySecurePreference[INNER_TYPE, OUTER_TYPE]
  extends MyPreferencesCase[INNER_TYPE, OUTER_TYPE](Some(Bman.PREFERENCES_KEY));
