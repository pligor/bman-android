package components

import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class ExtraLazy[T](notYetEvaluatedValue: => T) {
  var isSet = false;
  lazy val value: T = {
    log log ("lazy value being evaluated");
    isSet = true;
    notYetEvaluatedValue;
  }
}
