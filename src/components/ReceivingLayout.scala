package components

import com.pligor.bman.{R, TR, TypedViewHolder}
import android.os.Bundle
import android.view.View
import com.todddavies.components.progressbar.ProgressWheel
import myandroid.log
import android.app.Activity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait ReceivingLayout extends Activity with TypedViewHolder {
  //abstract

  protected def setOrigin(origin: String): Unit = {
    receivingOriginTextView.setText(getResources.getString(R.string.receiving_origin).format(origin));
  }

  //concrete

  private var isResultRendered = false;

  protected lazy val receivingProgressWheel = findViewById(R.id.receivingProgressWheel).asInstanceOf[ProgressWheel];

  private lazy val receivingOriginTextView = findView(TR.receivingOriginTextView);
  private lazy val receivingResultLayout = findView(TR.receivingResultLayout);
  private lazy val receivingResultIcon = findView(TR.receivingResultIcon);
  private lazy val receivingResultMessage = findView(TR.receivingResultMessage);

  override def onPostCreate(savedInstanceState: Bundle): Unit = {
    super.onPostCreate(savedInstanceState);

    if(Option(receivingResultLayout).isDefined) {
      receivingResultLayout.setVisibility(View.GONE);
    }
  }

  protected def renderReceivingResult(success: Boolean, messageId: Int, args: String*): Unit = {
    if (isResultRendered) {
      log log "no we are not going to render the receive result again";
    } else {
      receivingProgressWheel.setVisibility(View.GONE);
      receivingResultLayout.setVisibility(View.VISIBLE);

      receivingResultIcon.setImageResource(if (success) R.drawable.yes else R.drawable.no);
      receivingResultMessage.setText(getResources.getString(messageId, args));

      isResultRendered = true;
    }
  }
}
