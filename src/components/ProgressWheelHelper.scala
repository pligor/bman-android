package components

import com.todddavies.components.progressbar.ProgressWheel

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object ProgressWheelHelper {
  val defaultProgressWheelSymbol = "%";

  //set by the creator
  private val fullProgress = 360;

  private def percentFloatToProgress(percent: Float): Int = {
    val result = math.ceil(percent * fullProgress).toInt;
    if (result < 0) 0;
    else if (result > 360) 360;
    else result;
  }

  /**
   * @param percentage from 0 to 1
   */
  def renderProgressWheel(percentage: Float, progressWheel: ProgressWheel) {

    //minus one to prevent it from showing 100% and still loading
    val percentShift = -0.01.toFloat;

    val fixedPercent = if (percentage < 0) {
      math.max(0, percentShift);
    } else if (percentage > 1) {
      math.min(1, 1 + percentShift);
    } else {
      percentage + percentShift;
    }

    progressWheel.setProgress(percentFloatToProgress(fixedPercent));
    progressWheel.setText(math.ceil(fixedPercent * 100).toInt + defaultProgressWheelSymbol);
  }

}
