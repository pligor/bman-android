package components

import java.nio.charset.Charset
import scala.io.Codec

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * http://stackoverflow.com/questions/18592578/how-could-i-name-devices-using-alljoyn-not-with-the-default-dbus-kind-of-name-bu/18592579#18592579
 */
object LetterConverter {
  private def toTwoLetters(byte: Byte): String = {
    val num = byte + 128;
    //range now from 0 to 255
    val division = num / 26;
    val modulo = num % 26;
    val firstletter = ('a' + division).toChar.toString;
    val secondletter = ('a' + modulo).toChar.toString;
    firstletter + secondletter;
  }

  val byteRange = (-128 to 127).map(_.toByte);

  val letters = byteRange.map(n => toTwoLetters(n));

  private val byteToLetters = byteRange.zip(letters).toMap;
  require(byteToLetters.size == 256, "size really is: " + byteToLetters.size);

  private val lettersToByte = byteToLetters.map(_.swap);
  require(lettersToByte.size == 256, "size really is: " + lettersToByte.size);

  def bytesToLetters(bytes: Seq[Byte]): String = {
    bytes.map(byte => byteToLetters(byte)).mkString;
  }

  def lettersToBytes(letters: String): Seq[Byte] = {
    letters.sliding(2, 2).map(twoLetters => lettersToByte(twoLetters)).toSeq;
  }

  def encode(str: String, charset: Charset = Codec.UTF8.charSet): String = {
    bytesToLetters(str.getBytes(charset))
  }

  def decode(letters: String, charset: Charset = Codec.UTF8.charSet): String = {
    new String(lettersToBytes(letters).toArray, charset);
  }
}
