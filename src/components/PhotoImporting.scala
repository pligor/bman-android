package components

import android.provider.MediaStore
import android.content.Intent
import android.app.Activity
import android.net.Uri

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait PhotoImporting extends MyActivity {
  //RequestCode.SELECT_IMAGE.id

  protected def photoImportRequestCode: Int;

  protected def importFromGallery() = {
    //import from gallery
    startActivityForResult(
      new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI),
      photoImportRequestCode
    );
  }

  protected def onImportFromGallerySuccess(photoUriOption: Option[Uri]): Unit;

  protected def onImportFromGalleryFailure(): Unit;

  override def onActivityResult(requestCode: Int, resultCode: Int, intent: Intent) {
    if (requestCode == photoImportRequestCode) {
      if (resultCode == Activity.RESULT_OK) {

        val intentOption = Option(intent);
        val uriOption: Option[Uri] = if (intentOption.isDefined) {
          Option(intentOption.get.getData);
        } else {
          None;
        }

        onImportFromGallerySuccess(uriOption);
      } else {
        onImportFromGalleryFailure();
      }
    } else {
      super.onActivityResult(requestCode, resultCode, intent);
    }
  }
}
