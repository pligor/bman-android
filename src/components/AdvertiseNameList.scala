package components

import _root_.myalljoyn.AlljoynHelper._
import _root_.myalljoyn.{AlljoynTargetWrapper, RawCheck, AdvertisedName, AlljoynRemoteService}
import android.os._
import myandroid.{MyServiceObject, log}
import android.widget.{AdapterView, ListView, Toast}
import myandroid.AndroidHelper._
import adapters.AdvertisedNameListAdapter
import android.widget.AdapterView.OnItemClickListener
import android.view.View
import android.content.Intent
import myandroid.WifiHelper._
import models.UserInfo
import services.SenderImmutableService

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object AdvertiseNameList {

  object MESSAGE extends Enumeration {
    val FOUND_ADVERTISED_NAME = Value

    val FAILED_INITIALIZING_ALLJOYN_SENDER = Value

    val ALLJOYN_SENDER_DESTROYED = Value

    val STARTED_FINDING_BOTH_SERVICES = Value
  }

  object EXTRA extends Enumeration {
    val INIT_SENDER_FAILURE_STRING = Value
  }

}

trait AdvertiseNameList extends AlljoynRemoteService {
  //abstract

  protected def advertisedNameListView: ListView

  protected def onAdvertisedListItemClick(targetWrapper: AlljoynTargetWrapper): Unit

  protected def onBeforeStartedFindingBothAlljoynServices(): Unit

  protected def onAfterStartedFindingBothAlljoynServices(): Unit

  protected def onAfterFakeExpiringStartedFinding(): Unit

  //concrete

  private var isAlljoynSendingStarted = false

  private val timeoutForScanningMsecs = 15 * 1000

  require(timeoutForScanningMsecs > 12000,
    "prefer larger than 12 seconds that is the default for bluetooth")

  //we are not using this solution right now
  //private var waitToScanTask: Option[DelayTask] = None;

  protected def isFinding = false

  //waitToScanTask.isDefined;

  protected implicit lazy val wifiManager = getWifiManager

  private lazy val advertisedNameListAdapter = new AdvertisedNameListAdapter

  override def onPostCreate(savedInstanceState: Bundle): Unit = {
    super.onPostCreate(savedInstanceState)

    advertisedNameListView.setAdapter(advertisedNameListAdapter)

    advertisedNameListView.setOnItemClickListener(new OnItemClickListener {
      def onItemClick(parent: AdapterView[_], view: View, position: Int, id: Long): Unit = {
        require(position == id)

        val targetWrapper = advertisedNameListAdapter.getItem(position)

        assert(targetWrapper.isFullyDefined, "a clickable item should be fully defined")

        isAlljoynSendingStarted = true

        onAdvertisedListItemClick(targetWrapper)
      }
    })
  }

  override def onStart(): Unit = {
    super.onStart()

    if (isWifiAvailable) {
      log log "sender immutable service state (trait): " + SenderImmutableService.getIsStarted

      startupAdvertiseNameListSafe()
    } else {
      //alljoyn service MUST be already finished either because we just started the activity
      //meaning that we have stopped it previously pressing back button or either because
      //we proceeded to send but sending succeeded or failed, and the service was stopped as well
      assert(!SenderImmutableService.getIsStarted)
      //we have the wifi on, we get a notification, go to another activity and then
      // return to this one but the wifi is now closed
      //so we either should have closed the service (recommended)
      //OR we should give up with this assertion, which means the service is running and the wifi is off
      //when the wifi is back on, the service is already running. but will alljoyn catchup with that?
      //I highly doubt it !!
    }
  }

  override def onStop(): Unit = {
    super.onStop()

    shutdownAdvertiseNameList()
  }

  protected def onAlljoynServiceConnected(): Unit = {

    assert(alljoynServiceMessenger.isDefined,
      "must send message after the alljoyn connection is established")

    alljoynServiceMessenger.get.send({
      val message = Message.obtain()

      message.what = SenderImmutableService.MESSAGES.MY_LIST_ACTIVITY_STARTED.id

      message.setData({
        val bundle = new Bundle

        bundle.putParcelable(
          SenderImmutableService.EXTRAS.THE_MESSENGER.toString,
          new Messenger(handler)
        )

        bundle.putInt(SenderImmutableService.EXTRAS.SOURCE_ACTIVITY.toString,
          SenderImmutableService.ACTIVITIES.MY_LIST.id)

        /*bundle.putString(SenderImmutableService.EXTRAS.FULL_FILEPATH.toString,
          tempJsonFile.get.getCanonicalPath)*/

        bundle.putString(SenderImmutableService.EXTRAS.OWN_NAME.toString, {
          val ownName = UserInfo.getName

          assert(ownName.isDefined, "we must have a name in order to send")

          ownName.get
        })

        bundle
      })

      message
    })

    clearAdapter()

    advertisedNameListView.setVisibility(View.VISIBLE)
  }

  private object handler extends Handler {
    override def handleMessage(msg: Message): Unit = {
      super.handleMessage(msg)

      AdvertiseNameList.MESSAGE(msg.what) match {
        case AdvertiseNameList.MESSAGE.STARTED_FINDING_BOTH_SERVICES =>
          onAfterStartedFindingBothAlljoynServices()

        case AdvertiseNameList.MESSAGE.ALLJOYN_SENDER_DESTROYED =>
          onAfterShutdownCallback.map(_.apply())

          onAfterShutdownCallback = None

        case AdvertiseNameList.MESSAGE.FOUND_ADVERTISED_NAME =>
          val advertisedName = AdvertisedName(msg)

          log log "found name suffix: " + advertisedName.suffix +
            " found transport: " + transportsToString(advertisedName.transport) +
            " is raw: " + (advertisedName.rawCheck == RawCheck.IS_RAW)

          foundAdvertisedName(advertisedName)

        case AdvertiseNameList.MESSAGE.FAILED_INITIALIZING_ALLJOYN_SENDER => {
          val bundle = msg.getData

          val initializationFailure = bundle.getString(AdvertiseNameList.EXTRA.INIT_SENDER_FAILURE_STRING.toString)

          Toast.makeText(context, initializationFailure, Toast.LENGTH_LONG).show()

          shutdownAdvertiseNameList()
        }
      }
    }
  }

  private def foundAdvertisedName(advertisedName: AdvertisedName): Unit = {
    onUI {
      //TODO make this global for all adapters
      advertisedNameListView.requestLayout()

      advertisedNameListAdapter.safeAdd(advertisedName)
    }
  }

  private def clearAdapter(): Unit = {
    onUI {
      advertisedNameListView.requestLayout()

      advertisedNameListAdapter.clearItems()
    }
  }

  protected def startupAdvertiseNameListSafe(): Unit = {
    assert(isWifiAvailable, "wifi should be enabled to call startup advertise name list")

    isAlljoynSendingStarted = false

    //assert(!generateTempJsonFileTaskOption.isDefined, "generate temp json file task must have ended and be cleared");

    if (SenderImmutableService.getIsStarted) {
      log log "sender immutable service is already started"
    } else {
      onBeforeStartedFindingBothAlljoynServices()

      MyServiceObject.startAndBind(classOf[SenderImmutableService], alljoynServiceConnection)

      /*{
        waitToScanTask = Some(new DelayTask {
          protected def onAfterDelay() {
            onAfterFakeExpiringStartedFinding();
            waitToScanTask = None;
          }
        });
        waitToScanTask.get.execute(timeoutForScanningMsecs);
      }*/
    }
  }

  private var onAfterShutdownCallback: Option[() => Unit] = None

  protected def shutdownAdvertiseNameList(onSuccessfulShutdown: => Unit): Unit = {
    assert(isWifiSupported, "wifi should be supported to call shutdown advertise name list")

    safeUnbindAlljoynService()

    if (isAlljoynSendingStarted) {
      log log "no need to stop the sender service since we need it"
    } else {
      stopService(new Intent(this, classOf[SenderImmutableService]))

      onAfterShutdownCallback = Some(() => onSuccessfulShutdown)
    }

    clearAdapter()

    advertisedNameListView.setVisibility(View.GONE)
  }

  /*protected def unsetMessengerAdvertiseNameList(): Unit = {
    alljoynServiceMessenger.map(
      _.send({
        val message = Message.obtain()

        message.what = SenderImmutableService.MESSAGES.UNSET_MESSENGER.id

        message
      })
    )
  }*/
}
