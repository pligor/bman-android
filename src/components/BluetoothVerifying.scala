package components

import android.content.{Intent, Context, BroadcastReceiver, IntentFilter}
import android.bluetooth.{BluetoothDevice, BluetoothAdapter}
import bluetooth.{BluetoothHelper, Bluetooth}
import java.util.UUID
import android.os.ParcelUuid
import BluetoothHelper._
import scala.collection.mutable
import com.pligor.bman.Bman
import myandroid.{ScalaAsyncTask, log}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait BluetoothVerifying extends MyActivity {

  //abstract

  protected def onFoundTargetedUUID(device: BluetoothDevice): Unit;

  //concrete

  private val waitFetchUUIDsTasks = mutable.Set.empty[WaitFetchUUIDsTask];
  private var waitAllFetchesTask: Option[WaitAllFetchesTask] = None;

  private val waitFetchUUIDTaskMsecs = 30 * 1000;

  private val checkTasksEmptyPeriodMsecs = 100;

  protected def waitFetchUUIDsTasksToFinish(onAfterWait: () => Unit) {
    waitAllFetchesTask = Some(new WaitAllFetchesTask(onAfterWait));
    waitAllFetchesTask.get.execute(checkTasksEmptyPeriodMsecs);
  }

  protected def executeFetchUUID(device: BluetoothDevice) {
    val task = new WaitFetchUUIDsTask(device);
    waitFetchUUIDsTasks += task;
    task.execute(waitFetchUUIDTaskMsecs);
  }

  protected def clearFetchUUIDsTasks(): Unit = {
    waitFetchUUIDsTasks.clear();
  }

  override def onStart() {
    super.onStart();

    clearFetchUUIDsTasks();

    val filter = new IntentFilter(Bluetooth.DEVICE_ACTION_UUID_STRAIGHT);
    filter.addAction(Bluetooth.DEVICE_ACTION_UUID_REVERSED);
    registerReceiver(broadcastReceiver, filter);
  }

  override def onStop() {
    super.onStop();
    waitAllFetchesTask.map(_.cancel {
      val mayInterruptIfRunning = true;
      mayInterruptIfRunning;
    });

    waitFetchUUIDsTasks.foreach(_.cancel {
      val mayInterruptIfRunning = true;
      mayInterruptIfRunning;
    });

    safeUnregisterReceiver(broadcastReceiver);
  }


  private class WaitAllFetchesTask(onAfterWait: () => Unit) extends ScalaAsyncTask[Long, AnyRef, Unit] {
    protected def doInBackground(period: Long) {
      while (!waitFetchUUIDsTasks.isEmpty) {
        Thread.sleep(period);
        log log ("wait for tasks to end. still left this many tasks: " + waitFetchUUIDsTasks.size);
      }
    }

    override def onPostExecute(result: Unit) {
      super.onPostExecute(result);
      onAfterWait();
    }
  }

  private class WaitFetchUUIDsTask(val device: BluetoothDevice)
    extends ScalaAsyncTask[Long, AnyRef, Unit] {
    override def onPreExecute() {
      super.onPreExecute();
      val isFetching = fetchUuidsWithSdpFromDevice(device);
      assert(isFetching);
      log log ("fetching uuids from " + device);
    }

    protected def doInBackground(waitPeriodMsecs: Long) {
      Thread.sleep(waitPeriodMsecs);
    }

    override def onPostExecute(result: Unit) {
      super.onPostExecute(result);
      waitFetchUUIDsTasks -= this; //remove itself from the list
    }

    def processDevice(finalUUIDs: Array[UUID]) {
      if (finalUUIDs.exists(_ == Bman.MY_UUID_INSECURE)) {
        onFoundTargetedUUID(device);
      }

      cancel({
        val mayInterruptIfRunning = true;
        mayInterruptIfRunning;
      });

      waitFetchUUIDsTasks -= this; //remove itself from the list
    }

    override def equals(other: Any): Boolean = {
      other match {
        case that: WaitFetchUUIDsTask => this.device == that.device;
        case _ => throw new ClassCastException;
      }
    }
  }

  private object broadcastReceiver extends BroadcastReceiver {
    def onReceive(context: Context, intent: Intent) {
      intent.getAction match {
        case Bluetooth.DEVICE_ACTION_UUID_STRAIGHT | Bluetooth.DEVICE_ACTION_UUID_REVERSED => {
          //If the procedure is finished and a new procedure has started AND
          //we get the uuids from a fetch request that belonged to the previous procedure
          //we have a serious problem!
          //SOLUTION: do NOT deal with fetched UUIDs unless the discovery has finished
          //BETTER SOLUTION: if the bluetooth adapters is discovering meaning the below assertion
          //fails, you must INCREASE THE AMOUNT OF TIME we wait for each newly discovered device
          //to respond with its uuids

          //assert(!bluetoothAdapter.isDiscovering, "we got a response from a fetch request while discovering, increase time period in WaitFetchUUIDsTask class");

          val device: BluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
          log log ("received uuids from device: " + device.getName);

          val uuids: Array[UUID] = try {
            val parcelUuids: Array[ParcelUuid] = intent.getParcelableArrayExtra(Bluetooth.DEVICE_EXTRA_UUID).map(_.asInstanceOf[ParcelUuid]);
            parcelUuids.map(_.getUuid);
          } catch {
            //case e: RuntimeException => {
            case e: Exception => {
              log log "getting uuids has failed for unknown reason. we just ignore it";
              Array.empty[UUID];
            }
          }

          /*uuids.foreach(uuid => Logger("uuid: " + uuid));
          Logger("uuids length: " + uuids.length);*/

          //check task state
          val taskOption = waitFetchUUIDsTasks.find(_.device == device);
          //assert(taskOption.isDefined, "we received uuids for an unknown task and this might mean to increase task period time");
          taskOption.map(_.processDevice(uuids));
        }
      }
    }
  }

}
