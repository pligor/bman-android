package components

import java.io.File
import models.Card
import android.content.Context
import myandroid.ScalaAsyncTask

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class GenerateTempJsonFileTask(onTempJsonReady: Option[File] => Unit)(implicit context: Context)
  extends ScalaAsyncTask[Long, AnyRef, Option[File]] {

  protected def doInBackground(cardId: Long): Option[File] = {
    Card.genTempJsonFileForSending(cardId)
  }

  override def onPostExecute(tempJsonFileOption: Option[File]): Unit = {
    super.onPostExecute(tempJsonFileOption)

    if(tempJsonFileOption.isDefined)
      isFileGeneratedFlag setValue Unit

    onTempJsonReady(tempJsonFileOption)
  }

  private val isFileGeneratedFlag = new WriteOnce[Unit]()

  def isFileGeneratedSuccessfully = isFileGeneratedFlag.isInitialized
}
