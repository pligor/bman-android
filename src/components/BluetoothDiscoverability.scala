package components

import android.bluetooth.BluetoothAdapter
import myandroid.AndroidHelper._
import android.content.{IntentFilter, Intent, Context, BroadcastReceiver}
import myandroid.log
import models.RequestCode
import android.app.Activity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait BluetoothDiscoverability extends MyActivity {

  //abstract

  protected def onBluetoothTurnDiscoverabilityOff(): Unit;

  protected def onBluetoothTurnDiscoverabilityOn(success: Boolean): Unit;

  //concrete

  //you will have to refactor if a device is found with two bluetooth devices!! weird but it could happen
  //protected implicit val bluetoothAdapter: Option[BluetoothAdapter];
  private implicit lazy val bluetoothAdapter = Option(BluetoothAdapter.getDefaultAdapter);

  override def onStart() {
    super.onStart();

    registerReceiver(broadcastReceiver,
      new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));
  }


  override def onStop() {
    super.onStop();

    safeUnregisterReceiver(broadcastReceiver);
  }

  /**
   * Tip: Enabling discoverability will automatically enable Bluetooth
   * @param duration zero means the maximum possible allowed by the phone.
   *                 on newer phones, indefinately, on old phones the max allowed time (typically 120secs)
   */
  protected def requestBluetoothDiscoverable(duration: Int = 0) {
    //require(bluetoothAdapter.get.isEnabled, "do not use the auto enable that is offered, rather explicitly call enable");
    require(duration >= 0);

    require(!isBluetoothDiscoverable);

    //if (bluetoothAdapter.get.getScanMode != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
    log log "we ask to have bluetooth adapter discoverable";

    val discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE).
      putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, duration);

    startActivityForResult(discoverableIntent, RequestCode.BLUETOOTH_DISCOVERABLE.id);
    /*} else {
      log log "bluetooth adapter is already discoverable";
    }*/
  }

  private def isScanModeDiscoverable(scanMode: Int): Boolean = {
    log log ("scan mode: " + scanMode);
    scanMode == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE;
  }

  protected def isBluetoothDiscoverable = {
    require(isBluetoothSupported);
    isScanModeDiscoverable(bluetoothAdapter.get.getScanMode);
  }

  private object broadcastReceiver extends BroadcastReceiver {
    def onReceive(context: Context, intent: Intent) {
      intent.getAction match {
        case BluetoothAdapter.ACTION_SCAN_MODE_CHANGED => {
          val extras = intent.getExtras;
          val scanMode = extras.getInt(BluetoothAdapter.EXTRA_SCAN_MODE);
          val previousScanMode = extras.getInt(BluetoothAdapter.EXTRA_PREVIOUS_SCAN_MODE);

          /*Toast.makeText(activity,
            "prev scan mode was: " + previousScanMode + " and now is: " + scanMode,
            Toast.LENGTH_LONG).show();*/

          /*BluetoothAdapter.SCAN_MODE_CONNECTABLE //21
          BluetoothAdapter.SCAN_MODE_NONE //20*/

          if ((previousScanMode != scanMode) && !isScanModeDiscoverable(scanMode)) {
            onBluetoothTurnDiscoverabilityOff();
          } else {
            //nop
          }
        }
      }
    }
  }


  override def onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
    RequestCode(requestCode) match {
      case RequestCode.BLUETOOTH_DISCOVERABLE => {
        //Activity.RESULT_CANCELED  //0
        //Activity.RESULT_FIRST_USER  //1 //Start of user-defined activity results. means the first that belongs to users :P
        //Activity.RESULT_OK  //-1
        log log ("result code is: " + resultCode);

        //val x = Option(data).flatMap(d => Option(d.getExtras)).map(_.size());
        //Logger("the size of extras: " + x);
        //data is verified that is empty

        val didUserClickedYes = resultCode != Activity.RESULT_CANCELED;
        if (didUserClickedYes) {
          //wait for scan mode to be enabled before proceeding
          new Thread(new Runnable {
            def run() {
              val totalMsecs = 5 * 1000;
              val period = 100;
              var loops = totalMsecs.toFloat / period.toFloat;

              while (loops > 0 && !isBluetoothDiscoverable) {
                loops -= 1;
                Thread.sleep(period);
              }

              onUI {
                onBluetoothTurnDiscoverabilityOn(didUserClickedYes);
              }
            }
          }).start();
        } else {
          onBluetoothTurnDiscoverabilityOn(didUserClickedYes);
        }
      }
      case _ => super.onActivityResult(requestCode, resultCode, data);
    }
  }
}
