package components

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object ByteHelper {
  def generateRandomByteArray(len: Int): Array[Byte] = {
    val bytes = new Array[Byte](len);
    bytes.map(b => ByteConv.boolean2byte(value = math.random < 0.5));
  }
}
