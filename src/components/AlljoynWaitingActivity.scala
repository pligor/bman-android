package components

import android.os._
import myalljoyn.AlljoynRemoteService
import android.content.Intent
import models.UserInfo
import myandroid.{MyServiceObject, log}
import myandroid.WifiHelper._
import services.ReceiverImmutableService

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object AlljoynWaitingActivity {

  object MESSAGE extends Enumeration {
    val JOINED_MESSAGE_SESSION = Value;
    val SUCCESS_INITIALIZING_ALLJOYN_RECEIVER = Value;
    val FAILED_INITIALIZING_ALLJOYN_RECEIVER = Value;
    val ALLJOYN_RECEIVER_DESTROYED = Value;
  }

  object EXTRA extends Enumeration {
    val INIT_RECEIVER_FAILURE_STRING = Value;
  }
}

trait AlljoynWaitingActivity extends AlljoynRemoteService {
  //abstract

  protected def onAlljoynReceivingStarted(): Unit;

  protected def onAlljoynStartWaitingFail(toastId: Int): Unit;

  protected def onAlljoynStartWaitingSuccess(): Unit;

  protected def onAfterDestroyedAlljoynWaiting(): Unit;

  //concrete

  private var isAlljoynReceivingStarted = false;

  protected def isOnTheProcessOfStartWaiting = _isOnTheProcessOfStartWaiting;

  protected def isOnTheProcessOfStopWaiting = _isOnTheProcessOfStopWaiting;
  private var _isOnTheProcessOfStartWaiting: Boolean = false;
  private var _isOnTheProcessOfStopWaiting: Boolean = false;

  protected def isWaiting: Boolean = ReceiverImmutableService.getIsStarted;

  protected implicit lazy val wifiManager = getWifiManager;

  override def onStart() {
    super.onStart();
    isAlljoynReceivingStarted = false;
  }

  override def onStop() {
    super.onStop(); //no worries to execute this first since actually the parents are being called first

    if (isAlljoynReceivingStarted) {
      log log "no need to stop the receiver service since we need it";
      safeUnbindAlljoynService();
    } else {
      stopWaitingSafe();
    }
  }

  /*protected def restartWaitingSafe(): Boolean = {
    val check = isWifiAvailable(wifiManager);
    if (check) {
      restartWaiting();
    } else {
      //nop
    }
    check;
  }

  private def restartWaiting() {
    stopWaitingSafe();
    startWaiting();
  }*/

  /**
   * @return willBeWaiting
   */
  def startWaitingSafe(): Boolean = {
    log log "start alljoyn waiting safely";
    val willBeWaiting = isWifiAvailable && !ReceiverImmutableService.getIsStarted;
    if (willBeWaiting) {
      _isOnTheProcessOfStartWaiting = true;
      startWaiting();
    } else {
      //nop
    }
    willBeWaiting;
  }

  private def startWaiting() {
    MyServiceObject.startAndBind(classOf[ReceiverImmutableService], alljoynServiceConnection);
  }

  /**
   * @return if it was started
   */
  protected def stopWaitingSafe(): Boolean = {
    log log "stop alljoyn waiting safely";
    val wasStarted = ReceiverImmutableService.getIsStarted;
    _isOnTheProcessOfStopWaiting = wasStarted;

    safeUnbindAlljoynService();

    stopService(new Intent(this, classOf[ReceiverImmutableService]));

    wasStarted;
  }

  protected def onAlljoynServiceConnected() {
    alljoynServiceMessenger.get.send({
      val message = Message.obtain();
      message.what = ReceiverImmutableService.MESSAGES.WAITING_ACTIVITY_STARTED.id;
      message.setData({
        val bundle = new Bundle();
        bundle.putParcelable(ReceiverImmutableService.EXTRAS.THE_MESSENGER.toString,
          new Messenger(alljoynServiceHandler));
        bundle.putInt(ReceiverImmutableService.EXTRAS.SOURCE_ACTIVITY.toString,
          ReceiverImmutableService.ACTIVITIES.WAITING.id);
        bundle.putString(ReceiverImmutableService.EXTRAS.SUFFIX_NAME.toString, {
          val nameOption = UserInfo.getName;
          assert(nameOption.isDefined, "we always have an available name except before first run of app");
          log log "the suffix name before encoding: " + nameOption.get;
          LetterConverter.encode(nameOption.get);
        });
        bundle;
      });
      message;
    });
  }

  protected val alljoynServiceHandler = new Handler {
    override def handleMessage(msg: Message) {
      super.handleMessage(msg);
      //log log "exoume lavei to what " + MainActivity.MESSAGE(msg.what);
      AlljoynWaitingActivity.MESSAGE(msg.what) match {
        case AlljoynWaitingActivity.MESSAGE.JOINED_MESSAGE_SESSION => {
          isAlljoynReceivingStarted = true;
          onAlljoynReceivingStarted();
        }
        case AlljoynWaitingActivity.MESSAGE.SUCCESS_INITIALIZING_ALLJOYN_RECEIVER => {
          _isOnTheProcessOfStartWaiting = false;
          onAlljoynStartWaitingSuccess();
        }
        case AlljoynWaitingActivity.MESSAGE.FAILED_INITIALIZING_ALLJOYN_RECEIVER => {
          stopWaitingSafe();
          val bundle = msg.getData;
          val toastId = bundle.getInt(AlljoynWaitingActivity.EXTRA.INIT_RECEIVER_FAILURE_STRING.toString);
          onAlljoynStartWaitingFail(toastId);
        }
        case AlljoynWaitingActivity.MESSAGE.ALLJOYN_RECEIVER_DESTROYED => {
          _isOnTheProcessOfStartWaiting = false;
          _isOnTheProcessOfStopWaiting = false;
          onAfterDestroyedAlljoynWaiting();
        }
      }
    }
  }
}
