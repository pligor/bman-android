package components

//import javax.mail.internet.{AddressException, InternetAddress}
//import org.apache.commons.validator.EmailValidator;

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 * https://github.com/oblac/jodd
 */
object EmailHelper {
  //private val emailRegex = """(?i)^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$""".r;
  //private val emailRegex = """(?i)^[a-z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+(\.[a-z0-9,!#\$%&'\*\+/=\?\^_`\{\|}~-]+)*@[a-z0-9][a-z0-9-]*(\.[a-z0-9-]+)*\.([a-z]{2,})$""".r;

  /*def isValidEmailAddress(email: String): Boolean = {
    try {
      new InternetAddress(email).validate();
      true;
    } catch {
      case ex: AddressException => false;
    };
  }*/
  //def isValidEmailAddress(email: String): Boolean = EmailValidator.getInstance().isValid(email);
  //def isValidEmailAddress(email: String): Boolean = emailRegex.findFirstIn(email).isDefined;

  def isValidEmailAddress(email: String): Boolean = {
    val pos = email.lastIndexOf("@")

    (email.count(_ == '@') == 1) &&
      (pos > 0) &&
      (email.lastIndexOf(".") > pos) &&
      (email.length - pos > 4)
  }
}
