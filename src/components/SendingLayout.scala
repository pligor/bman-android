package components

import com.pligor.bman.{R, TR, TypedViewHolder}
import android.os.Bundle
import android.view.View
import com.todddavies.components.progressbar.ProgressWheel
import myandroid.log
import android.app.Activity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait SendingLayout extends Activity with TypedViewHolder {

  //abstract

  protected def setDestination(destination: String): Unit = {
    sendingDestinationTextView.setText(getResources.getString(R.string.sending_destination).format(destination));
  }

  //concrete

  private var isResultRendered = false;

  protected lazy val sendingProgressWheel = findViewById(R.id.sendingProgressWheel).asInstanceOf[ProgressWheel];

  private lazy val sendingResultLayout = findView(TR.sendingResultLayout);
  private lazy val sendingRetryMessage = findView(TR.sendingRetryMessage);
  private lazy val sendingResultIcon = findView(TR.sendingResultIcon);
  private lazy val sendingResultMessage = findView(TR.sendingResultMessage);
  private lazy val sendingDestinationTextView = findView(TR.sendingDestinationTextView);

  override def onPostCreate(savedInstanceState: Bundle) {
    super.onPostCreate(savedInstanceState);

    if(Option(sendingResultLayout).isDefined) {
      sendingResultLayout.setVisibility(View.GONE);
    }
  }

  protected def renderSendingResult(success: Boolean, messageId: Int, args: String*): Unit = {
    if (isResultRendered) {
      log log "no we are not going to render the send result again";
    } else {
      sendingProgressWheel.setVisibility(View.GONE);
      sendingResultLayout.setVisibility(View.VISIBLE);

      sendingRetryMessage.setVisibility(View.INVISIBLE);
      sendingResultIcon.setImageResource(if (success) R.drawable.yes else R.drawable.no);
      sendingResultMessage.setText(getResources.getString(messageId, args));

      isResultRendered = true;
    }
  }
}
