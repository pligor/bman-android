package components

import android.bluetooth.BluetoothAdapter
import preferences.BluetoothNamePreference
import models.UserInfo
import android.content.{Intent, BroadcastReceiver, IntentFilter, Context}
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait BluetoothNameOverwriter extends BluetoothActivity {

  //abstract

  //concrete

  //you will have to refactor if a device is found with two bluetooth devices!! weird but it could happen
  //protected implicit val bluetoothAdapter: Option[BluetoothAdapter];
  private implicit lazy val bluetoothAdapter = Option(BluetoothAdapter.getDefaultAdapter);

  /**
   * @return true if bluetooth actually got disabled, false if not
   */
  protected def onBeforeBluetoothTurnOff(disableBluetooth: => Boolean): Boolean = {
    val writtenBackOption = BluetoothNamePreference.writeBack;
    assert(writtenBackOption.isDefined,
      "this only fails when bluetooth is not supported, but if it is not then you should not come to this method at all");

    writtenBackOption.get && {
      val isBluetoothDisabled = disableBluetooth;
      if (!isBluetoothDisabled) {
        //correct the whole thing
        tryWriteBluetoothNameIfDifferent;
      }

      log log ("the written back is: " + writtenBackOption.get + " and the is bluetooth disabled " + isBluetoothDisabled);

      isBluetoothDisabled;
    }
  }

  /**
   * DO NOT USE onStart because it might overlap with other activities who have the bluetooth name overwriter
   */
  override def onResume() {
    super.onResume();
    BluetoothNamePreference.saveName;

    assert(!UserInfo.isEmpty, "ensure we have some name");

    //not was empty check is necessary because otherwise if was empty we are going to hurry too much and
    // write the bluetooth name with empty name
    //if (!wasEmpty) {
    tryWriteBluetoothNameIfDifferent;
    //}

    registerReceiver(
      broadcastReceiver,
      new IntentFilter(BluetoothAdapter.ACTION_LOCAL_NAME_CHANGED)
    );
  }

  /**
   * DO NOT USE onStop because it might overlap with other activities who have the bluetooth name overwriter
   */
  override def onPause() {
    super.onPause();
    safeUnregisterReceiver(broadcastReceiver);
    BluetoothNamePreference.writeBack;
  }

  private object broadcastReceiver extends BroadcastReceiver {
    def onReceive(context: Context, intent: Intent) {
      intent.getAction match {
        case BluetoothAdapter.ACTION_LOCAL_NAME_CHANGED => {
          //if an exterior service/app tries to change the name, we force it back to our name
          //make sure that this does write on same name to avoid infinite loop
          tryWriteBluetoothNameIfDifferent;
        }
      }
    }
  }

  /**
   * @return None if bluetooth adapter is not available, false if has not or failed to change, and true if has changed
   */
  protected def tryWriteBluetoothNameIfDifferent: Option[Boolean] = {
    val nameOption = UserInfo.getName;
    //if (nameOption.isDefined) {
    assert(nameOption.isDefined, "use this function only after you have saved the user name");
    val name = nameOption.get;

    bluetoothAdapter.map {
      adapter =>
      //this is important to prevent infinite loop of the broadcast receiver
        val isChanged = name != adapter.getName;
        if (isChanged) {
          adapter.setName(name);
        } else {
          false;
        }
    }
  }

  protected def onBluetoothTurnOff() {
    //nop
  }

  protected def onBluetoothTurnOn(success: Boolean) {
    if (success) {
      tryWriteBluetoothNameIfDifferent;
    }
  }
}
