package components

import com.pligor.bman.R
import android.os.{Message, Handler}
import android.bluetooth.{BluetoothAdapter, BluetoothSocket}
import bluetooth.{BluetoothHelper, AcceptThread}
import myandroid.AndroidHelper._
import myandroid.log
import BluetoothHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait BluetoothListeningActivity extends BluetoothDiscoverability {
  //abstract

  protected def onBeforeStopListening(): Unit;

  protected def onAfterStopListening(): Unit;

  protected def onStartListening(acceptThreadName: String): Unit;

  protected def onSocketReceived(socket: BluetoothSocket): Boolean;

  //concrete

  //you will have to refactor if a device is found with two bluetooth devices!! weird but it could happen
  //protected implicit val bluetoothAdapter: Option[BluetoothAdapter];
  private implicit lazy val bluetoothAdapter = Option(BluetoothAdapter.getDefaultAdapter);

  private var acceptThread: Option[AcceptThread] = None;

  protected def isListening: Boolean = acceptThread.isDefined;

  protected def isOnTheProcessOfStartListening = _isOnTheProcessOfStartListening;

  protected def isOnTheProcessOfStopListening = _isOnTheProcessOfStopListening;
  private var _isOnTheProcessOfStartListening: Boolean = false;
  private var _isOnTheProcessOfStopListening: Boolean = false;

  override def onStop() {
    super.onStop();

    //remember: cancelling thread does NOT cancel socket so the connection is not lost if stop listening is invoked
    //remember that is ok to stop listening since the bluetooth socket remains intact despite
    //the bluetooth server socket being closed
    stopListeningSafe();
  }

  private val acceptHandler = new Handler {
    override def handleMessage(msg: Message) {
      super.handleMessage(msg);

      AcceptThread.MESSAGE(msg.what) match {
        case AcceptThread.MESSAGE.ACCEPTED => {
          //get socket
          val socket = msg.obj.asInstanceOf[ /*CloseOnce*/ BluetoothSocket];
          val acceptThreadName = msg.getData.getString(AcceptThread.EXTRA.ACCEPT_THREAD_NAME.toString);

          assert(acceptThread.get.getName == acceptThreadName,
            "now we have only one accepting thread, so this is redundant but ok");

          //later create a black list. currently we are accepting connection from ANY device

          val readyToReceive = onSocketReceived(socket);

          if (!readyToReceive) {
            showToast(R.string.receiving_failed);
            log log "restart bluetooth listening";
            stopListeningSafe(startListeningSafe());
          }
        }
        case AcceptThread.MESSAGE.START_ACCEPTING => {
          val acceptThreadName = msg.getData.getString(AcceptThread.EXTRA.ACCEPT_THREAD_NAME.toString);
          onStartListening(acceptThreadName);
          _isOnTheProcessOfStartListening = false;
        }
      }
    }
  }

  /**
   * on one single server socket
   */
  protected def startListeningSafe(): Boolean = {
    log log "start bluetooth listening safely";
    val check = isBluetoothDiscoverable && acceptThread == None;
    if (check) {
      _isOnTheProcessOfStartListening = true;
      log log "start bluetooth listening";

      acceptThread = AcceptThread(handler = acceptHandler, timeout = false)(bluetoothAdapter.get);
      assert(acceptThread.isDefined,
        "tried to create one accept thread, must be room available for this server socket");
      acceptThread.get.start();
    }
    check;
  }

  protected def stopListeningSafe(afterStopListeningDynamic: => Unit): Boolean = {
    log log "stop bluetooth listening safely";
    val wasListening = isListening;
    if (wasListening) {
      _isOnTheProcessOfStopListening = true;

      //synchronous way
      onBeforeStopListening();
      acceptThread.get.cancel {
        () =>
          onUI {
            afterStopListeningDynamic;
            onAfterStopListening();
            _isOnTheProcessOfStopListening = false;
          }
      }
      acceptThread = None;

      //in case you want to try asychronous way, I already did. it does not make a difference
      //either in case of errors, or in case of performance
    }
    wasListening;
  }
}
