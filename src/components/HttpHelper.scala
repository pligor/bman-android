package components

import java.net.{URL, HttpURLConnection}
import java.io.{File, DataOutputStream}
import java.text.SimpleDateFormat
import generic.FileSender

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object HttpHelper {
  private val bufferSize = 16 * 1024;
  //16kBytes

  private val lineEnd = "\r\n";
  private val twoHyphens = "--";
  private val boundary = "*****";
  private val partSeperator = twoHyphens + boundary + lineEnd;
  private val partEnd = twoHyphens + boundary + twoHyphens + lineEnd;

  case class FileMultipart(name: String, filename: String, mime: String, file: File) {
    def toHeader = {
      "Content-Disposition: form-data; name=\"" + name + "\";filename=\"" + filename + "\"" + lineEnd +
        "Content-Type: " + mime + lineEnd + lineEnd;
    }
  }

  private def tuple2multipart(tuple: (String, String)) = {
    val (name, value) = tuple;
    partSeperator +
      "Content-Disposition: form-data; name=\"" + name + "\"" + lineEnd +
      lineEnd +
      value + lineEnd;
  }

  /**
   * CURRENTLY ONLY ONE FILE IS SUPPORTED
   * http://stackoverflow.com/questions/9630430/upload-large-file-in-android-with-outofmemory-error
   * http://www.w3.org/TR/html401/interact/forms.html#h-17.13.4
   * @return response code
   */
  def postMultipart(targetUrl: URL,
                    params: Map[String, String],
                    fileMultipart: FileMultipart,
                    connectionTimeoutMsecs: Int,
                    readTimeoutMsecs: Int)(onPercentageSent: (Float) => Unit): Int = {
    //this is how you seperate each part of the multipart
    val df = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss");

    val connection = targetUrl.openConnection.asInstanceOf[HttpURLConnection];

    connection.setConnectTimeout(connectionTimeoutMsecs);
    connection.setReadTimeout(readTimeoutMsecs);

    //log log "Allow Inputs & Outputs";
    connection.setDoInput(true);
    connection.setDoOutput(true);
    connection.setUseCaches(false);
    connection.setChunkedStreamingMode(bufferSize);

    //log log "Enable POST method";
    connection.setRequestMethod("POST");
    connection.setRequestProperty("Connection", "Keep-Alive");
    connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

    val dataOutputStream = new DataOutputStream(connection.getOutputStream);
    multipartToDataOutputStream(params, fileMultipart, dataOutputStream)(onPercentageSent);

    val serverResponseCode = connection.getResponseCode;

    //val serverResponseMessage = connection.getResponseMessage;

    //log log "Server Response Code: " + serverResponseCode;
    //log log "Server Response Message: " + serverResponseMessage;
    //log log "Server Response Time: " + df.format(new Date(connection.getDate));
    /*filename = CDate + filename.substring(filename.lastIndexOf("."), filename.length)
    Log.i("File Name in Server : ", filename)*/

    serverResponseCode;
  }

  def multipartToDataOutputStream(params: Map[String, String],
                                  fileMultipart: FileMultipart,
                                  dataOutputStream: DataOutputStream)(onPercentageSent: (Float) => Unit) {
    try {
      params foreach {
        param =>
          dataOutputStream.writeBytes(tuple2multipart(param));
      }

      dataOutputStream.writeBytes(partSeperator);
      dataOutputStream.writeBytes({
        val header = fileMultipart.toHeader;
        //log log "contentDisposition: " + header;
        header;
      });
      val fileSender = new FileSender(fileMultipart.file);
      try {
        fileSender.sendToOutStream(dataOutputStream, bufferSize)(onPercentageSent);
      } finally {
        fileSender.end();
      }

      dataOutputStream.writeBytes(lineEnd);

      dataOutputStream.writeBytes(partEnd);
    } finally {
      dataOutputStream.flush();
      dataOutputStream.close();
    }
  }
}
