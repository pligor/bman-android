package components

import myInAppGooglePurchase.InAppBillingActivity
import com.pligor.bman.{TR, Bman, R, TypedViewHolder}
import myandroid.AndroidHelper._
import myandroid.{MyContextWrapper, log}
import android.content.Intent
import activities.blank.BlankActivity
import android.app.Dialog
import android.view.{View, Window}
import android.view.View.OnClickListener
import preferences.{FortumoDeviceReceiptPreference, InAppUnmanagedSubscriptionPreference, FirstRunTimestampPreference, PreTrialPreference}
import models.SubscriptionProduct
import com.fortumo.android.{PaymentResponse, Fortumo}
import com.bugsense.trace.BugSenseHandler
import myfortumo.FortumoDeviceReceipt

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait SubscriptionDialog extends InAppBillingActivity with MyContextWrapper
with MyFortumoPaymentActivity {

  override def onStart(): Unit = {
    super.onStart()

    bindInAppBillingService(onInAppBillingServiceConnectionFailure = {
      //nop
    })
  }

  protected def getSubscriptionDialog = {
    assert(!Bman.isSubscribed, "you should not call this dialog if bman is subscribed")

    val dialog = new Dialog(this) with TypedViewHolder

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background)

    dialog.setCanceledOnTouchOutside(false)

    dialog.setCancelable(true)

    dialog.setContentView(R.layout.dialog_subscription)

    dialog.findView(TR.notSubscribeButton).setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        dialog.dismiss();
      }
    });

    dialog.findView(TR.subscriptionPromptTextView).setText({
      val appName = resources.getString(R.string.app_name)

      if (PreTrialPreference.getValue) {
        resources.getString(R.string.on_pre_trial_prompt,
          appName,
          Bman.getTrialPeriodString,
          SubscriptionProduct.getPeriodString
        )
      } else if (FirstRunTimestampPreference.isOnTrial) {
        resources.getString(R.string.on_trial_prompt,
          appName,
          Bman.getTrialPeriodString,
          SubscriptionProduct.getPeriodString
        )
      } else {
        resources.getString(R.string.on_after_trial_prompt,
          appName,
          SubscriptionProduct.getPeriodString
        )
      }
    })

    dialog.findView(TR.creditCardPaymentButton).setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        assert(!InAppUnmanagedSubscriptionPreference.isSubscribed,
          "we should not reach up to this point if bman is already subscribed with in app billing")

        if (isInAppBillingServiceBound.isDefined && isInAppBillingServiceBound.get) {
          def afterConsumeAttempt(): Unit = {
            purchaseItem(
              SubscriptionProduct.productId,
              SubscriptionProduct.PURCHASE_TYPE,
              onItemAlreadyOwned = {
                //we have chosen to do exactly the same as on regular success
                //Either way this part or the regular success must be called only once
                //unless the subscription expires which means everything in the local variable we are saving has been reset
                onInAppPurchaseAlreadyOwned()
              },
              onError = {
                responseCode =>
                //this is when the request code is something else than normal
                //we have chosen to display the exact same message with the failure of
                //when we have proceeded with the purchase but something bad has happened along the way
                  onInAppPurchaseFailed();
              }
            )
          }

          consumePurchase(SubscriptionProduct.productId, SubscriptionProduct.PURCHASE_TYPE)(
            onConsumeSuccess = {
              log log "consume is successful. nothing to show to the user here"
              afterConsumeAttempt()
            },
            onConsumeError = {
              errorCode =>
                log log "consume error: " + errorCode.toString
                afterConsumeAttempt()
            },
            onNoPurchasedItemFound = {
              log log "there is no related item"
              afterConsumeAttempt()
            }
          )

          dialog.dismiss()
        } else {
          //TODO later visit the user to a webpage where he can see how to perform an Android update
          showToast(R.string.in_app_billing_unavailable)
        }
      }
    })


    dialog.findView(TR.smsPaymentButton).setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        makeFortumoPayment(SubscriptionProduct.getFortumoPaymentRequest)

        dialog.dismiss()
      }
    })

    dialog
  }

  private class FortumoStatusBilledOrPendingButNoDataException(billingStatus: Int) extends Exception

  protected def onFortumoBillingStatus(paymentResponse: PaymentResponse): Unit = {
    val billingStatus = paymentResponse.getBillingStatus

    billingStatus match {
      case Fortumo.MESSAGE_STATUS_BILLED | Fortumo.MESSAGE_STATUS_PENDING =>
        val fortumoDeviceReceiptOption = FortumoDeviceReceipt.createFromPaymentResponse(paymentResponse)

        if (fortumoDeviceReceiptOption.isDefined)
          FortumoDeviceReceiptPreference.setValue(fortumoDeviceReceiptOption)
        else {
          BugSenseHandler.sendException(
            new FortumoStatusBilledOrPendingButNoDataException(billingStatus)
          )
          log log "real problem here because fortumo is being billed but no data"
        }

        onSubscribeSuccess()

      case Fortumo.MESSAGE_STATUS_NOT_SENT |
           Fortumo.MESSAGE_STATUS_FAILED |
           Fortumo.MESSAGE_STATUS_USE_ALTERNATIVE_METHOD =>

        FortumoDeviceReceiptPreference.clear

        onSubscriptionFailure()

      case _ => log log "we do not change the fortumo preference value because we do not know"
    }
  }

  protected def onFortumoNotOkResult(resultCode: Int): Unit = {
    onSubscriptionFailure()
  }

  //============================

  protected def onInAppPurchaseSuccess(): Unit = {
    onSubscribeSuccess()
  }

  /**
   * This might be executed twice but it is ok
   */
  protected def onInAppPurchaseAlreadyOwned(): Unit = {
    log log "we choose to have the same behavior as above, which is show a toast and then actually reset app";

    //two toasts, either you are inside valid period
    //OR you have exceeded the period so what happens is just that consumption of product failed
    if (InAppUnmanagedSubscriptionPreference.isSubscribed) {
      onInAppPurchaseSuccess()
    } else {
      showToast(R.string.in_app_already_owned_but_invalid)
    }

  }

  protected def onInAppPurchaseFailed(): Unit = {
    onSubscriptionFailure()
  }

  //============================

  private def onSubscribeSuccess(): Unit = {
    log log "item purchased successfully";
    showToast(R.string.subscription_purchased_successfully);

    startActivity(new Intent(this, classOf[BlankActivity]).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
    finish()
  }

  private def onSubscriptionFailure(): Unit = {
    showToast(R.string.subscription_purchase_failed)
  }
}
