package components

import android.app.Activity
import android.content.{Intent, Context, BroadcastReceiver, IntentFilter}
import android.bluetooth.BluetoothAdapter
import models.RequestCode
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait BluetoothActivity extends MyActivity {

  //abstract

  /**
   * @return true if bluetooth actually got disabled, false if not
   */
  protected def onBeforeBluetoothTurnOff(disableBluetooth: => Boolean): Boolean;

  protected def onBluetoothTurnOff(): Unit;

  protected def onBluetoothTurnOn(success: Boolean): Unit;

  //concrete

  //you will have to refactor if a device is found with two bluetooth devices!! weird but it could happen
  //protected implicit val bluetoothAdapter: Option[BluetoothAdapter];
  private implicit lazy val bluetoothAdapter = Option(BluetoothAdapter.getDefaultAdapter);

  protected def requestBluetoothEnable() {
    startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), RequestCode.ENABLE_BT.id);
  }

  protected def bluetoothDisable(): Boolean = {
    require(bluetoothAdapter.isDefined, "bluetooth must be supported to disable it");
    onBeforeBluetoothTurnOff(bluetoothAdapter.get.disable());
  }

  /**
   * Optionally, your application can also listen for the ACTION_STATE_CHANGED broadcast
   * Intent, which the system will broadcast whenever the Bluetooth state has changed.
   * This broadcast contains the extra fields EXTRA_STATE and EXTRA_PREVIOUS_STATE,
   * containing the new and old Bluetooth states, respectively. Possible values for these
   * extra fields are STATE_TURNING_ON, STATE_ON, STATE_TURNING_OFF, and STATE_OFF.
   * Listening for this broadcast can be useful to detect changes made to the Bluetooth
   * state while your app is running.
   */
  override def onStart() {
    super.onStart();
    val filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
    registerReceiver(broadcastReceiver, filter);
  }

  override def onStop() {
    super.onStop();
    safeUnregisterReceiver(broadcastReceiver);
  }

  private object broadcastReceiver extends BroadcastReceiver {
    def onReceive(context: Context, intent: Intent) {
      intent.getAction match {
        case BluetoothAdapter.ACTION_STATE_CHANGED => {
          val possibleStates = List[Int](
            BluetoothAdapter.STATE_TURNING_ON,
            BluetoothAdapter.STATE_ON,
            BluetoothAdapter.STATE_OFF,
            BluetoothAdapter.STATE_TURNING_OFF
          );

          val invalidState: Int = possibleStates.min - 1;
          assert(!possibleStates.exists(_ == invalidState));

          val prevState = intent.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_STATE, invalidState);
          val curState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, invalidState);

          assert(prevState != invalidState && curState != invalidState);

          if (prevState == BluetoothAdapter.STATE_ON &&
            (curState == BluetoothAdapter.STATE_TURNING_OFF ||
              curState == BluetoothAdapter.STATE_OFF)) {
            onBluetoothTurnOff();
          }
        }
      }
    }
  }

  override def onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
    RequestCode(requestCode) match {
      case RequestCode.ENABLE_BT => {
        onBluetoothTurnOn(resultCode == Activity.RESULT_OK);
      }
      case _ => super.onActivityResult(requestCode, resultCode, data);
    }
  }
}
