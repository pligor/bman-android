package components

import android.app.Dialog
import android.os.Bundle
import android.view.{View, Window}
import com.pligor.bman.R
import android.widget.{CheckBox, Button, TextView}
import android.view.View.OnClickListener
import preferences.HelperPreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait SwipeHelperDialogTrait extends MyActivity {
  override def onPostCreate(savedInstanceState: Bundle) {
    super.onPostCreate(savedInstanceState);

    if (HelperPreference.getValue) {
      val dialog = new Dialog(context);

      dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
      dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
      dialog.setCanceledOnTouchOutside(false);

      dialog.setCancelable(true);

      dialog.setContentView(R.layout.dialog_helper_swipe);

      val noHelperMessagesCheckBox = dialog.findViewById(R.id.noHelperMessagesCheckBox).asInstanceOf[CheckBox];
      val swipeUnderstoodButton = dialog.findViewById(R.id.swipeUnderstoodButton).asInstanceOf[Button];

      swipeUnderstoodButton.setOnClickListener(new OnClickListener {
        def onClick(view: View) {
          HelperPreference.setValue(!noHelperMessagesCheckBox.isChecked);
          dialog.dismiss();
        }
      });

      dialog.show();
    }
  }
}
