package components

import scala.collection.mutable

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MyEnum {
  private var counter = 0;
  private val vmap = mutable.HashMap.empty[Int, Val];

  private def nextId = {
    counter += 1;
    counter;
  }

  protected class Val(i: Int) {
    def this() {
      this(nextId);
    }

    def id = i;

    override def equals(other: Any): Boolean = {
      other match {
        case x: Val => {
          this.id == x.id;
        }
        case _ => false;
      }
    }
  }

  protected def Val(myval: Val) {
    vmap += myval.id -> myval;
  }

  def apply(x: Int) = vmap(x);
}
