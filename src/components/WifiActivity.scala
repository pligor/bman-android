package components

import android.app.Activity
import android.content.{Intent, Context, BroadcastReceiver, IntentFilter}
import android.net.wifi.WifiManager
import myandroid.log
import myandroid.AndroidHelper._
import com.pligor.bman.R

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait WifiActivity extends MyActivity {
  //abstract
  protected def onWifiEnabled(): Unit;

  protected def onWifiDisabled(): Unit;

  //concrete
  override def onStart() {
    super.onStart();
    registerReceiver(broadcastReceiver, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
  }

  override def onStop() {
    super.onStop();
    safeUnregisterReceiver(broadcastReceiver);
  }

  private object broadcastReceiver extends BroadcastReceiver {
    def onReceive(context: Context, intent: Intent) {
      intent.getAction match {
        case WifiManager.WIFI_STATE_CHANGED_ACTION => {
          val curState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);

          curState match {
            case WifiManager.WIFI_STATE_ENABLED => {
              log log "wifi got enabled, there's a bug(?) which makes it get executed as soon as the activity begins";
              onWifiEnabled();
            }
            case WifiManager.WIFI_STATE_DISABLED => {
              log log "wifi just got disabled";
              //alljoyn does not have a problem if we are on a waiting state and the wifi turns off
              //so it is valid to turn off ReceiverImmutableService AFTER having disabled wifi
              onWifiDisabled();
            }
            case WifiManager.WIFI_STATE_DISABLING |
                 WifiManager.WIFI_STATE_ENABLING |
                 WifiManager.WIFI_STATE_UNKNOWN => {}
          }
        }
      }
    }
  }

}
