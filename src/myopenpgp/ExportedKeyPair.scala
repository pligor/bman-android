package myopenpgp

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case class ExportedKeyPair(publicKey: Array[Byte], secretKey: Array[Byte]);
