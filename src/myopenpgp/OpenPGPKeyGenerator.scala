package myopenpgp

import java.security._
import java.io.{OutputStream, FileOutputStream, File, ByteArrayOutputStream}
import org.spongycastle.bcpg.ArmoredOutputStream
import org.spongycastle.openpgp.{PGPSignature, PGPSecretKey}
import java.util.Date
import org.spongycastle.crypto.AsymmetricCipherKeyPair
import org.spongycastle.crypto.generators.RSAKeyPairGenerator
import org.spongycastle.crypto.params.RSAKeyGenerationParameters
import scala.collection.JavaConverters._
import OpenPGPConfig._
import models.PgpIdentity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object OpenPGPKeyGenerator {
  def getInfoProvidersAndAlgorithms(certainProvider: Option[String] = None): String = {
    val buffer = new StringBuffer;
    val provs = Security.getProviders;
    provs.foreach {
      provider =>
        if ((certainProvider.isDefined && certainProvider.get == provider.getName) || !certainProvider.isDefined) {
          buffer append "provider: " + provider.getName + "\n";
          val services = provider.getServices.asScala;
          services.foreach {
            service =>
              buffer append "\talgorithm: " + service.getAlgorithm + "\n";
          }
        }
    }
    buffer toString;
  }

  def exportKeyPairToByteArrays(keyPair: KeyPair, identity: PgpIdentity, passPhrase: Option[String] = None): ExportedKeyPair = {
    val secretOutputStream = new ByteArrayOutputStream();
    val publicOutputStream = new ByteArrayOutputStream();

    exportKeyPair(
      secretOut = secretOutputStream,
      publicOut = publicOutputStream,
      publicKey = keyPair.getPublic,
      privateKey = keyPair.getPrivate,
      publicKeyAlgorithmTag = PUBLIC_KEY_ALGORITHM_TAG,
      symmetricKeyAlgorithmTag = SYMMETRIC_KEY_ALGORITHM_TAG,
      identity = identity,
      passPhrase = if (passPhrase.isDefined) passPhrase.get.toCharArray else Array.empty[Char],
      armor = ARMORED_KEYS
    );

    ExportedKeyPair(
      publicKey = publicOutputStream.toByteArray,
      secretKey = secretOutputStream.toByteArray
    );
  }

  def exportKeyPairToFiles(keyPair: KeyPair, privateKeyFile: File, publicKeyFile: File,
                           identity: PgpIdentity, passPhrase: Option[String] = None) {
    exportKeyPair(
      secretOut = new FileOutputStream(privateKeyFile),
      publicOut = new FileOutputStream(publicKeyFile),
      publicKey = keyPair.getPublic,
      privateKey = keyPair.getPrivate,
      publicKeyAlgorithmTag = PUBLIC_KEY_ALGORITHM_TAG,
      symmetricKeyAlgorithmTag = SYMMETRIC_KEY_ALGORITHM_TAG,
      identity = identity,
      passPhrase = if (passPhrase.isDefined) passPhrase.get.toCharArray else Array.empty[Char],
      armor = ARMORED_KEYS
    );
  }

  private def exportKeyPair(secretOut: OutputStream,
                            publicOut: OutputStream,
                            publicKey: PublicKey,
                            privateKey: PrivateKey,
                            publicKeyAlgorithmTag: Int,
                            symmetricKeyAlgorithmTag: Int,
                            identity: PgpIdentity,
                            passPhrase: Array[Char],
                            armor: Boolean) {

    val secretOutputStream = if (armor) {
      new ArmoredOutputStream(secretOut);
    } else {
      secretOut;
    }

    val secretKey = new PGPSecretKey(
      PGPSignature.DEFAULT_CERTIFICATION,
      publicKeyAlgorithmTag,
      publicKey,
      privateKey,
      new Date,
      identity.toString,
      symmetricKeyAlgorithmTag,
      passPhrase,
      null,
      null,
      new SecureRandom(),
      PROVIDER_NAME
    );

    secretKey.encode(secretOutputStream);
    secretOutputStream.close();

    val publicOutputStream = if (armor) {
      new ArmoredOutputStream(publicOut);
    } else {
      publicOut;
    }

    val pgpPublicKey = secretKey.getPublicKey;
    pgpPublicKey.encode(publicOutputStream);
    publicOutputStream.close();
  }

  def genKeyPairFromJavaSecurity: KeyPair = {
    val generator = KeyPairGenerator.getInstance(PUBLIC_KEY_ALGORITHM_STRING, PROVIDER_NAME);
    generator.initialize(KEY_SIZE_IN_BITS, new SecureRandom());
    generator.generateKeyPair();
  }

  private def genKeyPairFromBouncyCastle: AsymmetricCipherKeyPair = {
    val keyPairGenerator = getPreinitializedRSAKeyPairGenerator;
    keyPairGenerator.generateKeyPair();
  }

  private def getPreinitializedRSAKeyPairGenerator: RSAKeyPairGenerator = {
    val keyPairGenerator = new RSAKeyPairGenerator;

    val publicExponent = new java.math.BigInteger(65537.toString);

    //http://crypto.stackexchange.com/questions/3114/what-is-the-correct-value-for-certainty-in-rsa-key-pair-generation
    val certainty = 112; //or 100 or 80

    //public exponent must be a fermat number
    val params = new RSAKeyGenerationParameters(
      publicExponent,
      new SecureRandom(),
      KEY_SIZE_IN_BITS, //strength
      certainty
    );

    keyPairGenerator.init(params);

    keyPairGenerator;
  }
}
