package myopenpgp

import java.io._
import org.spongycastle.openpgp.{PGPPublicKeyRing, PGPPublicKeyRingCollection, PGPUtil, PGPPublicKey}
import scala.collection.JavaConverters._
import components.JavaStreamHelper
import JavaStreamHelper._
import OpenPGPConfig._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object OpenPGPEncrypter {

  private class EncryptionKeyNotFoundException extends Exception;

  def encryptString(utf8dataString: String,
                    publicKeyBytes: Array[Byte],
                    encryptedFile: File,
                    decryptedFilename: String) {
    encryptStringInternal(
      utf8dataString = utf8dataString,
      publicKeyInputStream = new ByteArrayInputStream(publicKeyBytes),
      encryptedFile = encryptedFile,
      decryptedFilename = decryptedFilename
    );
  }

  def encryptString(utf8dataString: String,
                    publicKeyFile: File,
                    encryptedFile: File,
                    decryptedFilename: String) {
    encryptStringInternal(
      utf8dataString = utf8dataString,
      publicKeyInputStream = new FileInputStream(publicKeyFile),
      encryptedFile = encryptedFile,
      decryptedFilename = decryptedFilename
    );
  }

  private def encryptStringInternal(utf8dataString: String,
                                    publicKeyInputStream: InputStream,
                                    encryptedFile: File,
                                    decryptedFilename: String) {
    encryptFunc(
      publicKeyInputStream,
      encryptedFile,
      decryptedFilename,
      literalDataFormat = LiteralDataFormat.STRING,
      armored = ARMORED_DATA,
      withIntegrityPacket = INTEGRITY_CHECK,
      zipped = ZIPPED
    ) {
      outputStream =>
      //we will use a print writer for convenience
        val printWriter = new java.io.PrintWriter(outputStream);
        try {
          printWriter.print(utf8dataString);
          printWriter.flush();
        } finally {
          printWriter.close();
        }
    }
  }

  def encryptFile(inputFile: File, publicKeyBytes: Array[Byte], encryptedFile: File, decryptedFilename: String) {
    encryptFileInternal(
      inputFile = inputFile,
      publicKeyInputStream = new ByteArrayInputStream(publicKeyBytes),
      encryptedFile = encryptedFile,
      decryptedFilename = decryptedFilename
    );
  }

  def encryptFile(inputFile: File, publicKeyFile: File, encryptedFile: File, decryptedFilename: String) {
    encryptFileInternal(
      inputFile = inputFile,
      publicKeyInputStream = new FileInputStream(publicKeyFile),
      encryptedFile = encryptedFile,
      decryptedFilename = decryptedFilename
    );
  }


  private def encryptFileInternal(inputFile: File,
                                  publicKeyInputStream: InputStream,
                                  encryptedFile: File,
                                  decryptedFilename: String) {
    encryptInputStreamInternal(
      inputStream = new FileInputStream(inputFile),
      publicKeyInputStream = publicKeyInputStream,
      encryptedFile = encryptedFile,
      decryptedFilename = decryptedFilename
    );
  }

  def encryptInputStream(inputStream: InputStream,
                         publicKeyBytes: Array[Byte],
                         encryptedFile: File,
                         decryptedFilename: String) {
    encryptInputStreamInternal(
      inputStream = inputStream,
      publicKeyInputStream = new ByteArrayInputStream(publicKeyBytes),
      encryptedFile = encryptedFile,
      decryptedFilename = decryptedFilename
    );
  }

  def encryptInputStream(inputStream: InputStream,
                         publicKeyFile: File,
                         encryptedFile: File,
                         decryptedFilename: String) {
    encryptInputStreamInternal(
      inputStream = inputStream,
      publicKeyInputStream = new FileInputStream(publicKeyFile),
      encryptedFile = encryptedFile,
      decryptedFilename = decryptedFilename
    );
  }

  private def encryptInputStreamInternal(inputStream: InputStream,
                                 publicKeyInputStream: InputStream,
                                 encryptedFile: File,
                                 decryptedFilename: String) {
    encryptFunc(
      publicKeyInputStream,
      encryptedFile,
      decryptedFilename,
      literalDataFormat = LiteralDataFormat.BINARY,
      armored = ARMORED_DATA,
      withIntegrityPacket = INTEGRITY_CHECK,
      zipped = ZIPPED
    ) {
      outputStream =>
        try {
          copyInputToOutputImperative(inputStream, outputStream, BUFFER_SIZE)();
        } finally {
          inputStream.close();
        }
    }
  }

  /**
   * Encrypts a file using a public key
   * @param publicKeyInputStream the stream that the public key will be read from
   * @param encryptedFile the encrypted file that we want to be created
   * @param decryptedFilename the path the file will be put when receiver decides to decrypt
   * @param armored whether to use armor or not (armor is ascii conversion)
   * @param withIntegrityPacket whether to have integrity check or not
   */
  private def encryptFunc(publicKeyInputStream: InputStream,
                          encryptedFile: File,
                          decryptedFilename: String,
                          literalDataFormat: LiteralDataFormat.Value,
                          armored: Boolean,
                          withIntegrityPacket: Boolean,
                          zipped: Boolean)(op: (OutputStream) => Unit) {
    val publicKeyOption = getFirstEncryptionKey(publicKeyInputStream);
    if (publicKeyOption.isDefined) {
      val publicKey = publicKeyOption.get;

      //make sure that the directories required exist. no check of boolean because it returns true only on first creation
      encryptedFile.getParentFile.mkdirs();

      val fileOutputStream = new FileOutputStream(encryptedFile);
      try {
        //make one of our encryption utilities
        val encryptionUtil = new PGPEncryptionUtil(
          publicKey,
          decryptedFilename,
          fileOutputStream,
          literalDataFormat,
          armored = armored,
          withIntegrityPacket = withIntegrityPacket,
          zipped = zipped
        );
        try {
          op(encryptionUtil.getPayloadOutputStream);
        } finally {
          encryptionUtil.close();
        }
      } finally {
        fileOutputStream.close();
      }
    } else {
      throw new EncryptionKeyNotFoundException;
    }
  }

  private def getFirstEncryptionKey(inputStream: InputStream): Option[PGPPublicKey] = {
    val publicKeyrings = getPublicKeyrings(inputStream);
    val stream = publicKeyrings.map(getEncryptionKey).filter(_.isDefined).toStream;
    if (stream.headOption.isDefined) {
      stream.head;
    } else {
      None;
    }
  }

  private def getPublicKeyrings(inputStream: InputStream) = {
    require(Option(inputStream).isDefined);

    val decoderStream = PGPUtil.getDecoderStream(inputStream);
    val publicKeyringCollection = new PGPPublicKeyRingCollection(decoderStream);

    publicKeyringCollection.getKeyRings.asScala.map(_.asInstanceOf[PGPPublicKeyRing]);
  }

  private def getEncryptionKey(publicKeyRing: PGPPublicKeyRing): Option[PGPPublicKey] = {
    require(Option(publicKeyRing).isDefined);
    //iterate over the keys on the ring, look for the one which is suitable for encryption
    publicKeyRing.getPublicKeys.asScala.map(_.asInstanceOf[PGPPublicKey]).find(_.isEncryptionKey);
  }
}
