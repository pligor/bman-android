package myopenpgp

import org.spongycastle.bcpg.{SymmetricKeyAlgorithmTags, PublicKeyAlgorithmTags}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * http://sloanseaman.com/wordpress/2012/05/13/revisited-pgp-encryptiondecryption-in-java/
 */
private object OpenPGPConfig {
  val ZIPPED = false;

  /**
   * Armored means converting to ascii or leaving it as bytes
   * recommended to false if you are saving in files
   */
  val ARMORED_DATA = false;

  /**
   * FALSE is NOT tested
   * recommended to true if you are saving the keys inside databases or json/xml
   */
  val ARMORED_KEYS = true;

  val KEY_SIZE_IN_BITS = 2048;

  //private val BUFFER_SIZE: Int = 1 << 16; //65536
  //private val BUFFER_SIZE: Int = 1 << 12; //4096;
  val BUFFER_SIZE: Int = 1 << 14;
  //16384;

  //for original bouncy castle
  //private val PROVIDER_NAME = "BC";
  //for spongy castle (bouncy castle version for android)
  val PROVIDER_NAME = "SC";

  //private val PUBLIC_KEY_ALGORITHM_TAG = PublicKeyAlgorithmTags.ELGAMAL_GENERAL;
  val PUBLIC_KEY_ALGORITHM_TAG = PublicKeyAlgorithmTags.RSA_GENERAL;

  //private val PUBLIC_KEY_ALGORITHM_STRING = "RSA/ECB/PKCS1Padding";
  val PUBLIC_KEY_ALGORITHM_STRING = "RSA";
  //private val PUBLIC_KEY_ALGORITHM_STRING = "ELGAMAL";

  //http://stackoverflow.com/questions/3076222/top-hashing-and-encryption-algorithms
  //PGP uses a symmetric key to encrypt data and uses the public key to encrypt the symmetric key used on the payload
  //private val SYMMETRIC_KEY_ALGORITHM_TAG = SymmetricKeyAlgorithmTags.AES_256;
  //private val SYMMETRIC_KEY_ALGORITHM_TAG = SymmetricKeyAlgorithmTags.CAST5;
  //private val SYMMETRIC_KEY_ALGORITHM_TAG = SymmetricKeyAlgorithmTags.BLOWFISH;
  val SYMMETRIC_KEY_ALGORITHM_TAG = SymmetricKeyAlgorithmTags.TWOFISH;

  /**
   * UNTESTED WHAT THE TRUE DOES. AVOID SETTING TO TRUE UNLESS YOU KNOW WHAT YOU ARE DOIN
   */
  val INTEGRITY_CHECK = false;
}
