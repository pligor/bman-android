package myopenpgp

import java.io._
import components.JavaStreamHelper
import JavaStreamHelper._
import org.spongycastle.openpgp._
import OpenPGPConfig._
import scala.collection.JavaConverters._
import generic.FileHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object OpenPGPDecrypter {

  private class PrivateKeyNotFoundException extends Exception;

  private class EncryptedDataListNotFoundException extends Exception;

  private class CompressedDataNotFoundException extends Exception;

  private class LiteralDataNotFoundException extends Exception;

  private class NorOnePassSignatureListNorLiteralDataException extends Exception;

  def decryptFile(encryptedFile: File,
                  secretKey: Either[File, Array[Byte]],
                  outputPath: File,
                  randomFilename: Boolean = false,
                  passphrase: Option[String] = None): File = {
    require(outputPath.exists() && outputPath.isDirectory);

    val literalData = decrypt(new FileInputStream(encryptedFile), passphrase, ZIPPED) {
      secretKey.fold(
        file => new FileInputStream(file),
        byteArray => new ByteArrayInputStream(byteArray)
      );
    }

    val fileout = new File(outputPath.getCanonicalPath + File.separator + (if (randomFilename) {
      getRandomFilename();
    } else {
      literalData.getFileName
    }));

    copyInputToOutputImperative(
      inputStream = literalData.getInputStream,
      outputStream = new FileOutputStream(fileout),
      BUFFER_SIZE
    )();

    fileout;
  }

  /**
   * A string was encrypted and the result was saved in a file
   * Providing this file and the private key we can decrypt it
   */
  def decryptString(encryptedStringFile: File, secretKey: Either[File, Array[Byte]], passphrase: Option[String] = None): String = {
    val literalData = decrypt(new FileInputStream(encryptedStringFile), passphrase, ZIPPED) {
      secretKey.fold(
        file => new FileInputStream(file),
        byteArray => new ByteArrayInputStream(byteArray)
      );
    }

    val byteOutStream = new ByteArrayOutputStream();

    copyInputToOutputImperative(
      inputStream = literalData.getInputStream,
      outputStream = byteOutStream,
      BUFFER_SIZE
    )();

    new String(byteOutStream.toByteArray);
  }

  /**
   * Function requires an armored, zip compressed, pgp encrypted file
   * http://stackoverflow.com/questions/10999542/bouncy-castle-c-sharp-pgp-decryption-example
   */
  private def decrypt(inputStream: InputStream, passphrase: Option[String], zipped: Boolean)
                     (privateKeyInputStream: => InputStream): PGPLiteralData = {
    val privateKeyOption = getFirstPrivateKey(privateKeyInputStream, passphrase);
    if (privateKeyOption.isDefined) {
      val privateKey = privateKeyOption.get;

      val encryptedInputStream = PGPUtil.getDecoderStream(inputStream);

      val encryptedObjectFactory = new PGPObjectFactory(encryptedInputStream);

      val encryptedDataListOption = findInPGPObjectFactoryImperative[PGPEncryptedDataList](encryptedObjectFactory);

      if (encryptedDataListOption.isDefined) {
        val encryptedDataList = encryptedDataListOption.get;

        val encryptedDataObjects = encryptedDataList.getEncryptedDataObjects.asScala.
          map(_.asInstanceOf[PGPPublicKeyEncryptedData]).toStream;

        val encryptedData = encryptedDataObjects.head;

        val finalInputStream = if (zipped) {
          val decryptedInputStream = encryptedData.getDataStream(privateKey, PROVIDER_NAME);

          val decryptedObjectFactory = new PGPObjectFactory(decryptedInputStream);

          val compressedDataOption = findInPGPObjectFactoryImperative[PGPCompressedData](decryptedObjectFactory);

          if (compressedDataOption.isDefined) {
            val compressedData = compressedDataOption.get;
            compressedData.getDataStream;
          } else {
            throw new CompressedDataNotFoundException;
          }
        } else {
          encryptedData.getDataStream(privateKey, PROVIDER_NAME);
        }

        //val objectFactory = new PGPObjectFactory(compressedDataInputStream);
        val objectFactory = new PGPObjectFactory(finalInputStream);

        val objectOption = findInPGPObjectFactoryImperative[AnyRef](objectFactory);

        if (objectOption.isDefined) {
          val myObject = objectOption.get;
          if (myObject.getClass == classOf[PGPOnePassSignatureList]) {
            objectFactory.nextObject().asInstanceOf[PGPLiteralData];
          } else if (myObject.getClass == classOf[PGPLiteralData]) {
            myObject.asInstanceOf[PGPLiteralData];
          } else {
            throw new NorOnePassSignatureListNorLiteralDataException;
          }
        } else {
          throw new LiteralDataNotFoundException;
        }
      } else {
        throw new EncryptedDataListNotFoundException;
      }
    } else {
      throw new PrivateKeyNotFoundException;
    }
  }


  private def getSecretKeyrings(inputStream: InputStream) = {
    require(Option(inputStream).isDefined);

    val decoderStream = PGPUtil.getDecoderStream(inputStream);
    val secretKeyringCollection = new PGPSecretKeyRingCollection(decoderStream);

    secretKeyringCollection.getKeyRings.asScala.map(_.asInstanceOf[PGPSecretKeyRing]);
  }

  private def getSecretKey(secretKeyRing: PGPSecretKeyRing): Option[PGPSecretKey] = {
    require(Option(secretKeyRing).isDefined);
    //iterate over the keys on the ring, look for the one which is suitable for encryption
    secretKeyRing.getSecretKeys.asScala.map(_.asInstanceOf[PGPSecretKey]).find(_.isMasterKey);
    //secretKeyRing.getPublicKeys.asScala.map(_.asInstanceOf[PGPPublicKey]).find(_.isEncryptionKey);
  }

  private def getFirstSecretKey(inputStream: InputStream): Option[PGPSecretKey] = {
    val secretKeyrings = getSecretKeyrings(inputStream);
    val stream = secretKeyrings.map(getSecretKey(_)).filter(_.isDefined).toStream;
    if (stream.headOption.isDefined) {
      stream.head;
    } else {
      None;
    }
  }

  private def getFirstPrivateKey(inputStream: InputStream,
                                 passphrase: Option[String]): Option[PGPPrivateKey] = {
    val secretKeyOption = getFirstSecretKey(inputStream);

    if (secretKeyOption.isDefined) {
      val secretKey = secretKeyOption.get;
      try {
        val passphraseCharArray = if (passphrase.isDefined) {
          passphrase.get.toCharArray;
        } else {
          Array.empty[Char];
        }
        Some(secretKey.extractPrivateKey(passphraseCharArray, PROVIDER_NAME));
      } catch {
        case e: PGPException => {
          None;
        }
      }
    } else {
      None;
    }
  }

  private def findInPGPObjectFactoryImperative[T](objectFactory: PGPObjectFactory)(implicit mani: Manifest[T]): Option[T] = {
    var obj: Option[AnyRef] = None;
    //var counter = 0;
    while ( {
      obj = Option(objectFactory.nextObject());
      obj.isDefined && !obj.get.isInstanceOf[T];
    }) {
      //counter += 1;
    }

    /*val tManifest = manifest[PGPOnePassSignatureList];
    mani match {
      case `tManifest` => {
        println(obj.get.getClass.getName);
        println("counter: " + counter);

        if (mani.toString == obj.get.getClass.getName) {
          println("einai idia");
        } else {
          println("vevaia den einai idia");
        }
      }
      case _ => {}
    }*/

    //manifest[T].getClass
    if (obj.isDefined /*&& (mani.toString == obj.get.getClass.getName)*/ ) {
      Some(obj.get.asInstanceOf[T]);
    } else {
      None;
    }
  }


  /**
   * @deprecated
   */
  private def getFirstKeyring(inputStream: InputStream): PGPPublicKeyRing = {
    require(Option(inputStream).isDefined);

    //Decode a PGP public key block
    //PGPUtil.getDecoderStream will detect ASCII-armor automatically and decode it
    val decoderStream = PGPUtil.getDecoderStream(inputStream);

    //return the keyring it represents
    //the PGPObject factory then knows how to read all the data in the encoded stream
    val factory = new PGPObjectFactory(decoderStream);
    factory.nextObject() match {
      case obj: PGPPublicKeyRing => obj;
      case _ => throw new IllegalArgumentException("Input text does not contain a PGP Public Key");
    }
  }
}
