package myopenpgp

import org.spongycastle.openpgp.{PGPLiteralDataGenerator, PGPCompressedDataGenerator, PGPEncryptedDataGenerator, PGPPublicKey}
import java.io.OutputStream
import org.spongycastle.bcpg.{CompressionAlgorithmTags, ArmoredOutputStream}
import java.security.SecureRandom
import java.util.Date
import OpenPGPConfig._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
/**
 * LITERAL OUTPUT STREAM -> COMPRESSED OUTPUT STREAM : apo to file dimiourgoume ena named piece of data pou onomazetai literal kai to sympiezoume me zip
 * COMPRESSED OUTPUT STREAM -> ENCRYPTED OUTPUT STREAM : to sympiesmeno to kanoume encrypted me to tyxaio symmetric key pou exoume dhmiourghsei
 * ENCRYPTED OUTPUT STREAM -> ARMORED OUTPUT STREAM : ginetai apo kanoniko se Armored diladi ascii based
 * ARMORED OUTPUT STREAM -> OUTPUT STREAM : to armored ginetai kanoniko stream gia na fygei pros ton ekso kosmo
 *
 * we have the literal stream connected to a data compressor, which is connected to a data encryptor,
 * which is connected to an ascii-encoder
 */
/**
 *
 * @param publicKey of the receiver. I repeat, not our own public key but receivers
 * @param payloadFilename the name of the file which will be created AFTER decryption
 * @param outputStream the output stream where the encrypted data will be written (e.g. FileOutputStream, ByteArrayOutputStream etc.)
 */
private class PGPEncryptionUtil(private val publicKey: PGPPublicKey,
                                private val payloadFilename: String,
                                private val outputStream: OutputStream,
                                private val literalDataFormat: LiteralDataFormat.Value,
                                private val armored: Boolean,
                                private val withIntegrityPacket: Boolean,
                                private val zipped: Boolean) {
  //write data out using ascii-armor encoding . this is not binary. This is the normal PGP text output
  private val afterEncryptionOutputStream = if (armored) {
    new ArmoredOutputStream(outputStream);
  } else {
    outputStream;
  }

  private val encryptedOut: OutputStream = {
    //create an encrypted payload and set the public key on the data generator
    val encryptGen = new PGPEncryptedDataGenerator(
      SYMMETRIC_KEY_ALGORITHM_TAG, //the symmetric algorithm to use for the payload
      withIntegrityPacket,
      new SecureRandom(), //source of randomness
      PROVIDER_NAME //the provider name to use for encryption algorithms.
    );
    encryptGen.addMethod(publicKey);
    //open an output stream connected to the encrypted data generator
    //and have the generator write its data to the ascii-encoded stream
    encryptGen.open(
      afterEncryptionOutputStream,
      new Array[Byte](BUFFER_SIZE)
    );
  }

  private val afterLiteralOutputStream = if (zipped) {
    new PGPCompressedDataGenerator(CompressionAlgorithmTags.ZIP).open(encryptedOut);
  } else {
    encryptedOut;
  }

  //What we want to do specifically is make a file containing what PGP calls a “literal” data packet.
  //This type of packet is simply a named series of bytes, in our case a named series of plain text characters.

  //into that we want to write a PGP literal object, which is just a named piece of data
  // (as opposed to a specially-formatted key, signature, etc)
  private val literalOut = new PGPLiteralDataGenerator().open(
    afterLiteralOutputStream, //output stream
    LiteralDataFormat.toPGPLiteralDataFormat(literalDataFormat), //format
    payloadFilename, //name
    new Date, //modification time
    new Array[Byte](BUFFER_SIZE) //buffer
  );

  def getPayloadOutputStream = literalOut;

  /**
   * Close the encrypted output writers in the reverse order from which they were opened
   */
  def close() {
    literalOut.close();
    afterLiteralOutputStream.close();
    encryptedOut.close();
    afterEncryptionOutputStream.close();
  }
}
