package myopenpgp

import org.spongycastle.openpgp.PGPLiteralDataGenerator

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
private object LiteralDataFormat extends Enumeration {
  val STRING = Value;
  val BINARY = Value;

  def toPGPLiteralDataFormat(value: LiteralDataFormat.Value) = {
    value match {
      case LiteralDataFormat.STRING => PGPLiteralDataGenerator.UTF8;
      case LiteralDataFormat.BINARY => PGPLiteralDataGenerator.BINARY;
    }
  }
}
