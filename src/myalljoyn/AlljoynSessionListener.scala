package myalljoyn

import org.alljoyn.bus.SessionListener

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class AlljoynSessionListener(onSessionLost: (Int) => Unit) extends SessionListener {
  override def sessionLost(sessionId: Int) {
    super.sessionLost(sessionId);
    onSessionLost(sessionId);
  }
}
