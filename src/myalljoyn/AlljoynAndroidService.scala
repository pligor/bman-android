package myalljoyn

import android.app.Service
import android.os.{Bundle, Messenger}
import myandroid.MyContextWrapper

abstract class AlljoynAndroidServiceExtras extends Enumeration {
  val THE_MESSENGER = Value;
  val SOURCE_ACTIVITY = Value;
}

abstract class AlljoynAndroidServiceActivities extends Enumeration {
  val INVALID = Value;
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class AlljoynAndroidService extends Service with MyContextWrapper {
  protected var messenger: Option[Messenger] = None

  /**
   * Return true if you would like to have the service's onRebind(Intent) method later called when new clients bind to it.
   */
  /*override def onUnbind(intent: Intent): Boolean = {
    super.onUnbind(intent);
    messenger = None;
    true;
  }*/
  protected def setMessenger(bundle: Bundle): Unit;

  protected def setActivity(bundle: Bundle): Unit;
}
