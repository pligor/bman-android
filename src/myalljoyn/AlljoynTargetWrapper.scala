package myalljoyn

import android.os.{Parcel, Parcelable}
import android.os.Parcelable.Creator
import myandroid.log

object AlljoynTargetWrapper {
  final val CREATOR: Creator[AlljoynTargetWrapper] = new Creator[AlljoynTargetWrapper] {
    def newArray(size: Int) = new Array[AlljoynTargetWrapper](size);

    def createFromParcel(parcel: Parcel): AlljoynTargetWrapper = {
      def retrieveTargetOption() = Option(parcel.readParcelable[AlljoynTarget](classOf[AlljoynTarget].getClassLoader));

      val messageTargetOption = retrieveTargetOption();
      val rawTargetOption = retrieveTargetOption();

      val wrapper = new AlljoynTargetWrapper;

      if (messageTargetOption.isDefined) {
        wrapper.setMessageTarget(messageTargetOption.get);
      } else {
        //nop
      }

      if (rawTargetOption.isDefined) {
        wrapper.setRawTarget(rawTargetOption.get);
      } else {
        //nop
      }

      wrapper;
    }
  }
}


/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * http://developer.android.com/guide/components/aidl.html
 */
class AlljoynTargetWrapper extends Parcelable {

  def this(target: AlljoynTarget, rawCheck: RawCheck.Value) {
    this();
    setTarget(target, rawCheck);
  }

  private def setTarget(target: AlljoynTarget, rawCheck: RawCheck.Value) {
    rawCheck match {
      case RawCheck.NOT_RAW => setMessageTarget(target);
      case RawCheck.IS_RAW => setRawTarget(target);
    }
  }

  private var messageTarget: Option[AlljoynTarget] = None;

  private var rawTarget: Option[AlljoynTarget] = None;

  def writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeParcelable(messageTarget.getOrElse(null), flags);
    parcel.writeParcelable(rawTarget.getOrElse(null), flags);
  }

  def describeContents(): Int = 0;

  def getMessageTarget: Option[AlljoynTarget] = messageTarget;

  def getRawTarget: Option[AlljoynTarget] = rawTarget;

  def isFullyDefined = messageTarget.isDefined && rawTarget.isDefined;

  def rewriteTarget(target: AlljoynTarget, rawCheck: RawCheck.Value) {
    rawCheck match {
      case RawCheck.NOT_RAW => rewriteMessageTarget(target);
      case RawCheck.IS_RAW => rewriteRawTarget(target);
    }
  }

  private def rewriteMessageTarget(target: AlljoynTarget) {
    if (messageTarget.isDefined) {
      messageTarget = Some(messageTarget.get + target);
    } else {
      setMessageTarget(target);
    }
  }

  private def rewriteRawTarget(target: AlljoynTarget) {
    if (rawTarget.isDefined) {
      rawTarget = Some(rawTarget.get + target);
    } else {
      setRawTarget(target);
    }
  }

  def setMessageTarget(target: AlljoynTarget) {
    if (rawTarget.isDefined) {
      require(target.name == rawTarget.get.name);
    } else {
      //log log "no need to check if message target name and raw target name are equal YET";
    }
    messageTarget = Some(target);
  }

  def setRawTarget(target: AlljoynTarget) {
    if (messageTarget.isDefined) {
      require(target.name == messageTarget.get.name);
    } else {
      //log log "no need to check if message target name and raw target name are equal YET";
    }
    rawTarget = Some(target);
  }

  def getName = {
    if (messageTarget.isDefined && rawTarget.isDefined) {
      assert(messageTarget.get.name == rawTarget.get.name);
      messageTarget.get.name;
    } else if (messageTarget.isDefined) {
      messageTarget.get.name;
    } else if (rawTarget.isDefined) {
      rawTarget.get.name;
    } else {
      throw new UninitializedError;
    }
  }

  override def equals(other: Any): Boolean = {
    other match {
      case that: AlljoynTargetWrapper => {
        this.getName == that.getName;
      }
      case _ => false;
    }
  }
}
