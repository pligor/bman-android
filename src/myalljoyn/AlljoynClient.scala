package myalljoyn

import org.alljoyn.bus._
import AlljoynHelper._
import components.{WriteOnce}
import android.content.Context
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait AlljoynClient {

  def genBusAttachment(implicit context: Context): BusAttachment;

  protected def findServiceName(implicit busAttachment: BusAttachment): Boolean;

  def leaveSessions(sessionIds: Seq[Either[WriteOnce[Int], Option[Int]]])(implicit busAttachment: BusAttachment) {
    def leaveSession(sessionId: Int) {
      busAttachment.leaveSession(sessionId);
      //log log "left session with id: " + sessionId;
    }

    sessionIds foreach {
      sessionId => sessionId.fold(
        writeOnce => if (writeOnce.isInitialized) {
          leaveSession(writeOnce());
        },
        option => if (option.isDefined) {
          leaveSession(option.get);
        }
      );
    }
  }

  def disconnect(sessionIds: Seq[Either[WriteOnce[Int], Option[Int]]])(implicit busAttachment: BusAttachment) {
    leaveSessions(sessionIds);
    busAttachment.disconnect();
  }

  def connect(onFoundAdvertisedName: (AdvertisedName) => Unit)
             (implicit context: Context): Option[BusAttachment] = {
    implicit val busAttachment = genBusAttachment;

    debugAlljoyn(enabled = true);

    //Register a BusListener; in this case it is required for the foundAdvertisedName signal
    busAttachment.registerBusListener(new BusListener {

      override def foundAdvertisedName(name: String, transport: Short, namePrefix: String) {
        super.foundAdvertisedName(name, transport, namePrefix);
        log log "just found the advertised name: %s with transport: 0x%04x (%s) and name prefix: %s".format(
          name, transport, transportsToString(transport), namePrefix
        );
        //WE GOT A SPECIFIC TRANSPORT. WE SHOULD TRY AND USE ONLY THAT

        onFoundAdvertisedName(AdvertisedName(name, transport, namePrefix));

        /*
         * This client will only join the first service that it sees advertising the indicated well-known
         * name.  If the program is already a member of a session (i.e. connected to a service) we will
         * not attempt to join another session.
         * It is possible to join multiple session however joining multiple sessions is not shown in this sample.
         */
        //http://stackoverflow.com/questions/13359560/bus-blocking-call-not-allowed-error-in-alljoyn
        //if we wanted to execute join session in here we must have called
        //bus.enableConcurrentCallbacks() prior to calling bus.joinSession
        //otherwise you get this exception message: BUS_BLOCKING_CALL_NOT_ALLOWED
      }
    });

    val connectedToBusStatus = busAttachment.connect();
    if (connectedToBusStatus == Status.OK) {
      Some(busAttachment);
    } else {
      log log "client bus connection has failed with status: " + connectedToBusStatus;
      None;
    }
  }

  def joinSession(name: String, transport: Short, contactPort: Short)(onMessageSessionLost: (Int) => Unit)
                 (implicit busAttachment: BusAttachment): Option[Int] = {
    //When the foundAdvertisedName signal is received, join the session
    val sessionIdMutable = new Mutable.IntegerValue;

    val sessionOptions = getDefaultSessionOptions(RawCheck.NOT_RAW, transport);

    /*log log "joining message session with name: %s | port: %s | and session transports: %s".format(
      name, contactPort, sessionOptions.transports
    );*/
    val status = busAttachment.joinSession(
      name,
      contactPort,
      sessionIdMutable,
      sessionOptions,
      new AlljoynSessionListener(onMessageSessionLost)
    );

    if (status == Status.OK) {
      //log log "session has been joined " + status;
      Some(sessionIdMutable.value);
    } else {
      //log log "failed joining the session: " + status + " and the session value is: " + sessionIdMutable.value;
      None;
    }
  }

  /**
   * setReplyTimeout may be used. The default reply timeout is 25 seconds
   * https://www.alljoyn.org/docs/api/java/org/alljoyn/bus/ProxyBusObject.html#setReplyTimeout%28int%29
   */
  protected def getProxyInterface(wellKnownName: String,
                                  mainAbsoluteServicePath: String,
                                  sessionId: Int,
                                  busInterfaces: Array[Class[_]])
                                 (implicit busAttachment: BusAttachment) = {
    /*
     * To communicate with an AllJoyn object, we create a ProxyBusObject.
     * A ProxyBusObject is composed of a name, path, sessionID and interfaces.
     *
     * This ProxyBusObject is located at the well-known SERVICE_NAME, under path
     * "/MyService", uses sessionID of MESSAGE_PORT, and implements the MyInterface.
     */
    val proxyObj = busAttachment.getProxyBusObject(
      wellKnownName,
      mainAbsoluteServicePath,
      sessionId, //Note: Valid SessionPort values range from 1 to 0xFFFF.
      // When using getProxyBusObject(), list all of the interfaces you are interested in using
      busInterfaces
    );

    //if you want to get crazy and more dynamic you can get the properties interface
    //properties = proxyObj.getInterface(classOf[Properties]);

    // We make calls to the methods of the AllJoyn object through one of its interfaces
    proxyObj;
  }
}
