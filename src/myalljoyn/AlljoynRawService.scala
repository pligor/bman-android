package myalljoyn

import org.alljoyn.bus.BusAttachment
import AlljoynHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait AlljoynRawService extends AlljoynService {

  def RAW_PORT: Short;

  protected def getRawName(suffix: String): String;

  override def advertiseName(suffix: String, transports: Short)(implicit busAttachment: BusAttachment): Boolean = {
    super.advertiseName(suffix, transports)(busAttachment) && advertiseFullName(getRawName(suffix), transports);
  }

  def bindBothSessions(transports: Short)(onMessageSessionJoined: (SessionJoiner) => Unit,
                       onMessageSessionLost: (Int) => Unit)
                      (onRawSessionJoined: (SessionJoiner) => Unit,
                       onRawSessionLost: (Int) => Unit)
                      (implicit busAttachment: BusAttachment): Boolean = {
    //log log "binding on raw port: " + RAW_PORT;
    bindSession(transports)(onMessageSessionJoined)(onMessageSessionLost) &&
      bindSession(getDefaultSessionOptions(RawCheck.IS_RAW, transports), RAW_PORT, onRawSessionJoined, onRawSessionLost);
  }

  override def request(suffix: String)(implicit busAttachment: BusAttachment): Boolean = {
    super.request(suffix) && requestName(getRawName(suffix));
  }

  override protected def getNames(suffix: String) = Seq(getMessageName(suffix), getRawName(suffix));

  private class UnavailableMethodException extends Exception;
}
