package myalljoyn

import org.alljoyn.bus._
import android.content.Context
import java.io.FileDescriptor
import scala.Some
import scala.Some

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object AlljoynHelper {
  private val busNameSeperator: Char = '.';

  private class IllegalSuffixOfPrefixException extends Exception;

  def getSockedFileDescriptor(sockFd: Int): FileDescriptor = {
    /*
     * We have a socked FD, but now we need a Java file
     * descriptor.  There is no appropriate constructor,
     * public or not in the Dalvik FileDescriptor, so
     * we new up a file descriptor and set its internal
     * descriptor field using reflection.
     */
    val field = classOf[FileDescriptor].getDeclaredField("descriptor");
    field.setAccessible(true);
    val fd = new FileDescriptor;
    field.set(fd, sockFd);
    fd;
  }

  def transportsToString(transports: Short): String = {
    val possibleTransports = Set(
      SessionOpts.TRANSPORT_BLUETOOTH,
      SessionOpts.TRANSPORT_ICE,
      SessionOpts.TRANSPORT_LAN,
      SessionOpts.TRANSPORT_LOCAL,
      SessionOpts.TRANSPORT_WFD,
      SessionOpts.TRANSPORT_WLAN,
      SessionOpts.TRANSPORT_WWAN
    );
    val transportStrings = possibleTransports.
      map(curTransport => transportToString((transports & curTransport).toShort)).
      filter(_.isDefined).map(_.get);

    transportStrings.mkString(", ");
  }

  private def transportToString(transport: Short): Option[String] = {
    transport match {
      case SessionOpts.TRANSPORT_BLUETOOTH => Some("bluetooth");
      case SessionOpts.TRANSPORT_ICE => Some("ice");
      case SessionOpts.TRANSPORT_LAN => Some("lan");
      case SessionOpts.TRANSPORT_LOCAL => Some("local");
      case SessionOpts.TRANSPORT_WFD => Some("wifi direct");
      case SessionOpts.TRANSPORT_WLAN => Some("wlan");
      case SessionOpts.TRANSPORT_WWAN => Some("wwan");
      case _ => None;
    }
  }

  def getRawCheck(namePrefix: String) = {
    val elements = namePrefix.split(busNameSeperator);
    val lastElement = elements.last;
    if (lastElement == AlljoynRaw.RAW_SERVICE_NAME) {
      //assert(lastTwo.last.take(AlljoynRaw.rawPortPrefix.length) == AlljoynRaw.rawPortPrefix);
      RawCheck.IS_RAW;
    } else if (lastElement == AlljoynConfiguration.MESSAGE_SERVICE_NAME) {
      RawCheck.NOT_RAW;
    } else {
      throw new IllegalSuffixOfPrefixException;
    }
  }

  def concatNames(names: String*) = {
    require(names.length > 1);
    names.mkString(busNameSeperator.toString);
  }

  def getSuffix(name: String, namePrefix: String) = {
    name.drop((namePrefix + busNameSeperator).length);
  }

  /**
   * you better check documentation just to be sure that transport any means all except wifi direct
   * https://www.alljoyn.org/docs/api/java/org/alljoyn/bus/SessionOpts.html
   */
  def getAllTransportsExceptWfd = SessionOpts.TRANSPORT_ANY;

  def getAllTransportsExceptWfdAndBluetooth = (
    SessionOpts.TRANSPORT_ICE | //32
      SessionOpts.TRANSPORT_LAN | //16
      SessionOpts.TRANSPORT_LOCAL | //1
      SessionOpts.TRANSPORT_WLAN | //4
      //SessionOpts.TRANSPORT_TCP | //4 supposed to be the same as wlan either way
      SessionOpts.TRANSPORT_WWAN //8
    ).toShort;

  def getAllTransportsExceptWfdBluetoothAndIce = {
    (getAllTransportsExceptWfdAndBluetooth - SessionOpts.TRANSPORT_ICE).toShort;
  }

  def getDefaultSessionOptions(rawCheck: RawCheck.Value, transports: Short): SessionOpts = {
    val sessionOpts = new SessionOpts();

    sessionOpts.traffic = rawCheck match {
      case RawCheck.NOT_RAW => SessionOpts.TRAFFIC_MESSAGES;
      //reliable is for tcp. useful to get everything without error checking
      //unreliable is udp. you must do some error checking yourself. no way! transmission is not that critical right now
      //case RawCheck.IS_RAW => SessionOpts.TRAFFIC_RAW_UNRELIABLE;
      case RawCheck.IS_RAW => SessionOpts.TRAFFIC_RAW_RELIABLE;
    }

    sessionOpts.isMultipoint = false; //I guess this is how you define it to be one-to-one

    sessionOpts.proximity = SessionOpts.PROXIMITY_ANY;

    sessionOpts.transports = transports;

    sessionOpts;
  }

  def findAdvertisedName(wellKnownNamePrefix: String)(implicit busAttachment: BusAttachment): Boolean = {
    val findAdvertisedNameStatus = busAttachment.findAdvertisedName(wellKnownNamePrefix);
    /*if (findAdvertisedNameStatus == Status.OK) {
      log log "find advertised name executed " + findAdvertisedNameStatus;
      log log "searching for service with well known name prefix: " + wellKnownNamePrefix;
    }
    else log log "could not execute finding advertised name with status: " + findAdvertisedNameStatus;*/
    findAdvertisedNameStatus == Status.OK;
  }

  def reFindAdvertisedName(wellKnownNamePrefix: String)(implicit busAttachment: BusAttachment): Boolean = {
    val status = busAttachment.findAdvertisedName(wellKnownNamePrefix);
    //log log "find advertised name executed with status: " + status;
    //log log "tried to search for service with well known name prefix: " + wellKnownNamePrefix;
    status == Status.OK || status == Status.ALLJOYN_FINDADVERTISEDNAME_REPLY_ALREADY_DISCOVERING;
  }

  def generateBusAttachment(name: String, communicationBetweenDevices: Boolean)(implicit context: Context) = {
    //useful both for client and service
    org.alljoyn.bus.alljoyn.DaemonInit.PrepareDaemon(context.getApplicationContext);

    /**
     * All communication through AllJoyn begins with a BusAttachment.
     */
    new BusAttachment(
      /**
       * A BusAttachment needs a name. The actual name is unimportant except for internal
       * security. As a default we use the class name as the name.
       */
      name,

      /**
       * By default AllJoyn does not allow communication between devices (i.e. bus to bus
       * communication). The second argument must be set to Receive to allow communication
       * between devices.
       */
      if (communicationBetweenDevices) {
        BusAttachment.RemoteMessage.Receive
      } else {
        BusAttachment.RemoteMessage.Ignore
      }
    );
  }

  def debugAlljoyn(enabled: Boolean)(implicit busAttachment: BusAttachment) {
    //write debug output to adb logcat
    busAttachment.useOSLogging(enabled);
    if (enabled) {
      //all debugging output from java language bindings
      busAttachment.setDebugLevel("ALLJOYN_JAVA", 7);
    }
  }

  def genBusAttachmentPreRegistered(obj: BusObject, absoluteServicePath: String,
                                    name: String, communicationBetweenDevices: Boolean)(implicit context: Context): Option[BusAttachment] = {
    val busAttachment = generateBusAttachment(name, communicationBetweenDevices);

    //register objects before connecting
    val objectRegistrationStatus = busAttachment.registerBusObject(obj, absoluteServicePath);

    if (objectRegistrationStatus == Status.OK) {
      Some(busAttachment);
    } else {
      busAttachment.unregisterBusObject(obj);
      None;
    }
  }

}
