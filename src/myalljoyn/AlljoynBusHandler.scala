package myalljoyn

import android.os.{Message, Handler}
import myandroid.{log, AndroidHelper}
import AndroidHelper._
import components.{WriteOnce, MyEnum}
import org.alljoyn.bus.BusAttachment

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class AlljoynBusHandler extends Handler(getHandlerLooper) {
  protected var isDisconnecting = false;

  protected val busAttachment = new WriteOnce[BusAttachment];

  /**
   * remember to override with a singleton (object)
   */
  val MyMessage: MessageEnumeration;

  protected def getSessionIds: Seq[Either[WriteOnce[Int], Option[Int]]];

  /**
   * ARE ALL SESSION IDS INITIALIZED ??
   * it would not matter if it was fold left or fold right.
   * note the initial value which must be true
   */
  def isConnected = getSessionIds.foldLeft(true) {
    (curbool, sessionId) =>
      curbool && sessionId.fold(writeOnce => writeOnce.isInitialized, option => option.isDefined);
  }

  protected class MessageEnumeration extends MyEnum {
    def MessageEnum(callback: (Message) => Unit) = {
      val myval = new MessageEnum(callback);
      Val(myval);
      myval;
    }

    class MessageEnum(val callback: (Message) => Unit) extends Val;

    override def apply(x: Int): MessageEnum = super.apply(x).asInstanceOf[MessageEnum];
  }

  override def handleMessage(msg: Message) {
    super.handleMessage(msg);
    val message = MyMessage(msg.what);
    if (!isDisconnecting) {
      message.callback(msg);
    } else {
      //log log "we are disconnecting, therefore we dont accept any other messages";
    }
  }
}
