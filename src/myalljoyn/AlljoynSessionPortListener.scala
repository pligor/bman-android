package myalljoyn

import org.alljoyn.bus.{BusAttachment, SessionOpts, SessionPortListener}
import myandroid.log

case class SessionJoiner(sessionPort: Short, sessionId: Int, joiner: String);

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class AlljoynDefaultSessionPortListener(val contactPort: Short) extends SessionPortListener {
  override def acceptSessionJoiner(sessionPort: Short, joiner: String, opts: SessionOpts): Boolean = {
    super.acceptSessionJoiner(sessionPort, joiner, opts);
    //log log "session is about to be accepted and the joiner is: " + joiner;
    sessionPort == contactPort;
  }
}

class AlljoynSessionPortListener(val onSessionJoined: (SessionJoiner) => Unit,
                                 val onSessionLost: (Int) => Unit,
                                 contactPort: Short)(implicit busAttachment: BusAttachment)
  extends AlljoynDefaultSessionPortListener(contactPort) {
  override def sessionJoined(sessionPort: Short, sessionId: Int, joiner: String) {
    super.sessionJoined(sessionPort, sessionId, joiner);
    //log log "session joined with sessionPort: %s, messageSessionId: %s and joiner %s".format(sessionPort, sessionId, joiner);

    busAttachment.setSessionListener(sessionId, new AlljoynSessionListener(onSessionLost));

    onSessionJoined(SessionJoiner(sessionPort, sessionId, joiner));
  }
}
