package myalljoyn

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * https://www.alljoyn.org/docs-and-downloads/documentation/guide-alljoyn-development-using-java-sdk-rev-j
Interfaces have names with type STRING, meaning that they must be valid UTF-8 characters.
There are also some additional restrictions that apply to interface names, specifically:
    Interface names are composed of one or more elements separated by a period (“.”) character. All elements must contain at least one character.
    Each element must only contain the ASCII characters [A-Z][a-z][0-9]_ and must not begin with a digit.
    Interface names must contain at least one “.” (period) character (and thus at least two elements).
    Interface names must not begin with a “.” (period) character.
    Interface names must not exceed the maximum name length of 255 characters.

 */
object AlljoynNameChecker {
  //not implemented yet
}
