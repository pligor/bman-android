package myalljoyn

import AlljoynHelper._

object RawCheck extends Enumeration {
  val NOT_RAW = Value;
  val IS_RAW = Value;
}

object AlljoynRaw {
  val RAW_SERVICE_NAME = "raw";
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait AlljoynRaw extends AlljoynConfiguration {
  val secure: Boolean = true;

  def RAW_PORT: Short;

  def BUFFER_LEN: Int;

  //we thought that this is necessary but actually isnt
  //private val rawPortHack = AlljoynRaw.rawPortPrefix + RAW_PORT;

  //to advetise name diladi tha moiazei: com.pligor.project . myraw . "port"+contact . private name
  //val WELL_KNOWN_RAW_NAME_PREFIX = concatNames(WELL_KNOWN_MESSAGE_NAME_PREFIX, AlljoynRaw.RAW_SERVICE_NAME /*, rawPortHack*/);
  final lazy val WELL_KNOWN_RAW_NAME_PREFIX = concatNames(wellKnownNamePrefix, AlljoynRaw.RAW_SERVICE_NAME);

  def getFullWellKnownRawName(suffix: String) = concatNames(WELL_KNOWN_RAW_NAME_PREFIX, suffix);
}
