package myalljoyn

import android.content.{ComponentName, ServiceConnection}
import android.os.{Messenger, IBinder}
import components.MyActivity
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
/**
 * If you need your service to communicate with remote processes, then you can use a Messenger
 * to provide the interface for your service. This technique allows you to perform interprocess
 * communication (IPC) without the need to use AIDL.
 */
trait AlljoynRemoteService extends MyActivity {
  //abstract

  protected def onAlljoynServiceConnected(): Unit

  //concrete
  protected var alljoynServiceMessenger: Option[Messenger] = None

  protected def safeUnbindAlljoynService(): Unit = {
    if (alljoynServiceMessenger.isDefined) {
      unbindService(alljoynServiceConnection)

      alljoynServiceMessenger = None
      //because onServiceDisconnected gets executed only on rare situation when the service crashes etc.
    }
  }

  protected val alljoynServiceConnection = new ServiceConnection {
    def onServiceConnected(className: ComponentName, boundService: IBinder): Unit = {
      alljoynServiceMessenger = Some(new Messenger(boundService))

      log log "service connected with name: " + className.toShortString

      onAlljoynServiceConnected()
    }

    def onServiceDisconnected(className: ComponentName): Unit = {
      alljoynServiceMessenger = None

      log log "service disconnected with name: " + className.toShortString
    }
  }
}
