package myalljoyn

import AlljoynHelper._
import org.alljoyn.bus._
import org.alljoyn.bus.Mutable
import java.io.FileOutputStream
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait AlljoynRawClient extends AlljoynClient {
  /*override def doSomething(str: String): String = {
    val hello = super.doSomething(str);
    "prefiX" + hello + "Suffix";
  }*/
  protected def findRawServiceName(implicit busAttachment: BusAttachment): Boolean;

  /*override def connectAndFind(onFoundAdvertisedName: (AdvertisedName) => Unit)
                             (implicit context: Context): Option[BusAttachment] = {
    val busAttachmentOption = super.connectAndFind(onFoundAdvertisedName);
    if (busAttachmentOption.isDefined) {
      implicit val busAttachment = busAttachmentOption.get;
      if (findRawServiceName) {
        Some(busAttachment);
      } else {
        None;
      }
    } else {
      None;
    }
  }*/

  def findBothServices(implicit busAttachment: BusAttachment): Boolean = {
    findServiceName && findRawServiceName;
  }

  protected def getRawSession(rawServiceName: String,
                              rawTransport: Short,
                              sessionRawPort: Short)(onRawSessionLost: (Int) => Unit)
                             (implicit busAttachment: BusAttachment): Option[RawSession] = {
    try {
      /*
       * Now join the raw session.
       * Once this happens, the session is ready for raw traffic that will not be encapsulated in AllJoyn messages.
       * //sessionOptions.transports = rawTransport;
       * //we must use this hack here
       */
      val sessionOptions = getDefaultSessionOptions(RawCheck.IS_RAW, getAllTransportsExceptWfdBluetoothAndIce);

      val sessionIdMutable = new Mutable.IntegerValue;

      /*log log "joining raw session with name: %s | port: %s | and session transports: %s".format(
        rawServiceName, sessionRawPort, sessionOptions.transports
      );*/
      val status = busAttachment.joinSession(
        rawServiceName,
        sessionRawPort,
        sessionIdMutable,
        sessionOptions,
        new AlljoynSessionListener(onRawSessionLost)
      );
      //log log "dont know if succeeded or not but session id is: " + sessionIdMutable.value;

      if (status == Status.OK) {
        //log log "joined raw session " + status;
        val sessionId = sessionIdMutable.value;

        /*
         * The session is in raw mode, but we need to get a
         * socket file descriptor back from AllJoyn that
         * represents the established connection.  We are then
         * free to do whatever we want with the sock.
         */
        val sockFd = new Mutable.IntegerValue;
        val statusFd = busAttachment.getSessionFd(sessionId, sockFd);
        if (statusFd == Status.OK) {
          //log log "session fd is " + statusFd + " and the socket value is: " + sockFd.value;

          val fd = getSockedFileDescriptor(sockFd.value);

          /*
           * Now that we have a FileDescriptor with an AllJoyn
           * raw session socket FD in it, we can use it like
           * any other "normal" FileDescriptor.
           */

          Some(RawSession(
            fileOutputStream = new FileOutputStream(fd),
            sessionId = sessionId
          ));
        } else {
          //log log "could not get session file descriptor";
          None;
        }
      } else {
        //log log "failed to join raw session status: " + status;
        None;
      }
    } catch {
      case e: Throwable => {
        e.printStackTrace();
        //log log "Exception bringing up raw stream: " + e.getMessage;
        None;
      }
    }
  }
}
