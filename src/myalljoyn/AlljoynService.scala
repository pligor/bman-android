package myalljoyn

import org.alljoyn.bus._
import android.content.Context
import myalljoyn.AlljoynHelper._
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait AlljoynService {

  def CONTACT_PORT: Short;

  def genBusAttachmentWithObject(busObject: BusObject)(implicit context: Context): Option[BusAttachment];

  protected def getMessageName(suffix: String): String;

  def advertiseName(suffix: String, transports: Short)(implicit busAttachment: BusAttachment): Boolean = {
    advertiseFullName(getMessageName(suffix), transports);
  }

  protected def advertiseFullName(theWellKnownName: String, transports: Short)(implicit busAttachment: BusAttachment): Boolean = {
    //Important: Although you can advertise different names than your service’s well-known name,
    // it would result in the bus not being able to use your service.
    // It is an error to advertise a different name than your service’s own well-known name.

    //transport here defines another thing! it defines how you could discover the service
    //but this does not mean that the session will be established over the same transport ;)

    log log ("WELL KNOWN NAME: " + theWellKnownName);

    val status = busAttachment.advertiseName(
      theWellKnownName,
      transports
    );

    if (status == Status.OK) {
      //log log "just advertised name: " + theWellKnownName;
    } else {
      //If we are unable to advertise the name, release the well-known name from the local bus
      busAttachment.releaseName(theWellKnownName);
    }
    status == Status.OK;
  }

  def bindSession(transports: Short)(onMessageSessionJoined: (SessionJoiner) => Unit)
                 (onMessageSessionLost: (Int) => Unit)
                 (implicit busAttachment: BusAttachment): Boolean = {
    //log log "binding on contact port: " + CONTACT_PORT;
    bindSession(getDefaultSessionOptions(RawCheck.NOT_RAW, transports), CONTACT_PORT, onMessageSessionJoined, onMessageSessionLost);
  }

  protected def bindSession(sessionOptions: SessionOpts,
                            contactPort: Short,
                            onSessionJoined: (SessionJoiner) => Unit,
                            onSessionLost: (Int) => Unit)
                           (implicit busAttachment: BusAttachment): Boolean = {
    //Bind the session port
    //As part of binding the session port you also need to create a SessionPort Listener that
    // is used to respond to JoinSession requests from the client.
    val sessionPort = new Mutable.ShortValue(contactPort);
    val bindSessionPortStatus = busAttachment.bindSessionPort(
      sessionPort,
      sessionOptions,
      new AlljoynSessionPortListener(onSessionJoined, onSessionLost, contactPort)
    );
    //session port is predefined for both client and service so we dont need to use this variable
    /*if (bindSessionPortStatus == Status.OK) log log "binding port session status is " + bindSessionPortStatus;
    else log log "binding port session has failed: " + bindSessionPortStatus;*/

    bindSessionPortStatus == Status.OK;
  }

  def connect(busObject: BusObject)(implicit context: Context): Option[BusAttachment] = {
    val busAttachmentOption = genBusAttachmentWithObject(busObject);

    if (busAttachmentOption.isDefined) {
      implicit val busAttachment = busAttachmentOption.get;
      debugAlljoyn(enabled = true);
      //The next step in making a service available to other AllJoyn peers is to connect the
      //BusAttachment to the bus with a well-known name.
      val connectToBusStatus = busAttachment.connect();
      if (connectToBusStatus == Status.OK) {
        log log "connection to service bus is " + connectToBusStatus;
        Some(busAttachment);
      } else {
        log log "service bus connection has failed " + connectToBusStatus;
        None;
      }
    } else {
      log log "service object registration has failed.";
      None;
    }
  }

  def request(suffix: String)(implicit busAttachment: BusAttachment): Boolean = {
    requestName(getMessageName(suffix));
  }

  protected def requestName(fullName: String)(implicit busAttachment: BusAttachment): Boolean = {
    //val flags = BusAttachment.ALLJOYN_REQUESTNAME_FLAG_DO_NOT_QUEUE |  BusAttachment.ALLJOYN_REQUESTNAME_FLAG_REPLACE_EXISTING
    val flags = BusAttachment.ALLJOYN_REQUESTNAME_FLAG_DO_NOT_QUEUE;
    val requestNameStatus = busAttachment.requestName(fullName, flags);

    /*if (requestNameStatus == Status.OK) log log "request name status is " + requestNameStatus;
    else log log "request name status has failed " + requestNameStatus;*/
    requestNameStatus == Status.OK;
  }

  protected def getNames(suffix: String) = Seq(getMessageName(suffix));

  def releaseNames(suffix: String)(implicit busAttachment: BusAttachment) {
    getNames(suffix) foreach {
      nameToBeReleased =>
        val status = busAttachment.releaseName(nameToBeReleased);
      //log log "the release name " + nameToBeReleased + " has status: " + status;
    }
  }

  def disconnect(suffix: String, busObject: BusObject)(implicit busAttachment: BusAttachment) {
    releaseNames(suffix);

    //It is important to unregister the BusObject before disconnecting from the bus.
    //Failing to do so could result in a resource leak.
    busAttachment.unregisterBusObject(busObject);

    busAttachment.disconnect();
  }
}
