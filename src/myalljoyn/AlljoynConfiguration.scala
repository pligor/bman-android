package myalljoyn

import android.content.Context
import AlljoynHelper._

object AlljoynConfiguration {
  val MESSAGE_SERVICE_NAME = "message";
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class AlljoynConfiguration {
  val interfaceName: String;

  //for convenience we can use same name as interface name if it uses only one(1) interface
  protected val wellKnownNamePrefix: String;

  final lazy val WELL_KNOWN_MESSAGE_NAME_PREFIX = concatNames(wellKnownNamePrefix, AlljoynConfiguration.MESSAGE_SERVICE_NAME);

  //this depends on how many bus objects has the service registered
  //I mean it could be many objects even if all have the same interface (various implementations)
  //but there will be at least one, so this is the main one
  val mainAbsoluteServicePath: String;

  //Note: valid SessionPort values range from 1 to 0xFFFF.
  val MESSAGE_PORT: Short;

  val communicationBetweenDevices = true;

  def getApplicationName(implicit context: Context) = context.getPackageName;

  def getFullWellKnownName(suffix: String) = concatNames(WELL_KNOWN_MESSAGE_NAME_PREFIX, suffix);

  val DEFAULT_TRANSPORTS: Short;
}
