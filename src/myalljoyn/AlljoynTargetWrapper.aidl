package myalljoyn;
/* The package where the aidl file is located */

//Like all aidl files, this file has to be in the same folder as the class file
//import myalljoyn.AlljoynTargetWrapper;

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

/* Declare our object as a class which implements the Parcelable interface */
parcelable AlljoynTargetWrapper;
