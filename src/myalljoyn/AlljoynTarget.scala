package myalljoyn

import android.os.Parcelable.Creator
import android.os.{Parcelable, Parcel}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object AlljoynTarget {
  final val CREATOR: Creator[AlljoynTarget] = new Creator[AlljoynTarget] {
    def createFromParcel(parcel: Parcel) = AlljoynTarget(
      parcel.readString(),
      parcel.readInt().toShort
    );

    def newArray(size: Int) = new Array[AlljoynTarget](size);
  }
}

case class AlljoynTarget(name: String, transport: Short) extends Parcelable {
  /**
   * @return use CONTENTS_FILE_DESCRIPTOR if you are passing a file descriptor
   */
  def describeContents(): Int = 0;

  def writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(name);
    parcel.writeInt(transport);
  }

  def +(target: AlljoynTarget): AlljoynTarget = {
    require(this.name == target.name);
    new AlljoynTarget(name = this.name, transport = (this.transport | target.transport).toShort);
  }
}
