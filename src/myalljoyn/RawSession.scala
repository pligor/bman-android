package myalljoyn

import java.io.FileOutputStream
import scala.Int

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case class RawSession(fileOutputStream: FileOutputStream, sessionId: Int);
