package myalljoyn

import myalljoyn.AlljoynHelper._
import android.os.Message

object AdvertisedName {
  def apply(name: String, transport: Short, namePrefix: String): AdvertisedName = {
    AdvertisedName(
      transport = transport,
      rawCheck = getRawCheck(namePrefix),
      suffix = getSuffix(name, namePrefix)
    )
  }

  def apply(message: Message): AdvertisedName = {
    AdvertisedName(
      transport = message.arg1.toShort,
      rawCheck = RawCheck(message.arg2),
      suffix = message.obj.asInstanceOf[String]
    );
  }
}

case class AdvertisedName(transport: Short, rawCheck: RawCheck.Value, suffix: String) {
  def toAlljoynTargetWrapper = new AlljoynTargetWrapper(AlljoynTarget(suffix, transport), rawCheck);

  def toMessage(message: Message): Message = {
    message.arg1 = transport;
    message.arg2 = rawCheck.id;
    message.obj = suffix;
    message;
  }
}
