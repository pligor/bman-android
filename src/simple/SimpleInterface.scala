package simple

import org.alljoyn.bus.annotation.{BusProperty, BusMethod, BusInterface}
import org.alljoyn.bus.BusException
import Simple._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
@BusInterface(name = interfaceName)
trait SimpleInterface {
  /*
  @throws(classOf[BusException])
  @BusProperty
  def setMyProperty(myProperty: String);*/

  @throws(classOf[BusException])
  @BusMethod
  def isFileLenAccepted(fileLen: Long): Boolean;

  @throws(classOf[BusException])
  @BusMethod
  def isInputStreamReady(sessionId: Int): Boolean;

  //@BusProperty we made this a bus method because of the incompatibility with iOS
  @throws(classOf[BusException])
  @BusMethod
  def getIsFileTransferedSuccessfully: Boolean;

  //@BusProperty we made this a bus method because of the incompatibility with iOS
  @throws(classOf[BusException])
  @BusMethod
  def setReason(reasonId: Int);

  @throws(classOf[BusException])
  @BusMethod
  def doesReceiverAcceptsCard(senderSuffixName: String): Boolean;

  /**
   * Ask the service to arrange a raw reliable session that can be used to
   * transfer a "file" and return a contact port over which this session
   * can be joined.
   *
   * return an integer session port with which the raw session may be joined
   */
  /*@throws(classOf[BusException])
  @BusMethod
  def requestRawSession: Short;*/
}
