package simple

import org.alljoyn.bus._
import android.content.Context
import myalljoyn.{AlljoynRawService, AlljoynService, AlljoynHelper}
import AlljoynHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object SimpleService extends AlljoynService with AlljoynRawService {
  //if client and service are seperated put it in here, otherwise put it in service companion object
  //final val mainAbsoluteServicePath = "/SimpleObj";

  val CONTACT_PORT: Short = Simple.MESSAGE_PORT;

  val RAW_PORT: Short = Simple.RAW_PORT;

  protected def getRawName(suffix: String): String = Simple.getFullWellKnownRawName(suffix);

  protected def getMessageName(suffix: String): String = Simple.getFullWellKnownName(suffix);

  def genBusAttachmentWithObject(busObject: BusObject)(implicit context: Context): Option[BusAttachment] = {
    genBusAttachmentPreRegistered(
      busObject,
      Simple.mainAbsoluteServicePath,
      Simple.getApplicationName,
      Simple.communicationBetweenDevices
    );
  }
}
