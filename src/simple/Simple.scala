package simple

import myalljoyn.{AlljoynRaw, AlljoynConfiguration}
import com.pligor.bman.Bman
import myalljoyn.AlljoynHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object Simple extends AlljoynConfiguration with AlljoynRaw {
  //final val interfaceName = concatNames(Bman.PACKAGE_NAME, "simple.SimpleInterface");
  final val interfaceName = "com.pligor.bman.simple.SimpleInterface";
  final protected val wellKnownNamePrefix = concatNames(Bman.PACKAGE_NAME, "simple");
  //for convenience we could use same name as interface name since it uses only one(1) interface

  //this depends on how many bus objects has the service registered
  //I mean it could be many objects even if all have the same interface (various implementations)
  //but there will be at least one, so this is the main one
  final val mainAbsoluteServicePath = "/SimpleObj";

  //Note: Valid SessionPort values range from 1 to 0xFFFF.
  final val MESSAGE_PORT: Short = 19;
  final val RAW_PORT: Short = 84;
  require(RAW_PORT != MESSAGE_PORT, "RAW_PORT: " + RAW_PORT + ". " + "MESSAGE_PORT: " + MESSAGE_PORT);

  final val BUFFER_LEN: Int = 1024;

  //this is not set explicitly yet but it is a reference for the alljoyn default value
  final val REPLY_TIMEOUT_msec = 25 * 1000;

  final val DEFAULT_TRANSPORTS: Short = getAllTransportsExceptWfdAndBluetooth;
  //final val DEFAULT_TRANSPORTS: Short = getAllTransportsExceptWfd;
}
