package simple

import android.content.Context
import myalljoyn._
import AlljoynHelper._
import org.alljoyn.bus._
import myalljoyn.AlljoynTarget

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object SimpleClient extends AlljoynClient with AlljoynRawClient {
  /*override def doSomething(str: String): String = {
    val hello = super.doSomething(str);
    "prefiX" + hello + "Suffix";
  }*/

  protected def findRawServiceName(implicit busAttachment: BusAttachment): Boolean = {
    reFindAdvertisedName(Simple.WELL_KNOWN_RAW_NAME_PREFIX);
  }

  protected def findServiceName(implicit busAttachment: BusAttachment): Boolean = {
    reFindAdvertisedName(Simple.WELL_KNOWN_MESSAGE_NAME_PREFIX);
  }

  private val busInterfaces: Array[Class[_]] = Array(classOf[SimpleInterface]);

  def genBusAttachment(implicit context: Context): BusAttachment = {
    generateBusAttachment(Simple.getApplicationName, Simple.communicationBetweenDevices);
  }

  def joinSession(name: String, transport: Short)(onSessionLost: (Int) => Unit)(implicit busAttachment: BusAttachment): Option[Int] = {
    super.joinSession(name, transport, Simple.MESSAGE_PORT)(onSessionLost);
  }

  def getProxyInterface(suffix: String, sessionId: Int)(implicit busAttachment: BusAttachment): SimpleInterface = {
    super.getProxyInterface(
      Simple.getFullWellKnownName(suffix),
      Simple.mainAbsoluteServicePath,
      sessionId,
      busInterfaces).getInterface(classOf[SimpleInterface]);
  }

  /**
   * REMEMBER: we do NOT use the transport found.
   * We rather ask it to connect to any available transport except wifi direct, bluetooth and ice
   * Mainly the fact is that ICE and RAW do NOT go together
   */
  def getRawSession(rawTarget: AlljoynTarget)(onRawSessionLost: (Int) => Unit)
                   (implicit busAttachment: BusAttachment): Option[RawSession] = {
    super.getRawSession(
      rawServiceName = Simple.getFullWellKnownRawName(rawTarget.name),
      rawTransport = rawTarget.transport,
      sessionRawPort = Simple.RAW_PORT
    )(onRawSessionLost);
  }
}
