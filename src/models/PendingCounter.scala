package models

import spray.json.DefaultJsonProtocol._

object PendingCounter {
  implicit val itsJsonFormat = jsonFormat[Long, Long, PendingCounter](
    PendingCounter.apply, "emails", "cellphones"
  );
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case class PendingCounter(emails: Long, cellphones: Long) {
  val emailsAndCellphones = emails + cellphones;
}
