package models

import spray.json.DefaultJsonProtocol._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

object TokenHashPair {

  implicit val itsJsonFormat = jsonFormat[String, String, TokenHashPair](
    TokenHashPair.apply, "token", "hash"
  );

  /*implicit object jsonFormat extends RootJsonFormat[TokenHashPair] {
    def write(model: TokenHashPair): JsValue = JsObject(
      "token" -> JsString(model.token),
      "hash" -> JsString(model.hash)
    );

    def read(json: JsValue): TokenHashPair = json match {
      case obj: JsObject => {
        obj.getFields("token", "hash") match {
          case Seq(JsString(token), JsString(hash)) => TokenHashPair(token, hash);
          case _ => throw new DeserializationException("single token json object expected");
        }
      }
      case _ => throw new DeserializationException("single token json object expected");
    }
  }*/

}

case class TokenHashPair(token: String, hash: String);
