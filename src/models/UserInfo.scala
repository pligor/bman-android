package models

import android.content.Context
import myopenpgp.OpenPGPKeyGenerator._
import io.Codec
import preferences.CurrentPgpKeyIdPreference
import myAndroidSqlite.MySQLiteOpenHelper

case class PgpIdentity(name: String, email: String) {
  override def toString = {
    //George Pligor <george@pligor.com>
    name + " <" + email + ">";
  }

  def saveNewKeyPair(implicit context: Context): Option[Long] = {
    val exported = exportKeyPairToByteArrays(
      genKeyPairFromJavaSecurity,
      this
    )

    val pgpPublic = new PgpPublic(
      publicKey = new String(exported.publicKey, Codec.UTF8.charSet),
      name = name,
      email = email
    )

    val pgpKeyId = pgpPublic.insert

    if (pgpKeyId != MySQLiteOpenHelper.invalidInsertId) {
      val pgpSecret = new PgpSecret(
        secretKey = new String(exported.secretKey, Codec.UTF8.charSet),
        passPhrase = None,
        pgppublicId = Some(pgpKeyId)
      )

      if (pgpSecret.insert != MySQLiteOpenHelper.invalidInsertId) {
        Some(pgpKeyId) //since they have the same id (remember pgpsecret is a child table of pgppublic)
      } else {
        //error
        pgpPublic.delete //erase the row of pgppublic that was just created

        None
      }
    } else {
      None
    }
  }
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object UserInfo {
  def isEmpty(implicit context: Context) = PgpSecret.isEmpty

  def getName(implicit context: Context): Option[String] = {
    //the parent row of pgp secret MUST exist, otherwise the db restrictions are not met
    PgpPublic.getById(CurrentPgpKeyIdPreference.getValue).map(_.name)
  }

  def getEmail(implicit context: Context): Option[String] = {
    PgpSecret.getById(CurrentPgpKeyIdPreference.getValue).map {
      pgpSecret =>
        val pgpPublicOption = PgpPublic.getById(pgpSecret.id.get)

        pgpPublicOption.get.email
    }
  }
}
