package models

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

import spray.json.DefaultJsonProtocol._

object CardInfo {
  val itsJsonFormat = jsonFormat[String, Long, CardInfo](
    CardInfo.apply, "senderName", "fileSize"
  );
}

case class CardInfo(senderName: String, fileSize: Long);
