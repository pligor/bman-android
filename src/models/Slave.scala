package models

import components.{DelayTask, ByteConv, WriteOnce}
import java.io.File
import android.content.{DialogInterface, Context}
import android.app.Dialog
import com.pligor.bman.{TR, TypedViewHolder, Bman, R}
import android.content.DialogInterface.OnCancelListener
import bluetooth.BufferLenSetter
import android.view.{View, Window}
import android.view.View.OnClickListener
import myandroid.log
import generic.FileReceiver

object Slave {
  /**
   * @param timeoutSecs optional, when equals to zero means that the dialog does not expire
   */
  def genConfirmDialog(deviceName: String,
                       acceptReceive: => Unit,
                       denyReceive: => Unit,
                       timeoutSecs: Int = 0)(implicit context: Context): Dialog = {
    val dialog = new Dialog(context) with TypedViewHolder

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background)

    dialog.setCanceledOnTouchOutside(false)

    dialog.setCancelable(true)

    dialog.setContentView(R.layout.dialog_two_buttons)

    {
      val singleTextView = dialog.findView(TR.singleTextView)
      singleTextView.setText(context.getResources.getString(R.string.transferring_confirm_dialog_prompt, deviceName))
    }

    {
      val singleButtonLeft = dialog.findView(TR.singleButtonLeft)

      singleButtonLeft.setText(R.string.transferring_confirm_dialog_negative_button)

      singleButtonLeft.setOnClickListener(new OnClickListener {
        def onClick(view: View): Unit = {
          denyReceive

          dialog.dismiss()
        }
      })
    }

    {
      val singleButtonRight = dialog.findView(TR.singleButtonRight)

      singleButtonRight.setText(R.string.transferring_confirm_dialog_positive_button)

      singleButtonRight.setOnClickListener(new OnClickListener {
        def onClick(view: View): Unit = {
          acceptReceive

          dialog.dismiss()
        }
      })
    }

    dialog.setOnCancelListener(new OnCancelListener {
      def onCancel(dialog: DialogInterface): Unit = {
        denyReceive
      }
    })

    if (timeoutSecs > 0) {
      new DelayTask {
        protected def onAfterDelay(): Unit = {
          if (dialog.isShowing) {
            dialog.cancel()
          }
        }
      }.execute(timeoutSecs * 1000)
    }

    dialog
  }
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class Slave(private val bufferLenSetter: BufferLenSetter) extends CardTransaction {

  /**
   * note this is a second level of protection just to remember how many bytes we have received
   */
  private var receiveBuffer: Option[ReceiveBuffer] = None

  private val fileReceiver = new WriteOnce[FileReceiver]

  def getPercentageReceived = fileReceiver().getPercentageReceived

  /*private val frontPath = new WriteOnce[String];
  private val backPath = new WriteOnce[String];*/
  private val filePath = new WriteOnce[String]

  /*def hasNoSides = (!frontPath.isInitialized) && (!backPath.isInitialized);
  def hasOneSide = frontPath.isInitialized && (!backPath.isInitialized);
  def hasTwoSides = frontPath.isInitialized && backPath.isInitialized;*/

  /**
   * return None if file could not be created
   */
  def toPeopleCardOption(implicit context: Context): Option[PeopleCard] = {
    PeopleCard.create(new File(filePath()))
  }

  /**
   * called if everything completes successfully
   */
  def end(): Unit = {
    //do nothing
    receiveBuffer = None
  }

  /**
   * called to finish everything abruptly
   */
  def cancel(): Unit = {
    if (fileReceiver.isInitialized) {
      fileReceiver().cancel()
    }

    safelyDeleteFileAtPath(filePath)

    receiveBuffer = None
  }

  private def resetReceiveBuffer(): Int = {
    receiveBuffer = Some(receiveBuffer.get.getEmptyClone)

    receiveBuffer.get.expectedLength
  }

  private def safelyDeleteFileAtPath(path: WriteOnce[String]): Unit = {
    if (path.isInitialized) {
      val file = new File(path())

      if (file.exists()) {
        val deleted = file.delete()

        assert(deleted)
      }
    }
  }

  def receiveHashConfirmation(bytes: Array[Byte]): HashStatus = {
    val isBufferReceivedCorrectly = ByteConv.arrayByte2boolean(bytes)

    if (isBufferReceivedCorrectly) {
      log log "buffer was correct.. so I write buffer to the file"

      fileReceiver().write(receiveBuffer.get.getBytes)

      receiveBuffer = None

      HashStatus(isBufferReceivedCorrectly = true)
    } else {
      log log "I prepare buffer to receive data with the length which is already known"

      HashStatus(isBufferReceivedCorrectly = false, bufferLen = Some(resetReceiveBuffer()))
    }
  }

  def receiveBufferData(bytes: Array[Byte]): Array[Byte] = {
    receiveBuffer.get.appendBytes(bytes)

    assert(receiveBuffer.get.isFull,
      "This is NO LONGER executed multiple times, I must have received the buffer completely")

    receiveBuffer.get.getHash
  }

  def receiveBufferLen(bytes: Array[Byte]): ReceiveStatus = {
    val bufferLenOrMinusOne = ByteConv.arrayByte2int(bytes)

    val isReceiveComplete: Boolean = {
      bufferLenOrMinusOne == CardTransaction.INVALID_BUFFER_BYTE_LENGTH
    }

    val isReceiveSuccessful: Boolean = if (isReceiveComplete) {
      log log "receiving the bytes of the file is finished. receive completed"

      val receivedSuccessfully = fileReceiver().isFileReceivedSuccessfully

      if (receivedSuccessfully) {
        fileReceiver().end()

        filePath.setValue(fileReceiver().fileRandomTemporaryPath)
      } else {
        fileReceiver().cancel()
      }

      receivedSuccessfully
    } else {
      //prepare buffer to receive data
      assert(!receiveBuffer.isDefined,
        "must have ensured that previous buffer is removed")

      receiveBuffer = Some(new ReceiveBuffer(bufferLenOrMinusOne))

      bufferLenSetter.setCurrentBufferBytesLen(bufferLenOrMinusOne)

      false
    }

    ReceiveStatus(isComplete = isReceiveComplete, isSuccessful = isReceiveSuccessful)
  }


  def receiveFileLen(bytes: Array[Byte]): Boolean = {
    val len = ByteConv.arrayByte2long(bytes)

    val isLenOk = Card.isJsonFileLenOk(len)

    if (isLenOk) {
      log log ("I just received the len of the file (" + len + " bytes) and seems ok")

      fileReceiver setValue new FileReceiver(len, fileExt = Some(Bman.FILE_EXT))
    } else {
      log log ("I received the len of the file (" + len + " bytes) and is INVALID")
    }

    isLenOk
  }

}

case class ReceiveStatus(isComplete: Boolean, isSuccessful: Boolean) {
  require(!isComplete && !isSuccessful || isComplete)
}

case class HashStatus(isBufferReceivedCorrectly: Boolean, bufferLen: Option[Int] = None) {
  require((isBufferReceivedCorrectly && !bufferLen.isDefined) ||
    (!isBufferReceivedCorrectly && bufferLen.isDefined))
}
