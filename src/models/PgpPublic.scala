package models

import android.content.{Context, ContentValues}
import components.EmailHelper
import EmailHelper._
import android.database.Cursor
import myAndroidSqlite.{Autocomplete, MyModelObject, MyModel}

object PgpPublic extends MyModelObject[PgpPublic] with Autocomplete {
  val TABLE_NAME = "pgppublic";

  case object cols extends Enumeration {
    val id = Value;
    val publicKey = Value;
    val name = Value;
    val email = Value;
  }

  def createFromCursor(cursor: Cursor): PgpPublic = new PgpPublic(
    publicKey = cursor.getString(cursor.getColumnIndexOrThrow(cols.publicKey.toString)),
    name = cursor.getString(cursor.getColumnIndexOrThrow(cols.name.toString)),
    email = cursor.getString(cursor.getColumnIndexOrThrow(cols.email.toString)),
    id = Some(cursor.getLong(cursor.getColumnIndexOrThrow(cols.id.toString)))
  );

  /**
   * TESTED
   */
  def findByEmail(email: String)(implicit context: Context): Option[PgpPublic] = {
    (new DatabaseHandler).getModelByQuery(
      """SELECT *
        |FROM pgppublic
        |WHERE email = ?""".stripMargin, Array[String](email))(createFromCursor);
  }
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class PgpPublic(val publicKey: String, val name: String, val email: String, id: Option[Long] = None) extends MyModel(id) {
  require(isValidEmailAddress(email));

  val TABLE_NAME = PgpPublic.TABLE_NAME;

  protected def generateContentValues: ContentValues = {
    val contentValues = new ContentValues();
    contentValues.put(PgpPublic.cols.publicKey.toString, publicKey);
    contentValues.put(PgpPublic.cols.name.toString, name);
    contentValues.put(PgpPublic.cols.email.toString, email);
    contentValues;
  }

  val ID_COLUMN_NAME: String = PgpPublic.ID_COLUMN_NAME;

  def saveNewName(newName: String)(implicit context: Context): Option[PgpPublic] = {
    require(!isNew)

    val model = new PgpPublic(publicKey = publicKey, name = newName, email = email, id = id)

    if (model.update) {
      Some(model)
    } else {
      None
    }
  }
}
