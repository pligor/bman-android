package models

import android.bluetooth.BluetoothDevice

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class BluetoothNameListItem(val device: BluetoothDevice, val rssi: Short, val verified: Boolean = false)
  extends Ordered[BluetoothNameListItem] {

  require(rssi < 0, "all rssi values are db and therefore are negative. current rssi: " + rssi);

  override def equals(that: Any): Boolean = {
    that match {
      case x: BluetoothNameListItem => this.device == x.device;
      case _ => throw new ClassCastException;
    }
  }

  /**
   * REVERSED ORDERING because we want from Greater to Lower
   * not the best way to do it but whatver
   */
  def compare(that: BluetoothNameListItem): Int = -(this.rssi - that.rssi);

  def verify = new BluetoothNameListItem(device, rssi, verified = true);
}
