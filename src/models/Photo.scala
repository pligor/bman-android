package models

import android.content.{Context, ContentValues}
import android.database.Cursor
import preferences.CurrentPgpKeyIdPreference
import android.graphics.Bitmap
import generic.FileHelper._
import myandroid._
import myandroid.BitmapHelper._
import com.pligor.bman.{Bman, R}
import java.io._
import android.graphics.Bitmap.CompressFormat
import android.net.Uri
import components._
import myopenpgp.OpenPGPEncrypter._
import myopenpgp.OpenPGPDecrypter._
import scala.io.Codec
import com.google.gson.stream.{JsonReader, JsonWriter}
import android.util.Base64
import myAndroidSqlite.{Autocomplete, MyModelObject, MyModel, MySQLiteOpenHelper}
import asyncbitmap.BitmapSource
import myandroid.AndroidHelper._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */

/**
 * We consider that all photos are located inside a single folder
 */
object Photo extends MyModelObject[Photo] with Autocomplete {
  val BASE64_FLAGS = Base64.NO_WRAP

  val JSON_BUFFER_SIZE = 1 << 14

  assert(JSON_BUFFER_SIZE == 16384)

  val TABLE_NAME = "photo"

  case object cols extends Enumeration {
    val filename = Value

    val pgpKeyId = Value
  }

  val PHOTO_DIRECTORY = Bman.FULLPATH_DIRECTORY + File.separator + "cards"

  /**
   * ranges from 1 to 100
   */
  val defaultImageCompressionQuality = 85
  //val compressFormat = CompressFormat.PNG
  val compressFormat = CompressFormat.JPEG

  val PHOTO_FILE_EXT: String = compressFormat.toString.toLowerCase

  val FILENAME_EXT: String = "asc"

  val FILENAME_PREFIX = "card"

  val sideMaxSize = Pixel(1280)

  //this is by testing a fully noisy picture of 1280x1280 size and saving at jpeg of 100 quality
  //with no color subsampling. So I guess we are in the safe zone!
  //1/3 of the corresponding bitmap (32bit => 32/8 = 4bytes) size
  val maxFileSize = math.round(math.pow(sideMaxSize.value, 2).toFloat * 4.toFloat / 3.toFloat)

  //the actual max allowed size is much larger!!
  //http://stackoverflow.com/questions/2734678/jpeg-calculating-max-size

  def genUnencryptedTempFileFromJson(implicit jsonReader: JsonReader): File = {
    jsonReader.beginArray()

    val tempFile = genRandomTemporaryFile(ext = Some(PHOTO_FILE_EXT))

    val decodedOutStream = new BufferedOutputStream(
      new FileOutputStream(tempFile),
      JSON_BUFFER_SIZE
    )

    try {
      while (jsonReader.hasNext) {
        decodedOutStream.write(
          Base64.decode(jsonReader.nextString, BASE64_FLAGS)
        )
      }
    } finally {
      decodedOutStream.close()
    }

    jsonReader.endArray()

    tempFile
  }

  /**
   * used from slave class in order to create the received photo files
   */
  def create(file: File)(implicit context: Context): Option[Photo] = {
    createFunc {
      photo =>
      //old solution: move input file to the new photo file
      //FileUtils.moveFile(file, photo.file);

      //encrypt file before moving it to folder. then delete temporary file
        photo.encryptTempFile(Left(file))

        true //we could check the encryptTempFile if deletion of temp redundant file is a success but is not critical
    }
  }

  /**
   * This is not used because we DO NOT create photos directly from bitmaps
   */
  /*def create(bitmap: Bitmap)(implicit context: Context): Option[Photo] = {
    createFunc(_.setBitmap(bitmap))
  }*/

  /**
   * We create photos directly from uris when we use the camera or pick them from gallery
   */
  def create(uri: Uri)(implicit context: Context): Option[Photo] = {
    //log log "the uri is: " + uri.toString

    createFunc(_.setUri(uri))
  }

  def create(inputStream: InputStream)(implicit context: Context): Option[Photo] = {
    createFunc {
      photo =>
        photo.encryptTempFile(Right(inputStream))

        true
    }
  }

  private def createFunc(op: (Photo) => Boolean)(implicit context: Context): Option[Photo] = {
    assert(!CurrentPgpKeyIdPreference.isDefault,
      "default pgp key id means no key pair is selected. you must have a key in order to encode")

    //in all three occasions above we have as the secret key the current pgp key

    val pgpKeyId = CurrentPgpKeyIdPreference.getValue

    /*//recursive is not very good in android because of easy stack overflow exceptions
    def createNewRecursive(index: Long)(implicit context: Context): Photo = {
      try {
        new Photo(genNewFilename(index), pgpKeyId);
      } catch {
        case e: IllegalArgumentException => {
          createNewRecursive(index + 1);
        }
      }
    }*/

    def createNewImperative(index: Long)(implicit context: Context): Photo = {
      var photo: Photo = null

      var curIndex = index

      while ( {
        photo = new Photo(genNewFilename(curIndex), pgpKeyId)

        //photo is new and file must not be existant. if is existant we repeat
        val isFileExistant = photo.isFileExistant

        if (isFileExistant) {
          curIndex += 1
        }

        isFileExistant
      }) {}

      photo
    }

    val newPhoto = createNewImperative(genNewIndex)

    if (op(newPhoto)) {
      //remember that id of photo table is auto increment, therefore insert should return the autogenerated id
      val newPhotoId = newPhoto.insert

      getById(newPhotoId)
    } else {
      None
    }
  }

  private def genNewFilename(index: Long)(implicit context: Context): String = {
    FILENAME_PREFIX + index + "." + FILENAME_EXT
  }

  private def genNewIndex(implicit context: Context): Long = {
    (new DatabaseHandler).getNextAutoIncrementOption(TABLE_NAME).getOrElse(
      MySQLiteOpenHelper.invalidAutoIncrementId
    )
  }

  //def createFromCursor[A](cursor: Cursor): Photo = new Photo(cursor.getString(1), Some(cursor.getInt(0)));
  //def createFromCursor[A <: MyModel](cursor: Cursor): Photo = new Photo(cursor.getString(1), Some(cursor.getInt(0)));
  def createFromCursor(cursor: Cursor) = new Photo(
    filename = cursor.getString(cursor.getColumnIndexOrThrow(cols.filename.toString)),
    pgpKeyId = cursor.getLong(cursor.getColumnIndexOrThrow(cols.pgpKeyId.toString)),
    Some(cursor.getLong(cursor.getColumnIndexOrThrow(ID_COLUMN_NAME)))
  )

  def deleteOnlyFromDB(modelId: Long)(implicit context: Context): Boolean = {
    (new DatabaseHandler).deleteById(TABLE_NAME, modelId, ID_COLUMN_NAME)
  }
}

class Photo(private val filename: String, private val pgpKeyId: Long, id: Option[Long] = None)
  extends MyModel(id) with BitmapSource[Long, (ImageSizeDp, Context)] {
  private val fullpath: String = if (filename.isEmpty) {
    filename
  } else {
    Photo.PHOTO_DIRECTORY + File.separator + filename
  }

  val file = new File(fullpath)

  def isFileExistant = file.exists() && file.isFile

  //whether the file is existant or not is not up to the source code of the developer, maybe the file was erased for some reason
  /*require((isNew && (!isFileExistant)) || (!isNew && isFileExistant),
    "the photo is " + (if (isNew) "new" else "old") + " and the file (" + file.getCanonicalPath + ") " +
      (if (isFileExistant) "exists" else "does not exist"));*/

  private val arr2str_str2arr_charset = Codec.UTF8.charSet

  def writeFileInJsonArray(implicit jsonWriter: JsonWriter, context: Context): Unit = {
    jsonWriter.beginArray()

    workWithDecryptedFile {
      decryptedFile =>
        val fis = new FileInputStream(decryptedFile)
        try {
          /*useEachBase64Buffer(fis, Photo.JSON_BUFFER_SIZE, doEncode = true) {
            buffer: Array[Byte] =>
              jsonWriter.value(new String(buffer, arr2str_str2arr_charset));
          }*/

          //we read file input stream with the buffer size as we write it
          useEachBuffer(fis, Photo.JSON_BUFFER_SIZE) {
            buffer: Array[Byte] =>
              jsonWriter.value(
                Base64.encodeToString(buffer, Photo.BASE64_FLAGS)
              )
          }
        } finally {
          fis.close()
        }
    }

    jsonWriter.endArray()
  }

  /**
   * I always downscale because I want to keep all cards approximately same size
   */
  private def writeFile[T](value: T, downscalor: (T, ImageSizePx) => Option[Bitmap])(implicit context: Context): Boolean = {
    //scale only if larger
    val size = Photo.sideMaxSize

    val scaledBitmapOption = downscalor(value, ImageSizePx(size, size))

    val scaleSucceeded = scaledBitmapOption.isDefined

    if (scaleSucceeded) {
      //write image in a temporary file in the temporary folder
      val tempFile = filePutBitmapToTemp(
        bitmap = scaledBitmapOption.get,
        Photo.PHOTO_FILE_EXT
      )(
        Photo.compressFormat,
        Some(Photo.defaultImageCompressionQuality)
      )

      //delete original encrypted file (we dont care if exists or not)
      fileDelete(fullpath)

      //encrypt the temporary file and save it in the bman folder
      encryptTempFile(Left(tempFile))
    } else {
      log log "some error prohibited of setting a new bitmap"
    }

    scaleSucceeded
  }

  /**
   * encrypt the temporary file and save it in the bman folder
   */
  def encryptTempFile(input: Either[File, InputStream])(implicit context: Context): Unit = {
    val pgpPublic = PgpPublic.getById(pgpKeyId)

    assert(pgpPublic.isDefined)

    assert(!onUIthread, "this is a heavy task and should not be run on ui")

    val publicKeyBytes = pgpPublic.get.publicKey.getBytes(arr2str_str2arr_charset)

    val decryptedFilename = filename.dropRight(("." + Photo.FILENAME_EXT).length) + "." + Photo.PHOTO_FILE_EXT

    input.fold({
      tempFile =>
        encryptFile(
          inputFile = tempFile,
          publicKeyBytes = publicKeyBytes,
          encryptedFile = file,
          decryptedFilename = decryptedFilename
        )
    }, {
      inputStream =>
        encryptInputStream(
          inputStream = inputStream,
          publicKeyBytes = publicKeyBytes,
          encryptedFile = file,
          decryptedFilename = decryptedFilename
        )
    })

    if (input.isLeft) {
      //delete the temporary file in order not to leave any traces
      input.left.get.delete()
    }
  }

  /**
   * BETTER ALWAYS WORK WITH URIs WITH BITMAPs INSTEAD OF SAVING INTO MEMORY
   */
  /*def setBitmap(bitmap: Bitmap)(implicit context: Context): Boolean = {
    writeFile[Bitmap](bitmap, downscaleBitmap)
  }*/

  def setUri(uri: Uri)(implicit context: Context): Boolean = {
    //log log "the uri is: " + uri.toString

    writeFile[Uri](uri, downscaleUri)
  }


  val getBitmapFunc = (tuple: (ImageSizeDp, Context)) => {
    val (targetSize, context) = tuple

    getBitmap(targetSize)(context)
  }

  protected def getUniqueId: Long = {
    require(isOld, "currently we use this only with old models")

    id.get
  }

  def getBitmap(targetSize: ImageSizeDp)(implicit context: Context): /*Option[*/ Bitmap /*]*/ = {
    if (isFileExistant) {
      val optimallySampledBitmapOption = workWithDecryptedFile {
        decryptedFile =>
        //log log "decode temporary file " + decryptedFile.getCanonicalPath + " into a bitmap in memory";

          decodeOptimalSampledBitmapFromFile(
            decryptedFile.getCanonicalPath,
            targetSize
          )
      }

      optimallySampledBitmapOption
    }
    else {
      decodeOptimalSampledBitmapFromResource(
        R.drawable.dummy_not_found_card,
        //CardView.defaultImageSizeDp
        targetSize
      )
    }
  }

  override def delete(implicit context: Context): Boolean = {
    val filenameBeforeDeletion = fullpath

    //delete from database
    val isDeletedFromDB = super.delete(context)
    //assert(isDeletedFromDB);
    if (isDeletedFromDB) {
      //delete file don't care if does not succeed
      fileDelete(filenameBeforeDeletion)
    }
    else {
      false
    }
  }

  val TABLE_NAME = Photo.TABLE_NAME

  protected def generateContentValues: ContentValues = {
    val contentValues = new ContentValues()

    contentValues.put(Photo.cols.filename.toString, filename)

    contentValues.put(Photo.cols.pgpKeyId.toString, new java.lang.Long(pgpKeyId))

    contentValues
  }

  val ID_COLUMN_NAME: String = Photo.ID_COLUMN_NAME

  private def workWithDecryptedFile[T](op: (File) => T)(implicit context: Context): /*Option[*/ T /*]*/ = {
    //log log "decrypt encrypted file " + fullpath + " in a temporary file"

    val pgpSecret = PgpSecret.getById(pgpKeyId)

    assert(pgpSecret.isDefined,
      "pgp does not exist. the model " + (if (isNew) "is new" else "is not new") + ". if the photo model does not exist" + "\n" +
        "then something weird must have happened in the database between creation of this model and calling this" + "\n" +
        "function because restrictions (see in require executions) would not have met." + "\n" +
        "But if the photo model does really exist then something wrong in the database configuration because of" + "\n" +
        "ON DELETE CASCADE the model should not have been there")

    val secretKeyBytes = pgpSecret.get.secretKey.getBytes(arr2str_str2arr_charset)

    val decryptedFile = decryptFile(
      file,
      Right(secretKeyBytes),
      getTempDir,
      randomFilename = true
    )

    val result = op(decryptedFile)

    if (!Option(result).isDefined) {
      log log "THE RESULT IS CALCULATED BUT NOT DEFINED!!!"
    }

    val deleted = decryptedFile.delete()

    //log log "deleted temporary file in order not to leave traces has " + (if (deleted) "succeeded" else "failed")

    result
  }
}
