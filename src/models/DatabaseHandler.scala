package models

import android.content.{ContentValues, Context}
import myAndroidSqlite.{MySQLiteOpenHelper, MySQLiteOpenHelperSecure}
import android.database.Cursor
import com.pligor.bman.Bman
import net.sqlcipher.database.SQLiteDatabase

//import android.database.sqlite.SQLiteDatabase

object DatabaseHandler {
  def safeCloseCursor(cursorOption: Option[Cursor]): Unit = {
    cursorOption.map(c => if (!c.isClosed) {
      c.close()
    })
  }

  def safeCloseDb(dbOption: Option[SQLiteDatabase]): Unit = {
    dbOption.map(d => if (d.isOpen) {
      d.close()
    })
  }

  def getInstance(implicit context: Context) = {
    //if you do NOT want a secure encrypted database also comment this line
    MySQLiteOpenHelper.loadSqlcipherLibs

    new DatabaseHandler()
  }
}

/**
 * We create this object for the specific database, NOT for each table
 */
class DatabaseHandler(implicit context: Context) extends MySQLiteOpenHelperSecure(
  Bman.FULLPATH_DIRECTORY,
  DatabaseConfig.DATABASE_NAME,
  DatabaseConfig.DATABASE_VERSION,
  dbPass = DatabaseConfig.DB_PASS,
  creator = TableCreator.apply
) {
  /**
   * make sure you have created PGP identity before calling it
   */
  def populate(implicit context: Context): Unit = {
    TablePopulator(insert)
  }

  protected def populator(insert: (String, ContentValues) => Long)(implicit context: Context): Unit = {
    //TestingTablePopulator(insert);
  }

  /**
   * http://blog.adamsbros.org/2012/02/28/upgrade-android-sqlite-database/
   */
  protected def onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int): Unit = {
    //drop older tables
    //create some tables again, etc.
    //do nothing for the time being
  }
}
