package models

import components._
import android.content.{ContentValues, Context}
import android.database.Cursor
import android.graphics.Bitmap
import com.pligor.bman.R
import java.io.File
import java.util.Date
import myAndroidSqlite.{Autocomplete, MyModelObject, OrderNumberObject, MySQLiteOpenHelper}
import android.net.Uri
import myandroid.{BitmapHelper, log}

object MyCard extends MyModelObject[MyCard] with Autocomplete with OrderNumberObject {
  private val CHILD_ID_COLUMN_NAME = "cardId"

  val ORDERING_COLUMN: String = cols.orderNumber.toString

  object cols extends Enumeration {
    val cardId = Value

    val orderNumber = Value
  }

  val TABLE_NAME = "mycard"

  //Log.i("pl", "Please note it applies only the joined table are: card, mycard (mycard, card gives different order");
  val TWO_TABLES = Card.TABLE_NAME + MySQLiteOpenHelper.seperator + TABLE_NAME

  def createFromContentValues(contentValues: ContentValues): MyCard = {
    //remember getAs.... returns null if the value is missing or cannot be converted

    val orderNumber = contentValues.getAsInteger(MyCard.cols.orderNumber.toString)

    require(Option(orderNumber).isDefined)

    val frontPhotoId = contentValues.getAsLong(Card.cols.frontPhotoId.toString)
    if (frontPhotoId == null) {
      throw new IllegalArgumentException
    }

    val backPhotoId = Option(contentValues.getAsLong(Card.cols.backPhotoId.toString).toLong)

    val timeCreated = contentValues.getAsLong(Card.cols.timeCreated.toString)

    if (timeCreated == null) {
      throw new IllegalArgumentException
    }

    val isPublic = contentValues.getAsBoolean(Card.cols.isPublic.toString)

    val id = Option(contentValues.getAsLong(MyCard.cols.cardId.toString).toLong)

    new MyCard(orderNumber, frontPhotoId, backPhotoId, timeCreated, isPublic, id)
  }

  def createFromCursor(cursor: Cursor): MyCard = {
    val id = cursor.getLong(cursor.getColumnIndexOrThrow(MySQLiteOpenHelper.defaultIdCol))

    val cardId = cursor.getLong(cursor.getColumnIndexOrThrow(MyCard.cols.cardId.toString))

    assert(cardId == id, "the id of the retrieved card row must match the id of the child row")

    val backPhotoId = {
      val backPhotoIdIndex = cursor.getColumnIndexOrThrow(Card.cols.backPhotoId.toString)
      if (cursor.isNull(backPhotoIdIndex)) {
        None
      }
      else {
        Some(cursor.getLong(backPhotoIdIndex))
      }
    }

    new MyCard(
      cursor.getInt(cursor.getColumnIndexOrThrow(MyCard.cols.orderNumber.toString)),
      cursor.getLong(cursor.getColumnIndexOrThrow(Card.cols.frontPhotoId.toString)),
      backPhotoId,
      cursor.getLong(cursor.getColumnIndexOrThrow(Card.cols.timeCreated.toString)),

      //we have to get integer and then compare with number because sqlite does not have boolean
      cursor.getShort(cursor.getColumnIndexOrThrow(Card.cols.isPublic.toString)) == 1,

      Some(id)
    )
  }

  def getBottom(implicit context: Context): Option[MyCard] = {
    (new DatabaseHandler).getModelByQuery(
      """SELECT *
        |FROM card, mycard
        |WHERE cardId = id
        |AND orderNumber = (
        |  SELECT MIN(orderNumber) as minOrderNumber
        |  FROM mycard
        |)""".stripMargin
    )(createFromCursor)
  }

  def getTop(implicit context: Context): Option[MyCard] = {
    (new DatabaseHandler).getModelByQuery(
      """SELECT *
        |FROM card, mycard
        |WHERE cardId = id
        |AND orderNumber = (
        |  SELECT MAX(orderNumber) AS maxOrderNumber
        |  FROM mycard
        |)""".stripMargin
    )(createFromCursor)
  }

  override def getAll(implicit context: Context): Seq[MyCard] = {
    (new DatabaseHandler).getModelsByQuery(
      """SELECT *
        |FROM  card, mycard
        |WHERE cardId = id""".stripMargin
    )(createFromCursor)
  }

  def getAllIds(ascending: Boolean = false)(implicit context: Context): Seq[Long] = {
    DatabaseHandler.getInstance.getScalars[Long](
      """SELECT cardId
        |FROM mycard
        |ORDER BY orderNumber %s""".stripMargin.format(if (ascending) "ASC" else "DESC")
    )
  }

  override def getById(modelId: Long)(implicit context: Context): Option[MyCard] = {
    (new DatabaseHandler).getModelByQuery(
      """SELECT *
        |FROM  card, mycard
        |WHERE cardId = id
        |AND id = ?""".stripMargin, Array[String](modelId.toString))(createFromCursor)
  }

  def create(frontFile: File, backFile: Option[File])(implicit context: Context): Option[MyCard] = {
    Card.preparePhotos(frontFile, backFile).map {
      prepared =>
        val (frontPhotoId, backPhotoIdOption) = prepared

        create(frontPhotoId, backPhotoIdOption)
    }
  }

  def create(frontPhotoId: Long, backPhotoIdOption: Option[Long])(implicit context: Context): MyCard = {
    val card = new MyCard(
      frontPhotoId,
      backPhotoIdOption
    )

    MyCard.getById(card.insert).get
  }

  def create(uri: Uri)(implicit context: Context): Option[MyCard] = {
    create(Photo.create(uri))
  }

  //BETTER DEAL WITH URIs to avoid out of memory exceptions
  /*def create(bitmap: Bitmap)(implicit context: Context): Option[MyCard] = {
    create(Photo.create(bitmap))
  }*/

  private def create(photoCreator: => Option[Photo])(implicit context: Context): Option[MyCard] = {
    photoCreator.map {
      photo =>
        assert(photo.id.isDefined, "the newly created photo must have an id")

        val newCard = new MyCard(
          photo.id.get,
          None
        )

        MyCard.getById(newCard.insert).get
    }
  }

  /**
   * We take the last card and make it first
   * Not used with the current layout but you could have a button
   * to rotate a stack of cards that you could not see in any other way
   */
  def makeBottomTop(implicit context: Context): Option[MyCard] = {
    val bottomMyCard = getBottom

    if (bottomMyCard.isDefined) {
      if (bottomMyCard.get == getTop.get) {
        log log "case of only one card"

        bottomMyCard
      } else {
        val nextOrderNumber = getNextOrderNumber

        if (nextOrderNumber.isDefined) {
          Some(bottomMyCard.get.setOrderNumber(nextOrderNumber.get).asInstanceOf[MyCard])
        } else {
          None
        }
      }
    } else {
      None
    }
  }
}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class MyCard(val orderNumber: Int,
             frontPhotoId: Long,
             backPhotoId: Option[Long],
             timeCreated: Long,
             isPublic: Boolean,
             id: Option[Long] = None,
             currentDimension: Dimension.Value = Dimension.frontSide)
  extends Card(frontPhotoId, backPhotoId, timeCreated, isPublic, currentDimension, id) {

  def this(newFrontPhotoId: Long,
           newBackPhotoId: Option[Long],
           newTimeCreated: Long,
           newIsPublic: Boolean,
           newId: Option[Long] = None)(implicit context: Context) = this(
    MyCard.getNextOrderNumber.getOrElse(MyCard.ORDER_LOWER_BOUND),
    newFrontPhotoId,
    newBackPhotoId,
    newTimeCreated,
    newIsPublic,
    newId
  )

  def this(newFrontPhotoId: Long,
           newBackPhotoId: Option[Long])(implicit context: Context) = this(
    newFrontPhotoId,
    newBackPhotoId,
    new Date().getTime,
    Card.defaultIsPublicValue
  )


  def setDimension(newDimension: Dimension.Value): Card = new MyCard(
    orderNumber = orderNumber,
    frontPhotoId = frontPhotoId,
    backPhotoId = backPhotoId,
    timeCreated = timeCreated,
    isPublic = isPublic,
    id = id,
    currentDimension = newDimension
  )

  def countMetaValues(implicit context: Context) = new DatabaseHandler().getCount(MyMetaValue.TABLE_NAME, Some(
    (MetaValueCols.cardId.toString, id.get.toString)
  ))

  /*def serializeToContentValues: ContentValues = {
    //get content values from Card
    val contentValues = super.generateContentValues;
    //and fill with more data. putAll adds all values from the passed in ContentValues
    contentValues.putAll(this.generateContentValues);
    contentValues;
  }*/

  def getDummyFrontBitmap(targetSize: ImageSizeDp)(implicit context: Context): Bitmap = {
    BitmapHelper.decodeOptimalSampledBitmapFromResource(
      R.drawable.dummy_mycard_front,
      targetSize
    )
  }

  def getDummyBackBitmap(targetSize: ImageSizeDp)(implicit context: Context) = {
    BitmapHelper.decodeOptimalSampledBitmapFromResource(
      R.drawable.dummy_mycard_back,
      targetSize
    )
  }

  override val TABLE_NAME = MyCard.TABLE_NAME

  def getById(modelId: Long)(implicit context: Context): Option[Card] = MyCard.getById(modelId)

  /**
   * Setting it to null is not currently supported
   */
  def setOrderNumber(newOrderNumber: Int)(implicit context: Context): Card = {
    //update the row of the card model
    val values = generateContentValues

    val key = MyCard.cols.orderNumber.toString

    //just to be sure
    values.remove(key)

    values.put(key, new java.lang.Integer(newOrderNumber))

    val tableName = MyCard.TABLE_NAME

    val idCol = MyCard.cols.cardId.toString

    (new DatabaseHandler).updateById(tableName, id.get, values, idCol)

    val modelOption = getById(id.get)
    assert(modelOption.isDefined, "we just updated the row! the model must still be there")

    modelOption.get
  }

  //remember the convetion. Next card is the lower card, previous card is the higher card
  def getNext(implicit context: Context): Option[MyCard] = {
    (new DatabaseHandler).getModelByQuery(
      """SELECT * FROM
        |card, mycard
        |WHERE cardId = id
        |AND orderNumber < ?
        |ORDER BY orderNumber DESC
        |LIMIT 1""".stripMargin, Array[String](orderNumber.toString))(MyCard.createFromCursor)
  }

  def getPrev(implicit context: Context): Option[MyCard] = {
    (new DatabaseHandler).getModelByQuery(
      """SELECT * FROM
        |card, mycard
        |WHERE cardId = id
        |AND orderNumber > ?
        |ORDER BY orderNumber
        |LIMIT 1""".stripMargin, Array[String](orderNumber.toString))(MyCard.createFromCursor)
  }

  override def insert(implicit context: Context): Long = {
    require(isNew)

    val parentId = (new DatabaseHandler).insert(Card.TABLE_NAME, super.generateContentValues)

    val values = generateContentValues

    values.put(MyCard.cols.cardId.toString, new java.lang.Long(parentId))

    val childId = (new DatabaseHandler).insert(TABLE_NAME, values)

    assert(parentId == childId, "the id of one table must be the same as the id of the other table." +
      "Now we have parentId: " + parentId + ", childId: " + childId)

    childId
  }

  override protected def generateContentValues: ContentValues = {
    val contentValues = new ContentValues()

    contentValues.put(MyCard.cols.orderNumber.toString, new java.lang.Integer(orderNumber))

    contentValues
  }

  val ID_COLUMN_NAME: String = MyCard.ID_COLUMN_NAME

  override def isIdAutoIncrement(implicit context: Context): Boolean = new DatabaseHandler().isAutoIncrement(Card.TABLE_NAME).get

  /**
   * This is defined only for My Card because for People Card we only set this value on creation
   */
  def setIsPublic(willBePublic: Boolean)(implicit context: Context): Option[Card] = {
    require(isOld)

    val contentValues = super.generateContentValues

    val key = Card.cols.isPublic.toString

    contentValues.remove(key)

    contentValues.put(key, willBePublic)

    (new DatabaseHandler).updateById(
      tableName = Card.TABLE_NAME,
      modelId = id.get,
      contentValues = contentValues,
      columnName = ID_COLUMN_NAME
    )

    MyCard.getById(id.get)
  }

  def setFrontUri(uri: Uri)(implicit context: Context): Option[Card] = {
    setFrontImage(
      isImageSet = {
        photo =>
          photo.setUri(uri)
      },
      creator = MyCard.create(uri)
    )
  }

  //BETTER DEAL WITH FILES (URIs) with BITMAPs to avoid out of memory exceptions
  /*def setFrontBitmap(bitmap: Bitmap)(implicit context: Context) = {
    setFrontImage(
      isImageSet = {
        photo =>
          photo.setBitmap(bitmap)
      },
      creator = MyCard.create(bitmap)
    )
  }*/

  private def setFrontImage(isImageSet: Photo => Boolean, creator: => Option[Card])(implicit context: Context): Option[Card] = {
    if (hasFrontSide) {
      //replace
      val photoOption = Photo.getById(frontPhotoId)

      assert(photoOption.isDefined, "the front photo id must be a pointer to a real row in the photo table")

      if (isImageSet(photoOption.get)) {
        Some(this)
      } else {
        None
      }
    } else if (isNew) {
      creator
    } else {
      throw new InvalidSideConfigurationException
    }
  }

  def setBackUri(uri: Uri)(implicit context: Context) = {
    setBackImage(
      isImageSet = {
        photo =>
          photo.setUri(uri)
      },
      cardCreator = MyCard.create(uri),
      photoCreator = Photo.create(uri)
    )
  }

  //BETTER DEAL only WITH FILES (URIs) with BITMAPs to avoid out of memory exceptions
  /*def setBackBitmap(bitmap: Bitmap)(implicit context: Context) = {
    setBackImage(
      isImageSet = {
        photo =>
          photo.setBitmap(bitmap)
      },
      cardCreator = MyCard.create(bitmap),
      photoCreator = Photo.create(bitmap)
    )
  }*/

  /**
   * BE AWARE: use only small bitmaps, large ones will throw out of memory exception
   */
  def setBackImage(isImageSet: Photo => Boolean,
                   cardCreator: => Option[Card],
                   photoCreator: => Option[Photo])(implicit context: Context): Option[Card] = {
    if (hasFrontSide) {
      if (hasBackSide) {
        //update Photo row
        assert(backPhotoId.isDefined,
          "if the photo has back side then the back photo id must be defined")

        val photoOption = Photo.getById(backPhotoId.get)

        assert(photoOption.isDefined,
          "the back photo id must be a pointer to a real row in the photo table")

        val photo = photoOption.get

        if (isImageSet(photo)) {
          Some(this)
        } else {
          None
        }
      }
      else {
        photoCreator.map {
          photo =>
            assert(photo.id.isDefined, "the newly created photo must have an id")

            setBackPhotoId(photo.id.get)
        }
      }
    } else {
      //if it doesn't have a front side then this comes first! we are going to set the front bitmap instead!
      setFrontImage(isImageSet, cardCreator)
    }
  }
}
