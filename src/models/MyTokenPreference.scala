package models

import myAndroidAccount.TokenPreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MyTokenPreference  extends TokenPreference {
  protected def ACCOUNT_TYPE: String = "bman.account.type"

  /**
   * mAuthTokenType is the type of token that I request from the server.
   * I can have the server give me different tokens for read-only or full access to an account,
   * or even for different services within the same account.
   * A good example is the Google account, which provides several auth-token types:
   * “Manage your calendars”, “Manage your tasks”, “View your calendars” and more..
   * On this particular example I don’t do anything different for the various auth-token types.
   */
  protected def DEFAULT_TYPE_AUTH_TOKEN: String = "auth.token"

  protected def dummyUsername: String = "bman"
}
