package models

import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import spray.json.DefaultJsonProtocol._
import spray.json._
import generic.PhoneNumberHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object AccountInfo {
  private val emailAccountKey = "email_account";
  private val smsAccountKey = "sms_account";
  private val passLengthKey = "passLength";

  implicit object itsJsonFormat extends RootJsonFormat[AccountInfo] {
    def write(model: AccountInfo): JsValue = {
      val emailPendingJsonOption = model.emailPendingPair.map {
        pair =>
          pair.toJson(EmailPendingPair.itsJsonFormat)
      }

      val cellphonePendingJsonOption = model.cellphonePendingPair.map {
        pair =>
          pair.toJson(CellphonePendingPair.itsJsonFormat)
      }

      val passLenJson = JsNumber(model.passLength);

      if (emailPendingJsonOption.isDefined && !cellphonePendingJsonOption.isDefined) {
        JsObject(
          emailAccountKey -> emailPendingJsonOption.get,
          passLengthKey -> passLenJson
        );
      } else if (!emailPendingJsonOption.isDefined && cellphonePendingJsonOption.isDefined) {
        JsObject(
          smsAccountKey -> cellphonePendingJsonOption.get,
          passLengthKey -> passLenJson
        );
      } else {
        JsObject(
          emailAccountKey -> emailPendingJsonOption.get,
          smsAccountKey -> cellphonePendingJsonOption.get,
          passLengthKey -> passLenJson
        );
      }
    }

    def read(json: JsValue): AccountInfo = {
      val exception = new DeserializationException("AccountInfo json object expected");
      json match {
        case obj: JsObject => {
          val fields = obj.fields;
          val hasEmailAccount = fields.isDefinedAt(emailAccountKey);
          val hasSmsAccount = fields.isDefinedAt(smsAccountKey);

          (hasEmailAccount, hasSmsAccount) match {
            case (true, true) => {
              obj.getFields(emailAccountKey, smsAccountKey, passLengthKey) match {
                case Seq(x, y, JsNumber(z)) => {
                  AccountInfo(
                    Some(x.convertTo[EmailPendingPair](EmailPendingPair.itsJsonFormat)),
                    Some(y.convertTo[CellphonePendingPair](CellphonePendingPair.itsJsonFormat)),
                    z.toShortExact //throws exception if it is not integer
                  );
                }
                case _ => throw exception;
              }
            }
            case (false, true) => {
              obj.getFields(smsAccountKey, passLengthKey) match {
                case Seq(y, JsNumber(z)) => {
                  AccountInfo(
                    None,
                    Some(y.convertTo[CellphonePendingPair](CellphonePendingPair.itsJsonFormat)),
                    z.toShortExact //throws exception if it is not integer
                  );
                }
                case _ => throw exception;
              }
            }
            case (true, false) => {
              obj.getFields(emailAccountKey, passLengthKey) match {
                case Seq(x, JsNumber(z)) => {
                  AccountInfo(
                    Some(x.convertTo[EmailPendingPair](EmailPendingPair.itsJsonFormat)),
                    None,
                    z.toShortExact //throws exception if it is not integer
                  );
                }
                case _ => throw exception;
              }
            }
            case (false, false) => throw exception;
          }
        }
        case _ => throw exception;
      }
    }
  }

}

case class AccountInfo(emailPendingPair: Option[EmailPendingPair],
                       cellphonePendingPair: Option[CellphonePendingPair],
                       passLength: Short) {
  require(emailPendingPair.isDefined || cellphonePendingPair.isDefined,
    "you must have an email account or a cellphone account, non registered users do not have account information");

  def setPassLength(newPassLen: Short) = AccountInfo(emailPendingPair, cellphonePendingPair, newPassLen);

  def setNewCellphone(cellphone: PhoneNumber, pending: Boolean = true) = {
    AccountInfo(emailPendingPair, Some(CellphonePendingPair(cellphone, isPending = pending)), passLength);
  }

  def setNewEmail(mail: String, pending: Boolean = true) = {
    AccountInfo(Some(EmailPendingPair(mail, isPending = pending)), cellphonePendingPair, passLength)
  }

  def isComplete = emailPendingPair.isDefined && cellphonePendingPair.isDefined;
}

object EmailPendingPair {
  implicit val itsJsonFormat = jsonFormat[String, Boolean, EmailPendingPair](
    EmailPendingPair.apply, "mail", "isPending"
  );
}

case class EmailPendingPair(mail: String, isPending: Boolean);


object CellphonePendingPair {

  implicit object itsJsonFormat extends RootJsonFormat[CellphonePendingPair] {
    def write(model: CellphonePendingPair): JsValue = JsObject(
      "cellphone" -> JsString(getPhoneString(model.cellphone)),
      "isPending" -> JsBoolean(model.isPending)
    );

    def read(json: JsValue) = json match {
      case obj: JsObject => {
        obj.getFields("cellphone", "isPending") match {
          case Seq(JsString(cellphone), JsBoolean(isPending)) => CellphonePendingPair(parsePhoneString(cellphone).get, isPending);
          case _ => throw new DeserializationException("CellphonePendingPair json object expected");
        }
      }
      case _ => throw new DeserializationException("CellphonePendingPair json object expected");
    }
  }

}

case class CellphonePendingPair(cellphone: PhoneNumber, isPending: Boolean);
