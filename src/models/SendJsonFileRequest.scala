package models

import java.io.File

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case class SendJsonFileRequest(jsonFile: File, identification: String, token: String) {

}
