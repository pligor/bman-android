package models

import android.content.Context
import bluetooth.BluetoothHelper
import BluetoothHelper._
import myandroid.WifiHelper._
import android.app.Activity
import spray.json.{NullOptions, DefaultJsonProtocol}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object Trial {

  //use NullOptions when you want the None values to be rendered as null
  //without NullOptions, None values are not rendered at all
  object TrialJsonProtocol extends DefaultJsonProtocol /*with NullOptions*/ {
    implicit val itsJsonFormat = jsonFormat[String, Option[Long], Option[String], Trial](
      Trial.apply, "mac_address", "timestamp", "token"
    )
  }

  def getOption(timestampOption: Option[Long])(implicit context: Context, activity: Activity): Option[Trial] = {
    val wifiAddressOption = getWifiMacAddress;
    val bluetoothAddressOption = getBluetoothAddress;

    //we are trying to get wifi mac address first because we realized that is always available
    //while bluetooth address is not available when bluetooth is turned off
    val macAddressOption = if (wifiAddressOption.isDefined) {
      wifiAddressOption
    } else if (bluetoothAddressOption.isDefined) {
      bluetoothAddressOption;
    } else {
      None;
    }
    macAddressOption.map {
      macAddress =>
        Trial(macAddress, timestampOption, MyTokenPreference.getValue)
    }
  }
}

case class Trial(mac_address: String, timestamp: Option[Long], token: Option[String]);
