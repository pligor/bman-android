package models

import android.content.{Context, ContentValues}
import android.graphics.Bitmap
import components.ImageSizeDp
import java.util.Date
import java.io._
import com.google.gson.stream.{JsonReader, JsonWriter}
import scala.io.Codec
import generic.FileHelper._
import myandroid.AndroidHelper._
import java.text.SimpleDateFormat
import myAndroidSqlite.{Autocomplete, MyModel}
import myandroid.log
import scala.Some

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */

object Card extends Autocomplete {
  val defaultIsPublicValue = true

  val frontSideJsonName = "frontCard"

  val backSideJsonName = "backCard"

  val TABLE_NAME = "card"

  object Type extends Enumeration {
    val INVALID = Value

    val MINE = Value

    val PEOPLE = Value
  }

  object cols extends Enumeration {
    val frontPhotoId = Value

    val backPhotoId = Value

    val timeCreated = Value

    val isPublic = Value
  }

  def getById(modelId: Long)(implicit context: Context): Option[Card] = {
    val myCard = MyCard.getById(modelId)

    if (myCard.isDefined) {
      myCard
    }
    else {
      PeopleCard.getById(modelId)
    }
  }

  def preparePhotos(frontFile: File, backFileOption: Option[File])
                   (implicit context: Context): Option[(Long, Option[Long])] = {
    Photo.create(frontFile).flatMap {
      frontPhoto =>
        assert(frontPhoto.id.isDefined, "the newly created photo must have an id")

        val frontPhotoId = frontPhoto.id.get

        if (backFileOption.isDefined) {
          val backPhotoOption = Photo.create(backFileOption.get)

          if (backPhotoOption.isDefined) {
            Some(frontPhotoId, backPhotoOption.get.id)
          } else {
            //erase front photo because is redundant
            frontPhoto.delete

            None
          }
        } else {
          Some(frontPhotoId, None)
        }
      /*val backPhotoOptiona: Option[Photo] = if (backFileOption.isDefined) {
        Some(Photo.create(backFileOption.get));
      } else {
        None;
      }

      val backPhotoIdOption = if (backPhotoOptiona.isDefined) {
        backPhotoOptiona.get.id;
      } else {
        None;
      }*/
    }
  }

  private val jsonMaxFileSize = ((1 + base64overhead) * 2 * Photo.maxFileSize).toLong

  def isJsonFileLenOk(len: Long): Boolean = (len > 0) && (len < jsonMaxFileSize)

  def readIsPublicFromJson(implicit jsonReader: JsonReader): Boolean = {
    //this was used when they were read in order
    /*assert(jsonReader.nextName() == cols.isPublic.toString,
      "json name must have been the same string as the column name");*/

    jsonReader.nextBoolean()
  }

  def genTempJsonFileForSending(cardId: Long)(implicit context: Context) = {
    require(!onUIthread)

    val cardOption = Card.getById(cardId)

    assert(cardOption.isDefined, "the card must be an existing card")

    val card = cardOption.get

    assert(card.hasFrontSide, "no! you may not get a temp json file of empty cards")

    assert(card.isAllowedToBeSent)

    card.generateJsonTempFile
  }
}

abstract class Card(val frontPhotoId: Long,
                    val backPhotoId: Option[Long],
                    val timeCreated: Long,
                    val isPublic: Boolean,
                    val currentDimension: Dimension.Value,
                    id: Option[Long] = None)
  extends MyModel(id) {

  def getTimeCreatedString = new SimpleDateFormat("EEE, dd MMMM yyyy HH:mm").format(getTimeCreatedDate)

  def isAllowedToBeSent: Boolean = {
    getType match {
      case Card.Type.MINE => true //always allow my cards to get sent
      case Card.Type.PEOPLE => isPublic
    }
  }

  private def writeIsPublicToJson(implicit jsonWriter: JsonWriter): Unit = {
    jsonWriter.name(Card.cols.isPublic.toString)

    jsonWriter.value(isPublic)
  }

  private def writeSenderNameToJson(implicit jsonWriter: JsonWriter, context: Context): Unit = {
    jsonWriter.name(PeopleCard.cols.senderName.toString)

    val senderName = UserInfo.getName

    assert(senderName.isDefined, "user must have already established his name")

    jsonWriter.value(senderName.get)
  }

  def generateJsonTempFile(implicit context: Context): Option[File] = {

    val tempFile = genRandomTemporaryFile(ext = Some("json"))

    val bufferedOutStream = new BufferedOutputStream(new FileOutputStream(tempFile), Photo.JSON_BUFFER_SIZE)

    try {
      implicit val jsonWriter = new JsonWriter(new OutputStreamWriter(bufferedOutStream, Codec.UTF8.charSet))

      try {
        //jsonWriter.setIndent(" " * 2); ///////////////this is only for pretty writing///////////////////////
        jsonWriter.beginObject()

        {
          writeIsPublicToJson

          writeSenderNameToJson

          val metavalues = getMetaValues

          if (!metavalues.isEmpty) {
            MetaValue.writeMetaValuesInJson(metavalues)
          }

          val frontPhotoOption = getFrontPhoto

          assert(frontPhotoOption.isDefined,
            "only convert to json old card models which have of course at least one card side")

          jsonWriter.name(Card.frontSideJsonName)

          {
            frontPhotoOption.get.writeFileInJsonArray
          }

          val backPhotoOption = getBackPhoto

          if (backPhotoOption.isDefined) {
            jsonWriter.name(Card.backSideJsonName)

            {
              backPhotoOption.get.writeFileInJsonArray
            }
          }
        }

        jsonWriter.flush() //just to make sure that everything is written, because of an exception seen in below line

        jsonWriter.endObject()

        Some(tempFile)
      } catch {
        case e: IOException => None
      } finally {
        jsonWriter.close()
      }
    } catch {
      case e: IOException => None
    } finally {
      bufferedOutStream.close()
    }
  }

  def getLabels(notIncluded: Boolean = false)(implicit context: Context): Seq[MetaLabel] = {
    val curType = getType

    val factory = curType match {
      case Card.Type.MINE => MyMetaLabel.createFromCursor _
      case Card.Type.PEOPLE => PeopleMetaLabel.createFromCursor _
    }

    val tableLabel = curType match {
      case Card.Type.MINE => MyMetaLabel.TABLE_NAME
      case Card.Type.PEOPLE => PeopleMetaLabel.TABLE_NAME
    }

    val tableValue = curType match {
      case Card.Type.MINE => MyMetaValue.TABLE_NAME
      case Card.Type.PEOPLE => PeopleMetaValue.TABLE_NAME
    }

    val query =
      """SELECT *
        |FROM %s AS p
        |WHERE p.id %s IN (
        |    SELECT labelId
        |    FROM %s
        |    WHERE cardId = ?
        |)""".stripMargin.format(
        tableLabel,
        if (notIncluded) "NOT" else "",
        tableValue)

    (new DatabaseHandler).getModelsByQuery(
      query, Array[String](id.get.toString)
    )(factory)
  }

  def getMetaValues(implicit context: Context): Seq[MetaValue] = {
    val curType = getType

    val factory = curType match {
      case Card.Type.MINE => MyMetaValue.createFromCursor _
      case Card.Type.PEOPLE => PeopleMetaValue.createFromCursor _
    }

    val tableName = curType match {
      case Card.Type.MINE => MyMetaValue.TABLE_NAME
      case Card.Type.PEOPLE => PeopleMetaValue.TABLE_NAME
    }

    val query =
      """SELECT *
        |FROM %s
        |WHERE cardId = ?""".stripMargin.format(tableName)
    //Logger(query);
    (new DatabaseHandler).getModelsByQuery(
      query, Array[String](id.get.toString)
    )(factory)
  }

  def getTimeCreatedDate: Date = new Date(timeCreated)

  val orderNumber: Int

  /**
   * @return current card, target card
   */
  /*def swapOrderNumberWithCard(card: Card)(implicit context: Context): (Card, Card) = {
    if (card == this) {
      (this, this);
    }
    else {
      val curOrderNumber = orderNumber;
      val targetOrderNumber = card.orderNumber;
      /*Logger("cur card order number: " + curOrderNumber);
      Logger("target card order number: " + targetOrderNumber);*/

      //because of unique constraint we must the the one to an invalid order number
      val temporaryCurCard = setOrderNumber(PeopleCard.INVALID_ORDER_NUMBER);

      val updatedTargetCard = card.setOrderNumber(curOrderNumber);
      val updatedCurCard = temporaryCurCard.setOrderNumber(targetOrderNumber);

      (updatedCurCard, updatedTargetCard);
    }
  }*/

  def nextDimension(): Card = {
    setDimension(Dimension.nextDimension(currentDimension))
  }

  //abstract

  def setDimension(newDimension: Dimension.Value): Card

  def getById(modelId: Long)(implicit context: Context): Option[Card]

  //implemented

  def hasFrontSide = !isNew

  def hasBackSide = !isNew && backPhotoId.isDefined

  def isTwoSided = hasFrontSide && hasBackSide

  def isOneSided = {
    assert(!(hasBackSide && !hasFrontSide), "having only back side should not be possible")
    hasFrontSide && !hasBackSide; //hasOnlyFrontSide
  }

  /**
   * by cascade of the database, along with photo row, card row is deleted as well ;)
   */
  override def delete(implicit context: Context): Boolean = {
    val cardOption = deleteFrontCard

    !cardOption.flatMap(_.deleteFrontCard).isDefined
    //below line is not used because actually by cascade both card and mycard/people card rows get deleted
    //super.delete(context)
  }

  def deleteFrontCard(implicit context: Context): Option[Card] = {
    if (!hasFrontSide) {
      //does not have a front card, so nothing to delete
      Some(this)
    }
    else if (hasFrontSide && !hasBackSide) {
      //has only one side so must be completely deleted
      val frontPhotoOption = getFrontPhoto

      if (frontPhotoOption.isDefined) {
        val photoModelAndFileDeleted = frontPhotoOption.get.delete; //by cascade of the database card is deleted as well ;)
        //assert(photoModelAndFileDeleted);
        if (!photoModelAndFileDeleted) {
          log log "Image file was not found. You did well to clear this out :-)"
        }
      } else {
        Photo.deleteOnlyFromDB(frontPhotoId)
      }

      None
    } else if (hasFrontSide && hasBackSide) {
      //has two sides so we have the back side to fill the front side

      //swap backside with frontside
      val swappedCard = swapSides

      //delete backside
      Some(swappedCard.deleteBackCard)
    }
    else {
      throw new Exception("normally we should not get into this because the above statements cover all the possible logic")
    }
  }

  def deleteBackCard(implicit context: Context): Card = {
    val backPhotoOption = getBackPhoto

    assert(backPhotoOption.isDefined, "obviously must have a valid back photo id if it has a back side")

    val photoModelAndFileDeleted = backPhotoOption.get.delete //by cascade the database column for the back card is set to null

    //assert(photoModelAndFileDeleted, "normally both the file and the corresponding row on the photo table must be deleted")

    if (!photoModelAndFileDeleted) {
      log log "Image file was not found. You did well to clear this out :-)"
    }

    getById(id.get).getOrElse(throw new Exception("deleting the back side of the card should NOT delete the card itself"))
  }

  def getType = this match {
    case x: MyCard => Card.Type.MINE
    case y: PeopleCard => Card.Type.PEOPLE
  }

  /**
   * Setting it to null is not currently supported
   */
  /*def setOrderNumber(newOrderNumber: Int)(implicit context: Context): Card = {
    //update the row of the card model
    val values = generateContentValues;

    val key = getType match {
      case Card.Type.MINE => MyCard.cols.orderNumber.toString;
      case Card.Type.PEOPLE => PeopleCard.cols.orderNumber.toString;
    }

    //just to be sure
    values.remove(key);

    values.put(key, new java.lang.Integer(newOrderNumber));

    val tableName = getType match {
      case Card.Type.MINE => MyCard.TABLE_NAME;
      case Card.Type.PEOPLE => PeopleCard.TABLE_NAME;
    }

    val idCol = getType match {
      case Card.Type.MINE => MyCard.cols.cardId.toString;
      case Card.Type.PEOPLE => PeopleCard.cols.cardId.toString;
    }

    (new DatabaseHandler).updateById(tableName, id.get, values, idCol);

    val modelOption = getById(id.get);
    assert(modelOption.isDefined, "we just updated the row! the model must still be there");
    modelOption.get;
  }*/

  def getFrontPhoto(implicit context: Context): Option[Photo] = Photo.getById(frontPhotoId)

  def getBackPhoto(implicit context: Context): Option[Photo] = if (backPhotoId.isDefined) {
    Photo.getById(backPhotoId.get)
  } else {
    None
  }

  def getDummyFrontBitmap(targetSize: ImageSizeDp)(implicit context: Context): Bitmap

  def getDummyBackBitmap(targetSize: ImageSizeDp)(implicit context: Context): Bitmap

  /*def getFrontBitmap(targetSize: ImageSizeDp)(implicit context: Context): Bitmap = {
    val photoOption = Photo.getById(frontPhotoId)

    if (photoOption.isDefined) {
      photoOption.get.getBitmap(targetSize)
    } else {
      getDummyFrontBitmap(targetSize)
    }
  }

  def getBackBitmap(targetSize: ImageSizeDp)(implicit context: Context): Bitmap = {
    backPhotoId.flatMap {
      photoId =>
        Photo.getById(photoId).map {
          photo =>
            photo.getBitmap(targetSize)
        }
    }.getOrElse({
      getDummyBackBitmap(targetSize)
    })
  }*/

  protected class InvalidSideConfigurationException extends Exception

  /**
   * Setting it to null is not currently supported
   */
  def setBackPhotoId(newBackPhotoId: Long)(implicit context: Context): Card = {
    //update the row of the card model
    val values = getCardContentValues

    //just to be sure
    values.remove(Card.cols.backPhotoId.toString)

    values.put(Card.cols.backPhotoId.toString, new java.lang.Long(newBackPhotoId))

    (new DatabaseHandler).updateById(Card.TABLE_NAME, id.get, values, ID_COLUMN_NAME)

    val modelOption = getById(id.get)

    assert(modelOption.isDefined, "we just updated the row! the model must still be there")

    modelOption.get
  }

  def swapSides(implicit context: Context): Card = {
    //require(isTwoSided, "you can swap sides only on a two sided card of course");
    if (isTwoSided) {
      val contentValues = getCardContentValues

      assert(contentValues.size() == Card.cols.values.size, "size really is: " + contentValues.size())

      //swapping
      val backPhotoId = contentValues.getAsLong(Card.cols.frontPhotoId.toString)

      val frontPhotoId = contentValues.getAsLong(Card.cols.backPhotoId.toString)

      //just to be sure
      contentValues.remove(Card.cols.frontPhotoId.toString)

      contentValues.remove(Card.cols.backPhotoId.toString)

      //inserting swapped values back to content values
      contentValues.put(Card.cols.frontPhotoId.toString, frontPhotoId)

      contentValues.put(Card.cols.backPhotoId.toString, backPhotoId)

      (new DatabaseHandler).updateById(Card.TABLE_NAME, id.get, contentValues, ID_COLUMN_NAME)

      val modelOption = getById(id.get)

      assert(modelOption.isDefined, "we just updated the row! the model must still be there")

      modelOption.get
    }
    else {
      this
    }
  }

  def getCurrentDummy(targetSize: ImageSizeDp)(implicit context: Context) = currentDimension match {
    case Dimension.frontSide => getDummyFrontBitmap(targetSize)
    case Dimension.backSide => getDummyBackBitmap(targetSize)
  }

  def getCurrentPhoto(implicit context: Context): Option[Photo] = currentDimension match {
    case Dimension.frontSide => Photo.getById(frontPhotoId)
    case Dimension.backSide => backPhotoId.flatMap(Photo.getById)
  }

  /*def getCurrentBitmap(targetSize: ImageSizeDp)(implicit context: Context): Bitmap = {
    currentDimension match {
      case Dimension.frontSide => getFrontBitmap(targetSize)
      case Dimension.backSide => getBackBitmap(targetSize)
    }
  }*/

  val TABLE_NAME = Card.TABLE_NAME

  protected def generateContentValues: ContentValues = getCardContentValues

  def getCardContentValues: ContentValues = {
    val contentValues = new ContentValues()

    contentValues.put(Card.cols.frontPhotoId.toString, new java.lang.Long(frontPhotoId))

    if (backPhotoId.isDefined) {
      contentValues.put(Card.cols.backPhotoId.toString, new java.lang.Long(backPhotoId.get))
    }
    else {
      contentValues.putNull(Card.cols.backPhotoId.toString)
    }

    contentValues.put(Card.cols.timeCreated.toString, new java.lang.Long(timeCreated))

    contentValues.put(Card.cols.isPublic.toString, isPublic)

    contentValues
  }
}
