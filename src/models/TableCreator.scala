package models

import net.sqlcipher.database.SQLiteDatabase
//import android.database.sqlite.SQLiteDatabase

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object TableCreator {
  def apply(db: SQLiteDatabase) {
    val createStatements = Seq[String](
      """CREATE TABLE "pgppublic" (
        |    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        |    "publicKey" TEXT NOT NULL,
        |    "name" TEXT NOT NULL,
        |    "email" TEXT NOT NULL,
        |    UNIQUE("email")
        |)""",

      """CREATE TABLE "pgpsecret" (
        |    "pgppublicId" INTEGER PRIMARY KEY NOT NULL,
        |    "secretKey" TEXT NOT NULL,
        |    "passPhrase" TEXT,
        |    FOREIGN KEY(pgppublicId) REFERENCES pgppublic(id)
        |	ON UPDATE RESTRICT ON DELETE CASCADE
        |)""",

      """CREATE TABLE "photo" (
        |    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        |    "filename" TEXT NOT NULL,
        |    "pgpKeyId" INTEGER NOT NULL,
        |    UNIQUE("filename"),
        |    FOREIGN KEY (pgpKeyId) REFERENCES pgpsecret(pgppublicId)
        |	ON DELETE CASCADE ON UPDATE RESTRICT
        |)""",

      """CREATE TABLE "card" (
        |    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        |    frontPhotoId INTEGER NOT NULL,
        |    backPhotoId INTEGER NULL,
        |    timeCreated INTEGER NOT NULL,
        |    isPublic INTEGER NOT NULL,
        |    UNIQUE("frontPhotoId"),
        |    UNIQUE("backPhotoId"),
        |    FOREIGN KEY(frontPhotoId) REFERENCES photo(id)
        |      ON DELETE CASCADE ON UPDATE RESTRICT,
        |    FOREIGN KEY(backPhotoId) REFERENCES photo(id)
        |      ON DELETE SET NULL ON UPDATE RESTRICT
        |)""",

      """CREATE TABLE "mycard" (
        |    "cardId" INTEGER PRIMARY KEY NOT NULL,
        |    "orderNumber" INTEGER NOT NULL DEFAULT (0),
        |    UNIQUE("orderNumber"),
        |    FOREIGN KEY(cardId) REFERENCES card(id)
        |	ON UPDATE RESTRICT ON DELETE CASCADE
        |)""",

      """CREATE TABLE peoplecard (
        |    cardId INTEGER PRIMARY KEY NOT NULL,
        |    orderNumber INTEGER NOT NULL DEFAULT (0),
        |    senderName TEXT NOT NULL,
        |    UNIQUE(orderNumber),
        |    FOREIGN KEY(cardId) REFERENCES card(id)
        |	ON UPDATE RESTRICT ON DELETE CASCADE
        |)""",

      """CREATE TABLE mymetalabel (
        |    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        |    label TEXT NOT NULL,
        |    UNIQUE(label)
        |)""",

      """CREATE TABLE mymetavalue (
        |    cardId INTEGER NOT NULL,
        |    labelId INTEGER NOT NULL,
        |    dataValue TEXT NOT NULL,
        |    PRIMARY KEY(cardId,labelId),
        |    FOREIGN KEY(cardId) REFERENCES mycard(cardId)
        |	ON UPDATE RESTRICT ON DELETE CASCADE,
        |    FOREIGN KEY(labelId) REFERENCES mymetalabel(id)
        |	ON UPDATE RESTRICT ON DELETE CASCADE
        |)""",

      """CREATE TABLE peoplemetalabel (
        |    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        |    label TEXT NOT NULL,
        |    UNIQUE(label)
        |)""",

      """CREATE TABLE peoplemetavalue (
        |    cardId INTEGER NOT NULL,
        |    labelId INTEGER NOT NULL,
        |    dataValue TEXT NOT NULL,
        |    PRIMARY KEY(cardId,labelId),
        |    FOREIGN KEY(cardId) REFERENCES peoplecard(cardId)
        |	ON UPDATE RESTRICT ON DELETE CASCADE,
        |    FOREIGN KEY(labelId) REFERENCES peoplemetalabel(id)
        |	ON UPDATE RESTRICT ON DELETE CASCADE
        |)"""
    );

    createStatements foreach {
      createStatement =>
        db.execSQL(createStatement.stripMargin);
    }
  }
}
