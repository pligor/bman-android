package models

import android.content.{DialogInterface, Context, ContentValues}
import android.database.Cursor
import android.app.Dialog
import com.pligor.bman.R
import android.widget.{Button, EditText}
import android.view.View
import android.content.DialogInterface.OnDismissListener
import myAndroidSqlite.{Autocomplete, MyModelObject, MyModel, MySQLiteOpenHelper}
import myandroid.AndroidHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

case object MetaLabelCols extends Enumeration {
  val id = Value;
  val label = Value;
}

abstract class MetaLabelObject(val TABLE_NAME: String) extends MyModelObject[MetaLabel] with Autocomplete {
  private val labelWordSeperator = ", ";

  def metaLabelsToString(labels: Seq[MetaLabel]) = labels.map("'" + _.label + "'").mkString(labelWordSeperator);

  def createFromCursor(cursor: Cursor) = new MetaLabel(
    label = cursor.getString(cursor.getColumnIndexOrThrow(MetaLabelCols.label.toString)),
    id = Some(cursor.getLong(cursor.getColumnIndexOrThrow(MetaLabelCols.id.toString))),
    TABLE_NAME = TABLE_NAME
  );

  def findByLabel(label: String)(implicit context: Context): Option[MetaLabel] = {
    (new DatabaseHandler).getModelByQuery(
      """SELECT *
        |FROM %s
        |WHERE label = ?""".stripMargin.format(TABLE_NAME), Array[String](label)
    )(createFromCursor);
  }

  protected def insertOrRetrieve(metaLabelId: Long, label: String)(implicit context: Context): MetaLabel = {
    val metaLabelOption = if (metaLabelId == MySQLiteOpenHelper.invalidInsertId) {
      findByLabel(label);
    } else {
      getById(metaLabelId);
    }
    assert(metaLabelOption.isDefined, "if insert fails it must exists");
    metaLabelOption.get;
  }

  protected def getUnused(childTableName: String)(implicit context: Context): Seq[MetaLabel] = {
    (new DatabaseHandler).getModelsByQuery(
      """SELECT *
        |FROM %s
        |WHERE id NOT IN(
        |	SELECT labelId FROM %s
        |)""".stripMargin.format(TABLE_NAME, childTableName)
    )(createFromCursor);
  }

  def getUnused(implicit context: Context): Seq[MetaLabel];

  /**
   * @return returns those that did not got deleted for some reason, on success this is empty
   */
  def deleteUnused(implicit context: Context) = getUnused.filter(!_.delete);
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class MetaLabel(val label: String, id: Option[Long], val TABLE_NAME: String) extends MyModel(id) {
  require(label.length <= 20);

  protected def generateContentValues: ContentValues = {
    val contentValues = new ContentValues();
    contentValues.put(MetaLabelCols.label.toString, label);
    contentValues;
  }

  val ID_COLUMN_NAME = MyMetaLabel.ID_COLUMN_NAME;

  override def toString: String = {
    //super.toString
    label;
  }
}

object MyMetaLabel extends MetaLabelObject(TABLE_NAME = "mymetalabel") {
  def getUnused(implicit context: Context): Seq[MetaLabel] = getUnused("mymetavalue");
}

class MyMetaLabel(label: String, id: Option[Long] = None)
  extends MetaLabel(label, id, MyMetaLabel.TABLE_NAME) {

  def safeInsert(implicit context: Context): Option[Long] = {
    val modelOption = MyMetaLabel.findByLabel(label);
    if (modelOption.isDefined) {
      None
    } else {
      Some(insert);
    }
  }
}

object PeopleMetaLabel extends MetaLabelObject(TABLE_NAME = "peoplemetalabel") {
  def getUnused(implicit context: Context): Seq[MetaLabel] = getUnused("peoplemetavalue");

  def insertOrRetrieve(label: String)(implicit context: Context): MetaLabel = {
    val metaLabelId = new PeopleMetaLabel(label).insert;
    insertOrRetrieve(metaLabelId, label);
  }

}

class PeopleMetaLabel(label: String, id: Option[Long] = None)
  extends MetaLabel(label, id, PeopleMetaLabel.TABLE_NAME) {

}

