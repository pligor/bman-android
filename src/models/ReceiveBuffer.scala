package models

import collection.mutable.ArrayBuffer
import components.{DelayTask, ByteSize}
import generic.Hash

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class ReceiveBuffer(val expectedLength: Int) extends DelayTask {
  private object ReceiveBufferOverflowException extends Exception;

  require(expectedLength > 0, "len happens to be: " + expectedLength);
  //if fails because of zero might be a bizarre occasion where buffer is empty (?)

  private val bytesBuffer = ArrayBuffer.empty[Byte];

  def appendBytes(bytes: Array[Byte]) {
    bytesBuffer ++= bytes;
    if (bytesBuffer.length > expectedLength) {
      throw ReceiveBufferOverflowException;
    }
  }

  def getBytes: Array[Byte] = bytesBuffer.toArray;

  def isFull: Boolean = (bytesBuffer.length == expectedLength);

  def getHash: Array[Byte] = {
    val bytes = Hash(getBytes).toByteArray;
    assert(bytes.length == ByteSize.MD5, "typically must be sixteen bytes with md5");
    bytes;
  }

  def getEmptyClone = new ReceiveBuffer(expectedLength);

  /**
   * can be overriden to supply specific implementation
   */
  protected def onAfterDelay() {}
}
