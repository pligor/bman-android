package models

import components.{ByteSize, ByteConv}
import android.content.Context
import java.io.File
import bluetooth.BufferLenGetter
import myandroid.log
import generic.{SendBuffer, FileSender}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class Master(private val file: File,
             private val bufferLenGetter: BufferLenGetter)
            (implicit context: Context) extends CardTransaction {
  private var sendBuffer: Option[SendBuffer] = None

  //val fileSender = new FileSender(file, ConnectedThread.CURRENT_BUFFER_BYTES_LEN)
  val fileSender = new FileSender(file)

  def cancel(): Unit = {
    end(); //it is the something because we just want to close the file read
    sendBuffer = None
  }

  def end(): Unit = {
    fileSender.end()
  }

  def fileSent(bytes: Array[Byte]): Boolean = {
    //just see if is received ok or not
    ByteConv.arrayByte2boolean(bytes)
  }

  def errorFileEmpty(bytes: Array[Byte]): Unit = {
    val slaveMustHaveSentFalse = ByteConv.arrayByte2boolean(bytes)

    assert(!slaveMustHaveSentFalse)
  }

  def getBufferHash(bytes: Array[Byte]): Boolean = {
    assert(bytes.length == ByteSize.MD5,
      "we must have received all the sixteen bytes of md5 hash")

    sendBuffer.get.isHashValid(bytes)
  }

  def sendBufferData(bytes: Array[Byte]): Array[Byte] = {
    val pingBit = ByteConv.arrayByte2boolean(bytes)

    assert(pingBit, "ping byte must be true for no reason")

    log log "Now let's send the bytes"

    sendBuffer.get.bytes
  }

  def sendBufferLen(bytes: Array[Byte]): Int = {
    val pingBit = ByteConv.arrayByte2boolean(bytes)

    assert(pingBit, "ping byte must be true for no reason")

    readNextBufferLen()
  }

  def fileLenAcceptance(bytes: Array[Byte]): Option[Int] = {
    val isFileLenAccepted = ByteConv.arrayByte2boolean(bytes)

    if (isFileLenAccepted) {
      log log "file len is accepted by slave"

      Some(readNextBufferLen())
    } else {
      log log "file length is NOT accepted from slave"

      None
    }
  }

  private def readNextBufferLen(): Int = {
    sendBuffer = fileSender.readNextBuffer(bufferLenGetter.getCurrentBufferBytesLen)

    val bufferLenOrInvalid = getSendBufferLenOrInvalid

    bufferLenOrInvalid
  }

  private def getSendBufferLenOrInvalid: Int = if (sendBuffer.isDefined) {
    sendBuffer.get.getLen
  } else {
    CardTransaction.INVALID_BUFFER_BYTE_LENGTH
  }

  def confirmCardSend(bytes: Array[Byte]): Option[Long] = {
    val isAccepted = ByteConv.arrayByte2boolean(bytes)

    if (isAccepted) {
      log log "my intention to send a file is accepted"

      Some(fileSender.fileLen)
    } else {
      log log "my intention to send a card has been declined"

      None
    }
  }

}
