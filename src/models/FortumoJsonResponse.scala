package models

import spray.json.DefaultJsonProtocol._
import java.util.Date
import spray.json.{DeserializationException, JsNumber, JsValue, JsonFormat}

protected trait DateJsonFormatTrait {

  /**
   * tell the spray creator that Date must be implemented
   */
  implicit object DateJsonFormat extends JsonFormat[Date] {
    def write(model: Date): JsValue = JsNumber(model.getTime)

    def read(json: JsValue): Date = json match {
      case JsNumber(x) => new Date(x.toLongExact)
      case _ => throw new DeserializationException("js number expected")
    }
  }

}

object FortumoJsonResponse extends DateJsonFormatTrait {
  implicit val itsJsonFormat = jsonFormat[Date, FortumoJsonResponse](
    FortumoJsonResponse.apply, "fortumo_receipt_received"
  )
}

case class FortumoJsonResponse(fortumo_receipt_received: Date) {

}

