package models

import com.bugsense.trace.BugSenseHandler

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MyBugsense {
  val API_KEY = "f8478163"

  object Event extends Enumeration {
    val CARD_CREATED = Value

    val CARD_SEND_SUCCESS_VIA_BLUETOOTH = Value
    val CARD_SEND_SUCCESS_VIA_ALLJOYN = Value
    val CARD_SEND_SUCCESS_VIA_EMAIL = Value
    val CARD_SEND_SUCCESS_VIA_SMS = Value
    val CARD_SEND_FAIL_VIA_BLUETOOTH = Value
    val CARD_SEND_FAIL_VIA_ALLJOYN = Value
    val CARD_SEND_FAIL_VIA_EMAIL = Value
    val CARD_SEND_FAIL_VIA_SMS = Value

    //val CARD_SEND_REJECTED

    def sendBugsenseEvent(event: MyBugsense.Event.Value): Unit = {
      BugSenseHandler.sendEvent(event.toString)
    }
  }

}
