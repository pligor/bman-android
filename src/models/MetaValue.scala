package models

import android.content.{Context, ContentValues}
import java.lang
import android.database.Cursor
import com.google.gson.stream.{JsonReader, JsonWriter}
import scala.collection.{immutable, mutable}
import myAndroidSqlite.MySQLiteOpenHelper

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

/*abstract class MetaValueObject(val TABLE_NAME: String) extends MyModelObject with Autocomplete {

  case object cols extends Enumeration {
    val id = Value;
    val label = Value;
  }

  def createFromCursor(cursor: Cursor) = new MetaLabel(
    label = cursor.getString(cursor.getColumnIndexOrThrow(cols.label.toString)),
    id = Some(cursor.getLong(cursor.getColumnIndexOrThrow(cols.id.toString))),
    TABLE_NAME = TABLE_NAME
  );
}*/

private case object MetaValueCols extends Enumeration {
  val cardId = Value;
  val labelId = Value;
  val dataValue = Value;
}

object MetaValue {
  val metadataJsonName = "metadata"

  def writeMetaValuesInJson(metaValues: Seq[MetaValue])(implicit jsonWriter: JsonWriter, context: Context): Unit = {
    require(!metaValues.isEmpty, "better not write at all in json if metavalues are empty")

    jsonWriter.name(metadataJsonName)

    {
      jsonWriter.beginObject()

      metaValues foreach {
        metaValue =>
          jsonWriter.name(metaValue.getLabel.label)

          jsonWriter.value(metaValue.dataValue)
      }

      jsonWriter.endObject()
    }
  }

  def readMetaDataFromJson/*(nextName: String)*/(implicit jsonReader: JsonReader): immutable.Map[String, String] = {
    //this was from when the keys were read in order
    //require(nextName == metadataJsonName, "better not use this function if the metadata name is not found");

    val map = mutable.HashMap.empty[String, String];
    {
      jsonReader.beginObject();

      while (jsonReader.hasNext) {
        map += jsonReader.nextName() -> jsonReader.nextString;
      }

      jsonReader.endObject();
    }

    map.toMap;
  }
}

abstract class MetaValueObject(val TABLE_NAME: String) {

  def getCount(implicit context: Context) = {
    (new DatabaseHandler).getCount(TABLE_NAME);
  }

  protected def getCardIdFromCursor(cursor: Cursor) = {
    cursor.getLong(cursor.getColumnIndexOrThrow(MetaValueCols.cardId.toString));
  }

  protected def getLabelIdFromCursor(cursor: Cursor) = {
    cursor.getLong(cursor.getColumnIndexOrThrow(MetaValueCols.labelId.toString));
  }

  protected def getDataValueFromCursor(cursor: Cursor) = {
    cursor.getString(cursor.getColumnIndexOrThrow(MetaValueCols.dataValue.toString));
  }

  def createFromCursor(cursor: Cursor): MetaValue;
}

object MyMetaValue extends MetaValueObject("mymetavalue") {
  val maxMetaValuesPerCard = 30;

  def createFromCursor(cursor: Cursor) = new MyMetaValue(
    cardId = getCardIdFromCursor(cursor),
    labelId = getLabelIdFromCursor(cursor),
    dataValue = getDataValueFromCursor(cursor)
  );
}

object PeopleMetaValue extends MetaValueObject("peoplemetavalue") {
  def createFromCursor(cursor: Cursor) = new PeopleMetaValue(
    cardId = getCardIdFromCursor(cursor),
    labelId = getLabelIdFromCursor(cursor),
    dataValue = getDataValueFromCursor(cursor)
  );
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class MetaValue(val cardId: Long,
                         val labelId: Long,
                         val dataValue: String,
                         private val TABLE_NAME: String) {
  require(dataValue.length <= 200);

  //def getLabel: MetaLabel;
  //private lazy val label = MyMetaLabel.getById(labelId).get;
  def getLabel(implicit context: Context): MetaLabel;

  def getCard(implicit context: Context) = Card.getById(cardId).get;

  //abstracts
  def setLabel(label: MetaLabel)(implicit context: Context): Option[MetaValue];

  def setDataValue(newDataValue: String)(implicit context: Context): MetaValue;

  //implemented

  protected def setDataValueToMetaValue(newDataValue: String)(newMetaValue: MetaValue)(implicit context: Context): MetaValue = {
    assert(newMetaValue.update);
    newMetaValue;
  }

  def delete(implicit context: Context): Boolean = {
    (new DatabaseHandler).deleteByCompoundId(TABLE_NAME, getCompoundId);
  }

  protected def setLabelToMetaValue(label: MetaLabel)(newMetaValue: MetaValue)(implicit context: Context): Option[MetaValue] = {
    if (newMetaValue.insert != MySQLiteOpenHelper.invalidInsertId) {
      assert(this.delete);
      Some(newMetaValue);
    } else {
      None;
    }
  }

  /*//didnt work
  private def updateEverything(implicit context: Context): Boolean = {
    (new DatabaseHandler).updateByCompoundId(
      tableName = TABLE_NAME,
      compoundId = getCompoundId,
      contentValues = generateContentValuesWithCompoundId
    );
  }*/

  private def update(implicit context: Context): Boolean = {
    (new DatabaseHandler).updateByCompoundId(
      tableName = TABLE_NAME,
      compoundId = getCompoundId,
      contentValues = generateContentValues
    );
  }

  private def getCompoundId = Map[String, Long](
    MetaValueCols.cardId.toString -> cardId,
    MetaValueCols.labelId.toString -> labelId
  );

  /**
   * With the equals you can safely use it on a set
   */
  override def equals(obj: Any): Boolean = {
    //super.equals(o)
    obj match {
      case that: MetaValue => {
        (this.cardId == that.cardId) &&
          (this.labelId == that.labelId);
      }
      case _ => false;
    }
  }

  def insert(implicit context: Context) = {
    (new DatabaseHandler).insert(TABLE_NAME, generateContentValuesWithCompoundId);
  }

  private def generateContentValuesWithCompoundId: ContentValues = {
    val contentValues = generateContentValues;
    contentValues.put(MetaValueCols.cardId.toString, new lang.Long(cardId));
    contentValues.put(MetaValueCols.labelId.toString, new lang.Long(labelId));
    contentValues;
  }

  protected def generateContentValues: ContentValues = {
    val contentValues = new ContentValues();
    contentValues.put(MetaValueCols.dataValue.toString, dataValue);
    contentValues;
  }
}

class MyMetaValue(cardId: Long,
                  labelId: Long,
                  dataValue: String)
  extends MetaValue(cardId, labelId, dataValue, MyMetaValue.TABLE_NAME) {

  def getLabel(implicit context: Context): MetaLabel = {
    val metaLabelOption = MyMetaLabel.getById(labelId);
    assert(metaLabelOption.isDefined, "do not call this function unless this is an old model");
    metaLabelOption.get;
  }

  def setLabel(label: MetaLabel)(implicit context: Context): Option[MetaValue] = {
    setLabelToMetaValue(label)(new MyMetaValue(
      cardId = cardId,
      labelId = label.id.get,
      dataValue = dataValue
    ));
  }

  def setDataValue(newDataValue: String)(implicit context: Context): MetaValue = {
    setDataValueToMetaValue(newDataValue)(new MyMetaValue(
      cardId = cardId,
      labelId = labelId,
      dataValue = newDataValue
    ));
  }
}

class PeopleMetaValue(cardId: Long, labelId: Long, dataValue: String)
  extends MetaValue(cardId, labelId, dataValue, PeopleMetaValue.TABLE_NAME) {
  //def getLabel: MetaLabel;
  def getLabel(implicit context: Context): MetaLabel = {
    val metaLabelOption = PeopleMetaLabel.getById(labelId);
    assert(metaLabelOption.isDefined, "do not call this function unless this is an old model");
    metaLabelOption.get;
  }

  def setLabel(label: MetaLabel)(implicit context: Context): Option[MetaValue] = {
    setLabelToMetaValue(label)(new PeopleMetaValue(
      cardId = cardId,
      labelId = label.id.get,
      dataValue = dataValue
    ));
  }

  def setDataValue(newDataValue: String)(implicit context: Context): MetaValue = {
    setDataValueToMetaValue(newDataValue)(new PeopleMetaValue(
      cardId = cardId,
      labelId = labelId,
      dataValue = newDataValue
    ));
  }
}

