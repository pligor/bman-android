package models

class NotCurrentlyValidRequestCodeException extends Exception;

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 *
 * Remember that each Value starts with index zero and then plus 1
 */
object RequestCode extends Enumeration {
  val CRUD = Value

  val CAMERA_DATA = Value

  val SELECT_IMAGE = Value

  val CREATE_BACK = Value

  val ZOOM = Value

  val ENABLE_BT = Value

  val BLUETOOTH_DISCOVERABLE = Value

  val IN_APP_PURCHASE = Value

  val FORTUMO_PAYMENT = Value
}
