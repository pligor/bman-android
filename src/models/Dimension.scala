package models

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 *
 * Remember that each Value starts with index zero and then plus 1
 */
object Dimension extends Enumeration {
  //do NOT refactor invalidSideId to be one of the values because
  //nextDimension will not work as expected
  val invalidSideId = -1;

  val frontSide = Value;
  val backSide = Value;

  def nextDimension(dim: Dimension.Value): Dimension.Value = {
    val nextIndex = Dimension.values.toSeq.indexOf(dim) + 1;
    val dimension_nomath = if (nextIndex >= Dimension.values.size) {
      (Dimension.values.toSeq)(0);
    }
    else {
      (Dimension.values.toSeq)(nextIndex);
    }

    val dimension_withmath = Dimension((dim.id + 1) % Dimension.values.size);
    assert(dimension_nomath == dimension_withmath);

    dimension_withmath;
  }
}
