package models

import android.content.{Context, ContentValues}
import android.database.Cursor
import myAndroidSqlite.{MyModelObject, MyModel}

object PgpSecret extends MyModelObject[PgpSecret] {
  val TABLE_NAME = "pgpsecret";
  override val ID_COLUMN_NAME = "pgppublicId";

  case object cols extends Enumeration {
    val pgppublicId = Value;
    val secretKey = Value;
    val passPhrase = Value;
  }

  def getFirst(implicit context: Context): Option[PgpSecret] = {
    //singleSecretRestriction();
    getAll.headOption;
  }

  /**
   * The awful truth is that if we want to change our secret key then all the cards should
   * be re-encoded with the new public key!! This could take forever!!
   * we link the card with the corresponding keypair
   * This means we will have many keypairs and one of them must be the current one
   * @deprecated
   */
  def singleSecretRestriction(implicit context: Context) {
    require({
      val secretCounter = getCount;
      secretCounter >= 0 && secretCounter <= 1;
    },"we have have at most only one secret key");
  }

  def isEmpty(implicit context: Context) = getCount == 0;

  //def createFromCursor[A <: MyModel](cursor: Cursor): A;
  def createFromCursor(cursor: Cursor) = new PgpSecret(
    pgppublicId = Some(cursor.getLong(cursor.getColumnIndexOrThrow(cols.pgppublicId.toString))),
    secretKey = cursor.getString(cursor.getColumnIndexOrThrow(cols.secretKey.toString)),
    passPhrase = {
      val passPhraseIndex = cursor.getColumnIndexOrThrow(cols.passPhrase.toString);
      if (cursor.isNull(passPhraseIndex)) {
        None;
      }
      else {
        Some(cursor.getString(passPhraseIndex))
      }
    }
  );
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class PgpSecret(val secretKey: String,
                val passPhrase: Option[String],
                pgppublicId: Option[Long] = None) extends MyModel(pgppublicId) {
  val TABLE_NAME = PgpSecret.TABLE_NAME;
  val ID_COLUMN_NAME = PgpSecret.ID_COLUMN_NAME;

  protected def generateContentValues: ContentValues = {
    require(pgppublicId.isDefined);
    val contentValues = new ContentValues();
    contentValues.put(PgpSecret.cols.pgppublicId.toString, new java.lang.Long(pgppublicId.get));

    contentValues.put(PgpSecret.cols.secretKey.toString, secretKey);

    val col = PgpSecret.cols.passPhrase.toString;
    if (passPhrase.isDefined) {
      contentValues.put(col, passPhrase.get);
    }
    else {
      contentValues.putNull(col);
    }

    contentValues;
  }
}
