package models

import android.database.Cursor
import android.content.{ContentValues, Context}
import components.{ImageSizeDp}
import android.graphics.Bitmap
import com.pligor.bman.R
import java.io.{InputStreamReader, FileInputStream, BufferedInputStream, File}
import java.util.Date
import com.google.gson.stream.JsonReader
import scala.io.Codec
import myAndroidSqlite._
import scala.Some
import myandroid.{BitmapHelper, log}

object PeopleCard extends MyModelObject[PeopleCard] with Autocomplete with OrderNumberObject {
  val ORDERING_COLUMN: String = cols.orderNumber.toString;

  def getOrderedDesc(ids: Seq[Long])(implicit context: Context): Seq[PeopleCard] = {
    val binders = MySQLiteOpenHelper.genBinders(ids.length);
    (new DatabaseHandler).getModelsByQuery(
      """SELECT *
        |FROM  card, peoplecard
        |WHERE cardId = id AND id IN (%s)
        |ORDER BY orderNumber DESC""".stripMargin.format(binders), ids.map(_.toString).toArray
    )(createFromCursor)
  }

  def getAllOrderedDesc(implicit context: Context): Seq[PeopleCard] = {
    (new DatabaseHandler).getModelsByQuery(
      """SELECT *
        |FROM  card, peoplecard
        |WHERE cardId = id
        |ORDER BY orderNumber DESC""".stripMargin
    )(createFromCursor)
  }

  def getAllIds(ascending: Boolean = false)(implicit context: Context): Seq[Long] = {
    DatabaseHandler.getInstance.getScalars[Long](
        """SELECT cardId
          |FROM peoplecard
          |ORDER BY orderNumber %s""".stripMargin.format(if(ascending) "ASC" else "DESC")
      )
  }

  def getAllOrdered(implicit context: Context): Seq[PeopleCard] = {
    (new DatabaseHandler).getModelsByQuery(
      """SELECT *
        |FROM  card, peoplecard
        |WHERE cardId = id
        |ORDER BY orderNumber""".stripMargin
    )(createFromCursor);
  }

  override def getAll(implicit context: Context): Seq[PeopleCard] = {
    (new DatabaseHandler).getModelsByQuery(
      """SELECT *
        |FROM  card, peoplecard
        |WHERE cardId = id""".stripMargin
    )(createFromCursor);
  }

  object cols extends Enumeration {
    val cardId = Value;
    val orderNumber = Value;
    val senderName = Value;
  }

  val TABLE_NAME: String = "peoplecard";

  def createFromCursor(cursor: Cursor): PeopleCard = {
    val id = cursor.getLong(cursor.getColumnIndexOrThrow(MySQLiteOpenHelper.defaultIdCol));
    val cardId = cursor.getLong(cursor.getColumnIndexOrThrow(PeopleCard.cols.cardId.toString));
    assert(cardId == id, "the id of the retrieved card row must match the id of the child row");

    val backPhotoId = {
      val backPhotoIdIndex = cursor.getColumnIndexOrThrow(Card.cols.backPhotoId.toString);
      if (cursor.isNull(backPhotoIdIndex)) {
        None;
      }
      else {
        Some(cursor.getLong(backPhotoIdIndex))
      }
    }

    new PeopleCard(
      orderNumber = cursor.getInt(cursor.getColumnIndexOrThrow(PeopleCard.cols.orderNumber.toString)),
      senderName = cursor.getString(cursor.getColumnIndexOrThrow(PeopleCard.cols.senderName.toString)),
      frontPhotoId = cursor.getLong(cursor.getColumnIndexOrThrow(Card.cols.frontPhotoId.toString)),
      backPhotoId = backPhotoId,
      timeCreated = cursor.getLong(cursor.getColumnIndexOrThrow(Card.cols.timeCreated.toString)),

      //we have to get integer and then compare with number because sqlite does not have boolean
      isPublic = cursor.getShort(cursor.getColumnIndexOrThrow(Card.cols.isPublic.toString)) == 1,

      id = Some(id)
    );
  }

  override def getById(modelId: Long)(implicit context: Context): Option[PeopleCard] = {
    (new DatabaseHandler).getModelByQuery(
      """SELECT *
        |FROM card, peoplecard
        |WHERE cardId = id
        |AND id = ?""".stripMargin, Array[String](modelId.toString))(createFromCursor)
  }

  def getTop(implicit context: Context): Option[PeopleCard] = {
    (new DatabaseHandler).getModelByQuery(
      """SELECT *
        |FROM card, peoplecard
        |WHERE cardId = id
        |AND orderNumber = (
        |  SELECT MAX(orderNumber) AS maxOrderNumber
        |  FROM peoplecard
        |)""".stripMargin
    )(createFromCursor);
  }

  private def create(frontFile: File, backFile: Option[File], senderName: String, isPublic: Boolean)
                    (implicit context: Context): Option[PeopleCard] = {
    Card.preparePhotos(frontFile, backFile).map {
      prepared =>
        val (frontPhotoId, backPhotoIdOption) = prepared;

        //order number is created automagically, check constructor for more info
        val card = new PeopleCard(
          senderName,
          frontPhotoId,
          backPhotoIdOption,
          isPublic
        );

        PeopleCard.getById(card.insert).get;
    }
  }

  def create(jsonFile: File)(implicit context: Context): Option[PeopleCard] = {
    log log "this is the temporary json path: " + jsonFile.getCanonicalPath;

    implicit val jsonReader = new JsonReader(new InputStreamReader(
      new BufferedInputStream(new FileInputStream(jsonFile), Photo.JSON_BUFFER_SIZE),
      Codec.UTF8.charSet
    ))

    val (isPublic, senderName, metadataOption, frontFile, backFileOption) = try {
      jsonReader.beginObject();
      //how iOS produces data. but this is actually random
      //isPublic
      //metadata
      //frontCard
      //senderName
      //backCard
      /*
      val isPublic = Card.readIsPublicFromJson;

      val senderName = readSenderNameFromJson;

      val nextName = jsonReader.nextName();

      val metadata = if (nextName == MetaValue.metadataJsonName) {
        val map = MetaValue.readMetaDataFromJson(nextName);
        assert(jsonReader.nextName() == Card.frontSideJsonName);
        map;
      } else {
        assert(nextName == Card.frontSideJsonName);
        Map.empty[String, String];
      }

      val frontFile = Photo.genUnencryptedTempFileFromJson;

      val backFileOption = if (jsonReader.hasNext) {
        assert(jsonReader.nextName() == Card.backSideJsonName);
        Some(Photo.genUnencryptedTempFileFromJson);
      } else {
        None;
      }

       */

      var isPublicOption: Option[Boolean] = None;
      var metadataOption: Option[Map[String, String]] = None;
      var frontFileOption: Option[File] = None;
      var senderNameOption: Option[String] = None;
      var backFileOption: Option[File] = None;

      val isPublicKey = Card.cols.isPublic.toString;
      val metadataKey = MetaValue.metadataJsonName;
      val frontCardKey = Card.frontSideJsonName;
      val senderNameKey = cols.senderName.toString;
      val backCardKey = Card.backSideJsonName;

      while (jsonReader.hasNext) {
        jsonReader.nextName() match {
          case `isPublicKey` => {
            isPublicOption = Some(Card.readIsPublicFromJson);
          }
          case `metadataKey` => {
            metadataOption = Some(MetaValue.readMetaDataFromJson);
          }
          case `frontCardKey` => {
            frontFileOption = Some(Photo.genUnencryptedTempFileFromJson);
          }
          case `senderNameKey` => {
            senderNameOption = Some(readSenderNameFromJson);
          }
          case `backCardKey` => {
            backFileOption = Some(Photo.genUnencryptedTempFileFromJson);
          }
        }
      }

      jsonReader.endObject();

      (isPublicOption.get, senderNameOption.get, metadataOption, frontFileOption.get, backFileOption);
    } finally {
      jsonReader.close();
      jsonFile.delete();
    }

    val peopleCardOption = create(frontFile, backFileOption, senderName, isPublic);

    peopleCardOption.map {
      peopleCard =>
        metadataOption.map(peopleCard.insertMetaData);
        peopleCard;
    }
  }

  private def readSenderNameFromJson(implicit jsonReader: JsonReader): String = {
    //this was for when the keys were read in order
    /*val nextName = jsonReader.nextName();
    val expectedName = cols.senderName.toString;
    assert(nextName == expectedName,
      "json name [" + nextName + "] must have been the same string as the column name [" + expectedName + "]");*/
    jsonReader.nextString();
  }

  def create(newSenderName: String,
             newFrontPhotoId: Long,
             newBackPhotoIdOption: Option[Long],
             newIsPublic: Boolean)(implicit context: Context): PeopleCard = {
    val card = new PeopleCard(
      newSenderName,
      newFrontPhotoId,
      newBackPhotoIdOption,
      newIsPublic
    );
    PeopleCard.getById(card.insert).get;
  }
}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class PeopleCard(val orderNumber: Int,
                 val senderName: String,
                 frontPhotoId: Long,
                 backPhotoId: Option[Long],
                 timeCreated: Long,
                 isPublic: Boolean,
                 id: Option[Long] = None,
                 currentDimension: Dimension.Value = Dimension.frontSide)
  extends Card(frontPhotoId, backPhotoId, timeCreated, isPublic, currentDimension, id) with OrderNumber {

  def this(newSenderName: String,
           newFrontPhotoId: Long,
           newBackPhotoId: Option[Long],
           newTimeCreated: Long,
           newIsPublic: Boolean,
           newId: Option[Long] = None)(implicit context: Context) = this(
    PeopleCard.getNextOrderNumber.getOrElse(PeopleCard.ORDER_LOWER_BOUND),
    newSenderName,
    newFrontPhotoId,
    newBackPhotoId,
    newTimeCreated,
    newIsPublic,
    newId
  );

  def this(newSenderName: String,
           newFrontPhotoId: Long,
           newBackPhotoId: Option[Long],
           newIsPublic: Boolean)(implicit context: Context) = this(
    newSenderName,
    newFrontPhotoId,
    newBackPhotoId,
    new Date().getTime,
    newIsPublic
  );

  def this(newSenderName: String,
           newFrontPhotoId: Long,
           newBackPhotoId: Option[Long])(implicit context: Context) = this(
    newSenderName,
    newFrontPhotoId,
    newBackPhotoId,
    new Date().getTime,
    Card.defaultIsPublicValue
  )

  def setDimension(newDimension: Dimension.Value): Card = new PeopleCard(
    orderNumber = orderNumber,
    senderName = senderName,
    frontPhotoId = frontPhotoId,
    backPhotoId = backPhotoId,
    timeCreated = timeCreated,
    isPublic = isPublic,
    id = id,
    currentDimension = newDimension
  )

  /**
   * Setting it to null is not currently supported
   */
  def setOrderNumber(newOrderNumber: Int)(implicit context: Context): PeopleCard = {
    //update the row of the card model
    val values = generateContentValues;
    val key = PeopleCard.cols.orderNumber.toString;
    //just to be sure
    values.remove(key);
    values.put(key, new java.lang.Integer(newOrderNumber));

    val tableName = PeopleCard.TABLE_NAME;

    val idCol = PeopleCard.cols.cardId.toString;

    (new DatabaseHandler).updateById(tableName, id.get, values, idCol);

    val modelOption = getById(id.get);
    assert(modelOption.isDefined, "we just updated the row! the model must still be there");
    modelOption.get;
  }

  def getNext(implicit context: Context): Option[PeopleCard] = {
    (new DatabaseHandler).getModelByQuery(
      """SELECT * FROM
        |card, peoplecard
        |WHERE cardId = id
        |AND orderNumber < ?
        |ORDER BY orderNumber DESC
        |LIMIT 1""".stripMargin, Array[String](orderNumber.toString))(PeopleCard.createFromCursor);
  }

  def getPrev(implicit context: Context): Option[PeopleCard] = {
    (new DatabaseHandler).getModelByQuery(
      """SELECT * FROM
        |card, peoplecard
        |WHERE cardId = id
        |AND orderNumber > ?
        |ORDER BY orderNumber
        |LIMIT 1""".stripMargin, Array[String](orderNumber.toString))(PeopleCard.createFromCursor);
  }

  def insertMetaData(map: Map[String, String])(implicit context: Context): Unit = {
    require(isOld)

    require(getMetaValues.isEmpty)

    map foreach {
      case (label, value) =>
        val metaLabel = PeopleMetaLabel.insertOrRetrieve(label)

        assert(
          new PeopleMetaValue(cardId = id.get, labelId = metaLabel.id.get, dataValue = value).insert
            != MySQLiteOpenHelper.invalidInsertId,
          "if this fails means you tried to re-insert metadata for an existing card"
        )
    }
  }

  override val TABLE_NAME = PeopleCard.TABLE_NAME

  def getById(modelId: Long)(implicit context: Context): Option[PeopleCard] = PeopleCard.getById(modelId);

  def getDummyFrontBitmap(targetSize: ImageSizeDp)(implicit context: Context): Bitmap = {
    BitmapHelper.decodeOptimalSampledBitmapFromResource(
      R.drawable.dummy_peoplecard_front,
      targetSize
    )
  }

  def getDummyBackBitmap(targetSize: ImageSizeDp)(implicit context: Context) = {
    BitmapHelper.decodeOptimalSampledBitmapFromResource(
      R.drawable.dummy_peoplecard_back,
      targetSize
    )
  }

  override def insert(implicit context: Context): Long = {
    require(isNew);
    val parentId = (new DatabaseHandler).insert(Card.TABLE_NAME, super.generateContentValues);

    val values = generateContentValues;
    values.put(PeopleCard.cols.cardId.toString, new java.lang.Long(parentId));

    val childId = (new DatabaseHandler).insert(TABLE_NAME, values);

    assert(parentId == childId, "the id of one table must be the same as the id of the other table");
    childId;
  }

  override protected def generateContentValues: ContentValues = {
    val contentValues = new ContentValues();
    contentValues.put(PeopleCard.cols.orderNumber.toString, new java.lang.Integer(orderNumber));
    contentValues.put(PeopleCard.cols.senderName.toString, senderName);
    contentValues;
  }

  val ID_COLUMN_NAME: String = PeopleCard.ID_COLUMN_NAME;

  override def isIdAutoIncrement(implicit context: Context): Boolean = new DatabaseHandler().isAutoIncrement(Card.TABLE_NAME).get;
}
