package models

import spray.json.DefaultJsonProtocol._

object IdentificationTokenPair {
  implicit val itsJsonFormat = jsonFormat[String, String, IdentificationTokenPair](
    IdentificationTokenPair.apply, "identification", "token"
  )
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case class IdentificationTokenPair(identification: String, token: String) {

}
