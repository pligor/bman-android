package models

object CardTransaction {
  val INVALID_BUFFER_BYTE_LENGTH = -1;

  /*object FileState extends Enumeration {
    val FRONT = Value;
    val BACK = Value;
    val CARD_END = Value;
  }*/
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class CardTransaction {
  /**
   * called if everything completes successfully
   */
  def end();

  /**
   * called to finish everything abruptly
   */
  def cancel();

  protected val INVALID_BUFFER_BYTE_LENGTH = CardTransaction.INVALID_BUFFER_BYTE_LENGTH;

  /*private var curFileState = CardTransaction.FileState.FRONT;

  def getCurFileState = curFileState;

  protected def advanceFileState() {
    curFileState = curFileState match {
      case CardTransaction.FileState.FRONT => {
        CardTransaction.FileState.BACK;
      }
      case CardTransaction.FileState.BACK => {
        CardTransaction.FileState.CARD_END;
      }
      case CardTransaction.FileState.CARD_END => {
        CardTransaction.FileState.CARD_END;
      }
    }
  }*/
}
