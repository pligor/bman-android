package models

import bluetooth.MyBluetoothDeviceWrapper
import components.ByteSize

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */


object MyBluetoothProtocolState extends Enumeration {
  val FIRST_PING: BytesVal = Value(Some(MyBluetoothDeviceWrapper.pingSize));
  val PONG: BytesVal = Value(Some(MyBluetoothDeviceWrapper.pingSize));

  val SLAVE_RECEIVE_REASON: BytesVal = Value(Some(ByteSize.Int));

  val MASTER_CONFIRM_FILE_SEND: BytesVal = Value(Some(ByteSize.Boolean), withTimeout = false);

  val SLAVE_RECEIVE_FILE_LEN: BytesVal = Value(Some(ByteSize.Long));

  val MASTER_FILE_LEN_ACCEPTANCE: BytesVal = Value(Some(ByteSize.Boolean));

  val SLAVE_RECEIVE_BUFFER_LEN: BytesVal = Value(Some(ByteSize.Int));

  val MASTER_SEND_BUFFER_DATA: BytesVal = Value(Some(ByteSize.Boolean));

  //dynamically get data here
  val SLAVE_RECEIVE_BUFFER_DATA: BytesVal = Value(None);

  val MASTER_GET_BUFFER_HASH: BytesVal = Value(Some(ByteSize.MD5));

  val SLAVE_RECEIVE_HASH_CONFIRMATION: BytesVal = Value(Some(ByteSize.Boolean));

  val MASTER_SEND_BUFFER_LEN: BytesVal = Value(Some(ByteSize.Boolean));

  val MASTER_FILE_SENT: BytesVal = Value(Some(ByteSize.Boolean));

  val MASTER_SEND_FILE_COMPLETED: BytesVal = Value(Some(ByteSize.Boolean));

  //here we are not expecting bytes but... we are expecting from master to close the connection
  //so it might be valid to wait for some amount of time and then close connection if master does not do so
  //but you can always just click the back button and enforce that. so my extra functionality is redundant
  val SLAVE_RECEIVE_FILE_COMPLETED: BytesVal = Value(Some(0));

  //FAULT CASES
  val MASTER_ERROR_FILE_EMPTY: BytesVal = Value(Some(ByteSize.Boolean));

  val SLAVE_DENY_FILE: BytesVal = Value(Some(0));

  val SLAVE_FILE_LEN_INVALID: BytesVal = Value(Some(0));

  val SLAVE_INVALID_FILE_BYTES_RECEIVED: BytesVal = Value(Some(0));

  val MASTER_SLAVE_CONNECTION_TIMEOUT = Value(Some(0));

  val SLAVE_INVALID_REASON = Value(Some(0));

  /**
   * @param expectedBytes when defined means it is known, otherwise it is unknown
   * @return
   */
  private def Value(expectedBytes: Option[Int], withTimeout: Boolean = true): BytesVal = {
    new BytesVal(expectedBytes, withTimeout);
  }

  class BytesVal(val expectedBytes: Option[Int], val withTimeout: Boolean) extends Val;
}
