package models

import java.io.File
import generic.FileHelper
import FileHelper._
import android.content.{Context, ContentValues}
import com.pligor.bman.R
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object TablePopulator {
  def apply(insert: (String, ContentValues) => Long)(implicit context: Context): Unit = {
    //ERASE ALL FILES OF CARD IMAGES
    val bmanFolder = new File(Photo.PHOTO_DIRECTORY)

    clearDirSafe(bmanFolder)

    //PGP KEYS
    log log "counting pgp secret we get: " + PgpSecret.getCount

    val pgpSecretOption = PgpSecret.getFirst

    assert(pgpSecretOption.isDefined,
      "we should call the populator only after the first key pair is created when the user enters his name for the first time")

    val pgpKeyId = pgpSecretOption.get.id.get

    //CREATE SAMPLE PHOTOS AND FILES

    //.get is valid because remember that creating photos via input stream always succeed (returns true)

    {
      val myFrontPhoto = Photo.create(
        context.getResources.openRawResource(R.raw.my_sample_front)
      ).get

      val myBackPhoto = Photo.create(
        context.getResources.openRawResource(R.raw.my_sample_back)
      ).get

      val myCard = MyCard.create(myFrontPhoto.id.get, myBackPhoto.id)

      val sample_my_label_address = new MyMetaLabel(
        label = context.getResources.getString(R.string.sample_my_label_address)
      ).insert

      val sample_my_label_cell = new MyMetaLabel(
        label = context.getResources.getString(R.string.sample_my_label_cell)
      ).insert

      val sample_my_label_email = new MyMetaLabel(
        label = context.getResources.getString(R.string.sample_my_label_email)
      ).insert

      val sample_my_label_website = new MyMetaLabel(
        label = context.getResources.getString(R.string.sample_my_label_website)
      ).insert

      val sample_my_label_fb = new MyMetaLabel(
        label = context.getResources.getString(R.string.sample_my_label_fb)
      ).insert

      new MyMetaValue(
        cardId = myCard.id.get, labelId = sample_my_label_address, dataValue = "St Nearby 2"
      ).insert

      new MyMetaValue(
        cardId = myCard.id.get, labelId = sample_my_label_cell, dataValue = "+1 424 543 7260"
      ).insert

      new MyMetaValue(
        cardId = myCard.id.get, labelId = sample_my_label_email, dataValue = "creator@bman.co"
      ).insert

      new MyMetaValue(
        cardId = myCard.id.get, labelId = sample_my_label_website, dataValue = "http://bman.co"
      ).insert

      new MyMetaValue(
        cardId = myCard.id.get, labelId = sample_my_label_fb, dataValue = "https://www.facebook.com/pages/Bman/345360278932641"
      ).insert
    }

    val sample_people_label_address = new PeopleMetaLabel(
      label = "address"
    ).insert

    val sample_people_label_Address = new PeopleMetaLabel(
      label = "Address"
    ).insert

    val sample_people_label_TEL = new PeopleMetaLabel(
      label = "TEL"
    ).insert

    val sample_people_label_Tel = new PeopleMetaLabel(
      label = "Tel"
    ).insert

    val sample_people_label_FAX = new PeopleMetaLabel(
      label = "FAX"
    ).insert

    val sample_people_label_fax = new PeopleMetaLabel(
      label = "fax"
    ).insert

    val sample_people_label_email = new PeopleMetaLabel(
      label = "email"
    ).insert

    val sample_people_label_Email = new PeopleMetaLabel(
      label = "Email"
    ).insert

    val sample_people_label_Website = new PeopleMetaLabel(
      label = "Website"
    ).insert

    val sample_people_label_Site = new PeopleMetaLabel(
      label = "Site"
    ).insert

    val sample_people_label_facebook = new PeopleMetaLabel(
      label = "facebook"
    ).insert

    val sample_people_label_twitter = new PeopleMetaLabel(
      label = "twitter"
    ).insert

    val sample_people_label_e = new PeopleMetaLabel(
      label = "e"
    ).insert

    val sample_people_label_tw = new PeopleMetaLabel(
      label = "tw"
    ).insert

    val sample_people_label_m = new PeopleMetaLabel(
      label = "m"
    ).insert

    {
      val receivedCardFoundation = PeopleCard.create(
        "FOUND.ATION",
        newFrontPhotoId = Photo.create(
          context.getResources.openRawResource(R.raw.foundation_front)
        ).get.id.get,
        newBackPhotoIdOption = Photo.create(
          context.getResources.openRawResource(R.raw.foundation_back)
        ).get.id,
        newIsPublic = true
      )

      new PeopleMetaValue(
        cardId = receivedCardFoundation.id.get, labelId = sample_people_label_address, dataValue = "EVRISTHEOS 2 & PEIRAIOS 123 118 54 KATO PETRALONA"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardFoundation.id.get, labelId = sample_people_label_TEL, dataValue = "+30 210 34 50 606"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardFoundation.id.get, labelId = sample_people_label_FAX, dataValue = "+30 210 34 22 939"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardFoundation.id.get, labelId = sample_people_label_email, dataValue = "info@thefoundation.gr"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardFoundation.id.get, labelId = sample_people_label_Website, dataValue = "http://www.thefoundation.gr"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardFoundation.id.get, labelId = sample_people_label_facebook, dataValue = "found.ationgr"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardFoundation.id.get, labelId = sample_people_label_twitter, dataValue = "@found_ationgr"
      ).insert
    }

    {
      val receivedCardOlgaAppsterdam = PeopleCard.create(
        "Olga Paraskevopoulou",
        newFrontPhotoId = Photo.create(
          context.getResources.openRawResource(R.raw.olga_appsterdam)
        ).get.id.get,
        newBackPhotoIdOption = None,
        newIsPublic = true
      )

      new PeopleMetaValue(
        cardId = receivedCardOlgaAppsterdam.id.get, labelId = sample_people_label_e, dataValue = "olga@appsterdam.rs"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardOlgaAppsterdam.id.get, labelId = sample_people_label_tw, dataValue = "@olmageddon"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardOlgaAppsterdam.id.get, labelId = sample_people_label_m, dataValue = "+31 634 877 600"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardOlgaAppsterdam.id.get, labelId = sample_people_label_address, dataValue = "Groenburgwal 38A 1011 HW Amsterdam"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardOlgaAppsterdam.id.get, labelId = sample_people_label_fax, dataValue = "+31 84 223 4087"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardOlgaAppsterdam.id.get, labelId = sample_people_label_address, dataValue = "http://www.appsterdam.rs"
      ).insert
    }

    {
      val receivedCardTheCube = PeopleCard.create(
        "The Cube - Stavros Messinis",
        newFrontPhotoId = Photo.create(
          context.getResources.openRawResource(R.raw.the_cube)
        ).get.id.get,
        newBackPhotoIdOption = None,
        newIsPublic = true
      )

      new PeopleMetaValue(
        cardId = receivedCardTheCube.id.get, labelId = sample_people_label_Email, dataValue = "Stavros@thecube.gr"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardTheCube.id.get, labelId = sample_people_label_Site, dataValue = "http://www.thecube.gr"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardTheCube.id.get, labelId = sample_people_label_Tel, dataValue = "+30 2130110500"
      ).insert

      new PeopleMetaValue(
        cardId = receivedCardTheCube.id.get, labelId = sample_people_label_Address, dataValue = "8, Kleisovis Str Athens 10677 Greece"
      ).insert
    }
  }
}
