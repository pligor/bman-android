package models

import com.pligor.bman.R
import android.content.Context
import components.ExtraLazy
import android.media.{AudioManager, SoundPool}
import android.media.SoundPool.OnLoadCompleteListener
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * We have all the sounds being lazy to not overload the memory
 * In addition because some already loaded sounds might be useless to the next activity
 * on each start of a new activity the sounds are being initialized.
 * Obviously there is a connection between sounds and an activity since all actions (and sounds) happen at foreground
 * An occasion where a sound must play on the background when the app is not loaded is not yet supported
 */
object SoundWrapper {
  val soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);

  def apply(): Sounds = {
    assert(sounds.isDefined);
    sounds.get;
  }

  def init(implicit context: Context): Unit = {
    sounds = Some(new Sounds);
  }

  private var sounds: Option[Sounds] = None;

  protected class Sounds(implicit context: Context) {
    protected class LazySoundClip(notYetLoadedClip: => Int) extends ExtraLazy[Int](notYetLoadedClip) {
      def play(): Unit = {
        if (isSet) {
          soundPool.play(value, 1, 1, 0, 0, 1);
          log log "synchronous play";
        } else {
          soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener {
            def onLoadComplete(sound_pool: SoundPool, sampleId: Int, status: Int): Unit = {
              assert(status == 0, "status of loading of sample sound actually is: " + status);
              sound_pool.play(sampleId, 1, 1, 0, 0, 1);
              log log "asynchronous play";
            }
          });
          value;
        }
      }
    }

    val quickSlideSound = new LazySoundClip({
      log log "get out of bed you lazy..";
      soundPool.load(context, R.raw.swap_sides, 1);
    });
  }
}
