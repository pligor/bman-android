package myAndroidAPIs

import android.app.Activity
import android.os.Bundle
import com.bugsense.trace.{ExceptionCallback, BugSenseHandler}
import models.MyBugsense
import components.DebuggableTrait

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * TRY USING THIS TRAIT AS "RIGHT" AS POSSIBLE because if it is left the right traits will take precedence
 *
 * custom dialogs on crash: http://blog.bugsense.com/post/58773101249/bugsense-3-5-helpshift-magical-product-insight
 */
trait BugsenseActivity extends Activity with DebuggableTrait {
  private def enableBugsense = !isDebuggable;

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);
    if (enableBugsense) {
      BugSenseHandler.initAndStartSession(this, MyBugsense.API_KEY);
      //log log "on bugsense initialization";

      /*BugSenseHandler.setExceptionCallback(new ExceptionCallback {
        def lastBreath(exception: Exception) {
          Logger("here we see something after it crashes. exception msg: " + exception.getMessage);

          Toast.makeText(getApplicationContext, "yes we see after crash", Toast.LENGTH_LONG).show();
          val builder = new AlertDialog.Builder(getApplicationContext);
          builder.setTitle("title here");
          builder.setMessage("message here");
          builder.setNeutralButton("button", new OnClickListener {
            def onClick(p1: DialogInterface, p2: Int) {
              Logger("clicked");
            }
          });
          builder.create().show();

          startActivity(new Intent(BugsenseActivity.this, classOf[MyCardsActivity]));

          Logger("exoume ftase edwi");
        }
      });*/
    } else {
      //log log "we do NOT enable bugsense on debuggable version of the apk";
    }
  }

  override def onStop() {
    super.onStop();
    if (enableBugsense) {
      BugSenseHandler.flush(this);
    } else {
      //log log "we do not need to flush anything";
    }
  }

  /*override def onDestroy() {
    super.onDestroy();
    BugSenseHandler.closeSession(this);
  }*/
}