package fragments

import android.os.Bundle
import models.{PeopleCard, MyCard}
import android.content.Context
import android.support.v4.app.Fragment
import android.view.{View, ViewGroup, LayoutInflater}
import com.pligor.bman.R
import views.CardWidgetLayout
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
/**
 * a hack (not currently used) to make notify data set changed work
 * http://stackoverflow.com/questions/7263291/viewpager-pageradapter-not-updating-the-view/7287121#7287121
 *
 * yet another not suitable solution: http://stackoverflow.com/questions/13664155/dynamically-add-and-remove-view-to-viewpager
 *
 * IF EVERYTHING FAILS USE THIS: https://github.com/commonsguy/cwac-pager (commons ware, commonsware)
 */
object PeopleCardFragment {
  private final val EXTRA_PEOPLE_CARD_ID = "EXTRA_PEOPLE_CARD_ID"

  def newInstance(): PeopleCardFragment = newInstance(PeopleCard.invalidId)(null)

  def newInstance(cardId: Long)(implicit context: Context): PeopleCardFragment = {
    val fragment = new PeopleCardFragment

    fragment.context = context

    fragment.setArguments({
      val bundle = new Bundle(1)

      bundle.putLong(EXTRA_PEOPLE_CARD_ID, cardId)

      bundle
    })

    fragment
  }
}


class PeopleCardFragment extends Fragment {
  implicit private var context: Context = _

  def getCardId = getArguments.getLong(PeopleCardFragment.EXTRA_PEOPLE_CARD_ID)

  override def onCreateView(inflater: LayoutInflater,
                            container: ViewGroup,
                            savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)

    val cardWidgetLayout = inflater.inflate(R.layout.card_widget, container, false).asInstanceOf[CardWidgetLayout]

    def renderCard(): Unit = {
      val cardId = getCardId

      val cardOption = PeopleCard.getById(cardId)

      val isDefined = cardOption.isDefined //cardId != 9 //Random.nextBoolean()

      log log s"this view with id $cardId is going to be " + (if (isDefined) "visible" else "invisible")

      /*for(i <- 0 to (parentPageAdapter.getCount - 1)) {
        if(parentPageAdapter.getItem(i) == this)
          log log s"position is $i"
        else {
          log log "position is not found as expected"
        }
      }*/

      if (isDefined) {
        cardWidgetLayout.setCard(cardOption.get)
      } else {
        //parentPageAdapter.remove(cardId)
        //parentPageAdapter.map(_.remove(this))
      }
    }

    if (cardWidgetLayout.init(renderCard())) {
      //render a first card after view is ready. nothing more to be done here
    } else {
      renderCard() //render the card if the view is already initialized
    }

    cardWidgetLayout
  }

}


