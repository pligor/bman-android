package fragments

import android.support.v4.app.Fragment
import android.view.{View, ViewGroup, LayoutInflater}
import android.os.Bundle
import com.pligor.bman.R
import views.CardWidgetLayout
import models.MyCard
import myandroid.log
import adapters.page.MyCardPageAdapter
import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
/**
 * a hack (not currently used) to make notify data set changed work
 * http://stackoverflow.com/questions/7263291/viewpager-pageradapter-not-updating-the-view/7287121#7287121
 *
 * yet another not suitable solution: http://stackoverflow.com/questions/13664155/dynamically-add-and-remove-view-to-viewpager
 *
 * IF EVERYTHING FAILS USE THIS: https://github.com/commonsguy/cwac-pager (commons ware, commonsware)
 */
object MyCardFragment {
  private final val EXTRA_CARD_ID = "EXTRA_CARD_ID"

  def newInstance(): MyCardFragment = newInstance(MyCard.invalidId, null)

  def newInstance(cardId: Long, context: Context): MyCardFragment = {
    val myfragment = new MyCardFragment

    myfragment.context = context  //even if is private this works as a setContext

    myfragment.setArguments({
      val bundle = new Bundle(1)

      bundle.putLong(EXTRA_CARD_ID, cardId)

      bundle
    })

    myfragment
  }
}

class MyCardFragment extends Fragment {
  implicit private var context: Context = _

  def getCardId = getArguments.getLong(MyCardFragment.EXTRA_CARD_ID)

  override def onCreateView(inflater: LayoutInflater,
                            container: ViewGroup,
                            savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)

    /*val view = inflater.inflate(R.layout.fragment_my_card_widget, container, false)
    val myCardWidgetLayout = view.findViewById(R.id.myCardWidgetLayout).asInstanceOf[CardWidgetLayout]*/
    val myCardWidgetLayout = inflater.inflate(R.layout.card_widget, container, false).asInstanceOf[CardWidgetLayout]

    def renderCard(): Unit = {
      val cardId = getCardId

      val myCardOption = MyCard.getById(cardId)

      val isDefined = myCardOption.isDefined //cardId != 9 //Random.nextBoolean()

      log log s"this view with id $cardId is going to be " + (if (isDefined) "visible" else "invisible")

      /*for(i <- 0 to (parentPageAdapter.getCount - 1)) {
        if(parentPageAdapter.getItem(i) == this)
          log log s"position is $i"
        else {
          log log "position is not found as expected"
        }
      }*/

      if (isDefined) {
        myCardWidgetLayout.setCard(myCardOption.get)
      } else {
        //parentPageAdapter.remove(cardId)
        //parentPageAdapter.map(_.remove(this))
      }
    }

    if (myCardWidgetLayout.init(renderCard())) {
      //render a first card after view is ready. nothing more to be done here
    } else {
      renderCard() //render the card if the view is already initialized
    }

    myCardWidgetLayout
  }

}
