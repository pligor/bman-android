package bluetooth

import android.os.Handler
import java.io.IOException
import android.bluetooth.BluetoothAdapter
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
private class AcceptThreadNoTimeout(private val secure: Boolean,
                                    private val handler: Handler)(implicit bluetoothAdapter: BluetoothAdapter)
  extends AcceptThread(secure, handler) {

  private var isCancelled = false

  def cancel(onCancel: () => Unit): Unit = {
    //remember that re-closing a server socket is safe. should not throw anything
    //but just to be sure we are not going to close it twice

    /*if (isCancelled) {
      Logger("server socket is already closed");
    } else {*/
    try {
      isCancelled = true

      bluetoothServerSocket.close()

      log log "closed a server socket"

      onCancel()
    } catch {
      case e: IOException => throw new ServerSocketNotClosedException
    }
    //}
  }

  override def run(): Unit = {
    super.run()

    try {
      //This is a blocking call and will only return on a successful connection or an exception, or on timeout
      val socket = bluetoothServerSocket.accept() //no time param means infinite wait

      connectionAccepted(socket)
    } catch {
      case e: IOException => {
        if (isCancelled) {
          log log "failed accepting socket because user cancelled it"
        } else {
          if (bluetoothAdapter.getState == BluetoothAdapter.STATE_ON) {
            throw new FailedAcceptingSocketException
          } else {
            log log "failed accepting socket because bluetooth is forcefully being turned off";
          }
        }
        isCancelled = false; //in case we do a re-run, but we wont do because threads are not working like that
      }
    } /*finally {
      cancel();
    }*/
  }
}
