package bluetooth

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
private class SocketNotClosedException extends Exception;

private class SocketNotCreatedException extends Exception;

private class FailedListeningServerSocketException extends Exception;

private class ServerSocketNotClosedException extends Exception;

private class FailedAcceptingSocketException extends Exception;

private class CouldNotGetInputStreamException extends Exception;

private class CouldNotGetOutputStreamException extends Exception;

private class FailedToWriteBytesException extends Exception;

private class FailedToReadBytesException extends Exception;
