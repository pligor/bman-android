package bluetooth

import android.bluetooth.{BluetoothAdapter, BluetoothDevice}
import java.io.IOException
import android.os.Handler
import BluetoothHelper._
import com.pligor.bman.{CommunicationReason, Bman}

object ConnectThreadMessage extends Enumeration {
  val CONNECTION_FAILED = Value;
  val CONNECTION_SUCCEEDED = Value;
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

class ConnectThread(private val device: BluetoothDevice,
                    private val secure: Boolean,
                    private val reason: CommunicationReason.Value,
                    private val bufferLenGetter: BufferLenGetter
                     ) extends Thread with BluetoothThread {
  require(!secure, "currently we are only working with insecure connections");

  def setHandler(connectHandler: Handler) {
    handler = Some(connectHandler);
  }

  private var handler: Option[Handler] = None;

  private val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter;

  private val name = (if (secure) "Secure" else "Insecure") + "ConnectThread" + device.getAddress;

  private val bluetoothSocket = try {
    if (secure) {
      device.createRfcommSocketToServiceRecord(Bman.MY_UUID_SECURE);
    } else {
      device.createInsecureRfcommSocketToServiceRecord(Bman.MY_UUID_INSECURE);
    }
    /*we did a test and we managed to create one hundred sockets with a single device
      without closing any of them. this means that probably under normal circumstances
      the above statement should succeed*/
  }
  catch {
    case e: IOException => throw new SocketNotCreatedException;
  } /*)*/ ;

  def cancel() {
    try {
      bluetoothSocket.close();
    }
    catch {
      case e: IOException => throw new SocketNotClosedException;
    }
  }

  override def run() {
    super.run();
    setName(name);

    //cancel discovery because it will slow down your connection
    safeCancelDiscovery(bluetoothAdapter);

    try {
      //This is a blocking call and will only return on a successful connection or an exception
      bluetoothSocket.connect();

      connectionSucceeded();
    }
    catch {
      case e: IOException => {
        cancel();
        connectionFailed();
      }
    }
  }

  private def connectionSucceeded() {
    assert(handler.isDefined);
    val msg = handler.get.obtainMessage(ConnectThreadMessage.CONNECTION_SUCCEEDED.id);
    msg.arg1 = reason.id;
    msg.obj = SocketBufferLenGiver(socket = bluetoothSocket, getter = bufferLenGetter);
    msg.sendToTarget();
  }

  private def connectionFailed() {
    assert(handler.isDefined);
    handler.get.sendEmptyMessage(ConnectThreadMessage.CONNECTION_FAILED.id);
  }
}
