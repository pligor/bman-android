package bluetooth

import java.io.IOException
import android.os.Handler
import models.MyBluetoothProtocolState
import android.bluetooth.BluetoothSocket
import myandroid.log

object ConnectedThreadMessage extends Enumeration {
  val BYTES_READ = Value;
  val BYTES_WRITTEN = Value;
  val CONNECTION_STOPPED = Value;
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class ConnectedThread(private val socket: /*CloseOnce*/ BluetoothSocket,
                      private val bufferLenGetter: BufferLenGetter,
                      /*private*/ var state: MyBluetoothProtocolState.BytesVal)
  extends Thread
  with BluetoothThread {
  /**
   * BE AWARE: getInputStream and getOutputStream methods must be executed only once on a connected socket
   */
  def setHandler(connectedHandler: Handler) {
    handler = Some(connectedHandler);
  }

  private var handler: Option[Handler] = None;

  /*def setState(newState: MyBluetoothProtocolState.BytesVal): ConnectedThread = {
    state = newState;
    this;
  }*/

  private val inputStream = try {
    socket.getInputStream;
  } catch {
    case e: IOException => throw new CouldNotGetInputStreamException;
  }

  private val outputStream = try {
    socket.getOutputStream;
  } catch {
    case e: IOException => throw new CouldNotGetOutputStreamException;
  }

  private var connectionValid = true;

  override def run() {
    super.run();
    //cannot use recursive function because of stack overflow !!! damn!!!
    //def readBytesUntilLoseConnection() {
    while (connectionValid) {
      connectionValid = try {
        val buffer = new Array[Byte](bufferLenGetter.getCurrentBufferBytesLen);
        val noOfBytesReceived = inputStream.read(buffer);
        assert(noOfBytesReceived > 0, "number of bytes received (" + noOfBytesReceived + ") cannot be negative! and must not be zero");
        assert(handler.isDefined);
        val msg = handler.get.obtainMessage(ConnectedThreadMessage.BYTES_READ.id);
        msg.arg1 = noOfBytesReceived;
        msg.arg2 = state.id;
        msg.obj = buffer;
        msg.sendToTarget();

        //readBytesUntilLoseConnection();
        true;
      } catch {
        case e: IOException => {
          log log "connection is lost";

          val msg = handler.get.obtainMessage(ConnectedThreadMessage.CONNECTION_STOPPED.id);
          msg.arg1 = state.id;
          msg.sendToTarget();

          false;
        }
      }
    }
    //}
    //readBytesUntilLoseConnection();
  }

  def write(buffer: Int) {
    if (connectionValid) {
      try {
        outputStream.write(buffer);
      } catch {
        case e: IOException => throw new FailedToWriteBytesException;
      }
    }
  }

  def write(buffer: Array[Byte]) {
    if (connectionValid) {
      try {
        outputStream.write(buffer);
        //we can send a message to handler that bytes have been written if necessary
      } catch {
        case e: IOException => throw new FailedToWriteBytesException;
      }
    }
  }

  def cancel() {
    try {
      socket.close();
    } catch {
      case e: IOException => throw new SocketNotClosedException;
    }
  }
}
