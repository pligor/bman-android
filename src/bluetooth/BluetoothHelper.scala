package bluetooth

import android.bluetooth.{BluetoothAdapter, BluetoothDevice}
import android.os.ParcelUuid
import scala.collection.JavaConversions._
import android.content.Context
import myandroid.AndroidHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object BluetoothHelper {
  def getBluetoothAddress(implicit context: Context): Option[String] = {
    getDefaultBluetoothAdapter.flatMap(adapter => Option(adapter.getAddress));
  }

  def safelyWriteBluetoothName(name: String,
                               bluetoothAdapterOption: Option[BluetoothAdapter])(implicit context: Context): Unit = {
    if (bluetoothAdapterOption.isDefined && bluetoothAdapterOption.get.isEnabled) {
      bluetoothAdapterOption.get.setName(name);
    }
  }

  def isBluetoothAvailable(implicit bluetoothAdapter: Option[BluetoothAdapter]): Boolean = {
    isBluetoothSupported && bluetoothAdapter.get.isEnabled;
  }

  def getBluetoothPairedDevices(implicit bluetoothAdapter: BluetoothAdapter): Set[BluetoothDevice] = {
    bluetoothAdapter.getBondedDevices.toSet
  }

  def getDefaultBluetoothAdapter = Option(BluetoothAdapter.getDefaultAdapter)

  /**
   * 700Kbits / sec >= 80Kbytes/sec
   * also have a lower bound
   */
  def bytesToBluetoothTimeoutMsecs(bytes: Int): Long = {
    val correctionFactor = 10  //make it larger so to give more tolerance

    val eightyKbytes: Long = 80 * 1024

    val msecs = correctionFactor * math.round(1000.0 * (bytes.toDouble / eightyKbytes.toDouble))

    math.max(msecs, 1500L)
  }

  def isDevicePaired(device: BluetoothDevice) = {
    require(device != null, "you have used a null object as a bluetooth device")

    device.getBondState == BluetoothDevice.BOND_BONDED
  }

  def forceDeviceDiscovery(bluetoothAdapter: BluetoothAdapter): Boolean = {
    require(Option(bluetoothAdapter).isDefined,
      "you have used a null object as a bluetooth adapter")

    // If we're already discovering, stop it
    if (bluetoothAdapter.isDiscovering) {
      bluetoothAdapter.cancelDiscovery
    }

    bluetoothAdapter.startDiscovery()
  }

  def safeDeviceDiscovery(bluetoothAdapter: BluetoothAdapter): Unit = {
    // If we're not already discovering, only then start
    if (!bluetoothAdapter.isDiscovering) {
      val startedDiscovery = bluetoothAdapter.startDiscovery()

      assert(startedDiscovery, "current bluetooth adapter state: " +
        getBluetoothAdapterStateString(bluetoothAdapter)
      )
    }
  }

  /**
   * @return is cancelled (but can only be cancelled if the discovery was on
   */
  def safeCancelDiscovery(bluetoothAdapter: BluetoothAdapter): Boolean = {
    bluetoothAdapter.isDiscovering && bluetoothAdapter.cancelDiscovery()
  }

  def getUuidsFromDevice(device: BluetoothDevice): Option[Array[ParcelUuid]] = {
    val methodName = "getUuids"

    val cl = Class.forName("android.bluetooth.BluetoothDevice")

    /*val par = Array.empty[Class[_]];
    val method = cl.getMethod(methodName, par: _ *);*/
    val method = cl.getMethod(methodName)

    /*val args = Array.empty[AnyRef];
    method.invoke(device, args: _ *).asInstanceOf[Array[ParcelUuid]];*/
    val parcelUuids = method.invoke(device).asInstanceOf[Array[ParcelUuid]]

    if (parcelUuids == null) {
      None
    } else {
      Some(parcelUuids)
    }

    //device.getClass.getMethod(methodName, null).invoke(device, null).asInstanceOf[Array[ParcelUuid]];
    //Class.forName("android.bluetooth.BluetoothDevice").getMethod(methodName, null).invoke(device, null).asInstanceOf[Array[ParcelUuid]];
  }

  /*
  registerReceiver(uuidReceiver, new IntentFilter(Bluetooth.DEVICE_ACTION_UUID));
  safeUnregisterReceiver(uuidReceiver);
  private object uuidReceiver extends BroadcastReceiver {
    def onReceive(context: Context, intent: Intent) {
      intent.getAction match {
        case Bluetooth.DEVICE_ACTION_UUID => {
          if (myDev.answerExpected) {
            myDev.recordAndDelay();

            val deviceExtra: BluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            //Logger("dev name: " + deviceExtra.getName);

            val parcelUuids: Array[ParcelUuid] = intent.getParcelableArrayExtra(Bluetooth.DEVICE_EXTRA_UUID).map(_.asInstanceOf[ParcelUuid]);
            //parcelUuids.foreach(parcelUuid => Logger("uuid: " + parcelUuid.getUuid.toString));
            //Logger("parcelUuids.length: " + parcelUuids.length);
          }
          else {
            Logger("uuids ignored");
          }
        }
      }
    }
  }
  */
  def fetchUuidsWithSdpFromDevice(device: BluetoothDevice): Boolean = {
    val cl = Class.forName("android.bluetooth.BluetoothDevice")

    val method = cl.getMethod("fetchUuidsWithSdp")

    method.invoke(device).asInstanceOf[Boolean]
  }

  private def getBluetoothAdapterStateString(bluetoothAdapter: BluetoothAdapter): String = {
    bluetoothAdapter.getState match {
      case BluetoothAdapter.STATE_CONNECTED => "STATE_CONNECTED"
      case BluetoothAdapter.STATE_CONNECTING => "STATE_CONNECTING"
      case BluetoothAdapter.STATE_DISCONNECTED => "STATE_DISCONNECTED"
      case BluetoothAdapter.STATE_DISCONNECTING => "STATE_DISCONNECTING"
      case BluetoothAdapter.STATE_OFF => "STATE_OFF"
      case BluetoothAdapter.STATE_ON => "STATE_ON"
      case BluetoothAdapter.STATE_TURNING_OFF => "STATE_TURNING_OFF"
      case BluetoothAdapter.STATE_TURNING_ON => "STATE_TURNING_ON"
    }
  }
}
