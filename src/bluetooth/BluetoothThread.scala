package bluetooth

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait BluetoothThread {
  def cancel(): Unit;
}
