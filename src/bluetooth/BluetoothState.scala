package bluetooth

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * Constants that indicate the current connection state
 */
object BluetoothState extends Enumeration {
  val NONE = Value; //doing nothing
  val LISTEN = Value; //listening for incoming connections
  val CONNECTING = Value; //initiating an outgoing connection
  val CONNECTED = Value; //connected to a remote device
}
