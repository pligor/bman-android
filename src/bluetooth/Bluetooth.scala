package bluetooth

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object Bluetooth {
  //http://wiresareobsolete.com/wordpress/2010/11/android-bluetooth-rfcomm/

  //SOME DEVICES AND/OR SOME ANDROID VERSIONS SUPPORT THE STRAIGHT ONE OR THE REVERSED ONE
  val DEVICE_ACTION_UUID_STRAIGHT = "android.bleutooth.device.action.UUID";
  val DEVICE_ACTION_UUID_REVERSED = "android.bluetooth.device.action.UUID";

  val DEVICE_EXTRA_UUID = "android.bluetooth.device.extra.UUID";
}
