package bluetooth

import android.os.Handler
import java.io.IOException
import components.WriteOnce
import android.bluetooth.BluetoothAdapter
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
private class AcceptThreadWithTimeout(private val secure: Boolean,
                              private val handler: Handler)(implicit bluetoothAdapter: BluetoothAdapter)
  extends AcceptThread(secure, handler) {

  private val fixedTimeoutMsecs = 10 * 1000;

  private val cancelEvent = new WriteOnce[() => Unit];

  def cancel(onCancel: () => Unit) {
    //toContinue = false;
    cancelEvent setValue onCancel;
  }

  override def run() {
    super.run();

    while (
      try {
        //This is a blocking call and will only return on a successful connection or an exception, or on timeout
        val socket = bluetoothServerSocket.accept(fixedTimeoutMsecs); //msec
        connectionAccepted(socket);
        false;
      } catch {
        case e: IOException => {
          log log "accepting timed out";
          if (cancelEvent.isInitialized) {
            log log "accepting canceled";
            cancelEvent().apply();
            false;
          } else {
            log log "continue accepting";
            true;
          }
        }
      }
    ) {}
  }
}
