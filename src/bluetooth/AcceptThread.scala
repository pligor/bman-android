package bluetooth

import android.bluetooth.{BluetoothSocket, BluetoothAdapter, BluetoothServerSocket}
import java.io.IOException
import java.util.UUID
import android.os.{Bundle, Handler}
import com.pligor.bman.Bman
import scala.Enumeration
import myandroid.log

object AcceptThread {

  def apply(handler: Handler, timeout: Boolean)(implicit bluetoothAdapter: BluetoothAdapter): Option[AcceptThread] = {
    try {
      Some(
      if(timeout) {
        new AcceptThreadWithTimeout(secure = false, handler);
      } else {
        new AcceptThreadNoTimeout(secure = false, handler);
      }
      );
    } catch {
      case e: FailedListeningServerSocketException => None;
    }
  }

  object MESSAGE extends Enumeration {
    val ACCEPTED = Value;
    val START_ACCEPTING = Value;
  }

  object EXTRA extends Enumeration {
    val ACCEPT_THREAD_NAME = Value;
  }

}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * IT IS VERIFIED THAT WHEN WE CLOSE THE SERVER SOCKET
 * THE BLUETOOTH SOCKET CREATED FROM THE SERVER SOCKET IS NOT LOST!
 * So we can safely cancel the thread but the socket of a successful connection remains as is
 */
abstract class AcceptThread(private val secure: Boolean,
                            private val handler: Handler)(implicit bluetoothAdapter: BluetoothAdapter) extends Thread {
  require(!secure, "currently we are only working with insecure connections");

  //abstract

  def cancel(onCancel: () => Unit): Unit;

  //concrete

  val randomUniqueName = (if (secure) "Secure" else "Insecure") + "AcceptThread" + UUID.randomUUID();

  override def equals(other: Any): Boolean = {
    other match {
      case x: AcceptThread => this.randomUniqueName == x.randomUniqueName
      case _ => false;
    }
  }

  protected val bluetoothServerSocket: BluetoothServerSocket = try {
    if (secure) {
      bluetoothAdapter.listenUsingRfcommWithServiceRecord(Bman.MY_SERVICE_NAME_SECURE, Bman.MY_UUID_SECURE);
    } else {
      bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(Bman.MY_SERVICE_NAME_INSECURE, Bman.MY_UUID_INSECURE);
    }
  } catch {
    case e: IOException => throw new FailedListeningServerSocketException;
  }

  override def run() {
    super.run();
    setName(randomUniqueName);

    log log ("listening on a server socket on accepting thread: " + randomUniqueName);

    handler.sendMessage({
      val message = handler.obtainMessage(AcceptThread.MESSAGE.START_ACCEPTING.id);
      message.setData({
        val bundle = new Bundle;
        bundle.putString(AcceptThread.EXTRA.ACCEPT_THREAD_NAME.toString, randomUniqueName);
        bundle;
      });
      message;
    });
  }

  protected def connectionAccepted(socket: /*CloseOnce*/ BluetoothSocket) {
    val message = handler.obtainMessage(AcceptThread.MESSAGE.ACCEPTED.id, socket);

    message.setData({
      val bundle = new Bundle;
      bundle.putString(AcceptThread.EXTRA.ACCEPT_THREAD_NAME.toString, randomUniqueName);
      bundle
    });

    handler.sendMessage(message);
  }
}
