package bluetooth

import android.bluetooth.{BluetoothSocket, BluetoothDevice}
import android.os.{Message, Handler}
import models.{ReceiveBuffer, MyBluetoothProtocolState}
import components._
import ByteHelper._
import BluetoothHelper._
import com.pligor.bman.CommunicationReason
import myandroid.log

class MyBluetoothDeviceWrapperEqualityException extends Exception

class InvalidDeviceStateException extends Exception

//you could actually get rid of this object and the corresponding method because nobody is using them
object MyBluetoothDeviceState extends Enumeration {
  val NO_CONNECTION = Value

  val CONNECTING = Value

  val CONNECTED = Value
}

object MyBluetoothDeviceWrapper {
  val pingSize = 1024
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * NO we cannot have a listening thread here because a listening thread is not attached to one
 * device. A listening thread just listens for a uuid and accepts. Meaning that we must have
 * one listening thread which when accepts the connection does not end! Keeps listening for a new
 * connection!
 */
class MyBluetoothDeviceWrapper(
                                val bluetoothDevice: BluetoothDevice,
                                val parentConnectHandler: Option[Handler],
                                val parentConnectedHandler: Option[Handler],
                                private val connectThread: Option[ConnectThread],
                                private val connectedThread: Option[ConnectedThread]
                                ) {
  require(
    !(connectThread.isDefined && connectedThread.isDefined),
    "may not have the two threads, connecting and connected, run simultaneously"
  );

  private var receiveBuffer: Option[ReceiveBuffer] = None;

  if (connectThread.isDefined) {
    connectThread.get.setHandler(connectHandler);
    connectThread.get.start();
  }

  if (connectedThread.isDefined) {
    connectedThread.get.setHandler(connectedHandler);
    connectedThread.get.start();

    val curState = connectedThread.get.state;

    forceReWaitForBytes(curState.expectedBytes.getOrElse(0), curState.withTimeout);
  }

  //private object MyLatencyEstimator extends LatencyEstimator(230);

  private object connectedHandler extends Handler {
    private def handleCurrentState(whatMessage: Int, curState: MyBluetoothProtocolState.Value, bytes: Array[Byte]): Unit = {
      curState match {
        case MyBluetoothProtocolState.SLAVE_RECEIVE_REASON => {
          try {
            //this can throw exception
            val reason = CommunicationReason(ByteConv.arrayByte2int(bytes));

            reason match {
              case CommunicationReason.PING_PONG => {
                /*log log "other device wants to ping pong, I respond with first ping";
                setConnectedState(MyBluetoothProtocolState.PONG).sendBytes(pingPongSetup());*/
              }
              case CommunicationReason.SEND_CARD => {
                //propagate to parent
                assert(parentConnectedHandler.isDefined);
                val propagateMsg = parentConnectedHandler.get.obtainMessage(whatMessage);
                propagateMsg.obj = bytes;
                propagateMsg.arg1 = curState.id;
                propagateMsg.sendToTarget();
              }
            }
          } catch {
            case e: NoSuchElementException => {
              setConnectedState(MyBluetoothProtocolState.SLAVE_INVALID_REASON);
              cancelConnThreads();
            }
          }
        }
        case MyBluetoothProtocolState.FIRST_PING => {
          /*log log "I have received first ping";
          setConnectedState(MyBluetoothProtocolState.PONG).sendBytes(pingPongSetup());*/
        }
        case MyBluetoothProtocolState.PONG => {
          /*val latency = MyLatencyEstimator.insertLatency();
          log log "latency is: " + latency + " ms";

          if (MyLatencyEstimator.hasEnoughMeasurements) {
            log log ("THIS IS THE END OF PING PONG STORY with " + MyLatencyEstimator.getNoOfMeasurements + " measurements");
            connectedThread.get.cancel();
          }
          else {
            //no need to set a new connected state
            sendBytes(ping());
          }*/
        }
        case _ => {
          //propagate to parent
          assert(parentConnectedHandler.isDefined);
          val propagateMsg = parentConnectedHandler.get.obtainMessage(whatMessage);
          propagateMsg.obj = bytes;
          propagateMsg.arg1 = curState.id;
          propagateMsg.sendToTarget();
        }
      }
    }

    override def handleMessage(msg: Message) {
      super.handleMessage(msg);

      ConnectedThreadMessage(msg.what) match {
        case ConnectedThreadMessage.BYTES_READ => {
          val buffer = msg.obj.asInstanceOf[Array[Byte]];
          val noOfBytesReceived = msg.arg1;
          val curState = MyBluetoothProtocolState(msg.arg2);
          val activeBytes = buffer.take(noOfBytesReceived);

          log log ("we have received " + noOfBytesReceived + " bytes");
          receiveBuffer.get.appendBytes(activeBytes);

          if (receiveBuffer.get.isFull) {
            log log ("we have received the total of " + receiveBuffer.get.expectedLength + " bytes");

            val bytes = receiveBuffer.get.getBytes;
            receiveBuffer.get.cancel(true);
            receiveBuffer = None;

            handleCurrentState(msg.what, curState, bytes);
          }
        }
        case ConnectedThreadMessage.BYTES_WRITTEN => {
          //nothing yet
        }
        case ConnectedThreadMessage.CONNECTION_STOPPED => {
          val curState = MyBluetoothProtocolState(msg.arg1);

          //propagate to parent
          assert(parentConnectedHandler.isDefined);
          val propagateMsg = parentConnectedHandler.get.obtainMessage(msg.what);
          propagateMsg.arg1 = curState.id;
          propagateMsg.sendToTarget();
        }
      }
    }
  }

  def setConnectedState(newState: MyBluetoothProtocolState.BytesVal): MyBluetoothDeviceWrapper = {
    require(connectedThread.isDefined);
    //connectedThread.get.setState(newState);
    connectedThread.get.state = newState;
    this;
  }

  /*def sendByte(byte: Int) {
    assert(connectedThread.isDefined, "connected thread must have been created before writing");
    connectedThread.get.write(byte);
  }*/

  def sendBytes(bytes: Array[Byte], customExpectedBytes: Option[Int] = None) {
    assert(connectedThread.isDefined, "connected thread must have been created before writing");

    val thread = connectedThread.get;
    thread.write(bytes);

    val curState = thread.state;

    val expectedBytes = customExpectedBytes.getOrElse(
      curState.expectedBytes.getOrElse(0)
    );

    log log ("expected bytes are: " + expectedBytes);

    forceReWaitForBytes(expectedBytes, curState.withTimeout);
  }


  /**
   * @param withTimeout sometimes we wait as long as we can e.g. other user has a dialog to interact with
   */
  private def forceReWaitForBytes(expectedBytes: Int, withTimeout: Boolean): Unit = {
    require(expectedBytes >= 0)

    if (receiveBuffer.isDefined) {
      receiveBuffer.get.cancel(true)

      receiveBuffer = None
    }

    if (expectedBytes > 0) {
      receiveBuffer = Some(
        if (withTimeout) {
          new ReceiveBuffer(expectedBytes) {
            override protected def onAfterDelay(): Unit = {
              log log "we have reached the timeout"

              setConnectedState(MyBluetoothProtocolState.MASTER_SLAVE_CONNECTION_TIMEOUT)

              cancelConnThreads()
            }
          }
        } else {
          new ReceiveBuffer(expectedBytes)
        }
      )

      receiveBuffer.get.execute(bytesToBluetoothTimeoutMsecs(expectedBytes))
    }
  }

  private object connectHandler extends Handler {
    override def handleMessage(msg: Message): Unit = {
      super.handleMessage(msg)

      ConnectThreadMessage(msg.what) match {
        case ConnectThreadMessage.CONNECTION_FAILED => {
          //propagate to service
          if (parentConnectHandler.isDefined) {
            val propagateMsg = parentConnectHandler.get.obtainMessage(
              msg.what,
              cancelConnThreads()
            )

            propagateMsg.sendToTarget()
          }
        }
        case ConnectThreadMessage.CONNECTION_SUCCEEDED => {
          val SocketBufferLenGiver(socket, bufferLenGiver) = msg.obj.asInstanceOf[SocketBufferLenGiver];
          val reason = CommunicationReason(msg.arg1);

          val nextProtocolState = reason match {
            case CommunicationReason.PING_PONG => {
              MyBluetoothProtocolState.FIRST_PING;
            }
            case CommunicationReason.SEND_CARD => {
              MyBluetoothProtocolState.MASTER_CONFIRM_FILE_SEND;
            }
          }

          val connectedDevWrapper = createAndStartConnectedThread(socket, nextProtocolState, bufferLenGiver);

          //propagate to service
          if (parentConnectHandler.isDefined) {
            val propagateMsg = parentConnectHandler.get.obtainMessage(msg.what);
            propagateMsg.arg1 = reason.id;
            propagateMsg.obj = connectedDevWrapper;
            propagateMsg.sendToTarget();
          }

          nextProtocolState match {
            case MyBluetoothProtocolState.FIRST_PING => {
              sendBytes(ByteConv.int2arrayByte(reason.id));
            }
            case MyBluetoothProtocolState.MASTER_CONFIRM_FILE_SEND => {
              //we chose not to send here the reason but to be parent's burden
            }
          }
        }
      }
    }
  }

  def getState: MyBluetoothDeviceState.Value = {
    if (!connectThread.isDefined && !connectedThread.isDefined) {
      MyBluetoothDeviceState.NO_CONNECTION;
    }
    else if (connectThread.isDefined && !connectedThread.isDefined) {
      MyBluetoothDeviceState.CONNECTING;
    } else if (!connectThread.isDefined && connectedThread.isDefined) {
      MyBluetoothDeviceState.CONNECTED;
    } else {
      throw new InvalidDeviceStateException;
    }
  }

  /**
   * Cancel any thread currently running a connection or attempting to make a new connection
   * Cancel any existing connecting and connected threads
   */
  def cancelConnThreads(): MyBluetoothDeviceWrapper = {
    if (connectThread.isDefined) {
      connectThread.get.cancel();
    }

    if (connectedThread.isDefined) {
      connectedThread.get.cancel();
    }

    new MyBluetoothDeviceWrapper(
      bluetoothDevice,
      parentConnectHandler,
      parentConnectedHandler,
      connectThread = None,
      connectedThread = None
    );
  }

  def createAndStartConnectThread(secure: Boolean,
                                  reason: CommunicationReason.Value,
                                  bufferLenGetter: BufferLenGetter): MyBluetoothDeviceWrapper = {
    new MyBluetoothDeviceWrapper(
      bluetoothDevice,
      parentConnectHandler,
      parentConnectedHandler,
      //connectThread = Some(new ConnectThread(bluetoothDevice, secure, connectHandler, reason)),
      connectThread = Some(new ConnectThread(bluetoothDevice, secure, reason, bufferLenGetter)),
      connectedThread = None
    );
  }

  def createAndStartConnectedThread(socket: /*CloseOnce*/ BluetoothSocket,
                                    initialState: MyBluetoothProtocolState.BytesVal,
                                    bufferLenGetter: BufferLenGetter): MyBluetoothDeviceWrapper = {
    assert(socket.getRemoteDevice == bluetoothDevice);

    new MyBluetoothDeviceWrapper(
      bluetoothDevice,
      parentConnectHandler,
      parentConnectedHandler,
      connectThread = None,
      connectedThread = Some(new ConnectedThread(socket, bufferLenGetter, initialState))
    );
  }

  override def equals(other: Any): Boolean = {
    //super.equals(o);
    other match {
      case x: MyBluetoothDeviceWrapper => {
        this.bluetoothDevice == x.bluetoothDevice;
      }
      case _ => throw new MyBluetoothDeviceWrapperEqualityException;
    }
  }

  /*private def pingPongSetup(): Array[Byte] = {
    MyLatencyEstimator.reset();
    ping();
  }*/

  /**
   * send random byte
   */
  /*private def ping(): Array[Byte] = {
    MyLatencyEstimator.startTimer();
    generateRandomByteArray(MyBluetoothDeviceWrapper.pingSize);
  }*/
}
