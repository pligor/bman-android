package bluetooth

import scala.collection.mutable.ListBuffer
import generic.MathHelper._
import scala.language.reflectiveCalls

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait BufferLenGetter {
  def getCurrentBufferBytesLen: Int
}

trait BufferLenSetter {
  def setCurrentBufferBytesLen(newBufferLen: Int): Unit
}

class MutableBuffer extends BufferLenGetter with BufferLenSetter {
  private val MIN_BUFFER_LEN = 1024

  private val MAX_BUFFER_LEN = 64 * 1024

  //note we have a steady step
  private val BUFFER_STEP = 1024

  private val MAX_FAULTS = 3

  /*def reset() {
    faultCounter = 0;
    curBufferBytesLen = MIN_BUFFER_LEN;
    curMaxBufferLen = MAX_BUFFER_LEN;
  }*/

  //private var faultCounter: Int = 0;
  private val faultCounter = ListBuffer.empty[Int]

  private var curMaxBufferLen: Int = MAX_BUFFER_LEN

  private var curBufferBytesLen: Int = MIN_BUFFER_LEN

  def getCurrentBufferBytesLen = curBufferBytesLen

  //THIS BELONGS TO SLAVE
  def setCurrentBufferBytesLen(newBufferLen: Int): Unit = {
    curBufferBytesLen = math.max(math.min(newBufferLen, curMaxBufferLen), MIN_BUFFER_LEN)
  }

  //THESE BELONG TO MASTER
  def increaseBuffer(): Unit = {
    val newBufferLen = curBufferBytesLen + BUFFER_STEP

    curBufferBytesLen = math.min(newBufferLen, curMaxBufferLen) //limit to maximum
    //do not decrease fault counter here because we could get in a loop where the buffer is increased
    //and then decreased over and over again where it would actually be faster if we decrease the
    //current max buffer len and everything is successful
  }

  def decreaseBuffer(): Unit = {
    val newBufferLen = curBufferBytesLen - BUFFER_STEP

    curBufferBytesLen = math.max(newBufferLen, MIN_BUFFER_LEN) //limit to minimum

    //since we are forced to decrease buffer means we had a fault occasion
    //faultCounter += 1;
    faultCounter += curBufferBytesLen

    //assert(faultCounter <= MAX_FAULTS);
    assert(faultCounter.length <= MAX_FAULTS)

    //if (faultCounter == MAX_FAULTS) {
    if (faultCounter.length == MAX_FAULTS) {
      //curMaxBufferLen = curBufferBytesLen  //this is the last value, we prefer having the mean value
      //faultCounter = 0

      curMaxBufferLen = faultCounter.avg().toInt

      faultCounter.clear()
    }
  }
}
