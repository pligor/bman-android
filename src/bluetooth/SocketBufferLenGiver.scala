package bluetooth

import android.bluetooth.BluetoothSocket

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
private case class SocketBufferLenGiver(socket: BluetoothSocket, getter: BufferLenGetter);
