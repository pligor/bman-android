package myauth

import spray.json._
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object SIGNUP_STATE extends Enumeration {
  val IDENTIFICATION_INVALID = Value;
  val ACCOUNT_EXISTS = Value;
  val SIGNUP_SUCCESS = Value;
}

object SignupState {

  //implicit val signupStateJsonFormat = jsonFormat1(SignupState);
  implicit object jsonFormat extends RootJsonFormat[SignupState] {
    def write(signupState: SignupState): JsValue = JsObject("SIGNUP_STATE" -> JsString(signupState.value));

    def read(json: JsValue): SignupState = json match {
      case obj: JsObject => {
        obj.getFields("SIGNUP_STATE") match {
          case Seq(JsString(signup_state)) => SignupState(signup_state);
          case _ => throw new DeserializationException("signup state json object expected");
        }
      }
      case _ => throw new DeserializationException("signup state json object expected");
    }
  }

}

//{"SIGNUP_STATE":"IDENTIFICATION_INVALID"}
case class SignupState(value: String) {
  def toEnumerationValue = {
    //SIGNUP_STATE.withName(value); //den leitourgei!
    //oute auto leitourgei :(
    /*SIGNUP_STATE.values.find({
      curval =>
        log log curval.toString; //<Invalid enum: no field for #0> ???? ti malakies einai auta?
        curval.toString == value
    }).get;*/

    value match {
      case "IDENTIFICATION_INVALID" => SIGNUP_STATE.IDENTIFICATION_INVALID;
      case "ACCOUNT_EXISTS" => SIGNUP_STATE.ACCOUNT_EXISTS;
      case "SIGNUP_SUCCESS" => SIGNUP_STATE.SIGNUP_SUCCESS;
    }
  };
}
