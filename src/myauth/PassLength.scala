package myauth

import spray.json.DefaultJsonProtocol._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object PassLength {

  implicit val itsJsonFormat = jsonFormat[Short, PassLength](
    PassLength.apply, "pass_len"
  );
}

case class PassLength(pass_len: Short);