package myauth

import spray.json._
import spray.json.DefaultJsonProtocol._


/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object SingleToken {

  implicit val itsJsonFormat = jsonFormat[String, SingleToken](
    SingleToken.apply, "token"
  );

  /*implicit object jsonFormat extends RootJsonFormat[SingleToken] {
    def write(singleToken: SingleToken): JsValue = JsObject("token" -> JsString(singleToken.token));

    def read(json: JsValue): SingleToken = json match {
      case obj: JsObject => {
        obj.getFields("token") match {
          case Seq(JsString(token)) => SingleToken(token);
          case _ => throw new DeserializationException("single token json object expected");
        }
      }
      case _ => throw new DeserializationException("single token json object expected");
    }
  }  */
}

case class SingleToken(token: String);
