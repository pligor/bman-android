package myauth

import spray.json.DefaultJsonProtocol._
import components.EmailHelper._
import generic.PhoneNumberHelper._
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object CredentialToken {
  implicit val itsJsonFormat = jsonFormat[String, String, String, CredentialToken](
    CredentialToken.apply, "token", "identification", "password"
  )
}

case class CredentialToken(token: String, identification: String, password: String) {
  def isIdentificationEmail = isValidEmailAddress(identification)

  def isIdentificationPhoneNumber = isValidPhoneNumber(identification)

  def isIdentificationValid = isIdentificationEmail || isIdentificationPhoneNumber

  def getEmailOrPhone: Option[Either[String, PhoneNumber]] = {
    if(isIdentificationEmail) {
      Some(Left(identification))
    } else if(isIdentificationPhoneNumber) {
      parsePhoneString(identification).map(Right.apply)
    } else {
      None
    }
  }
}
