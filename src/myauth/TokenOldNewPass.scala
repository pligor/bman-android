package myauth

import spray.json.DefaultJsonProtocol._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

object TokenOldNewPass {
  implicit val itsJsonFormat = jsonFormat[String, String, String, TokenOldNewPass](
    TokenOldNewPass.apply, "token", "old_pass", "new_pass"
  );
}

case class TokenOldNewPass(token: String, old_pass: String, new_pass: String) {
  def arePasswordsIdentical = old_pass == new_pass;
}
