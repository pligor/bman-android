package myauth

import components.{WriteOnce, MyActivity}
import scalaj.http.{HttpException, HttpOptions, Http}
import spray.json._
import org.apache.http.HttpStatus
import scala.Some
import com.pligor.bman.{Bman, R}
import android.app.{ProgressDialog, Activity}
import myandroid.{ScalaAsyncTask, log, ProgressDialogActivity}
import android.content.DialogInterface
import models.MyTokenPreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

trait SignOutTrait extends MyActivity with ProgressDialogActivity {
  private var signOutTask: Option[SignOutTask] = None;

  private val progressDialog = new WriteOnce[ProgressDialog];

  override def onStop() {
    super.onStop();
    signOutTask.map(_.cancel({
      val mayInterruptIfRunning = true;
      mayInterruptIfRunning;
    }))
  }

  protected def signout(onSignout: (Option[Boolean]) => Unit): Unit = {
    if (MyTokenPreference.isClear) {
      onSignout(Some(false));
    } else {
      signOutTask = Some(new SignOutTask(onSignout));
      signOutTask.get.execute(SingleToken(MyTokenPreference.getValue.get));
    }
  }

  /**
   * POST http://domain.name.com/signout
   * sample json: {"TOKEN":"the token in here"}
   */
  protected class SignOutTask(onSignout: (Option[Boolean]) => Unit)
    extends ScalaAsyncTask[SingleToken, AnyRef, Option[Int]] {

    private val signoutLink = Bman.SERVER_LINK + "/" + "signout";

    private val connectionTimeout_msec = 5000;
    private val readTimeout_msec = 2 * connectionTimeout_msec;

    override def onPreExecute() {
      super.onPreExecute();
      progressDialog setValue {
        val progDialog = registerProgressDialog({
          val indeterminate = true;
          val cancelable = true;
          ProgressDialog.show(
            SignOutTrait.this,
            getResources.getString(R.string.sign_progress_dialog_title),
            getResources.getString(R.string.sign_progress_dialog_message),
            indeterminate,
            cancelable
          );
        });

        progDialog.setOnCancelListener(new DialogInterface.OnCancelListener {
          def onCancel(dialog: DialogInterface) {
            cancel({
              val mayInterruptIfRunning = true;
              mayInterruptIfRunning;
            });
          }
        });

        progDialog;
      }
    }

    protected def doInBackground(singleToken: SingleToken): Option[Int] = {
      try {
        //log log "single token while signing out: " + singleToken;
        Some(
          Http.postData(signoutLink, singleToken.toJson(SingleToken.itsJsonFormat).toString()).
            header("Content-Type", "application/json").
            option(HttpOptions.connTimeout(connectionTimeout_msec)).
            option(HttpOptions.readTimeout(readTimeout_msec)).responseCode
        );
      } catch {
        case e: HttpException => Some(e.code);
        case e: Exception => None;
      }
    }

    override def onPostExecute(responseCodeOption: Option[Int]) {
      super.onPostExecute(responseCodeOption);
      log log "signout response code: " + responseCodeOption;

      val result = responseCodeOption.flatMap {
        _ match {
          case HttpStatus.SC_OK => Some(true);
          case HttpStatus.SC_UNAUTHORIZED => Some(false);
          case HttpStatus.SC_BAD_REQUEST => None;
          case _ => None;
        }
      }

      {
        assert(progressDialog.isInitialized);
        progressDialog().dismiss();
      }

      log log "token is (re)cleared either way"
      MyTokenPreference.clear;

      onSignout(result);
    }
  }

}
