package myauth

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object AccountConfig {
  val minPassLength = 6;

  def hasPasswordValidLength(pass: String) = pass.length >= minPassLength;
}
