package myauth

import components.{WriteOnce, MyActivity}
import android.content.DialogInterface
import android.app.{ProgressDialog, Dialog}
import myandroid.{ProgressDialogActivity, log}
import com.pligor.bman.{TR, TypedViewHolder, Bman, R}
import android.widget.{EditText, TextView, RadioGroup}
import android.view.{Window, View}
import android.view.View.OnClickListener
import scalaj.http.{HttpOptions, Http, HttpException}
import org.apache.http.HttpStatus
import android.content.DialogInterface.OnCancelListener
import android.widget.RadioGroup.OnCheckedChangeListener
import models.MyTokenPreference
import generic.{ContentType, HttpHeaderName}
import generic.PhoneNumberHelper._
import components.EmailHelper._
import generic.FutureHelper._
import scala.concurrent.Future
import myandroid.AndroidHelper._
import play.api.libs.json._
import scala.concurrent.ExecutionContext.Implicits.global
import preferences.{UsernameCellphonePreference, UsernameEmailPreference}
import scala.util.Random

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait AuthActionable extends MyActivity with ProgressDialogActivity {
  private val signinLink = Bman.SERVER_LINK + "/signin"

  private val signupLink = Bman.SERVER_LINK + "/signup"

  private val doubleSignupLink = Bman.SERVER_LINK + "/doubleSignup"

  def executeAuthAction(authAction: String => Unit): Unit = {
    val tokenOption = MyTokenPreference.getValue

    if (tokenOption.isDefined) {
      authAction(tokenOption.get)
    } else {
      log log "showing sign dialog"

      val dialog = getSignDialog(authAction)

      dialog.setOnCancelListener(new OnCancelListener {
        def onCancel(dialogInterface: DialogInterface): Unit = {
          saveEmailCellphone(dialog)
        }
      })

      dialog.show()
    }
  }

  /*protected class AuthAction(val action: (String) => Unit) {
    def execute(): Unit = {
      val tokenOption = MyTokenPreference.getValue

      if (tokenOption.isDefined) {
        action(tokenOption.get)
      } else {
        log log "showing sign dialog"

        getSignDialog(this).show()
      }
    }
  }*/

  protected def saveEmailCellphone(dialog: Dialog with TypedViewHolder): Boolean = {
    UsernameEmailPreference.setValue(Some(
      getEditTextTrimmed(dialog.findView(TR.accountEmailEditText))
    )) &&
      UsernameCellphonePreference.setValue(Some(
        getEditTextTrimmed(dialog.findView(TR.accountCellphoneEditText))
      ))
  }

  protected def getSignDialog(authAction: String => Unit) = {
    val dialog = new Dialog(context) with TypedViewHolder

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background)

    dialog.setCanceledOnTouchOutside(false)

    dialog.setCancelable(true)

    dialog.setContentView(R.layout.dialog_sign)

    dialog.findView(TR.accountSkipButton).setOnClickListener(new OnClickListener {
      def onClick(v: View): Unit = {
        dialog.cancel()
      }
    })

    val accountSubmitButton = dialog.findView(TR.accountSubmitButton)

    val accountNameEditText = dialog.findView(TR.accountNameEditText)

    val accountEmailEditText = dialog.findView(TR.accountEmailEditText)

    {
      UsernameEmailPreference.getValue.map(accountEmailEditText.setText)
    }

    val accountCellphoneEditText = dialog.findView(TR.accountCellphoneEditText)

    {
      UsernameCellphonePreference.getValue.map(accountCellphoneEditText.setText)
    }

    val accountPassEditText = dialog.findView(TR.accountPassEditText)

    val accountRepeatPassEditText = dialog.findView(TR.accountRepeatPassEditText)

    val accountErrorTextView = dialog.findView(TR.accountErrorTextView)

    val signRadioGroup = dialog.findView(TR.signRadioGroup)

    signRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener {
      def onCheckedChanged(radioGroup: RadioGroup, selectedId: Int): Unit = {
        val signupGroup = Seq(
          accountRepeatPassEditText,
          accountEmailEditText,
          accountCellphoneEditText
        )

        selectedId match {
          case R.id.signinRadioButton =>
            accountNameEditText.setText(if (Random.nextBoolean()) UsernameEmailPreference.getValue.getOrElse(UsernameCellphonePreference.getValue.getOrElse(""))
            else UsernameCellphonePreference.getValue.getOrElse(UsernameEmailPreference.getValue.getOrElse("")))

            signupGroup foreach (_.setVisibility(View.GONE))

            accountNameEditText.setVisibility(View.VISIBLE)

          case R.id.signupRadioButton =>
            signupGroup foreach (_.setVisibility(View.VISIBLE))

            accountNameEditText.setVisibility(View.GONE)
        }
      }
    })

    accountSubmitButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        if (isAndroidConnectedOnline) {
          signRadioGroup.getCheckedRadioButtonId match {
            case R.id.signinRadioButton =>
              val credential = Credential(
                identification = getEditTextTrimmed(accountNameEditText),
                password = accountPassEditText.getText.toString
              )

              if (credential.isIdentificationValid) {
                new SignInTask(accountErrorTextView, dialog, authAction).execute(credential)
              } else {
                renderErrorTextView(accountErrorTextView, R.string.invalid_identification)
              }

            case R.id.signupRadioButton => {
              val phoneNumberString = getEditTextTrimmed(accountCellphoneEditText)

              val email = getEditTextTrimmed(accountEmailEditText)

              saveEmailCellphone(dialog)

              val phoneNumberOption = parsePhoneString(phoneNumberString)

              if (phoneNumberOption.isDefined) {
                val emailCellPass = EmailCellPass(
                  email = email,
                  cell = phoneNumberOption.get,
                  password = accountPassEditText.getText.toString
                )

                if (isValidEmailAddress(emailCellPass.email)) {
                  if (emailCellPass.hasValidPassLen) {
                    if (emailCellPass.password == accountRepeatPassEditText.getText.toString) {
                      //new SignUpTask(accountErrorTextView).execute(credential)
                      new DoubleSignUp(accountErrorTextView, signRadioGroup).execute(emailCellPass)
                    } else {
                      renderErrorTextView(accountErrorTextView, R.string.repeated_pass_no_match)
                    }
                  } else {
                    renderErrorTextView(accountErrorTextView, getResources.getString(R.string.invalid_password_length, AccountConfig.minPassLength.toString))
                  }
                } else {
                  renderErrorTextView(accountErrorTextView, R.string.invalid_mail)
                }
              } else {
                renderErrorTextView(accountErrorTextView, R.string.invalid_cellphone)
              }
            }
            case _ => renderErrorTextView(accountErrorTextView, R.string.sign_action_not_selected)
          }
        } else {
          renderErrorTextView(accountErrorTextView, R.string.no_internet_connection)
        }
      }
    })

    dialog
  }

  private def getEditTextTrimmed(editText: EditText): String = editText.getText.toString.trim

  private class SignInTask(textView: TextView, signDialog: Dialog, authAction: String => Unit) extends SignClass[Credential] {
    protected def doInBackground(credential: Credential): Option[(Int, String)] = {
      try {
        Some(postData(signinLink, Json.toJson(credential)(Credential.itsJsonFormat)))
      } catch {
        case e: HttpException => {
          e.code match {
            case HttpStatus.SC_EXPECTATION_FAILED | HttpStatus.SC_UNAUTHORIZED => {
              Some(e.code, e.body)
            }
            case _ => {
              log log "http response has the code status: " + e.code

              None
            }
          }
        }
        case e: Exception => {
          log log "the connection had an unexpected error: " + e.getMessage

          None
        }
      }
    }

    override def onPostExecute(jsonOption: Option[(Int, String)]): Unit = {
      super.onPostExecute(jsonOption)

      import spray.json._

      val signinStateOption = jsonOption.flatMap {
        json =>
          try {
            Some(JsonParser(json._2).convertTo[SigninState])
          } catch {
            case e: org.parboiled.errors.ParsingException => None
          }
      }

      if (signinStateOption.isDefined) {
        val signinState = signinStateOption.get

        signinState.toEnumerationValue match {
          case SIGNIN_STATE.IDENTIFICATION_INVALID =>
            renderErrorTextView(textView, R.string.invalid_identification)

          case SIGNIN_STATE.IDENTIFICATION_NOT_EXIST =>
            renderErrorTextView(textView, R.string.identification_not_exist)

          case SIGNIN_STATE.ACTIVATION_PENDING =>
            renderErrorTextView(textView, R.string.activation_pending)

          case SIGNIN_STATE.PASSWORD_WRONG =>
            renderErrorTextView(textView, R.string.password_wrong)

          case SIGNIN_STATE.SIGNIN_SUCCESS => {
            showToast(R.string.signin_success)

            val tokenOption = signinState.token

            assert(tokenOption.isDefined, "in signin success the token must always be defined")

            tokenOption.map {
              token =>
                MyTokenPreference.setValue(token)

                signDialog.dismiss()

                authAction(tokenOption.get)
            }
          }
        }

      } else {
        renderErrorTextView(textView, R.string.server_connection_error)
      }

    }
  }

  private class DoubleSignUp(textView: TextView, radioGroup: RadioGroup) extends SignClass[EmailCellPass] {
    protected def doInBackground(emailCellPass: EmailCellPass): Option[(Int, String)] = {
      try {
        Some(
          postData(doubleSignupLink, Json.toJson(emailCellPass)(EmailCellPass.itsJsonFormat))
        )
      } catch {
        case e: HttpException => Some(e.code, e.body)
        case e: Exception => None
      }
    }

    override def onPostExecute(result: Option[(Int, String)]): Unit = {
      super.onPostExecute(result)

      if (result.isDefined) {
        result.get._1 match {
          case HttpStatus.SC_OK =>
            radioGroup.check(R.id.signinRadioButton) //this will trigger callback

            renderErrorTextView(textView, R.string.double_signup_success)

          case HttpStatus.SC_BAD_REQUEST =>
            renderErrorTextView(textView, getString(R.string.bad_request_server_response, Bman.URL))

          case HttpStatus.SC_PRECONDITION_FAILED =>
            renderErrorTextView(textView, R.string.invalid_mail)

          case HttpStatus.SC_EXPECTATION_FAILED =>
            renderErrorTextView(textView, R.string.invalid_cellphone)

          case HttpStatus.SC_CONFLICT =>
            renderErrorTextView(textView, R.string.account_already_exists)

          case HttpStatus.SC_NO_CONTENT => //sms could not be sent
            renderErrorTextView(textView, R.string.double_signup_no_content)

          case _ =>
            renderErrorTextView(textView, getString(R.string.unknown_response_code, Bman.URL))
        }
      } else {
        renderErrorTextView(textView, R.string.server_connection_error)
      }
    }
  }

  private class SignUpTask(textView: TextView) extends SignClass[Credential] {
    protected def doInBackground(credential: Credential): Option[(Int, String)] = {
      try {
        Some(postData(signupLink, Json.toJson(credential)(Credential.itsJsonFormat)))
      } catch {
        case e: HttpException => {
          e.code match {
            case HttpStatus.SC_EXPECTATION_FAILED | HttpStatus.SC_CONFLICT => {
              Some(e.code, e.body)
            }
            case _ => None
          }
        }
        case e: Exception => None
      }
    }

    override def onPostExecute(jsonOption: Option[(Int, String)]): Unit = {
      super.onPostExecute(jsonOption)

      import spray.json._

      val signupStateOption = jsonOption.flatMap {
        json =>
          try {
            Some(JsonParser(json._2).convertTo[SignupState].toEnumerationValue)
          } catch {
            case e: org.parboiled.errors.ParsingException => None
          }
      }

      if (signupStateOption.isDefined) {
        signupStateOption.get match {
          case SIGNUP_STATE.IDENTIFICATION_INVALID =>
            renderErrorTextView(textView, R.string.invalid_identification)

          case SIGNUP_STATE.ACCOUNT_EXISTS =>
            renderErrorTextView(textView, R.string.account_already_exists)

          case SIGNUP_STATE.SIGNUP_SUCCESS =>
            renderErrorTextView(textView, R.string.signup_success)

        }
      } else {
        renderErrorTextView(textView, R.string.server_connection_error)
      }
    }
  }

  private abstract class SignClass[T] {
    //abstract

    protected def doInBackground(param: T): Option[(Int, String)]

    //concrete

    private val theCancellor = new WriteOnce[() => Boolean]()

    private val progressDialog = new WriteOnce[ProgressDialog]

    private val connectionTimeout_msec = 5000

    private val readTimeout_msec = 3 * connectionTimeout_msec

    private def cancel(): Unit = {
      theCancellor.map(_.apply())
    }

    protected def postData(url: String, jsValue: JsValue) = {
      val request = Http.postData(url, jsValue.toString()).
        header(HttpHeaderName.CONTENT_TYPE.toString, ContentType.APPLICATION_JSON.toString).
        option(HttpOptions.connTimeout(connectionTimeout_msec)).
        option(HttpOptions.readTimeout(readTimeout_msec)) /*.asString; //avoid using it this way*/

      val (responseCode, headersMap, resultString) = request.asHeadersAndParse(Http.readString)

      (responseCode, resultString)
    }

    def onPreExecute(): Unit = {
      progressDialog setValue {
        val progDialog = registerProgressDialog({
          val indeterminate = true

          val cancelable = true

          ProgressDialog.show(
            context,
            getResources.getString(R.string.sign_progress_dialog_title),
            getResources.getString(R.string.sign_progress_dialog_message),
            indeterminate,
            cancelable
          )
        })

        progDialog.setOnCancelListener(new OnCancelListener {
          def onCancel(dialogInterface: DialogInterface): Unit = {
            cancel()
          }
        })

        progDialog
      }
    }

    def onPostExecute(result: Option[(Int, String)]): Unit = {
      assert(progressDialog.isInitialized)

      progressDialog().dismiss()
    }

    def execute(param: T): Unit = {
      onPreExecute()

      val (f, cancellor) = interruptableFuture {
        future: Future[Unit] =>
          val result = doInBackground(param)

          onUI(onPostExecute(result))
      }

      theCancellor setValue cancellor
    }
  }

  private def renderErrorTextView(textView: TextView, message: String): Unit = {
    textView.setVisibility(View.VISIBLE)

    textView.setText(message)
  }

  private def renderErrorTextView(textView: TextView, messageId: Int): Unit = {
    textView.setVisibility(View.VISIBLE)

    textView.setText(messageId)
  }
}