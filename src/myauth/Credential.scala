package myauth

import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import play.api.libs.json._
import generic.PhoneNumberHelper._
import components.EmailHelper._

//this is needed even if intellij thinks its redundant

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected object Credential {
  implicit val itsJsonFormat = Json.format[Credential]

  /*implicit val itsJsonFormat = jsonFormat[String, String, Credential](
    Credential.apply, "identification", "password"
  )*/
}

protected case class Credential(identification: String, password: String) extends PasswordTrait {
  def isIdentificationValid = isValidEmailAddress(identification) || isValidPhoneNumber(identification)
}

protected object EmailCellPass {
  implicit val itsJsonFormat = Json.format[EmailCellPass]
}

protected case class EmailCellPass(email: String,
                                   cell: PhoneNumber,
                                   password: String) extends PasswordTrait {

}

protected trait PasswordTrait {
  def password: String

  def hasValidPassLen = AccountConfig.hasPasswordValidLength(password)
}
