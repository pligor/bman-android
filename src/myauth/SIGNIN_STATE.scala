package myauth

import spray.json._
import spray.json.DefaultJsonProtocol._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object SIGNIN_STATE extends Enumeration {
  val IDENTIFICATION_INVALID = Value

  val IDENTIFICATION_NOT_EXIST = Value

  val ACTIVATION_PENDING = Value

  val PASSWORD_WRONG = Value

  val SIGNIN_SUCCESS = Value
}

object SigninState {
  //first the types of the parameters, then as arguments first the apply method of the case class
  //and also each name, with same order, of the corresponding json keys, either way much more neat!
  implicit val itsJsonFormat = jsonFormat[String, Option[String], SigninState](
    SigninState.apply, "SIGNIN_STATE", "TOKEN"
  )

  /*implicit object itsJsonFormat extends RootJsonFormat[SigninState] {
    def write(signinState: SigninState): JsValue = {
      val tuple = "SIGNIN_STATE" -> JsString(signinState.value);
      val token = signinState.token;
      if (token.isDefined) {
        JsObject(tuple, "TOKEN" -> JsString(token.get))
      } else {
        JsObject(tuple)
      }
    }

    def read(json: JsValue): SigninState = json match {
      case obj: JsObject => {
        val mapFields = obj.fields;
        if (mapFields.contains("TOKEN")) {
          obj.getFields("SIGNIN_STATE", "TOKEN") match {
            case Seq(JsString(signin_state), JsString(token)) => SigninState(signin_state, Some(token));
            case _ => throw new DeserializationException("signin state json object expected");
          }
        } else {
          obj.getFields("SIGNIN_STATE") match {
            case Seq(JsString(signin_state)) => SigninState(signin_state, None);
            case _ => throw new DeserializationException("signin state json object expected");
          }
        }
      }
      case _ => throw new DeserializationException("signin state json object expected");
    }
  }*/

}


//{"SIGNIN_STATE":"IDENTIFICATION_INVALID"}
//{"SIGNIN_STATE":"SIGNIN_SUCCESS","TOKEN":"YOUR_UNIQUE_TOKEN_IN_HERE"}
case class SigninState(value: String, token: Option[String]) {
  def toEnumerationValue = {
    //SIGNIN_STATE.withName(value); //den leitourgei!
    //oute auto leitourgei :(
    /*SIGNIN_STATE.values.find({
      curval =>
        log log curval.toString; //<Invalid enum: no field for #0> ???? ti malakies einai auta?
        curval.toString == value
    }).get;*/

    value match {
      case "IDENTIFICATION_INVALID" => SIGNIN_STATE.IDENTIFICATION_INVALID;
      case "IDENTIFICATION_NOT_EXIST" => SIGNIN_STATE.IDENTIFICATION_NOT_EXIST;
      case "ACTIVATION_PENDING" => SIGNIN_STATE.ACTIVATION_PENDING;
      case "PASSWORD_WRONG" => SIGNIN_STATE.PASSWORD_WRONG;
      case "SIGNIN_SUCCESS" => SIGNIN_STATE.SIGNIN_SUCCESS;
    }
  };
}
