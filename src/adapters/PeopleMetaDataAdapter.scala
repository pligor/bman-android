package adapters

import android.widget._
import android.content.Context
import android.view.{ViewGroup, View}
import com.pligor.bman.R
import models._
import myandroid.AndroidHelper
import AndroidHelper._
import android.view.View.OnClickListener
import components.MetaDataParser

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class PeopleMetaDataAdapter(implicit context: Context) extends MetaDataAdapter {

  protected def onDataSetChanged() {
    labels = PeopleMetaLabel.getAll;
  }

  def getView(position: Int, convertView: View, parent: ViewGroup): View = {
    val view = if (Option(convertView).isDefined) {
      convertView;
    } else {
      // no worries to attach to a parent. this job is done from the adapter
      val tempView = layoutInflater.inflate(R.layout.people_meta_data_item, null);

      val holder = new ViewHolder(
        peopleMetaIconImageView = tempView.findViewById(R.id.peopleMetaIconImageView).asInstanceOf[ImageView],
        peopleMetaLabelTextView = tempView.findViewById(R.id.peopleMetaLabelTextView).asInstanceOf[TextView],
        peopleMetaValueTextView = tempView.findViewById(R.id.peopleMetaValueTextView).asInstanceOf[TextView]
      );

      //save the holder inside the tag of the view. hackerish but it works (remember view holder is just for performance)
      tempView.setTag(holder);

      tempView;
    }

    val metaValue = getItem(position);

    //val curClickListener = clickListener(metaValue);

    val holder = view.getTag.asInstanceOf[ViewHolder];

    holder.peopleMetaIconImageView.setImageResource(
      MetaDataParser.getImageResource(metaValue.dataValue)
    );

    holder.peopleMetaLabelTextView.setText(getChosenLabel(metaValue).label);
    holder.peopleMetaValueTextView.setText(metaValue.dataValue);

    view;
  }

  /*private val clickListener: (MetaValue) => View.OnClickListener = (metaValue: MetaValue) => new OnClickListener {
    def onClick(view: View) {
      view.getId match {
        case R.id.peopleMetaIconImageView => {
          val value = metaValue.dataValue;
          textToClipboard(
            key = getChosenLabel(metaValue).label,
            text = value
          );

          Toast.makeText(
            context,
            context.getResources.getString(R.string.copied_to_clipboard).format(value),
            Toast.LENGTH_LONG
          ).show();
        }
      }
    }
  }*/

  /**
   * A tricky way to get the label.
   * Instead of retrieving again from the database we find the label from the already retrieved ones
   */
  private def getChosenLabel(model: MetaValue) = {
    val chosenLabelOption = labels.find(_.id.get == model.labelId);
    assert(chosenLabelOption.isDefined, "since the models exists must have an existing label");
    chosenLabelOption.get;
  }

  private class ViewHolder(
                            val peopleMetaIconImageView: ImageView,
                            val peopleMetaLabelTextView: TextView,
                            val peopleMetaValueTextView: TextView
                            ) {
  }

}
