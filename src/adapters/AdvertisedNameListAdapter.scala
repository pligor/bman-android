package adapters

import android.widget.{TextView, BaseAdapter}
import android.content.Context
import android.view.{ViewGroup, View, LayoutInflater}
import android.database.DataSetObserver
import scala.collection.mutable
import components.LetterConverter
import myalljoyn.AlljoynTargetWrapper
import myalljoyn.{AdvertisedName, AlljoynTarget}
import myalljoyn.AlljoynHelper._
import com.pligor.bman.R
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class AdvertisedNameListAdapter(implicit context: Context) extends BaseAdapter {
  /**
   * NOTE the list is continuing to get refreshed on the background
   * TODO remove items from list after a certain time, for example 1 minute
   */

  private val layoutInflater = LayoutInflater.from(context);

  private val items = mutable.LinkedHashSet.empty[AlljoynTargetWrapper];

  private def getFullyDefinedItems = items.filter(_.isFullyDefined);

  registerDataSetObserver(new DataSetObserver {
    override def onChanged() {
      super.onChanged();
      log log ("the items have changed. the new length is: " + getCount);
    }
  });

  private def findByName(name: String): Option[AlljoynTargetWrapper] = items.find(_.getName == name);

  private def addItem(newItem: AlljoynTargetWrapper) {
    val prevLen = items.size;
    items += newItem;
    assert(items.size == (prevLen + 1),
      "even though this is a set try to make implemenation that you only add unique items");
  }

  /**
   * If it does not exist, just add it
   * if exists then update internal values
   */
  def safeAdd(advertisedName: AdvertisedName) {
    val targetWrapperOption = findByName(advertisedName.suffix);
    if (targetWrapperOption.isDefined) {
      val targetWrapper = targetWrapperOption.get;

      /*if (targetWrapper.isFullyDefined) {
        log log "need to replace it";
        items -= targetWrapper;
        addItem(advertisedName.toAlljoynTargetWrapper);
      } else {*/
      //log log "need to update it";
      val target = AlljoynTarget(advertisedName.suffix, advertisedName.transport);
      targetWrapper.rewriteTarget(target, advertisedName.rawCheck);

      if (targetWrapper.isFullyDefined) {
        notifyDataSetChanged(); //notify the list view that the data has changed and refresh
      }
      //}
    } else {
      log log "it is new, so just add it";
      addItem(advertisedName.toAlljoynTargetWrapper);
    }
  }

  def clearItems() {
    items.clear();
    notifyDataSetChanged(); //notify the list view that the data has changed and refresh
  }

  def getCount: Int = getFullyDefinedItems.size;

  def getItem(position: Int): AlljoynTargetWrapper = getFullyDefinedItems.toArray.apply(position);

  /**
   * avoid using this
   */
  def getItemId(position: Int): Long = position;

  def getView(position: Int, convertView: View, parent: ViewGroup): View = {
    val curView = if (convertView == null) {
      // no worries to attach to a parent. this job is done from the adapter
      val tempView = layoutInflater.inflate(R.layout.list_item_advertised_name, null);

      //hackerish, we cache the elements of the view inside the view
      tempView.setTag(new ViewHolder(
        advertisedNameTextView = tempView.findViewById(R.id.advertisedNameTextView).asInstanceOf[TextView]//,
        //advertisedMessageTransportTextView = tempView.findViewById(R.id.advertisedMessageTransportTextView).asInstanceOf[TextView],
        //advertisedRawTransportTextView = tempView.findViewById(R.id.advertisedRawTransportTextView).asInstanceOf[TextView]
      ));

      tempView;
    } else {
      convertView;
    }

    val model = getItem(position);
    val holder = curView.getTag.asInstanceOf[ViewHolder];

    val decodedName = LetterConverter.decode(model.getName);

    holder.advertisedNameTextView.setText(decodedName);

    val messageTargetOption = model.getMessageTarget;
    if (messageTargetOption.isDefined) {
      val messageTransport = transportsToString(messageTargetOption.get.transport);
      /*holder.advertisedMessageTransportTextView.setVisibility(View.VISIBLE);
      holder.advertisedMessageTransportTextView.setText(messageTransport);*/
      log log decodedName + ", MESSAGE TRANSPORT(s): " + messageTransport;
    } else {
      //holder.advertisedMessageTransportTextView.setVisibility(View.GONE);
    }

    val rawTargetOption = model.getRawTarget;
    if (rawTargetOption.isDefined) {
      val rawTransport = transportsToString(rawTargetOption.get.transport);
      /*holder.advertisedRawTransportTextView.setVisibility(View.VISIBLE);
      holder.advertisedRawTransportTextView.setText(rawTransport);*/
      log log decodedName + ", RAW TRANSPORT(s): " + rawTransport;
    } else {
      //holder.advertisedRawTransportTextView.setVisibility(View.GONE);
    }

    /*val curColor = if (model.isFullyDefined) {
      Color.WHITE;
    } else {
      Color.GRAY;
    }

    holder.advertisedNameTextView.setTextColor(curColor);
    holder.advertisedMessageTransportTextView.setTextColor(curColor);
    holder.advertisedRawTransportTextView.setTextColor(curColor);

    curView.setClickable(model.isFullyDefined);*/

    curView;
  }

  private class ViewHolder(
                            val advertisedNameTextView: TextView//,
                            //val advertisedMessageTransportTextView: TextView,
                            //val advertisedRawTransportTextView: TextView
                            );
}
