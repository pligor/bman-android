package adapters.page

import android.support.v4.app.{Fragment, FragmentStatePagerAdapter, FragmentManager}
import scala.collection.mutable
import android.support.v4.view.PagerAdapter
import fragments.PeopleCardFragment

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
/**
 * a hack (not currently used) to make notify data set changed work
 * http://stackoverflow.com/questions/7263291/viewpager-pageradapter-not-updating-the-view/7287121#7287121
 *
 * yet another not suitable solution: http://stackoverflow.com/questions/13664155/dynamically-add-and-remove-view-to-viewpager
 *
 * IF EVERYTHING FAILS USE THIS: https://github.com/commonsguy/cwac-pager (commons ware, commonsware)
 */
class PeopleCardPageAdapter(fragmentManager: FragmentManager)
  extends FragmentStatePagerAdapter(fragmentManager) {

  def setFragments(initialFragments: Seq[PeopleCardFragment]) = {
    fragments.clear()

    fragments ++= initialFragments
  }

  private val fragments = mutable.Buffer.empty[PeopleCardFragment]

  def remove(cardId: Long): Option[Unit] = {
    fragments.find(_.getCardId == cardId)./*flatMap*/map(remove)
  }

  def remove(fragment: PeopleCardFragment): Unit = {
    /*val index = fragments().indexOf(fragment)
    log log s"we are removing item with index $index"
    val previous = fragments().remove(index)*/

    fragments -= fragment

    notifyDataSetChanged()

    //Option(previous)
  }

  /**
   * Used by ViewPager.  "Object" represents the page; tell the ViewPager where the
   * page should be displayed, from left-to-right.  If the page no longer exists,
   * return POSITION_NONE
   */
  override def getItemPosition(obj: scala.Any): Int = {
    /*super.getItemPosition(obj)

    val index = fragments.indexOf(obj)

    if (index == -1)
      PagerAdapter.POSITION_NONE
    else
      index*/
    PagerAdapter.POSITION_NONE
  }

  //implemented

  def getCount: Int = fragments.size

  def getItem(position: Int): Fragment = fragments(position)

  def getPosition(cardId: Long): Option[Int] = {
    fragments.find(_.getCardId == cardId).map {
      fragment =>
        fragments.indexOf(fragment)
    }
  }
}
