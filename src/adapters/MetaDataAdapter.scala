package adapters

import android.content.Context
import android.widget.BaseAdapter
import android.view.{LayoutInflater, ViewGroup, View}
import scala.collection.mutable
import models._
import android.database.DataSetObserver

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class MetaDataAdapter(implicit context: Context) extends BaseAdapter {
  registerDataSetObserver(new DataSetObserver {
    /**
     * this reacts on notifyDataSetChanged
     */
    override def onChanged() {
      super.onChanged();
      onDataSetChanged();
    }
  });

  def addItem(newItem: MetaValue) {
    val prevLen = items.size;
    items += newItem;
    assert(items.size == (prevLen + 1));
    //notify the list view that the data has changed and refresh
    notifyDataSetChanged();
  }

  protected def onDataSetChanged(): Unit;

  protected lazy val layoutInflater = LayoutInflater.from(context);

  protected var labels = Seq.empty[MetaLabel];

  //REMEMBER: LinkedHashSet keeps the order of the elements as they were written while
  //a regular set doesn't
  protected val items = mutable.LinkedHashSet.empty[MetaValue];

  def getCount: Int = items.size;

  def getItem(position: Int): MetaValue = {
    val item1 = items.toArray.apply(position);
    /*val item2 = items.seq.toSeq.apply(position);
    assert(item1 == item2); //remember equality has been defined*/
    item1;
  }

  def getItemId(position: Int): Long = position;

  def clearItems() {
    val previousCount = getCount;
    items.clear();
    val afterCount = getCount;
    if (previousCount != afterCount) {
      //notify the list view that the data has changed and refresh
      notifyDataSetChanged();
    }
  }

  def fillFromCard(card: Card, limit: Int) = {
    insertMetaValuesToItems(card.getMetaValues.take(limit));
  }

  def fillFromCard(card: Card) = {
    insertMetaValuesToItems(card.getMetaValues);
  }

  private def insertMetaValuesToItems(cardsMetaValues: Seq[MetaValue]) = {
    items.clear();
    items ++= cardsMetaValues;
    notifyDataSetChanged();
    this;
  }
}
