package adapters

import android.widget._
import android.content.{DialogInterface, Context}
import android.view.{Window, ViewGroup, View}
import com.pligor.bman.R
import models._
import components.{RadioGroupHelper, MetaDataParser}
import myandroid.AndroidHelper
import AndroidHelper._
import android.view.View.OnClickListener
import android.app.Dialog
import myandroid.DialogHelper._
import RadioGroupHelper._
import myAndroidSqlite.MySQLiteOpenHelper
import android.content.DialogInterface.OnDismissListener

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class MyMetaDataAdapter(implicit context: Context) extends MetaDataAdapter {

  protected def onDataSetChanged() {
    labels = MyMetaLabel.getAll;
  }

  def getView(position: Int, convertView: View, parent: ViewGroup): View = {
    val view = if (Option(convertView).isDefined) {
      convertView;
    } else {
      // no worries to attach to a parent. this job is done from the adapter
      val tempView = layoutInflater.inflate(R.layout.my_meta_data_item, null);

      val holder = new ViewHolder(
        myMetaIconImageView = tempView.findViewById(R.id.myMetaIconImageView).asInstanceOf[ImageView],
        myMetaLabelButton = tempView.findViewById(R.id.myMetaLabelButton).asInstanceOf[Button],
        myMetaValueButton = tempView.findViewById(R.id.myMetaValueButton).asInstanceOf[Button],
        myMetaValueDeleteImageView = tempView.findViewById(R.id.myMetaValueDeleteImageView).asInstanceOf[ImageView]
      );

      //save the holder inside the tag of the view. hackerish but it works (remember view holder is just for performance)
      tempView.setTag(holder);

      tempView;
    }

    val metaValue = getItem(position);

    val curClickListener = clickListener(metaValue);

    val holder = view.getTag.asInstanceOf[ViewHolder];

    holder.myMetaIconImageView.setImageResource(
      MetaDataParser.getImageResource(metaValue.dataValue)
    );

    holder.myMetaIconImageView.setOnClickListener(curClickListener);

    //holder.myMetaLabelButton.setAdapter(getSpinnerArrayAdapter(metaValue));
    //holder.myMetaLabelButton.setSelection(getSpinnerArrayAdapterPosition(metaValue));
    //holder.myMetaLabelButton.setOnItemSelectedListener(itemSelectedListener(metaValue));
    holder.myMetaLabelButton.setText(getChosenLabel(metaValue).label);
    holder.myMetaLabelButton.setOnClickListener(curClickListener);

    holder.myMetaValueButton.setText(metaValue.dataValue);
    holder.myMetaValueButton.setOnClickListener(curClickListener);

    holder.myMetaValueDeleteImageView.setOnClickListener(curClickListener);

    view;
  }

  private def getDataLabelDialog(metaValue: MetaValue, textView: TextView) = {
    val dialog = new Dialog(context);

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
    dialog.setCanceledOnTouchOutside(false);

    dialog.setCancelable(true);

    dialog.setContentView(R.layout.dialog_edit_metalabel);

    val myAvailableLabelsRadioGroup = dialog.findViewById(R.id.myAvailableLabelsRadioGroup).asInstanceOf[RadioGroup];
    val newLabelEditText = dialog.findViewById(R.id.newLabelEditText).asInstanceOf[EditText];
    val editMetaLabelSaveButton = dialog.findViewById(R.id.editMetaLabelSaveButton).asInstanceOf[Button];

    def getLabelText = newLabelEditText.getText.toString.trim;

    def buttonCheck = {
      (myAvailableLabelsRadioGroup.getCheckedRadioButtonId != invalidRadioButtonId) || (!getLabelText.isEmpty);
    };

    val card = MyCard.getById(metaValue.cardId).get;
    val labels = card.getLabels(notIncluded = true);
    inflateRadioGroupWithModels(
      myAvailableLabelsRadioGroup,
      labels,
      R.layout.label_radio_button
    );

    dialog.setOnDismissListener(new OnDismissListener {
      def onDismiss(dialog: DialogInterface) {
        notifyDataSetChanged();
      }
    });

    editMetaLabelSaveButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {

        if (buttonCheck) {

          //if adding new label or changing label ?
          val labelText = getLabelText;

          if (labelText.isEmpty) {
            //changing current label
            val label = getModelFromRadioGroup[MetaLabel](myAvailableLabelsRadioGroup);
            val newMetaValue = metaValue.setLabel(label);
            assert(newMetaValue.isDefined,
              "it is not defined which means already exists but this violates the logic that we only show available labels");
            items -= metaValue;
            items += newMetaValue.get;
            dialog.dismiss();
          } else {
            //adding a new label:
            val labelIdOption = new MyMetaLabel(label = labelText).safeInsert;

            if (labelIdOption.isDefined) {
              val labelId = labelIdOption.get;

              if (labelId == MySQLiteOpenHelper.invalidInsertId) {
                showToast(R.string.local_database_error);
              } else {
                //erase old metavalue
                if (metaValue.delete) {
                  //create a new one with this label
                  val newMetaValue = new MyMetaValue(
                    cardId = metaValue.cardId,
                    labelId = labelId,
                    dataValue = metaValue.dataValue
                  );

                  if (newMetaValue.insert == MySQLiteOpenHelper.invalidInsertId) {
                    //the new label remains but nobody is bothered with that
                    showToast(R.string.local_database_error);
                  } else {
                    items -= metaValue;
                    items += newMetaValue;
                  }
                  dialog.dismiss();

                } else {
                  //erase the created meta label
                  MyMetaLabel.getById(labelId).get.delete;
                  showToast(R.string.local_database_error);
                }
              }

            } else {
              showToast(R.string.meta_label_already_exists);
            }
          }
        } else {
          showToast(R.string.invalid_form);
        }

      }
    });

    dialog;
  }


  private val clickListener: (MetaValue) => View.OnClickListener = (metaValue: MetaValue) => new OnClickListener {
    def onClick(view: View) {
      view.getId match {
        case R.id.myMetaLabelButton => {
          getDataLabelDialog(
            metaValue,
            view.asInstanceOf[TextView] //we downgrade button to textview
          ).show();
        }
        case R.id.myMetaIconImageView => {
          val value = metaValue.dataValue;
          //key = getChosenLabel(metaValue).label
          MetaDataParser.execute(value);
        }
        case R.id.myMetaValueButton => {
          getDataValueDialog(
            metaValue,
            view.asInstanceOf[TextView] //we downgrade button to textview
          ).show();
        }
        case R.id.myMetaValueDeleteImageView => {
          getDeleteConfirmationDialog(metaValue).show();
        }
      }
    }
  }

  private def getDataValueDialog(metaValue: MetaValue, textView: TextView) = {
    val dialog = new Dialog(context);

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
    dialog.setCanceledOnTouchOutside(false);

    dialog.setCancelable(true);

    dialog.setContentView(R.layout.dialog_edit_metavalue);

    val editMetaValueTextView = dialog.findViewById(R.id.editMetaValueTextView).asInstanceOf[TextView];
    editMetaValueTextView.setText(context.getResources.getString(R.string.dialog_edit_metavalue_title).format(getChosenLabel(metaValue).label));

    val editMetaValueEditText = dialog.findViewById(R.id.editMetaValueEditText).asInstanceOf[EditText];
    editMetaValueEditText.setText(metaValue.dataValue);

    val editMetaValueSaveButton = dialog.findViewById(R.id.editMetaValueSaveButton).asInstanceOf[Button];

    editMetaValueSaveButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        val newValue = editMetaValueEditText.getText.toString.trim;
        if (!newValue.isEmpty && metaValue.dataValue != newValue) {
          items -= metaValue;
          val changedMetaValue = metaValue.setDataValue(newValue);
          items += changedMetaValue;
        }
        dialog.dismiss();
        //the notify data changed is called automatically after the dialog is dismissed therefore
        //I don't have to call it myself
      }
    });

    dialog;
  }

  private def eraseMetaValue(metaValue: MetaValue) {
    if (metaValue.delete) {
      items -= metaValue;
      notifyDataSetChanged();
    } else {
      showToast(R.string.local_database_error);
    }
  }

  private def getDeleteConfirmationDialog(metaValue: MetaValue) = {
    getOneButtonDialog(
      titleId = R.string.delete_meta_value_item_dialog_message,
      buttonTextId = R.string.delete_meta_value_item_dialog_button,
      backId = R.drawable.dialog_background
    )({
      dialog =>
        eraseMetaValue(metaValue);
        dialog.dismiss();
    });
  }

  /**
   * A tricky way to get the label.
   * Instead of retrieving again from the database we find the label from the already retrieved ones
   *
   */
  private def getChosenLabel(model: MetaValue) = {
    val chosenLabelOption = labels.find(_.id.get == model.labelId);
    assert(chosenLabelOption.isDefined, "since the models exists must have an existing label");
    chosenLabelOption.get;
  }

  private class ViewHolder(
                            val myMetaIconImageView: ImageView,
                            val myMetaLabelButton: Button,
                            val myMetaValueButton: Button,
                            val myMetaValueDeleteImageView: ImageView
                            );

  //IF YOU WANT TO USE A SPINNER FOR THE LABELS TRY THE CODE BELOW

  /*private def getSpinnerArrayAdapter(metaValue: MetaValue): ArrayAdapter[MetaLabel] = {
    new ArrayAdapter[MetaLabel](context, android.R.layout.simple_spinner_dropdown_item, labels.toArray);
  }*/


  /*private def getSpinnerArrayAdapterPosition(metaValue: MetaValue): Int = {
    val chosenLabel = getChosenLabel(metaValue);
    //val labelPosition = labelStrings.indexWhere(_ == chosenLabel.label);
    labels.map(_.label).indexWhere(_ == chosenLabel.label);
  }*/

  /*private val itemSelectedListener: (MetaValue) => OnItemSelectedListener = (metaValue: MetaValue) => new OnItemSelectedListener {

    private class NoLabelSelectedException extends Exception;

    def onNothingSelected(adapterView: AdapterView[_]) {
      throw new NoLabelSelectedException;
    }

    def onItemSelected(adapterView: AdapterView[_], view: View, position: Int, id: Long) {
      assert(id == position, "at current implementation id equals position unless you change the implementation");
      //Logger("meta label selected position: " + position);
      //Logger("meta label selected id: " + id);
      //Logger("meta label selected real id: " + labels(position).id.get);
      val label = labels(position);

      val hasIdChanged = metaValue.labelId != label.id.get;

      if (hasIdChanged) {
        //set new label id to current metavalue in database
        val newMetaValue = metaValue.setLabel(label);
        if (newMetaValue.isDefined) {
          Logger("meta value changed all ok, do nothing");
        } else {
          showToast(R.string.meta_value_already_exists);
          adapterView.setSelection(getSpinnerArrayAdapterPosition(metaValue));
        }
      } else {
        Logger("id has not changed, do nothing");
      }
    }
  }*/
}
