package adapters

import android.widget.{ImageView, TextView, BaseAdapter}
import android.content.Context
import android.view.{ViewGroup, View, LayoutInflater}
import models.BluetoothNameListItem
import collection.immutable.SortedSet
import com.pligor.bman.R
import android.database.DataSetObserver
import android.bluetooth.BluetoothDevice
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class BluetoothNameListAdapter(implicit context: Context) extends BaseAdapter {
  private val layoutInflater = LayoutInflater.from(context);

  private var items = SortedSet.empty[BluetoothNameListItem];

  /**
   * @return a clone of the current items
   */
  def getItems = items;

  registerDataSetObserver(new DataSetObserver {
    override def onChanged() {
      super.onChanged();

      log log "the items have changed. the new length is: " + getCount;
    }
  });

  def verifyItem(bluetoothDevice: BluetoothDevice) {
    items.find(_.device == bluetoothDevice).map {
      item =>
        items -= item;
        addItem(item.verify);
    }
  }

  def addItem(newItem: BluetoothNameListItem) {
    //val prevLen = items.size;
    items = items + newItem;
    //assert(items.size == (prevLen + 1));  //this is a set so no worries if this fails

    //notify the list view that the data has changed and refresh
    notifyDataSetChanged();
  }

  def clearItems() {
    items = SortedSet.empty[BluetoothNameListItem];

    //notify the list view that the data has changed and refresh
    notifyDataSetChanged();
  }

  def getCount: Int = items.size;

  def getItem(position: Int) = items.toArray.apply(position);

  /**
   * avoid using this
   */
  def getItemId(position: Int): Long = position;

  def getView(position: Int, convertView: View, parent: ViewGroup): View = {
    val curView = if (convertView == null) {
      // no worries to attach to a parent. this job is done from the adapter
      val tempView = layoutInflater.inflate(R.layout.list_item_bluetooth_name, null);

      val holder = new ViewHolder(
        bluetoothNameListView_name = tempView.findViewById(R.id.bluetoothNameListView_name).asInstanceOf[TextView],
        //bluetoothNameListView_address = tempView.findViewById(R.id.bluetoothNameListView_address).asInstanceOf[TextView],
        bluetoothVerifyImageView = tempView.findViewById(R.id.bluetoothVerifyImageView).asInstanceOf[ImageView]
      );

      tempView.setTag(holder);

      tempView;
    } else {
      convertView;
    }

    val model = getItem(position);

    val holder = curView.getTag.asInstanceOf[ViewHolder];

    val deviceName = model.device.getName;
    holder.bluetoothNameListView_name.setText(deviceName);

    val deviceAddress = model.device.getAddress;
    //holder.bluetoothNameListView_address.setText(deviceAddress);
    log log deviceName + ", BLUETOOTH ADDRESS: " + deviceAddress;

    holder.bluetoothVerifyImageView.setVisibility(if (model.verified) View.VISIBLE else View.GONE);

    curView;
  }

  private class ViewHolder(
                            val bluetoothNameListView_name: TextView,
                            //val bluetoothNameListView_address: TextView,
                            val bluetoothVerifyImageView: ImageView
                            );
}
