package adapters

import android.widget._
import android.content.Context
import android.view.{ViewGroup, View}
import com.pligor.bman.R
import models._
import components.MetaDataParser

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class ThinMetaDataAdapter(cardType: Card.Type.Value)(implicit context: Context) extends MetaDataAdapter {

  protected def onDataSetChanged() {
    labels = cardType match {
      case Card.Type.MINE => MyMetaLabel.getAll;
      case Card.Type.PEOPLE => PeopleMetaLabel.getAll;
    }
  }

  def getView(position: Int, convertView: View, parent: ViewGroup): View = {
    val view = if (Option(convertView).isDefined) {
      convertView;
    } else {
      // no worries to attach to a parent. this job is done from the adapter
      val tempView = layoutInflater.inflate(R.layout.thin_meta_data_item, null);

      val holder = new ViewHolder(
        thinMetaIconImageView = tempView.findViewById(R.id.thinMetaIconImageView).asInstanceOf[ImageView],
        thinMetaLabelTextView = tempView.findViewById(R.id.thinMetaLabelTextView).asInstanceOf[TextView],
        thinMetaValueTextView = tempView.findViewById(R.id.thinMetaValueTextView).asInstanceOf[TextView]
      );

      //save the holder inside the tag of the view. hackerish but it works (remember view holder is just for performance)
      tempView.setTag(holder);

      tempView;
    }

    val metaValue = getItem(position);

    val holder = view.getTag.asInstanceOf[ViewHolder];

    holder.thinMetaIconImageView.setImageResource(
      MetaDataParser.getImageResource(metaValue.dataValue)
    );

    holder.thinMetaLabelTextView.setText(getChosenLabel(metaValue).label);
    holder.thinMetaValueTextView.setText(metaValue.dataValue);

    view;
  }

  /**
   * A tricky way to get the label.
   * Instead of retrieving again from the database we find the label from the already retrieved ones
   */
  private def getChosenLabel(model: MetaValue) = {
    val chosenLabelOption = labels.find(_.id.get == model.labelId);
    assert(chosenLabelOption.isDefined, "since the models exists must have an existing label");
    chosenLabelOption.get;
  }

  private class ViewHolder(val thinMetaIconImageView: ImageView,
                           val thinMetaLabelTextView: TextView,
                           val thinMetaValueTextView: TextView);

}
