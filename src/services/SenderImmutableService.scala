package services

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

import android.app.Service
import components.{AdvertiseNameList, WriteOnce}
import android.content.Intent
import android.os._
import myalljoyn._
import simple.{Simple, SimpleClient, SimpleInterface}
import java.io.{IOException, File, FileOutputStream}
import myalljoyn.AlljoynTarget
import org.alljoyn.bus.BusException
import activities.AlljoynSendingActivity
import com.pligor.bman.{R, CommunicationReason}
import myandroid.{MyServiceObject, log}
import generic.FileSender

object SenderImmutableService extends MyServiceObject {

  object ACTIVITIES extends AlljoynAndroidServiceActivities {
    val MY_LIST = Value

    val SENDING = Value
  }

  object EXTRAS extends AlljoynAndroidServiceExtras {
    val FULL_FILEPATH = Value

    val OWN_NAME = Value

    val TARGET_WRAPPER = Value
  }

  object MESSAGES extends Enumeration {
    val MY_LIST_ACTIVITY_STARTED = Value

    val SENDING_ACTIVITY_STARTED = Value

    val UNSET_MESSENGER = Value
  }

}


/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class SenderImmutableService extends AlljoynAndroidService {
  private val finishedFlag = new WriteOnce[Unit]

  def safeSetFinishedFlag(): Unit = {
    if (!finishedFlag.isInitialized) {
      finishedFlag setValue()
    } else {
      //nop
    }
  }

  private val clientBusHandler = new WriteOnce[ClientBusHandler]()

  private var curActivityOption: Option[SenderImmutableService.ACTIVITIES.Value] = None

  /**
   * Multiple clients can connect to the service at once. However, the system calls your
   * service's onBind() method to retrieve the IBinder only when the first client binds.
   * The system then delivers the same IBinder to any additional clients that bind, without
   * calling onBind() again.
   */
  def onBind(intent: Intent): IBinder = {
    SenderImmutableService.setIsStarted(true)

    new Messenger(incomingHandler).getBinder
  }

  protected def setActivity(bundle: Bundle): Unit = {
    curActivityOption = Some(SenderImmutableService.ACTIVITIES(
      bundle.getInt(SenderImmutableService.EXTRAS.SOURCE_ACTIVITY.toString,
        SenderImmutableService.ACTIVITIES.INVALID.id)
    ))
  }

  protected def setMessenger(bundle: Bundle): Unit = {
    messenger = Option(bundle.getParcelable[Messenger](SenderImmutableService.EXTRAS.THE_MESSENGER.toString))
  }

  override def onStartCommand(intent: Intent, flags: Int, startId: Int): Int = {
    super.onStartCommand(intent, flags, startId);
    Service.START_STICKY; //to remain and be exploited
  }

  override def onDestroy(): Unit = {
    super.onDestroy()

    log log "on destroy of sender service"

    SenderImmutableService.setIsStarted(false)

    safeSetFinishedFlag()

    if (clientBusHandler.isInitialized) {
      clientBusHandler().sendEmptyMessage(clientBusHandler().MyMessage.DISCONNECT.id);
    } else {
      log log "we never actually started the sender service as we should have";
      messenger = None;
    }
  }

  private object incomingHandler extends Handler {
    override def handleMessage(msg: Message): Unit = {
      super.handleMessage(msg)

      SenderImmutableService.MESSAGES(msg.what) match {
        case SenderImmutableService.MESSAGES.MY_LIST_ACTIVITY_STARTED =>
          val bundle = msg.getData

          setMessenger(bundle)

          setActivity(bundle)

          val ownName = bundle.getString(SenderImmutableService.EXTRAS.OWN_NAME.toString)

          log log "OWNER NAME: " + ownName

          assert(!clientBusHandler.isInitialized)

          clientBusHandler setValue new ClientBusHandler(ownName)

          clientBusHandler().sendEmptyMessage(clientBusHandler().MyMessage.CONNECT.id)

        case SenderImmutableService.MESSAGES.SENDING_ACTIVITY_STARTED =>
          val bundle = msg.getData

          setMessenger(bundle)

          setActivity(bundle)

          val targetWrapper = bundle.getParcelable[AlljoynTargetWrapper](
            SenderImmutableService.EXTRAS.TARGET_WRAPPER.toString
          )

          val filePath = bundle.getString(SenderImmutableService.EXTRAS.FULL_FILEPATH.toString)

          val message = clientBusHandler().obtainMessage(clientBusHandler().MyMessage.JOIN_SESSION.id)

          message.setData({
            val bundle = new Bundle

            bundle.putString(clientBusHandler().EXTRAS.FULL_PATH.toString, filePath)

            bundle.putParcelable(clientBusHandler().EXTRAS.TARGET_WRAPPER.toString, targetWrapper)

            bundle
          })

          message.sendToTarget()

          log log "asked from client bus handler to join the session"

        case SenderImmutableService.MESSAGES.UNSET_MESSENGER =>
          messenger = None
      }
    }
  }

  private def fileSendingFailed(strId: Int): Unit = {
    assert(curActivityOption.get == SenderImmutableService.ACTIVITIES.SENDING,
      "we must fire this while sending");

    if (messenger.isDefined) {
      messenger.get.send({
        val message = Message.obtain()

        message.what = AlljoynSendingActivity.MESSAGE.FILE_SENDING_FAILED.id

        message.setData({
          val bundle = new Bundle

          bundle.putInt(AlljoynSendingActivity.EXTRAS.SENDING_FAILURE_STRING.toString, strId)

          bundle
        })
        message

      })
    } else {
      log log "user probably pressed the back button and the messenger was set to none that is why no need to send anything";
    }
  }

  private def initializingAlljoynFailed(str: String): Unit = {
    assert(curActivityOption.get == SenderImmutableService.ACTIVITIES.MY_LIST,
      "we must fire this while on the list with advertised names");

    messenger.get.send({
      val message = Message.obtain()

      message.what = AdvertiseNameList.MESSAGE.FAILED_INITIALIZING_ALLJOYN_SENDER.id

      message.setData({
        val bundle = new Bundle

        bundle.putString(AdvertiseNameList.EXTRA.INIT_SENDER_FAILURE_STRING.toString, str)

        bundle
      })

      message
    })
  }

  private class ClientBusHandler(private val ownName: String) extends AlljoynBusHandler {
    val fileToSend = new WriteOnce[File](
      file => assert(file.isFile)
    )

    require(Option(ownName).isDefined)

    object EXTRAS extends Enumeration {
      val TARGET_WRAPPER = Value

      val RAW_TARGET = Value

      val FULL_PATH = Value
    }

    private val messageSessionId = new WriteOnce[Int];
    private val rawSessionId = new WriteOnce[Int];

    protected def getSessionIds = Seq(Left(messageSessionId), Left(rawSessionId));

    private val proxyInterface = new WriteOnce[SimpleInterface];

    private val fileOutputStream = new WriteOnce[FileOutputStream];

    private object FinishedAbruptlyException extends Exception;

    object MyMessage extends MessageEnumeration {
      val SEND_RAW = MessageEnum {
        msg =>
          val bundle = msg.getData;
          val rawTarget = bundle.getParcelable[AlljoynTarget](EXTRAS.RAW_TARGET.toString);
          assert(!fileOutputStream.isInitialized);

          val rawSessionOption = SimpleClient.getRawSession(rawTarget)(
            onRawSessionLost = {
              sessionId =>
                log log "the raw session with id: " + sessionId + " is lost";
                safeSetFinishedFlag();
            }
          )(busAttachment());

          if (rawSessionOption.isDefined) {
            {
              val rawPrefix = Simple.WELL_KNOWN_RAW_NAME_PREFIX;
              log log "cancelling the interest of finding any well known names with prefix: " + rawPrefix;
              busAttachment().cancelFindAdvertisedName(rawPrefix);
            }

            val rawSession = rawSessionOption.get;
            rawSessionId setValue rawSession.sessionId;
            fileOutputStream setValue rawSession.fileOutputStream;

            log log "raw session is defined";

            if (
              try {
                proxyInterface().isInputStreamReady(rawSessionId());
              } catch {
                case e: BusException => false;
              }
            ) {
              log log "now sending file through a raw secure stream"

              val fileSender = new FileSender(fileToSend())

              //val fileToSendStream = new FileInputStream(fileToSend);

              assert(curActivityOption.get == SenderImmutableService.ACTIVITIES.SENDING, "inside raw we must be in the sending part");
              //val totalFileLen = fileToSend.length();
              //var sentLen = 0;


              val isFileSentOk = try {
                /*useEachBuffer(fileToSendStream, Simple.BUFFER_LEN) {
                  buffer: Array[Byte] =>
                    val bufferLen = buffer.length;
                    log log "we just sent: " + bufferLen + " number of bytes";

                    sentLen += bufferLen;

                    messenger.get.send({
                      val message = Message.obtain();
                      message.what = AlljoynSendingActivity.MESSAGE.BUFFER_SENT.id;
                      message.setData({
                        val bundle = new Bundle;
                        bundle.putFloat(AlljoynSendingActivity.EXTRAS.PERCENTAGE_SENT.toString, sentLen.toFloat / totalFileLen.toFloat);
                        bundle;
                      });
                      message;
                    });

                    fileOutputStream().write(buffer);

                    if (finishedFlag.isInitialized) {
                      throw FinishedAbruptlyException;
                    } else {
                      //this is too fast. buffer gets easily saturated. large files cannot be sent
                      //maybe this is a "solution" ???
                      Thread.sleep(50);
                    }
                }*/

                fileSender.sendToOutStream(fileOutputStream(), Simple.BUFFER_LEN)({
                  percentageSent =>
                    messenger.get.send({
                      val message = Message.obtain();
                      message.what = AlljoynSendingActivity.MESSAGE.BUFFER_SENT.id;
                      message.setData({
                        val bundle = new Bundle;
                        bundle.putFloat(AlljoynSendingActivity.EXTRAS.PERCENTAGE_SENT.toString, percentageSent);
                        bundle;
                      });
                      message;
                    });

                    if (finishedFlag.isInitialized) {
                      throw FinishedAbruptlyException;
                    } else {
                      //TODO this is too fast. buffer gets easily saturated. large files cannot be sent. Use Alljoyn File Module instead
                      Thread.sleep(50)
                    }
                });

                true;
              } catch {
                case e: IOException => false;
                case FinishedAbruptlyException => false;
              } finally {
                //fileToSendStream.close();
                fileSender.end();
                fileOutputStream().close();
              }

              if (isFileSentOk) {
                if (
                  try {
                    proxyInterface().getIsFileTransferedSuccessfully;
                  } catch {
                    case e: BusException => false;
                  }
                ) {
                  log log "informing the activity that we have finished successfully";
                  messenger.get.send({
                    val message = Message.obtain();
                    message.what = AlljoynSendingActivity.MESSAGE.FILE_SENT_SUCCESSFULLY.id;
                    message;
                  });
                } else {
                  log log "file was sent successfully but was NOT received as it should";
                  fileSendingFailed(R.string.received_reports_transfer_failed);
                }
              } else {
                log log "file is not sent ok";
                fileSendingFailed(R.string.file_not_sent_ok);
              }
            } else {
              log log "failed to initialize input stream on the other end"
              fileSendingFailed(R.string.failed_input_stream_at_receiver);
            }
          } else {
            log log "getting output stream has failed";
            fileSendingFailed(R.string.failed_getting_output_stream);
          }
      }

      val JOIN_SESSION = MessageEnum {
        msg =>
          val bundle = msg.getData

          fileToSend setValue new File(bundle.getString(EXTRAS.FULL_PATH.toString))

          val targetWrapper = bundle.getParcelable[AlljoynTargetWrapper](EXTRAS.TARGET_WRAPPER.toString)

          val messageTarget = targetWrapper.getMessageTarget.get

          val suffixName = messageTarget.name

          val sessionIdOption = SimpleClient.joinSession(
            name = Simple.getFullWellKnownName(suffixName),
            transport = messageTarget.transport
            //transport = getAllTransportsExceptWfdAndBluetooth
          )(onSessionLost = {
            sessionId =>
              log log "message session with id: " + sessionId + " is lost";
              safeSetFinishedFlag();
          })(busAttachment());

          if (sessionIdOption.isDefined) {
            messageSessionId setValue sessionIdOption.get;
            log log "yeahhhhh I have joined the session with session id: " + messageSessionId();

            {
              val prefixName = Simple.WELL_KNOWN_MESSAGE_NAME_PREFIX;
              log log "cancelling finding any well known names with prefix: " + prefixName + " for any transport";
              busAttachment().cancelFindAdvertisedName(prefixName);
            }

            proxyInterface setValue SimpleClient.getProxyInterface(suffixName, messageSessionId())(busAttachment());

            //RIGHT NOW THERE IS ONLY ONE SINGLE REASON
            val reason = CommunicationReason.SEND_CARD
            //WE HAVE DISABLED SET REASON BECAUSE WE HAD ISSUES WITH iOS
            /*try {
              proxyInterface().setReason(reason.id);
            } catch {
              case e: BusException => {
                log log "setting reason has failed with message: " + e.getMessage;
                fileSendingFailed(R.string.failed_setting_reason);
              };
            }*/

            reason match {
              case CommunicationReason.SEND_CARD => {
                assert(curActivityOption.get == SenderImmutableService.ACTIVITIES.SENDING,
                  "we must be already in the sending part")

                val receiverAccepted = try {
                  messenger.get.send({
                    val message = Message.obtain()

                    message.what = AlljoynSendingActivity.MESSAGE.WAIT_USER_CONFIRMATION.id

                    message
                  })

                  proxyInterface().doesReceiverAcceptsCard(ownName)
                } catch {
                  case e: BusException => false;
                }

                if (receiverAccepted) {
                  messenger.get.send({
                    val message = Message.obtain()

                    message.what = AlljoynSendingActivity.MESSAGE.USER_ACCEPTED_FILE.id

                    message
                  })

                  val isFileLenAcceptedAndSet = try {
                    proxyInterface().isFileLenAccepted(fileToSend().length())
                  } catch {
                    case e: BusException => false
                  }

                  if (isFileLenAcceptedAndSet) {
                    log log "ready to send raw information"
                    //supposingly we dont need the targetWrapper any longer, we just need the raw part
                    val message = obtainMessage(MyMessage.SEND_RAW.id)

                    message.setData({
                      val bundle = new Bundle

                      bundle.putParcelable(EXTRAS.RAW_TARGET.toString, targetWrapper.getRawTarget.get)

                      bundle
                    })

                    message.sendToTarget()
                  } else {
                    log log "file len is not accepted by receiver"

                    fileSendingFailed(R.string.file_too_large)
                  }
                } else {
                  messenger.get.send({
                    val message = Message.obtain()

                    message.what = AlljoynSendingActivity.MESSAGE.USER_REJECTED_FILE.id

                    message
                  })
                }
              }
            }
          } else {
            log log "session id is not defined"

            fileSendingFailed(R.string.session_id_not_defined)
          }
      }

      /*val RECONNECT = MessageEnum {
        msg =>
          if (busAttachment.isInitialized) {
            assert(busAttachment().isConnected, "we initialize only a connected bus");
            find();
          } else {
            connect(find);
          }
      }*/

      val CONNECT = MessageEnum {
        msg =>
          assert(!busAttachment.isInitialized);
          connect(find);
      }

      val DISCONNECT = MessageEnum {
        msg => {
          isDisconnecting = true;
          if (busAttachment.isInitialized) {
            SimpleClient.disconnect(getSessionIds)(busAttachment());
            if (fileOutputStream.isInitialized) {
              fileOutputStream().close();
            }
          } else {
            //everything else must be undefined
            getSessionIds foreach (session_id => assert(!session_id.a.isInitialized));
            assert(!fileOutputStream.isInitialized);
            assert(!proxyInterface.isInitialized);
          }

          fileToSend.map(_.delete())
        }

        {
          curActivityOption.get match {
            case SenderImmutableService.ACTIVITIES.MY_LIST => {
              messenger.get.send({
                val message = Message.obtain()

                message.what = AdvertiseNameList.MESSAGE.ALLJOYN_SENDER_DESTROYED.id

                message
              })
            }
            case SenderImmutableService.ACTIVITIES.SENDING => {
              messenger.get.send({
                val message = Message.obtain()

                message.what = AlljoynSendingActivity.MESSAGE.ALLJOYN_SENDER_DESTROYED.id

                message
              })
            }
          }
        }

        {
          getLooper.quit();

          messenger = None;

          log log "disconnected";
        }
      }

      /*val RESET_VARIABLES = MessageEnum {
        msg =>
        //do something with the output stream ????
          log log "resetting variables";
          SimpleClient.leaveSessions(getSessionIds)(busAttachment());
          messageSessionId = None;
          rawSessionId = None;
          proxyInterface = None;
          log log "variables are reset";
      }*/

      /*val CANCEL_FINDING = MessageEnum {
        msg =>
          val bundle = msg.getData;
          val prefixName = bundle.getString(EXTRA_NAME_PREFIX);
          log log "cancelling finding any well known names with prefix: " + prefixName + " for any transport";
          busAttachment().cancelFindAdvertisedName(prefixName);
      }*/

      private def connect(onSuccessfulConnection: () => Unit) {
        val clientBusAttachmentOption = SimpleClient.connect(onFoundAdvertisedName = {
          advertisedName =>
            messenger.get.send({
              val message = Message.obtain();
              message.what = AdvertiseNameList.MESSAGE.FOUND_ADVERTISED_NAME.id;
              advertisedName.toMessage(message);
            });
        });
        if (clientBusAttachmentOption.isDefined) {
          busAttachment setValue clientBusAttachmentOption.get;

          onSuccessfulConnection();
        } else {
          log log "clientBusAttachmentOption is not defined";
          initializingAlljoynFailed("initializing finding has failed");
        }
      }

      private def find() {
        if (SimpleClient.findBothServices(busAttachment())) {
          log log "started finding both message and raw service";
          messenger.get.send({
            val message = Message.obtain();
            message.what = AdvertiseNameList.MESSAGE.STARTED_FINDING_BOTH_SERVICES.id;
            message;
          });
        } else {
          log log "finding both services has failed";
          initializingAlljoynFailed("initializing finding has failed");
        }
      }
    }

  }

}
