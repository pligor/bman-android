package services

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

import android.app.Service
import android.content.Intent
import android.os._
import components.{AlljoynWaitingActivity, WriteOnce}
import myalljoyn.{AlljoynAndroidServiceExtras, AlljoynAndroidService, AlljoynBusHandler}
import simple.{SimpleInterface, SimpleService, Simple}
import org.alljoyn.bus.{BusObject, Status, Mutable}
import myalljoyn.AlljoynHelper._
import java.io.{File, IOException, FileInputStream}
import activities.AlljoynReceivingActivity
import models.{Card}
import com.pligor.bman.{Bman, R, CommunicationReason}
import preferences.ConfirmReceivePreference
import myandroid.{ScalaAsyncTask, MyServiceObject, log}
import generic.FileReceiver

object ReceiverImmutableService extends MyServiceObject {

  object EXTRAS extends AlljoynAndroidServiceExtras {
    val SUFFIX_NAME = Value;
    val USER_BOOLEAN_CONFIRM_DECISION = Value;
  }

  object MESSAGES extends Enumeration {
    val WAITING_ACTIVITY_STARTED = Value;
    val RECEIVING_ACTIVITY_STARTED = Value;
    val USER_CONFIRM_DECISION = Value;
  }

  object ACTIVITIES extends Enumeration {
    val INVALID = Value;
    val WAITING = Value;
    val RECEIVING = Value;
  }

}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class ReceiverImmutableService extends AlljoynAndroidService {
  private var curActivityOption: Option[ReceiverImmutableService.ACTIVITIES.Value] = None;

  private val serviceBusHandler = new WriteOnce[ServiceBusHandler];

  private val fileAccepted = new WriteOnce[Boolean];

  protected def setActivity(bundle: Bundle): Unit = {
    curActivityOption = Some(ReceiverImmutableService.ACTIVITIES(
      bundle.getInt(ReceiverImmutableService.EXTRAS.SOURCE_ACTIVITY.toString,
        ReceiverImmutableService.ACTIVITIES.INVALID.id)
    ));
  }

  protected def setMessenger(bundle: Bundle): Unit = {
    messenger = Option(bundle.getParcelable[Messenger](ReceiverImmutableService.EXTRAS.THE_MESSENGER.toString));
  }

  private def getSuffixName(bundle: Bundle): Option[String] = {
    Option(bundle.getString(ReceiverImmutableService.EXTRAS.SUFFIX_NAME.toString));
  }

  def onBind(intent: Intent): IBinder = {
    ReceiverImmutableService.setIsStarted(true)
    new Messenger(incomingHandler).getBinder;
  }

  override def onStartCommand(intent: Intent, flags: Int, startId: Int): Int = {
    super.onStartCommand(intent, flags, startId);
    Service.START_STICKY; //to remain and be exploited
  }

  override def onDestroy(): Unit = {
    super.onDestroy();
    log log "on destroy of receiver service";

    ReceiverImmutableService.setIsStarted(false);

    if (serviceBusHandler.isInitialized) {
      serviceBusHandler().sendEmptyMessage(serviceBusHandler().MyMessage.DISCONNECT.id);
    } else {
      log log "we never actually started the receiver service as we should have";
      messenger = None;
    }

  }

  private object incomingHandler extends Handler {
    override def handleMessage(msg: Message): Unit = {
      super.handleMessage(msg);
      ReceiverImmutableService.MESSAGES(msg.what) match {
        case ReceiverImmutableService.MESSAGES.USER_CONFIRM_DECISION => {
          val bundle = msg.getData;
          fileAccepted setValue bundle.getBoolean(ReceiverImmutableService.EXTRAS.USER_BOOLEAN_CONFIRM_DECISION.toString);
          //this must the be the case where we get out of the loop (the loop where fileAccepted is checked for initialization)
        }
        case ReceiverImmutableService.MESSAGES.WAITING_ACTIVITY_STARTED => {
          val bundle = msg.getData;
          setMessenger(bundle);
          setActivity(bundle);

          val suffixName = getSuffixName(bundle);
          assert(suffixName.isDefined, "we cannot work without the suffix name");
          assert(!serviceBusHandler.isInitialized);
          serviceBusHandler setValue new ServiceBusHandler(suffixName.get);
          serviceBusHandler().sendEmptyMessage(serviceBusHandler().MyMessage.CONNECT.id);
        }
        case ReceiverImmutableService.MESSAGES.RECEIVING_ACTIVITY_STARTED => {
          val bundle = msg.getData;
          setMessenger(bundle);
          setActivity(bundle);
          //from now on the sender must already have sent the first request
          //this first request is setReason. Inside there, we are waiting for activity to be set
          //in order for the binding of the activity to succeed and we know that the activity have
          //catch up. Because there is a chance that the transaction is too quick and the activity
          //has not loaded yet. This will cause assertions to fail
        }
      }
    }
  }

  private def initializingReceiverSuccess() {
    assert(curActivityOption.get == ReceiverImmutableService.ACTIVITIES.WAITING,
      "we must fire this while on the list with advertised names");

    messenger.get.send({
      val message = Message.obtain();
      message.what = AlljoynWaitingActivity.MESSAGE.SUCCESS_INITIALIZING_ALLJOYN_RECEIVER.id;
      message;
    });
  }

  private def initializingReceiverFailed(strId: Int) {
    assert(curActivityOption.get == ReceiverImmutableService.ACTIVITIES.WAITING,
      "we must fire this while on the list with advertised names");

    messenger.get.send({
      val message = Message.obtain();
      message.what = AlljoynWaitingActivity.MESSAGE.FAILED_INITIALIZING_ALLJOYN_RECEIVER.id;
      message.setData({
        val bundle = new Bundle;
        bundle.putInt(AlljoynWaitingActivity.EXTRA.INIT_RECEIVER_FAILURE_STRING.toString, strId);
        bundle;
      });
      message;
    });
  }

  private def failedInitializingInputStream() {
    assert(curActivityOption.get == ReceiverImmutableService.ACTIVITIES.RECEIVING,
      "in order to reach this failure we must be already inside the receiving activity");

    messenger.get.send({
      val message = Message.obtain();
      message.what = AlljoynReceivingActivity.MESSAGE.FAILED_INITIALIZING_INPUT_STREAM.id;
      message;
    });
  }

  private class ServiceBusHandler(private val suffixName: String) extends AlljoynBusHandler {
    private val messageSessionId = new WriteOnce[Int];
    private val rawSessionId = new WriteOnce[Int];

    private val communicationReason = new WriteOnce[CommunicationReason.Value];

    val areWeUnstuckFlag = new WriteOnce[Unit];

    //for the time being we dont use this collection. the client is responsible for closing the connection
    protected def getSessionIds = Seq(Left(messageSessionId), Left(rawSessionId));

    object EXTRAS extends Enumeration {
      //nothing at the time being
    }

    private val fileReceiveTask = new WriteOnce[FileReceiveTask];

    object simpleObj extends SimpleInterface with BusObject {
      private val waitingMsecs = 100L;

      def doesReceiverAcceptsCard(senderName: String): Boolean = {
        log log "communication reason: " + (if (communicationReason.isInitialized) communicationReason() else "TIPOTA");
        //WE HAVE DISABLED SET REASON BECAUSE WE HAD ISSUES WITH iOS
        /*assert(communicationReason.isInitialized &&
          communicationReason() == CommunicationReason.SEND_CARD);*/

        log log "cur activity: " + curActivityOption.get;
        while (curActivityOption.get != ReceiverImmutableService.ACTIVITIES.RECEIVING) {
          Thread.sleep(waitingMsecs);
        }

        if (ConfirmReceivePreference.getValue) {
          log log "the user is asked to confirm manually";
          messenger.get.send({
            val message = Message.obtain();
            message.what = AlljoynReceivingActivity.MESSAGE.USER_MANUAL_CONFIRM.id;
            message.setData({
              val bundle = new Bundle;
              bundle.putString(
                AlljoynReceivingActivity.EXTRAS.SENDER_NAME.toString,
                //LetterConverter.decode(senderSuffixName)
                senderName
              );
              bundle;
            });
            message;
          });
          //now we wait for a response from the activity
        } else {
          log log "the user is auto accepting";

          messenger.get.send({
            val message = Message.obtain();
            message.what = AlljoynReceivingActivity.MESSAGE.USER_AUTO_CONFIRM.id;
            message.setData({
              val bundle = new Bundle;
              bundle.putString(AlljoynReceivingActivity.EXTRAS.SENDER_NAME.toString, senderName);
              bundle;
            });
            message;
          });

          fileAccepted setValue true;
        }

        while (!fileAccepted.isInitialized) {
          Thread.sleep(waitingMsecs);
        }

        log log "the user's answer is: " + fileAccepted();
        fileAccepted();
      }

      /**
       * this is the first call
       */
      def setReason(reasonId: Int) {
        //remember this is the upper limit: Simple.REPLY_TIMEOUT_msec (it equals with the default that is why it is nowhere assigned)
        //waiting for activity to catch up
        new ScalaAsyncTask[Long, AnyRef, Unit] {
          protected def doInBackground(period: Long) {
            while (curActivityOption.get != ReceiverImmutableService.ACTIVITIES.RECEIVING) {
              Thread.sleep(waitingMsecs);
            }
          }
        }.execute(waitingMsecs);

        communicationReason setValue CommunicationReason(reasonId);
        log log "the communication reason is set: " + communicationReason();
        //no further preparation is necessary for the time being
      }

      def getIsFileTransferedSuccessfully: Boolean = {
        require(fileReceiveTask.isInitialized);
        require(fileReceiveTask().getStatus != AsyncTask.Status.PENDING, "the file receive task must have already been started");

        while (fileReceiveTask().getStatus != AsyncTask.Status.FINISHED) {
          log log "waiting to file receive task to finish";
          Thread.sleep(waitingMsecs);
        }

        val tempFileOption = fileReceiveTask().temporaryFileOption();

        assert(messenger.isDefined);
        messenger.get.send({
          assert(curActivityOption.get == ReceiverImmutableService.ACTIVITIES.RECEIVING, "we are still in receiving");
          val message = Message.obtain();
          message.what = AlljoynReceivingActivity.MESSAGE.FILE_RECEIVED.id;
          message.setData({
            val bundle = new Bundle;
            if (tempFileOption.isDefined) {
              bundle.putString(AlljoynReceivingActivity.EXTRAS.TEMP_FILE_CANONICAL_PATH.toString,
                tempFileOption.get.getCanonicalPath);
            } else {
              log log "leave the bundle empty because the file was not created ok";
            }
            bundle;
          });
          message;
        });

        tempFileOption.isDefined;
      }

      def isInputStreamReady(sessionId: Int): Boolean = {
        log log "the method for input stream was called, and now me as a service must open an input stream";
        val sockFd = new Mutable.IntegerValue;

        assert(rawSessionId.isInitialized);
        //http://stackoverflow.com/questions/13359560/bus-blocking-call-not-allowed-error-in-alljoyn
        busAttachment().enableConcurrentCallbacks();
        //without the above line we get an exception.
        // I don't see it harmful to have a bus call within a bus call
        // If you find issues, change the flow
        val status = busAttachment().getSessionFd(rawSessionId(), sockFd);

        if (status == Status.OK) {
          log log "getting the session fd status is: " + status;

          val fd = getSockedFileDescriptor(sockFd.value);
          val fileInputStream = new FileInputStream(fd);

          log log "we just enabled the file input stream";

          assert(fileReceiveTask.isInitialized);
          fileReceiveTask().execute(fileInputStream);

          true;
        } else {
          log log "failed getting the session fd. status: " + status;
          failedInitializingInputStream();
          false;
        }
      }

      def isFileLenAccepted(fileLen: Long): Boolean = {
        //WE HAVE DISABLED SET REASON BECAUSE WE HAD ISSUES WITH iOS
        /*assert(communicationReason.isInitialized &&
          communicationReason() == CommunicationReason.SEND_CARD);*/

        val isLenOk = Card.isJsonFileLenOk(fileLen);

        if (isLenOk) {
          fileReceiveTask setValue new FileReceiveTask(fileLen);
          log log "file len is accepted therefore file receive task has been initialized";
        } else {
          log log "file len was rejected";
          assert(curActivityOption.get == ReceiverImmutableService.ACTIVITIES.RECEIVING);
          messenger.get.send({
            val message = Message.obtain();
            message.what = AlljoynReceivingActivity.MESSAGE.FILE_LEN_INVALID.id;
            message;
          });
        }

        isLenOk;
      }

      /**
       * Ask the service to arrange a raw reliable session that can be used to
       * transfer a "file" and return a contact port over which this session
       * can be joined.
       *
       * return an integer session port with which the raw session may be joined
       */
      //def requestRawSession: Short = Simple.RAW_PORT;
    }

    object MyMessage extends MessageEnumeration {


      val CONNECT = MessageEnum {
        msg =>
          connect(requestAndAdvertise);
      }

      private def advertise(): Boolean = {
        assert(busAttachment.isInitialized);
        SimpleService.advertiseName(suffixName, Simple.DEFAULT_TRANSPORTS)(busAttachment());
      }

      private def requestAndAdvertise(): Boolean = {
        assert(busAttachment.isInitialized);
        SimpleService.request(suffixName)(busAttachment()) && advertise();
      }

      private def connect(onBinded: () => Boolean) {
        val busAttachmentOption = SimpleService.connect(simpleObj);
        if (busAttachmentOption.isDefined) {
          val bus = busAttachmentOption.get;
          //I guess we dont need that: bus.unbindSessionPort()
          val binded = SimpleService.bindBothSessions(Simple.DEFAULT_TRANSPORTS)(
            onMessageSessionJoined = {
              sessionJoiner =>
                messageSessionId setValue sessionJoiner.sessionId;

                log log "cancel advertising message name";
              {
                val messageName = Simple.getFullWellKnownName(suffixName);
                log log "cancelling advertising the message name " + messageName;
                bus.cancelAdvertiseName(
                  messageName,
                  Simple.DEFAULT_TRANSPORTS
                );
              }

                log log "tell activity message session is joined with session id: " + messageSessionId();
              {
                assert(curActivityOption.get == ReceiverImmutableService.ACTIVITIES.WAITING, "we should not have gone to receiving yet");
                assert(messenger.isDefined);
                val message = Message.obtain();
                message.what = AlljoynWaitingActivity.MESSAGE.JOINED_MESSAGE_SESSION.id;
                messenger.get.send(message);
              }
            }, onMessageSessionLost = {
              sessionId =>
                assert(messageSessionId() == sessionId);

                log log "message session is lost with session id: " + sessionId;
              {
                assert(curActivityOption.get == ReceiverImmutableService.ACTIVITIES.RECEIVING, "the session must only be lost if we are on receiving");
                assert(messenger.isDefined);

                if (areWeUnstuckFlag.isInitialized) {
                  log log "no need to tell activity because the whole process will end from another path";
                } else {
                  log log "message session is lost abruptly before receiving the file with session id: " + sessionId;
                  messenger.get.send({
                    val message = Message.obtain();
                    message.what = AlljoynReceivingActivity.MESSAGE.LOST_MESSAGE_SESSION.id;
                    message;
                  });
                }
              }
            })(onRawSessionJoined = {
            sessionJoiner =>
              assert(messageSessionId.isInitialized, "message session must be joined already");

              log log "raw session joined";
              rawSessionId setValue sessionJoiner.sessionId;

            {
              val rawName = Simple.getFullWellKnownRawName(suffixName);
              log log "cancelling advertising the raw name " + rawName;
              bus.cancelAdvertiseName(
                rawName,
                Simple.DEFAULT_TRANSPORTS
              );
            }

            {
              assert(messenger.isDefined, "we must ensure messenger is defined in order to be able to send the message");
              val message = Message.obtain();
              message.what = AlljoynReceivingActivity.MESSAGE.RAW_SESSION_JOINED.id;
              messenger.get.send(message);
            }
          }, onRawSessionLost = {
            sessionId =>
              log log "raw session lost with session id: " + sessionId;
              assert(rawSessionId() == sessionId);

              if (areWeUnstuckFlag.isInitialized) {
                log log "no need to tell activity because the whole process will end from another path";
              } else {
                log log "raw session is lost abruptly before receiving the file with session id: " + sessionId;
                messenger.get.send({
                  val message = Message.obtain();
                  message.what = AlljoynReceivingActivity.MESSAGE.LOST_RAW_SESSION.id;
                  message;
                });
              }
          })(bus);

          if (binded) {
            log log "ok we are binded";
            busAttachment setValue bus;
            if (onBinded()) {
              log log "request and advertise are ok";
              initializingReceiverSuccess();
            } else {
              log log "failed to request and/or advertise names";
              initializingReceiverFailed(R.string.alljoyn_name_taken);
            }
          } else {
            log log "failed to bind to sessions";
            initializingReceiverFailed(R.string.alljoyn_bind_sessions_failed);
          }
        } else {
          log log "failed to connect to bus";
          initializingReceiverFailed(R.string.alljoyn_bus_connection_failed);
        }
      }

      val DISCONNECT = MessageEnum {
        msg => {
          isDisconnecting = true;
          if (busAttachment.isInitialized) {
            SimpleService.disconnect(suffixName, simpleObj)(busAttachment());

            if (fileReceiveTask.isInitialized) {
              fileReceiveTask().cancel(true);
            }

          } else {
            //everything else must be undefined
            getSessionIds foreach (session_id => assert(!session_id.a.isInitialized));
            assert(!fileReceiveTask.isInitialized);
          }
        }

        {
          curActivityOption.get match {
            case ReceiverImmutableService.ACTIVITIES.WAITING => {
              messenger.get.send({
                val message = Message.obtain();
                message.what = AlljoynWaitingActivity.MESSAGE.ALLJOYN_RECEIVER_DESTROYED.id;
                message;
              });
            }
            case ReceiverImmutableService.ACTIVITIES.RECEIVING => {
              messenger.get.send({
                val message = Message.obtain();
                message.what = AlljoynReceivingActivity.MESSAGE.ALLJOYN_RECEIVER_DESTROYED.id;
                message;
              })
            }
          }
        }

        {
          getLooper.quit();

          messenger = None;

          log log "disconnected";
        }
      }

      /*val RECONNECT = MessageEnum {
        msg =>
          if (busAttachment.isInitialized) {
            assert(busAttachment().isConnected, "we initialize only a connected bus");
          } else {
            connect();
          }
      }*/

      /*val SET_SUFFIX_NAME = MessageEnum {
        msg =>
          val bundle = msg.getData;
          val newSuffixName = bundle.getString(BusExtras.SUFFIX_NAME.toString);

          assert(busAttachment.isInitialized);

          val check = if (suffixNameOption.isDefined) {
            if (suffixNameOption.get == newSuffixName) {
              log log "if equals no need to re-request, just advertise";
              advertise(newSuffixName);
            } else {
              log log "if different release and then request and advertise";
              SimpleService.releaseNames(suffixNameOption.get)(busAttachment());
              requestAndAdvertise(newSuffixName);
            }
          } else {
            log log "no need to release obviously, only request and advertise";
            requestAndAdvertise(newSuffixName);
          }

          if (check) {
            suffixNameOption = Some(newSuffixName);
          } else {
            //to do
          }
      }*/
    }

    private class FileReceiveTask(private val fileLen: Long) extends ScalaAsyncTask[FileInputStream, AnyRef, Option[File]] {

      private val milliSleepPerIteration = 100;

      private val fileReceiver = new FileReceiver(fileLen, fileExt = Some(Bman.FILE_EXT));

      val temporaryFileOption = new WriteOnce[Option[File]];

      protected def doInBackground(fileInputStream: FileInputStream): Option[File] = {
        try {
          while ( {
            //because .available returns an estimate, is not a good idea to use it in an assertion
            while (fileInputStream.available() == 0) {
              Thread.sleep(milliSleepPerIteration);
            }

            val buffer = new Array[Byte](Simple.BUFFER_LEN);

            //this is a blocking call
            val noOfBytesReceived = fileInputStream.read(buffer);

            fileReceiver.write(buffer.take(noOfBytesReceived));

            log log "we have received: " + fileReceiver.countBytesReceived + " number of the total of " + fileLen + " of bytes";
            messenger.get.send({
              val message = Message.obtain();
              message.what = AlljoynReceivingActivity.MESSAGE.BUFFER_RECEIVED.id;
              message.setData({
                val bundle = new Bundle;
                bundle.putFloat(AlljoynReceivingActivity.EXTRAS.PERCENTAGE_RECEIVED.toString, fileReceiver.getPercentageReceived);
                bundle;
              });
              message;
            });

            !fileReceiver.isFileReceivedSuccessfully;
          }) {}
          areWeUnstuckFlag setValue();
          Some(fileReceiver.end());
        } catch {
          case e: IOException => {
            log log "we got io exception";
            fileReceiver.cancel();
            None;
          }
          case e: InterruptedException => {
            log log "we got interrupted exception";
            if (fileReceiver.isFileReceivedSuccessfully) {
              Some(fileReceiver.end());
            } else {
              fileReceiver.cancel();
              None;
            }
          }
        } finally {
          fileInputStream.close();
        }
      }

      override def onPostExecute(tempFileOption: Option[File]) {
        super.onPostExecute(tempFileOption);
        temporaryFileOption setValue tempFileOption;
      }
    }

  }

}
