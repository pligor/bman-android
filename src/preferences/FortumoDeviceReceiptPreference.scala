package preferences

import android.content.Context
import components.MySecurePreference
import mycrypto.crypto.AES
import mycrypto.protocol.defaults._
import spray.json._
import myfortumo.FortumoDeviceReceipt

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * TESTED, writing a model and reading a model is successful
 */
case object FortumoDeviceReceiptPreference extends MySecurePreference[String, Option[FortumoDeviceReceipt]] {

  //remember to have the object as a case object in order for this work correctly
  //val preferenceKey = CurrentPgpKeyIdPreference.toString;
  val preferenceKey = "FortumoDeviceReceiptPreference"

  val defaultValue: String = null

  def setValue(newValue: Option[FortumoDeviceReceipt])(implicit context: Context): Boolean = {
    setInnerValue(
      if (newValue.isDefined)
        AES.encryptToString(
          newValue.get.toJson(FortumoDeviceReceipt.itsJsonFormat).toString(),
          secret
        )
      else defaultValue
    )
  }

  def getValue(implicit context: Context): Option[FortumoDeviceReceipt] = {
    Option(getInnerValue.asInstanceOf[String]).map {
      encryptedString =>
        JsonParser(
          AES.decryptFromString(encryptedString, secret)
        ).convertTo[FortumoDeviceReceipt](FortumoDeviceReceipt.itsJsonFormat)
    }
  }

  private val secret = """A$|$(5d"d15~%2jW"""
}