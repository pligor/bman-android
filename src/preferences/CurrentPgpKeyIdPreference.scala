package preferences

import android.content.Context
import models.PgpPublic
import components.MySecurePreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object CurrentPgpKeyIdPreference extends MySecurePreference[Long, Long] {
  //remember to have the object as a case object in order for this work correctly
  //val preferenceKey = CurrentPgpKeyIdPreference.toString;
  val preferenceKey = "CurrentPgpKeyIdPreference"

  val defaultValue: Long = PgpPublic.invalidId

  def getValue(implicit context: Context): Long = getInnerValue.asInstanceOf[Long]

  def setValue(newValue: Long)(implicit context: Context): Boolean = {
    setInnerValue(newValue)
  }
}
