package preferences

import android.net.Uri
import android.content.Context
import components.MySecurePreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

/**
 * REMEMBER use it as a temporary variable, clear it after reading its value
 */
case object UriTemporaryPreference extends MySecurePreference[String, Uri] {
  //val preferenceKey = UriTemporaryPreference.toString;
  //for some bizarre reason, above line causes a recursive functions which leads to StackOverflowError
  val preferenceKey = "UriTemporaryPreference"

  val defaultValue: String = Uri.EMPTY.toString

  def getValue(implicit context: Context): Uri = Uri.parse(getInnerValue.asInstanceOf[String])

  def getAndClear(implicit context: Context) = {
    val value = getValue

    clear

    value
  }

  def setValue(newValue: Uri)(implicit context: Context): Boolean = {
    setInnerValue(newValue.toString)
  }
}
