package preferences

import android.content.Context
import components.MySecurePreference
import mycrypto.crypto.AES
import mycrypto.protocol.defaults._
import java.util.{TimeZone, Calendar, Date}
import com.pligor.bman.{R, Bman}
import myandroid.AndroidHelper._
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * TESTED, encryption of the value works well
 */
case object FirstRunTimestampPreference extends MySecurePreference[String, Option[Long]] {

  //remember to have the object as a case object in order for this work correctly
  //val preferenceKey = CurrentPgpKeyIdPreference.toString;
  val preferenceKey = "FirstRunTimestampPreference"

  val defaultValue: String = null

  def getValue(implicit context: Context): Option[Long] = {
    Option(getInnerValue.asInstanceOf[String]).map {
      encryptedString =>
        AES.decryptFromString(encryptedString, secret).toLong;
    }
  }

  def setValue(newValue: Option[Long])(implicit context: Context): Boolean = {
    setInnerValue(
      if (newValue.isDefined) AES.encryptToString(newValue.get.toString, secret)
      else null
    )
  }

  private val secret = """-SbTB~^]{c?~oi.-"""

  def initialize(implicit context: Context): Option[Date] = {
    val date = new Date

    if (setValue(Some(date.getTime))) Some(date)
    else None
  }

  def isOnTrial(implicit context: Context): Boolean = {
    val nowDate = new Date

    val timeRegistered = getValue.getOrElse({
      log log "could not write it. Will write it next time. Either way the time is NOW"

      initialize.getOrElse(new Date).getTime
    })

    val registeredDate = new Date(timeRegistered)

    //give an extra hour to compensate for little misconfigurations of the phone clock and of the server's clock
    if (registeredDate.getTime > (nowDate.getTime + (3600 * 1000))) {
      showToast(R.string.clock_misconfigured)

      throw new AbsurdRegistrationTimeException(registered = registeredDate, now = nowDate)
    } else {
      //val cal = Calendar.getInstance()
      //the above has the same result
      val cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"))

      cal.setTime(registeredDate)

      cal.add(Bman.trialPeriod._2, Bman.trialPeriod._1)

      cal.getTime.getTime > nowDate.getTime
    }
  }

  private class AbsurdRegistrationTimeException(registered: Date, now: Date) extends Exception {
    override def getMessage: String = {
      super.getMessage + " and the time registered was " + registered + " while the time now was " + now
    }
  }

}
