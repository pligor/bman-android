package preferences

import android.content.Context
import components.MySecurePreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object SoundPreference extends MySecurePreference[Boolean, Boolean] {
  //remember to have the object as a case object in order for this work correctly
  val preferenceKey = "SoundPreference"

  val defaultValue: Boolean = true

  def getValue(implicit context: Context): Boolean = getInnerValue.asInstanceOf[Boolean]

  def setValue(newValue: Boolean)(implicit context: Context): Boolean = {
    setInnerValue(newValue)
  }
}
