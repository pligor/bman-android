package preferences

import android.content.Context
import components.MySecurePreference


/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object AutoReplyPreference extends MySecurePreference[Boolean, Boolean] {
  //remember to have the object as a case object in order for this work correctly
  //val preferenceKey = AutoReplyPreference.toString;
  //better be explicit
  val preferenceKey = "AutoReplyPreference"

  val defaultValue: Boolean = false

  def getValue(implicit context: Context): Boolean = getInnerValue.asInstanceOf[Boolean]

  def setValue(newValue: Boolean)(implicit context: Context): Boolean = {
    setInnerValue(newValue)
  }
}
