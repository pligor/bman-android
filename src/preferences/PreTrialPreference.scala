package preferences

import android.content.Context
import components.MySecurePreference
import mycrypto.crypto.AES
import mycrypto.protocol.defaults._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * true means we are in pre trial period
 * false means we are not anymore
 */
case object PreTrialPreference extends MySecurePreference[String, /*Option[*/ Boolean /*]*/ ] {
  private val secret = """T4."Ys]A=8~2+Z_N"""

  //remember to have the object as a case object in order for this work correctly
  //val preferenceKey = PreTrialPreference.toString
  val preferenceKey = "PreTrialPreference"

  val defaultValue: String = AES.encryptToString(true.toString, secret) //null

  def getValue(implicit context: Context): /*Option[*/ Boolean /*]*/ = {
    /*Option(getInnerValue.asInstanceOf[String]).map {
      encryptedString =>
        AES.decryptFromString(encryptedString, secret).toBoolean;
    }*/

    AES.decryptFromString(getInnerValue.asInstanceOf[String], secret).toBoolean
  }

  def setValue(newValue: /*Option[*/ Boolean /*]*/)(implicit context: Context): Boolean = {
    setInnerValue(
      /*if (newValue.isDefined)*/ AES.encryptToString(newValue. /*get.*/ toString, secret)
      //else null
    )
  }
}
