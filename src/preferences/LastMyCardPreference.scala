package preferences

import android.content.Context
import models.MyCard
import components.MySecurePreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object LastMyCardPreference extends MySecurePreference[Long, Long] {
  //val preferenceKey = CurrentDimensionPreference.toString;
  //for some bizarre reason, above line causes a recursive functions which leads to StackOverflowError
  val preferenceKey = "LastMyCardPreference"

  val defaultValue: Long = MyCard.invalidId

  def getValue(implicit context: Context): Long = getInnerValue.asInstanceOf[Long]

  def setValue(newValue: Long)(implicit context: Context): Boolean = {
    setInnerValue(newValue)
  }
}
