package preferences

import android.content.Context
import components.MySecurePreference
import components.EmailHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object UsernameEmailPreference extends MySecurePreference[String, Option[String]] {
  //remember to have the object as a case object in order for this work correctly
  //val preferenceKey = AutoReplyPreference.toString;
  //better be explicit
  val preferenceKey = "UsernameEmailPreference"

  val defaultValue: String = null

  def getValue(implicit context: Context): Option[String] = {
    Option(getInnerValue.asInstanceOf[String])
  }

  def setValue(newValue: Option[String])(implicit context: Context) = {
    setInnerValue(newValue.getOrElse(defaultValue))
  }
}
