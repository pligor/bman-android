package preferences

import android.content.Context
import components.MySecurePreference
import mycrypto.crypto.AES
import mycrypto.protocol.defaults._
import java.util.{TimeZone, Date, Calendar}
import models.SubscriptionProduct

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object InAppUnmanagedSubscriptionPreference extends MySecurePreference[String, Option[Long]]
with SubscriptionPreferenceTrait {

  //remember to have the object as a case object in order for this work correctly
  //val preferenceKey = CurrentPgpKeyIdPreference.toString;
  val preferenceKey = "InAppUnmanagedSubscriptionPreference"

  val defaultValue: String = null

  def getValue(implicit context: Context): Option[Long] = {
    Option(getInnerValue.asInstanceOf[String]).map {
      encryptedString =>
        AES.decryptFromString(encryptedString, secret).toLong
    }
  }

  def setValue(newValue: Option[Long])(implicit context: Context): Boolean = {
    setInnerValue(
      if (newValue.isDefined) AES.encryptToString(newValue.get.toString, secret)
      else null
    )
  }

  private val secret = """.A79;8]/w5k},,1V"""

}