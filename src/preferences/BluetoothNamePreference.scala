package preferences

import android.content.Context
import android.bluetooth.BluetoothAdapter
import components.MySecurePreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object BluetoothNamePreference extends MySecurePreference[String, String] {
  //remember to have the object as a case object in order for this work correctly
  //val preferenceKey = BluetoothNamePreference.toString;
  val preferenceKey = "BluetoothNamePreference"

  val defaultValue: String = BluetoothAdapter.getDefaultAdapter.getName

  def getValue(implicit context: Context): String = getInnerValue.asInstanceOf[String]

  def setValue(newValue: String)(implicit context: Context): Boolean = {
    setInnerValue(newValue)
  }

  def saveName(implicit bluetoothAdapterOption: Option[BluetoothAdapter], context: Context): Unit = {
    bluetoothAdapterOption.map(bluetoothAdapter => setValue(bluetoothAdapter.getName))
  }

  def writeBack(implicit bluetoothAdapterOption: Option[BluetoothAdapter], context: Context): Option[Boolean] = {
    bluetoothAdapterOption.map(_.setName(getValue))
  }
}
