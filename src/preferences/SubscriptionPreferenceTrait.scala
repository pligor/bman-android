package preferences

import android.content.Context
import java.util.{TimeZone, Calendar, Date}
import models.SubscriptionProduct
import myandroid.AndroidHelper._
import com.pligor.bman.R

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait SubscriptionPreferenceTrait {
  //abstract

  def getValue(implicit context: Context): Option[Long]

  //concrete

  /**
   * @return
   */
  def isSubscribed(implicit context: Context): Boolean = {
    val nowDate = new Date

    val timeSubscribed = getValue

    timeSubscribed.isDefined && {
      val subscribedDate = new Date(timeSubscribed.get)

      //give an extra hour to compensate for little misconfigurations of the phone clock and of the server's clock
      if (subscribedDate.getTime > (nowDate.getTime + (3600 * 1000))) {
        showToast(R.string.clock_misconfigured)

        throw new AbsurdSubscriptionTimeException(subscribed = subscribedDate, now = nowDate)
      } else {
        //val cal = Calendar.getInstance()
        //the above has the same result
        val cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"))

        cal.setTime(subscribedDate)

        cal.add(SubscriptionProduct.period._2, SubscriptionProduct.period._1)

        cal.getTime.getTime > nowDate.getTime
      }
    }
  }

  private class AbsurdSubscriptionTimeException(subscribed: Date, now: Date) extends Exception {
    override def getMessage: String = {
      super.getMessage + " and the time subscribed was " + subscribed + " while the time now was " + now
    }
  }

}
