package activities

import android.app.{ProgressDialog, Activity}
import components._
import com.pligor.bman.{Bman, TR, R, TypedViewHolder}
import android.os.Bundle
import android.view.{MotionEvent, View}
import myandroid.InternetHelper._
import models.{MyBugsense, Card, MyCard, RequestCode}
import android.net.Uri
import android.content.{DialogInterface, Intent}
import activities.crud.{Crud, CrudActivity}
import myandroid.AndroidHelper._
import android.view.View.{OnTouchListener, OnClickListener}
import myandroid.{log, ScalaAsyncTask, ProgressDialogActivity}
import android.webkit.{WebView, WebViewClient}
import preferences.LastMyCardPreference
import myAndroidAPIs.BugsenseActivity
import components.WebviewHelper._
import android.content.DialogInterface.OnCancelListener

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class CreateFrontActivity extends Activity with TypedViewHolder
with MyActivity

with AdmobActivity

with PhotoCapturing
with PhotoImporting
with ProgressDialogActivity
with BugsenseActivity {
  protected val photoImportRequestCode: Int = RequestCode.SELECT_IMAGE.id

  protected val photoCaptureRequestCode: Int = RequestCode.CAMERA_DATA.id

  private lazy val importPictureButton = findView(TR.importPictureButton)

  private lazy val shootPictureButton = findView(TR.shootPictureButton)

  private lazy val createCardVisitWebsiteBanner = findView(TR.createCardVisitWebsiteBanner)

  //private lazy val createCardWebView = defaultSetupWebview(findView(TR.createCardWebView))

  private lazy val createCardVisitWebsiteButton = findView(TR.createCardVisitWebsiteButton)

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_create_front)

    val wordFront = getResources.getString(R.string.word_front)

    importPictureButton.setText(getResources.getString(R.string.import_picture_button,
      wordFront
    ))

    shootPictureButton.setText(getResources.getString(R.string.shoot_picture_button,
      wordFront
    ))

    importPictureButton.setOnClickListener(new OnClickListener {
      def onClick(p1: View): Unit = {
        importFromGallery()
      }
    })

    shootPictureButton.setOnClickListener(new OnClickListener {
      def onClick(p1: View): Unit = {
        shootFromCamera(showToast(R.string.image_shot_impossible))
      }
    })

    createCardVisitWebsiteButton.setOnClickListener(new OnClickListener {
      def onClick(v: View): Unit = {
        showWebSite(Bman.CARDS_URL)
      }
    })

    /*createCardWebView.setOnTouchListener(new OnTouchListener {
      def onTouch(view: View, motionEvent: MotionEvent): Boolean = {
        if (motionEvent.getAction == MotionEvent.ACTION_UP) {
          showWebSite(Bman.URL)
        }
        true
      }
    })

    createCardWebView.setWebViewClient(new WebViewClient {
      override def onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String): Unit = {
        super.onReceivedError(view, errorCode, description, failingUrl)

        view.setVisibility(View.GONE)
      }
    })*/
  }

  override def onStart(): Unit = {
    super.onStart()

    createCardVisitWebsiteBanner.setVisibility(
      if (isAndroidConnectedOnline) View.VISIBLE else View.GONE
    )

    //loadUrlIfConnected(Bman.SERVER_LINK + "/createCard", createCardWebView)
  }


  override def onStop(): Unit = {
    super.onStop()

    createMyCardTaskOption.map(_.cancel {
      val mayInterruptIfRunning = true

      mayInterruptIfRunning
    })
  }

  override def onActivityResult(requestCode: Int, resultCode: Int, intent: Intent): Unit = {
    super.onActivityResult(requestCode, resultCode, intent)

    if (requestCode == RequestCode.CREATE_BACK.id && resultCode == Activity.RESULT_OK) {
      val cardId = intent.getLongExtra(CreateBackActivity.OUTPUT_EXTRA.CARD_ID.toString, Card.invalidId)

      assert(cardId != Card.invalidId, "if result is ok the card id must be present")

      val cardOption = MyCard.getById(cardId)

      assert(cardOption.isDefined, "this card id must belong to a my card")

      LastMyCardPreference.setValue(cardId)

      MyBugsense.Event.sendBugsenseEvent(MyBugsense.Event.CARD_CREATED)

      startActivity(
        new Intent(this, classOf[CrudActivity]).putExtra(
          Crud.InputExtra.MODEL_ID.toString, cardId
        ).putExtra(
          Crud.InputExtra.CARD_TYPE.toString, cardOption.get.getType.id
        )
        //we intentionally not pass the id of a view because this is not valid here
      )

      finish()
    } else {
      //nop
    }
  }

  private def proceedToCreateBackActivity(myCardOption: Option[MyCard]): Unit = {
    if (myCardOption.isDefined) {
      assert(myCardOption.get.isOld,
        "we should NOT have a new model here. we should proceed with an old one")

      startActivityForResult(new Intent(this, classOf[CreateBackActivity]).putExtra(
        CreateBackActivity.EXTRA.CARD_ID.toString, myCardOption.get.id.get
      ), RequestCode.CREATE_BACK.id)
    } else {
      showToast(R.string.failed_creating_new_card)
    }
  }

  protected def onImportFromGallerySuccess(photoUriOption: Option[Uri]): Unit = {
    if (photoUriOption.isDefined) {
      executeCreateMyCard(photoUriOption.get)(proceedToCreateBackActivity)
    } else {
      showToast(R.string.retrieving_new_image_error)
    }
  }

  protected def onShootFromCameraSuccess(photoUri: Uri, onAfterPhotoUriUsage: (Uri) => Unit): Unit = {
    executeCreateMyCard(photoUri) {
      cardOption =>
        onAfterPhotoUriUsage(photoUri)

        proceedToCreateBackActivity(cardOption)
    }
  }

  protected def onShootFromCameraFailure(): Unit = {
    showToast(R.string.retrieving_new_image_error)
  }

  protected def onImportFromGalleryFailure(): Unit = {
    showToast(R.string.retrieving_new_image_error)
  }

  private def executeCreateMyCard(uri: Uri)(op: Option[MyCard] => Unit): Unit = {
    log log "creating a new my card"

    createMyCardTaskOption = Some(new CreateMyCardTask(op))

    createMyCardTaskOption.get.execute(uri)
  }

  private var createMyCardTaskOption: Option[CreateMyCardTask] = None

  private class CreateMyCardTask(op: Option[MyCard] => Unit) extends ScalaAsyncTask[Uri, AnyRef, Option[MyCard]] {

    private val progressDialog = new WriteOnce[ProgressDialog]

    override def onPreExecute(): Unit = {
      super.onPreExecute()

      progressDialog setValue {
        val progDialog = registerProgressDialog {
          val indeterminate = true

          val cancelable = true

          ProgressDialog.show(
            CreateFrontActivity.this,
            getResources.getString(R.string.create_front_progress_dialog_title),
            getResources.getString(R.string.create_front_progress_dialog_message),
            indeterminate,
            cancelable
          )
        }

        progDialog.setOnCancelListener(new OnCancelListener {
          def onCancel(dialog: DialogInterface): Unit = {
            cancel({
              val mayInterruptIfRunning = true

              mayInterruptIfRunning
            })
          }
        })

        progDialog
      }
    }

    protected def doInBackground(photoUri: Uri): Option[MyCard] = {
      MyCard.create(photoUri)
    }

    override def onPostExecute(myCardOption: Option[MyCard]): Unit = {
      super.onPostExecute(myCardOption)

      progressDialog().dismiss()

      op(myCardOption)
    }
  }

}
