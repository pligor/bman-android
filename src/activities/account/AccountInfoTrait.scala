package activities.account

import components.{SimpleJsonRequestTrait, MyActivity}
import myauth.SingleToken
import com.pligor.bman.{Bman, R}
import org.apache.http.HttpStatus
import java.net.URL
import myandroid.AndroidHelper._
import spray.json._
import models.{MyTokenPreference, AccountInfo}
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait AccountInfoTrait extends MyActivity with SimpleJsonRequestTrait {
  //abstract

  protected var accountInfo: Option[AccountInfo];

  def renderAccountInfo(info: AccountInfo): Unit;

  protected def token: String;

  //concrete

  private var accountInfoTask: Option[SimpleJsonRequestTask] = None;

  private val url = new URL(Bman.SERVER_LINK + "/account/retrieveAccountInfo");

  private val connTimeout_msec = 5000;
  private val readTimeout_msec = 2 * connTimeout_msec;

  override def onStart(): Unit = {
    super.onStart();

    accountInfoTask = Some(
      new SimpleJsonRequestTask(
        R.string.server_communication_progress_dialog_title,
        R.string.server_communication_progress_dialog_message
      )({
        jsonResponseOption =>
          if (jsonResponseOption.isDefined) {
            val jsonResponse = jsonResponseOption.get;

            jsonResponse.code match {
              case HttpStatus.SC_OK => {
                log log "the account info response body is: " + jsonResponse.body;
                accountInfo = try {
                  Some(JsonParser(jsonResponse.body).convertTo[AccountInfo](AccountInfo.itsJsonFormat));
                } catch {
                  case e: org.parboiled.errors.ParsingException => None;
                }
                if (accountInfo.isDefined) {
                  renderAccountInfo(accountInfo.get);
                } else {
                  showToast(R.string.not_json_response);
                  finish();
                }
              }
              case HttpStatus.SC_UNAUTHORIZED => {
                showToast(R.string.unauthorized_auto_signout);
                MyTokenPreference.clear;
                finish();
              }
              case HttpStatus.SC_BAD_REQUEST => {
                showToast(R.string.bad_request_server_response, Bman.URL);
                finish();
              }
              case _ => {
                showToast(R.string.unexpected_server_response);
                finish();
              }
            }
          } else {
            showToast(R.string.server_communication_error);
            finish();
          }
      })
    );

    accountInfoTask.get.execute(
      JsonRequestParam(url, SingleToken(token).toJson, connTimeout_msec, readTimeout_msec)
    );
  }

  override def onStop(): Unit = {
    super.onStop()

    if (accountInfoTask.isDefined) {
      accountInfoTask.get.cancel({
        val mayInterruptIfRunning = true;
        mayInterruptIfRunning
      })
    } else {
      //nop
    }
  }

}




