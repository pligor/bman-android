package activities.account

import spray.json._
import components.{SimpleJsonRequestTrait, MyActivity}
import models.{EmailPendingPair, AccountInfo}
import java.net.URL
import com.pligor.bman.{R, Bman}
import android.app.Dialog
import android.widget.{Button, EditText}
import android.view.{Window, View}
import myauth.CredentialToken
import myandroid.AndroidHelper._
import org.apache.http.HttpStatus

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait ChangeMailTrait extends MyActivity with SimpleJsonRequestTrait {
  //abstract

  protected var accountInfo: Option[AccountInfo];

  protected def token: String;

  def renderEmail(emailPendingPair: Option[EmailPendingPair]): Unit;

  //concrete

  private val url = new URL(Bman.SERVER_LINK + "/account/changeEmail");

  private val connTimeout_msec = 5000;
  private val readTimeout_msec = 3 * connTimeout_msec;

  private var task: Option[SimpleJsonRequestTask] = None;

  def getChangeMailDialog = {
    val dialog = new Dialog(context);

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
    dialog.setCanceledOnTouchOutside(false);

    dialog.setCancelable(true);

    dialog.setContentView(R.layout.dialog_change_mail);

    val changeMailEditText = dialog.findViewById(R.id.changeMailEditText).asInstanceOf[EditText];
    val changeMailPassEditText = dialog.findViewById(R.id.changeMailPassEditText).asInstanceOf[EditText];
    val changeMailButton = dialog.findViewById(R.id.changeMailButton).asInstanceOf[Button];

    changeMailButton.setOnClickListener(new View.OnClickListener {
      def onClick(view: View) {
        val credentialToken = CredentialToken(
          token = token,
          identification = changeMailEditText.getText.toString.trim,
          password = changeMailPassEditText.getText.toString
        );

        if (credentialToken.isIdentificationEmail) {
          task = Some(
            new SimpleJsonRequestTask(
              R.string.server_communication_progress_dialog_title,
              R.string.server_communication_progress_dialog_message
            )({
              jsonResponseOption =>
                if (jsonResponseOption.isDefined) {
                  val jsonResponse = jsonResponseOption.get;

                  jsonResponse.code match {
                    case HttpStatus.SC_BAD_REQUEST => {
                      showToast(R.string.bad_request_server_response, Bman.URL);
                    }
                    case HttpStatus.SC_UNAUTHORIZED => {
                      showToast(R.string.unauthorized_server_response);
                    }
                    case HttpStatus.SC_EXPECTATION_FAILED => {
                      showToast(R.string.invalid_mail);
                    }
                    case HttpStatus.SC_PRECONDITION_FAILED => {
                      showToast(R.string.password_wrong);
                    }
                    case HttpStatus.SC_CONFLICT => {
                      showToast(R.string.mail_already_exists, credentialToken.identification);
                    }
                    case HttpStatus.SC_OK => {
                      val mail = credentialToken.identification;

                      assert(accountInfo.isDefined, "user must not be able to change his email if has not retrieved the account info first");
                      accountInfo = Some(accountInfo.get.setNewEmail(mail));

                      renderEmail(accountInfo.get.emailPendingPair);

                      dialog.dismiss();

                      showToast(R.string.change_mail_success);
                    }
                    case _ => {
                      showToast(R.string.unexpected_server_response);
                    }
                  }
                } else {
                  showToast(R.string.server_communication_error);
                  //and do nothing more
                }
            })
          );

          task.get.execute(
            JsonRequestParam(
              url,
              credentialToken.toJson(CredentialToken.itsJsonFormat),
              connTimeout_msec,
              readTimeout_msec
            )
          );

        } else {
          showToast(R.string.invalid_mail);
        }
      }
    });

    dialog;
  }
}
