package activities.account

import components.{SimpleJsonRequestTrait, MyActivity}
import java.net.URL
import com.pligor.bman.{R, Bman}
import myandroid.AndroidHelper._
import org.apache.http.HttpStatus
import models.AccountInfo
import myauth._
import android.app.Dialog
import android.widget.{EditText, Button}
import android.view.{Window, View}
import android.view.View.OnClickListener
import spray.json._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait PassChangeTrait extends MyActivity with SimpleJsonRequestTrait {
  //abstract

  protected var accountInfo: Option[AccountInfo];

  protected def token: String;

  def renderPasswordButton(curPassLength: Short): Unit;

  //concrete

  private var passChangeTask: Option[SimpleJsonRequestTask] = None;

  private val url = new URL(Bman.SERVER_LINK + "/account/changePassword");

  private val connTimeout_msec = 5000;
  private val readTimeout_msec = 2 * connTimeout_msec;

  def getPasswordChangeDialog = {
    val dialog = new Dialog(context);

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
    dialog.setCanceledOnTouchOutside(false);

    dialog.setCancelable(true);

    dialog.setContentView(R.layout.dialog_change_password);

    val oldPassEditText = dialog.findViewById(R.id.oldPassEditText).asInstanceOf[EditText];
    val newPassEditText = dialog.findViewById(R.id.newPassEditText).asInstanceOf[EditText];
    val repeatNewPassEditText = dialog.findViewById(R.id.repeatNewPassEditText).asInstanceOf[EditText];
    val changePasswordButton = dialog.findViewById(R.id.changePasswordButton).asInstanceOf[Button];

    changePasswordButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        val tokenOldNewPass = TokenOldNewPass(
          token,
          old_pass = oldPassEditText.getText.toString,
          new_pass = newPassEditText.getText.toString
        );

        if (tokenOldNewPass.new_pass == repeatNewPassEditText.getText.toString) {
          if (!tokenOldNewPass.arePasswordsIdentical) {
            if (AccountConfig.hasPasswordValidLength(tokenOldNewPass.new_pass)) {
              passChangeTask = Some(
                new SimpleJsonRequestTask(
                  R.string.server_communication_progress_dialog_title,
                  R.string.server_communication_progress_dialog_message
                )({
                  jsonResponseOption =>
                    if (jsonResponseOption.isDefined) {
                      val jsonResponse = jsonResponseOption.get;

                      jsonResponse.code match {
                        case HttpStatus.SC_BAD_REQUEST => {
                          showToast(R.string.bad_request_server_response, Bman.URL);
                        }
                        case HttpStatus.SC_UNAUTHORIZED => {
                          showToast(R.string.unauthorized_server_response);
                        }
                        case HttpStatus.SC_EXPECTATION_FAILED => {
                          showToast(R.string.unknown_server_database_error);
                        }
                        case HttpStatus.SC_FORBIDDEN => {
                          showToast(R.string.password_wrong);
                        }
                        case HttpStatus.SC_PRECONDITION_FAILED => {
                          showToast(R.string.invalid_password_length, AccountConfig.minPassLength.toString);
                        }
                        case HttpStatus.SC_OK => {
                          val passLength = try {
                            Some(JsonParser(jsonResponse.body).convertTo[PassLength](PassLength.itsJsonFormat));
                          } catch {
                            case e: org.parboiled.errors.ParsingException => None;
                          }

                          if (passLength.isDefined) {
                            assert(accountInfo.isDefined, "user must not be able to change password if has not retrieved the account info first");
                            accountInfo = Some(accountInfo.get.setPassLength(passLength.get.pass_len));

                            renderPasswordButton(accountInfo.get.passLength);

                            dialog.dismiss();

                            showToast(R.string.pass_change_success);
                          } else {
                            showToast(R.string.not_json_response);
                          }
                        }
                        case _ => {
                          showToast(R.string.unexpected_server_response);
                        }
                      }
                    } else {
                      showToast(R.string.server_communication_error);
                      //and do nothing more
                    }
                })
              );

              passChangeTask.get.execute(
                JsonRequestParam(
                  url,
                  tokenOldNewPass.toJson(TokenOldNewPass.itsJsonFormat),
                  connTimeout_msec,
                  readTimeout_msec
                )
              );
            } else {
              showToast(R.string.invalid_password_length, AccountConfig.minPassLength.toString);
            }
          } else {
            showToast(R.string.identical_passwords);
          }
        } else {
          showToast(R.string.repeated_pass_no_match);
        }
      }
    });

    dialog;
  }

  override def onStop() {
    super.onStop();

    if (passChangeTask.isDefined) {
      passChangeTask.get.cancel({
        val mayInterruptIfRunning = true;
        mayInterruptIfRunning
      })
    } else {
      //nop
    }
  }
}
