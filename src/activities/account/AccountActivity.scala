package activities.account

import android.app.Activity
import com.pligor.bman.{TypedViewHolder, TR, R}
import android.os.Bundle
import components.{AdmobActivity, MyActivity}
import models.{MyTokenPreference, EmailPendingPair, CellphonePendingPair, AccountInfo}
import myandroid.AndroidHelper._
import android.view.View
import generic.PhoneNumberHelper._
import myauth.SignOutTrait
import android.view.View.OnClickListener
import myAndroidAPIs.BugsenseActivity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class AccountActivity extends Activity with TypedViewHolder
with MyActivity

with AdmobActivity

with AccountInfoTrait
with PassChangeTrait
with AddCellphoneTrait
with AddEmailTrait
with ChangeCellphoneTrait
with ChangeMailTrait
with SignOutTrait
with MergeAccountsTrait
with BugsenseActivity {
  private val passChar = "*";

  protected var accountInfo: Option[AccountInfo] = None;

  protected def isProgressDialogShown: Boolean = true;

  private lazy val accountCellphoneButton = findView(TR.accountCellphoneButton);
  private lazy val cellphonePendingTextView = findView(TR.cellphonePendingTextView);

  private lazy val accountEmailButton = findView(TR.accountEmailButton);
  private lazy val mailPendingTextView = findView(TR.mailPendingTextView);

  private lazy val accountPasswordButton = findView(TR.accountPasswordButton);

  private lazy val signoutButton = findView(TR.signoutButton);

  private lazy val accountMergeButton = findView(TR.accountMergeButton);

  protected lazy val token = {
    val tokenOption = MyTokenPreference.getValue;
    assert(tokenOption.isDefined, "we should not start account activity if no token is present");
    tokenOption.get;
  };

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_account);

    Seq(accountCellphoneButton, cellphonePendingTextView, accountEmailButton,
      mailPendingTextView, accountPasswordButton, accountMergeButton).map(_.setVisibility(View.GONE));

    signoutButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        signout({
          result =>
            if (result.isDefined) {
              if (result.get) {
                showToast(R.string.signout_success);
              } else {
                showToast(R.string.signout_neutral);
              }
            } else {
              showToast(R.string.server_connection_error);
            }

            finish();
        });
      }
    });

    accountCellphoneButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        require(accountInfo.isDefined, "the account info must be defined before the user will be able to click");
        val cellPendingPair = accountInfo.get.cellphonePendingPair
        if (cellPendingPair.isDefined) {
          if (cellPendingPair.get.isPending) {
            showToast(R.string.change_cellphone_pending);
          } else {
            getChangeCellphoneDialog.show();
          }
        } else {
          getAddCellphoneDialog.show();
        }
      }
    });

    accountEmailButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        require(accountInfo.isDefined, "the account info must be defined before the user will be able to click");
        val emailPendingPair = accountInfo.get.emailPendingPair;
        if (emailPendingPair.isDefined) {
          if (emailPendingPair.get.isPending) {
            showToast(R.string.change_mail_pending);
          } else {
            getChangeMailDialog.show();
          }
        } else {
          getAddEmailDialog.show();
        }
      }
    });

    accountPasswordButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        getPasswordChangeDialog.show();
      }
    });

    accountMergeButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        getMergeAccountsDialog.show();
      }
    });
  }

  override def onStart() {
    super.onStart();
  }

  override def onStop() {
    super.onStop();
  }

  private def onBeforeRender() {
    //nop
  }

  private def onAfterRender() {
    accountMergeButton.setVisibility(
      if (accountInfo.get.isComplete) View.GONE else View.VISIBLE
    );
  }

  private def render[T](op: => Unit) {
    onBeforeRender();
    op;
    onAfterRender();
  }

  def renderAccountInfo(info: AccountInfo) {
    render {
      renderPasswordButton(info.passLength);

      renderCellphone(info.cellphonePendingPair);

      renderEmail(info.emailPendingPair);

      accountMergeButton.setVisibility(View.VISIBLE);
    }
  }

  def renderEmail(emailPendingPair: Option[EmailPendingPair]) {
    render {
      mailPendingTextView.setVisibility(
        if (emailPendingPair.isDefined && emailPendingPair.get.isPending) View.VISIBLE
        else View.GONE
      );

      accountEmailButton.setVisibility(View.VISIBLE);
      accountEmailButton.setText(
        if (emailPendingPair.isDefined) emailPendingPair.get.mail
        else getResources.getString(R.string.account_email_button)
      );
    }
  }

  def renderCellphone(cellphonePendingPair: Option[CellphonePendingPair]) {
    render {
      cellphonePendingTextView.setVisibility(
        if (cellphonePendingPair.isDefined && cellphonePendingPair.get.isPending) View.VISIBLE
        else View.GONE
      )

      accountCellphoneButton.setVisibility(View.VISIBLE)

      accountCellphoneButton.setText(
        if (cellphonePendingPair.isDefined) getPhoneString(cellphonePendingPair.get.cellphone)
        else getResources.getString(R.string.account_cellphone_button)
      )
    }
  }

  def renderPasswordButton(curPassLength: Short) {
    render {
      accountPasswordButton.setVisibility(View.VISIBLE)

      accountPasswordButton.setText(
        getResources.getString(R.string.account_password_button, passChar * curPassLength)
      )
    }
  }
}
