package activities.account

import components.{SimpleJsonRequestTrait, MyActivity}
import models.{CellphonePendingPair, AccountInfo}
import java.net.URL
import com.pligor.bman.{R, Bman}
import android.app.Dialog
import android.widget.{Button, EditText}
import android.view.{Window, View}
import android.view.View.OnClickListener
import myauth.CredentialToken
import myandroid.AndroidHelper._
import org.apache.http.HttpStatus
import generic.PhoneNumberHelper._
import spray.json._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait ChangeCellphoneTrait extends MyActivity with SimpleJsonRequestTrait {
  //abstract

  protected var accountInfo: Option[AccountInfo];

  protected def token: String;

  def renderCellphone(cellphonePendingPair: Option[CellphonePendingPair]): Unit;

  //concrete

  private val url = new URL(Bman.SERVER_LINK + "/account/changeCellphone");

  private val connTimeout_msec = 5000;
  private val readTimeout_msec = 3 * connTimeout_msec;

  private var task: Option[SimpleJsonRequestTask] = None;

  def getChangeCellphoneDialog = {
    val dialog = new Dialog(context);

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
    dialog.setCanceledOnTouchOutside(false);

    dialog.setCancelable(true);

    dialog.setContentView(R.layout.dialog_change_cellphone);

    val changeCellphoneEditText = dialog.findViewById(R.id.changeCellphoneEditText).asInstanceOf[EditText];
    val changeCellphonePassEditText = dialog.findViewById(R.id.changeCellphonePassEditText).asInstanceOf[EditText];
    val changeCellphoneButton = dialog.findViewById(R.id.changeCellphoneButton).asInstanceOf[Button];

    changeCellphoneButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        val credentialToken = CredentialToken(
          token = token,
          identification = changeCellphoneEditText.getText.toString.trim,
          password = changeCellphonePassEditText.getText.toString
        );

        if (credentialToken.isIdentificationPhoneNumber) {
          task = Some(
            new SimpleJsonRequestTask(
              R.string.server_communication_progress_dialog_title,
              R.string.server_communication_progress_dialog_message
            )({
              jsonResponseOption =>
                if (jsonResponseOption.isDefined) {
                  val jsonResponse = jsonResponseOption.get;

                  jsonResponse.code match {
                    case HttpStatus.SC_BAD_REQUEST => {
                      showToast(R.string.bad_request_server_response, Bman.URL)
                    }
                    case HttpStatus.SC_UNAUTHORIZED => {
                      showToast(R.string.unauthorized_server_response)
                    }
                    case HttpStatus.SC_EXPECTATION_FAILED => {
                      showToast(R.string.invalid_cellphone)
                    }
                    case HttpStatus.SC_PRECONDITION_FAILED => {
                      showToast(R.string.password_wrong)
                    }
                    case HttpStatus.SC_CONFLICT => {
                      showToast(R.string.cellphone_already_exists, credentialToken.identification)
                    }
                    case HttpStatus.SC_OK => {
                      val cellphone = parsePhoneString(credentialToken.identification).get

                      assert(accountInfo.isDefined, "user must not be able to change his cellphone if has not retrieved the account info first");
                      accountInfo = Some(accountInfo.get.setNewCellphone(cellphone))
                      renderCellphone(accountInfo.get.cellphonePendingPair)

                      dialog.dismiss()

                      showToast(R.string.change_cellphone_success, getPhoneString(cellphone))
                    }
                    case _ => {
                      showToast(R.string.unexpected_server_response)
                    }
                  }
                } else {
                  showToast(R.string.server_communication_error)
                  //and do nothing more
                }
            })
          );

          task.get.execute(
            JsonRequestParam(
              url,
              credentialToken.toJson(CredentialToken.itsJsonFormat),
              connTimeout_msec,
              readTimeout_msec
            )
          );

        } else {
          showToast(R.string.invalid_cellphone);
        }
      }
    });

    dialog;
  }
}
