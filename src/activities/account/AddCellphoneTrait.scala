package activities.account

import components.{SimpleJsonRequestTrait, MyActivity}
import java.net.URL
import com.pligor.bman.{R, Bman}
import models.{IdentificationTokenPair, CellphonePendingPair, AccountInfo}
import org.apache.http.HttpStatus
import myandroid.AndroidHelper._
import android.widget.{TextView, Button, EditText}
import generic.PhoneNumberHelper._
import spray.json._
import android.app.Dialog
import android.view.View.OnClickListener
import android.view.{Window, View}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait AddCellphoneTrait extends MyActivity with SimpleJsonRequestTrait {
  //abstract

  protected var accountInfo: Option[AccountInfo];

  protected def token: String;

  def renderCellphone(cellphonePendingPair: Option[CellphonePendingPair]): Unit;

  //concrete
  private val url = new URL(Bman.SERVER_LINK + "/account/addCellphone");

  private val connTimeout_msec = 5000;
  private val readTimeout_msec = 3 * connTimeout_msec;

  private var task: Option[SimpleJsonRequestTask] = None;

  def getAddCellphoneDialog = {
    val dialog = new Dialog(context);

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
    dialog.setCanceledOnTouchOutside(false);

    dialog.setCancelable(true);

    dialog.setContentView(R.layout.dialog_add_cellphone);

    val addCellphoneEditText = dialog.findViewById(R.id.addCellphoneEditText).asInstanceOf[EditText];

    val addCellphoneButton = dialog.findViewById(R.id.addCellphoneButton).asInstanceOf[Button];

    addCellphoneButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
          val cellphoneOption = parsePhoneString(addCellphoneEditText.getText.toString.trim);

          if (cellphoneOption.isDefined) {
            val cellphone = cellphoneOption.get;

            task = Some(
              new SimpleJsonRequestTask(
                R.string.server_communication_progress_dialog_title,
                R.string.server_communication_progress_dialog_message
              )({
                jsonResponseOption =>
                  if (jsonResponseOption.isDefined) {
                    val jsonResponse = jsonResponseOption.get

                    jsonResponse.code match {
                      case HttpStatus.SC_BAD_REQUEST => {
                        showToast(R.string.bad_request_server_response, Bman.URL)
                      }
                      case HttpStatus.SC_UNAUTHORIZED => {
                        showToast(R.string.unauthorized_server_response)
                      }
                      case HttpStatus.SC_EXPECTATION_FAILED => {
                        showToast(R.string.invalid_cellphone)
                      }
                      case HttpStatus.SC_CONFLICT => {
                        showToast(R.string.cellphone_already_exists, getPhoneString(cellphone))
                      }
                      case HttpStatus.SC_OK => {
                        assert(accountInfo.isDefined, "user must not be able to add a cellphone if has not retrieved the account info first");
                        accountInfo = Some(accountInfo.get.setNewCellphone(cellphone))

                        renderCellphone(accountInfo.get.cellphonePendingPair)

                        dialog.dismiss()

                        showToast(R.string.add_cellphone_success, getPhoneString(cellphone))
                      }
                      case _ => {
                        showToast(R.string.unexpected_server_response)
                      }
                    }
                  } else {
                    showToast(R.string.server_communication_error)
                    //and do nothing more
                  }
              })
            )

            val identificationTokenPair = IdentificationTokenPair(
              identification = getPhoneString(cellphone),
              token = token
            )

            task.get.execute(
              JsonRequestParam(
                url,
                identificationTokenPair.toJson(IdentificationTokenPair.itsJsonFormat),
                connTimeout_msec,
                readTimeout_msec
              )
            );

          } else {
            showToast(R.string.invalid_cellphone);
          }
      }
    });

    dialog;
  }

  override def onStop() {
    super.onStop();

    if (task.isDefined) {
      task.get.cancel({
        val mayInterruptIfRunning = true;
        mayInterruptIfRunning
      })
    } else {
      //nop
    }
  }
}
