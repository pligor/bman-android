package activities.account

import components.{EmailHelper, SimpleJsonRequestTrait, MyActivity}
import models.{IdentificationTokenPair, EmailPendingPair, AccountInfo}
import com.pligor.bman.{R, Bman}
import java.net.URL
import android.app.Dialog
import android.widget.{Button, EditText}
import android.view.{Window, View}
import EmailHelper._
import org.apache.http.HttpStatus
import myandroid.AndroidHelper._
import spray.json._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait AddEmailTrait extends MyActivity with SimpleJsonRequestTrait {
  //abstract

  protected var accountInfo: Option[AccountInfo];

  protected def token: String;

  def renderEmail(emailPendingPair: Option[EmailPendingPair]): Unit;

  //concrete
  private val url = new URL(Bman.SERVER_LINK + "/account/addEmail");

  private val connTimeout_msec = 5000;
  private val readTimeout_msec = 3 * connTimeout_msec;

  private var task: Option[SimpleJsonRequestTask] = None;

  def getAddEmailDialog = {
    val dialog = new Dialog(context);

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
    dialog.setCanceledOnTouchOutside(false);

    dialog.setCancelable(true);

    dialog.setContentView(R.layout.dialog_add_email);

    val addEmailEditText = dialog.findViewById(R.id.addEmailEditText).asInstanceOf[EditText];
    val addEmailButton = dialog.findViewById(R.id.addEmailButton).asInstanceOf[Button];

    addEmailButton.setOnClickListener(new View.OnClickListener {
      def onClick(view: View) {
        val mail = addEmailEditText.getText.toString.trim;

        if (isValidEmailAddress(mail)) {
          task = Some(
            new SimpleJsonRequestTask(
              R.string.server_communication_progress_dialog_title,
              R.string.server_communication_progress_dialog_message
            )({
              jsonResponseOption =>
                if (jsonResponseOption.isDefined) {
                  val jsonResponse = jsonResponseOption.get;

                  jsonResponse.code match {
                    case HttpStatus.SC_BAD_REQUEST => {
                      showToast(R.string.bad_request_server_response, Bman.URL);
                    }
                    case HttpStatus.SC_UNAUTHORIZED => {
                      showToast(R.string.unauthorized_server_response);
                    }
                    case HttpStatus.SC_EXPECTATION_FAILED => {
                      showToast(R.string.invalid_mail);
                    }
                    case HttpStatus.SC_CONFLICT => {
                      showToast(R.string.mail_already_exists, mail);
                    }
                    case HttpStatus.SC_OK => {
                      assert(accountInfo.isDefined, "user must not be able to add an email if has not retrieved the account info first");
                      accountInfo = Some(accountInfo.get.setNewEmail(mail));

                      renderEmail(accountInfo.get.emailPendingPair);

                      dialog.dismiss();

                      showToast(R.string.add_mail_success);
                    }
                    case _ => {
                      showToast(R.string.unexpected_server_response);
                    }
                  }
                } else {
                  showToast(R.string.server_communication_error);
                  //and do nothing more
                }
            })
          );

          val identificationTokenPair = IdentificationTokenPair(
            identification = mail,
            token = token
          );

          task.get.execute(
            JsonRequestParam(
              url,
              identificationTokenPair.toJson(IdentificationTokenPair.itsJsonFormat),
              connTimeout_msec,
              readTimeout_msec
            )
          );

        } else {
          showToast(R.string.invalid_mail);
        }
      }
    });

    dialog;
  }

  override def onStop() {
    super.onStop();

    if (task.isDefined) {
      task.get.cancel({
        val mayInterruptIfRunning = true;
        mayInterruptIfRunning
      })
    } else {
      //nop
    }
  }
}

