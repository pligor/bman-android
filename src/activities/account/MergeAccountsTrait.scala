package activities.account

import components.{SimpleJsonRequestTrait, MyActivity}
import models.{EmailPendingPair, CellphonePendingPair, AccountInfo}
import java.net.URL
import com.pligor.bman.{R, Bman}
import android.app.Dialog
import android.widget.{Button, EditText}
import android.view.{Window, View}
import android.view.View.OnClickListener
import myauth.CredentialToken
import myandroid.AndroidHelper._
import org.apache.http.HttpStatus
import spray.json._
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait MergeAccountsTrait extends MyActivity with SimpleJsonRequestTrait {
  //abstract

  protected var accountInfo: Option[AccountInfo];

  protected def token: String;

  def renderEmail(emailPendingPair: Option[EmailPendingPair]): Unit;

  def renderCellphone(cellphonePendingPair: Option[CellphonePendingPair]): Unit;

  //concrete

  private val url = new URL(Bman.SERVER_LINK + "/account/mergeAccounts");

  private val connTimeout_msec = 5000;
  private val readTimeout_msec = 3 * connTimeout_msec;

  private var task: Option[SimpleJsonRequestTask] = None;

  def getMergeAccountsDialog = {
    val dialog = new Dialog(context);

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
    dialog.setCanceledOnTouchOutside(false);

    dialog.setCancelable(true);

    dialog.setContentView(R.layout.dialog_merge_accounts);

    val mergeAccountsEditText = dialog.findViewById(R.id.mergeAccountsEditText).asInstanceOf[EditText];
    val mergeAccountsPassEditText = dialog.findViewById(R.id.mergeAccountsPassEditText).asInstanceOf[EditText];
    val mergeAccountsButton = dialog.findViewById(R.id.mergeAccountsButton).asInstanceOf[Button];

    mergeAccountsButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        val credentialToken = CredentialToken(
          token = token,
          identification = mergeAccountsEditText.getText.toString.trim,
          password = mergeAccountsPassEditText.getText.toString
        );

        if (credentialToken.isIdentificationValid) {
          task = Some(
            new SimpleJsonRequestTask(
              R.string.server_communication_progress_dialog_title,
              R.string.server_communication_progress_dialog_message
            )({
              jsonResponseOption =>
                if (jsonResponseOption.isDefined) {
                  val jsonResponse = jsonResponseOption.get;

                  log log "json response code on merging is: " + jsonResponse.code;

                  jsonResponse.code match {
                    case HttpStatus.SC_BAD_REQUEST => {
                      showToast(R.string.bad_request_server_response, Bman.URL);
                    }
                    case HttpStatus.SC_UNAUTHORIZED => {
                      showToast(R.string.unauthorized_server_response);
                    }
                    case HttpStatus.SC_CONFLICT => {
                      showToast(R.string.merge_accounts_identical);
                    }
                    case HttpStatus.SC_PRECONDITION_FAILED => {
                      showToast(R.string.merge_accounts_password_wrong);
                    }
                    case HttpStatus.SC_EXPECTATION_FAILED => {
                      showToast(R.string.merge_accounts_both_incomplete_warning);
                    }
                    case HttpStatus.SC_INTERNAL_SERVER_ERROR => {
                      showToast(R.string.merge_accounts_internal_server_error, Bman.URL);
                    }
                    case status@(HttpStatus.SC_NO_CONTENT | HttpStatus.SC_OK) => {
                      assert(accountInfo.isDefined, "user should not change either email or cellphone if has not retrieved the account info first");

                      val isPending = status == HttpStatus.SC_NO_CONTENT;

                      credentialToken.getEmailOrPhone.get.fold({
                        mail =>
                          accountInfo = Some(accountInfo.get.setNewEmail(mail, isPending));
                          renderEmail(accountInfo.get.emailPendingPair);
                      }, {
                        phoneNumber =>
                          accountInfo = Some(accountInfo.get.setNewCellphone(phoneNumber, isPending));
                          renderCellphone(accountInfo.get.cellphonePendingPair);
                      });

                      dialog.dismiss();

                      showToast(R.string.merge_accounts_success);
                    }
                    case _ => {
                      showToast(R.string.unexpected_server_response);
                    }
                  }
                } else {
                  showToast(R.string.server_communication_error);
                  //and do nothing more
                }
            })
          );

          task.get.execute(
            JsonRequestParam(
              url,
              credentialToken.toJson(CredentialToken.itsJsonFormat),
              connTimeout_msec,
              readTimeout_msec
            )
          );

        } else {
          showToast(R.string.invalid_identification);
        }
      }
    });

    dialog;
  }
}
