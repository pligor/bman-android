package activities

import android.os.Bundle
import components.WebviewHelper._
import com.pligor.bman.{Bman, R, TypedViewHolder, TR}
import android.webkit.{WebView, WebViewClient}
import components.MyActivity
import android.view.View

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class HelpActivity extends MyActivity with TypedViewHolder {
  private val HELP_LINK = "http://www.bman.co/#!help/c1omt";

  private lazy val helpWebView = findView(TR.helpWebView);

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_help);

    helpWebView.getSettings.setJavaScriptEnabled(true);
    helpWebView.getSettings.setLoadWithOverviewMode(false); // complete zoom out
    helpWebView.getSettings.setUseWideViewPort(false); //this is to say that we have a wide screen like a desktop
    helpWebView.getSettings.setLoadsImagesAutomatically(true);
    helpWebView.getSettings.setBlockNetworkImage(false);
    helpWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

    helpWebView.setWebViewClient(new WebViewClient {
      override def onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        //view.setVisibility(View.GONE);
      }
    });
  }

  override def onStart() {
    super.onStart();

    loadUrlIfConnected(HELP_LINK, helpWebView);
  }
}
