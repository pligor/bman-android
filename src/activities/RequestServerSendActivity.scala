package activities

import android.app.Activity
import com.pligor.bman.{Bman, R, TR, TypedViewHolder}
import components._
import android.os.Bundle
import models._
import java.io.File
import scalaj.http.{HttpOptions, Http}
import spray.json._
import org.apache.http.HttpStatus
import generic.FileHelper._
import java.net.URL
import myandroid.{ScalaAsyncTask, log}
import android.view.View
import com.todddavies.components.progressbar.ProgressWheel
import components.ProgressWheelHelper._
import HttpHelper._
import myAndroidAPIs.BugsenseActivity
import components.EmailHelper._
import generic.PhoneNumberHelper._
import generic.HashFile
import components.HttpHelper.FileMultipart
import scalaj.http.HttpException
import models.SendJsonFileRequest

object RequestServerSendActivity {

  object EXTRA extends Enumeration {
    val TOKEN = Value

    val IDENTIFICATION = Value

    val CARD_ID = Value
  }

}

//TODO unify this activity to have the same trait as the bluetooth and alljoyn sending activities (for sending layout etc.)

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class RequestServerSendActivity extends Activity with TypedViewHolder
with MyActivity

with AdmobActivity

with TransferringStatement
with BugsenseActivity {
  private var checkJsonHashTask: Option[CheckJsonHashTask] = None

  private var sendEntireJsonFileTask: Option[SendEntireJsonFileTask] = None

  private var resendJsonFileTask: Option[ResendJsonFileTask] = None

  private var generateTempJsonFileTaskOption: Option[GenerateTempJsonFileTask] = None

  private lazy val token = getIntent.getStringExtra(RequestServerSendActivity.EXTRA.TOKEN.toString)

  private lazy val identification = getIntent.getStringExtra(RequestServerSendActivity.EXTRA.IDENTIFICATION.toString)

  private lazy val cardId = {
    val card_id = getIntent.getLongExtra(RequestServerSendActivity.EXTRA.CARD_ID.toString, Card.invalidId)

    require(card_id != Card.invalidId, "we must have an existing saved on the db to work with")

    card_id
  }

  //create the file now for performance reasons
  //this file is deleted on stop of the activity
  private var jsonFile: Option[File] = None
  //new ExtraLazy[File](Card.genTempJsonFileForSending(cardId))

  private lazy val sendingDestinationTextView = findView(TR.sendingDestinationTextView)

  private lazy val sendingResultLayout = findView(TR.sendingResultLayout)

  private lazy val sendingProgressWheel = findViewById(R.id.sendingProgressWheel).asInstanceOf[ProgressWheel]

  private lazy val sendingRetryMessage = findView(TR.sendingRetryMessage)

  private lazy val sendingResultIcon = findView(TR.sendingResultIcon)

  private lazy val sendingResultMessage = findView(TR.sendingResultMessage)

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_sending)

    sendingResultLayout.setVisibility(View.GONE)

    sendingDestinationTextView.setText(
      getResources.getString(R.string.sending_destination).format(identification)
    )
  }

  override def onStart(): Unit = {
    super.onStart()

    generateTempJsonFileTaskOption = Some(new GenerateTempJsonFileTask({
      tempJsonFileOption =>
        if (tempJsonFileOption.isDefined) {
          val tempJsonFile = tempJsonFileOption.get

          jsonFile = Some(tempJsonFile)

          val jsonHash = HashFile(tempJsonFile)

          checkJsonHashTask = Some(new CheckJsonHashTask)

          checkJsonHashTask.get.execute({
            val tokenHashPair = TokenHashPair(token, jsonHash.toString)

            log log "tokenHashPair: " + tokenHashPair

            tokenHashPair
          })
        } else {
          renderResult(success = false, R.string.card_json_file_could_not_created)
        }
    }))

    generateTempJsonFileTaskOption.get.execute(cardId)
  }

  override def onStop(): Unit = {
    super.onStop()

    generateTempJsonFileTaskOption.get.cancel({
      val mayInterruptIfRunning = true

      mayInterruptIfRunning
    })

    jsonFile.map(_.delete)

    checkJsonHashTask.map(_.cancel({
      val mayInterruptIfRunning = true

      mayInterruptIfRunning
    }))

    sendEntireJsonFileTask.map(_.cancel({
      val mayInterruptIfRunning = true

      mayInterruptIfRunning
    }))

    resendJsonFileTask.map {
      task =>
        task.cancel({
          val mayInterruptIfRunning = true

          mayInterruptIfRunning
        })
    }
  }

  override def onBackPressed(): Unit = {
    //super.onBackPressed()
    finish()
  }

  private def renderResult(success: Boolean, messageId: Int, args: String*): Unit = {
    if (isValidEmailAddress(identification)) {
      MyBugsense.Event.sendBugsenseEvent(
        if (success) MyBugsense.Event.CARD_SEND_SUCCESS_VIA_EMAIL
        else MyBugsense.Event.CARD_SEND_FAIL_VIA_EMAIL
      )
    } else if (isValidPhoneNumber(identification)) {
      MyBugsense.Event.sendBugsenseEvent(
        if (success) MyBugsense.Event.CARD_SEND_SUCCESS_VIA_SMS
        else MyBugsense.Event.CARD_SEND_FAIL_VIA_SMS
      )
    } else {
      //we should never reach this place but we wont risk an exception just for a bugsense event
    }

    sendingProgressWheel.setVisibility(View.GONE)

    sendingResultLayout.setVisibility(View.VISIBLE)

    sendingRetryMessage.setVisibility(View.INVISIBLE)

    sendingResultIcon.setImageResource(if (success) R.drawable.yes else R.drawable.no)

    sendingResultMessage.setText(getResources.getString(messageId, args))
  }

  private class CheckJsonHashTask extends ScalaAsyncTask[TokenHashPair, AnyRef, Option[Int]] {
    private val connectionTimeout_msec = 5000

    private val readTimeout_msec = 2 * connectionTimeout_msec

    private val url = Bman.SERVER_LINK + "/card/checkJsonHash"

    protected def doInBackground(tokenHashPair: TokenHashPair): Option[Int] = {
      try {
        val responseCode = Http.postData(url, tokenHashPair.toJson(TokenHashPair.itsJsonFormat).toString()).
          header("Content-Type", "application/json").
          option(HttpOptions.connTimeout(connectionTimeout_msec)).
          option(HttpOptions.readTimeout(readTimeout_msec)).responseCode

        log log "check json hash response code: " + responseCode

        Some(responseCode)
      } catch {
        case e: HttpException => Some(e.code)
        case e: Exception => None
      }
    }

    override def onPostExecute(responseCodeOption: Option[Int]): Unit = {
      super.onPostExecute(responseCodeOption)

      responseCodeOption match {
        case Some(HttpStatus.SC_OK) => {
          log log "request from server to send that same card to the receipient"

          //set render progress wheel to 50 percent
          renderProgressWheel(0.5F, sendingProgressWheel)

          resendJsonFileTask = Some(new ResendJsonFileTask)

          resendJsonFileTask.get.execute(IdentificationTokenPair(identification, token))
        }
        case Some(HttpStatus.SC_EXPECTATION_FAILED) => {
          log log "send the entire json with the receipient and everything"

          sendEntireJsonFileTask = Some(new SendEntireJsonFileTask)

          sendEntireJsonFileTask.get.execute(SendJsonFileRequest(jsonFile.get, identification, token))
        }
        case Some(HttpStatus.SC_UNAUTHORIZED) => {
          renderResult(success = false, messageId = R.string.unauthorized_server_response)
        }
        case _ => {
          renderResult(success = false, messageId = R.string.server_connection_failed)
        }
      }
    }
  }

  private class ResendJsonFileTask extends ScalaAsyncTask[IdentificationTokenPair, AnyRef, Option[Int]] {
    private val connectionTimeout_msec = 5000

    private val readTimeout_msec = 2 * connectionTimeout_msec

    private val url = Bman.SERVER_LINK + "/card/reTransfer"

    protected def doInBackground(resendJsonFileRequest: IdentificationTokenPair): Option[Int] = {
      try {
        Some(
          Http.postData(url, resendJsonFileRequest.toJson(IdentificationTokenPair.itsJsonFormat).toString()).
            header("Content-Type", "application/json").
            option(HttpOptions.connTimeout(connectionTimeout_msec)).
            option(HttpOptions.readTimeout(readTimeout_msec)).responseCode
        )
      } catch {
        case e: HttpException => {
          log log "the http code we received was " + e.code

          Some(e.code)
        }
        case e: Exception => None
      }
    }

    override def onPostExecute(responseCodeOption: Option[Int]): Unit = {
      super.onPostExecute(responseCodeOption)

      //set render progress wheel to 100 percent
      renderProgressWheel(1, sendingProgressWheel)

      if (responseCodeOption.isDefined) {
        val responseCode = responseCodeOption.get

        log log "resending the response code is: " + responseCode

        responseCode match {
          case HttpStatus.SC_OK => {
            renderResult(success = true, messageId = R.string.resending_success)
          }
          case HttpStatus.SC_INTERNAL_SERVER_ERROR => {
            renderResult(success = false, messageId = R.string.resending_card_failed_please_retry)
          }
          case HttpStatus.SC_EXPECTATION_FAILED => {
            log log "user should not try this statement again, it seems like the current card is not found"
            renderResult(success = false, messageId = R.string.resending_card_failed_no_retry, Bman.URL)
          }
          case HttpStatus.SC_UNAUTHORIZED => {
            renderResult(success = false, messageId = R.string.unauthorized_server_response)
          }
          case HttpStatus.SC_BAD_REQUEST => {
            renderResult(success = false, messageId = R.string.bad_request_server_response, Bman.URL)
          }
          case _ => {
            renderResult(success = false, messageId = R.string.unexpected_server_response)
          }
        }
      } else {
        renderResult(success = false, messageId = R.string.server_connection_failed)
      }
    }
  }

  private class SendEntireJsonFileTask extends ScalaAsyncTask[SendJsonFileRequest, Float, Int] {
    private val connectionTimeout_msec = 5000

    private val readTimeout_msec = 3 * //minutes
      60 * 1000

    private val url = Bman.SERVER_LINK + "/card/uploadAndTrasfer"

    override def onPreExecute(): Unit = {
      super.onPreExecute()
      //reset progress wheel
      renderProgressWheel(0, sendingProgressWheel)
    }

    protected def doInBackground(sendJsonFileRequest: SendJsonFileRequest): Int = {
      val indifferentFilename = getRandomFilename(ext = Some("json"))

      val fileMultipart = new FileMultipart(
        name = "jsonFile",
        filename = indifferentFilename,
        mime = "application/json",
        file = sendJsonFileRequest.jsonFile
      ) {
        //we extend this class on the fly
        //something wrong with the whole scala-android thing and we cannot use scalaj 0.3.10
        /*def workWithScalajMultipart(op: (MultiPart) => Int, callback: (Int, Int) => Unit): Int = {
          val fileInputStream = new FileInputStream(file);
          try {
            val fileLen = file.length.toInt;
            op(MultiPart(
              name = name,
              filename = filename,
              mime = mime,
              data = fileInputStream,
              numBytes = fileLen,
              writeCallBack = {
                lenWritten: Int =>
                  callback(lenWritten, fileLen);
              }
            ));
          } finally {
            fileInputStream.close();
          }
        }*/
      }

      //params below cannot be created with scalaj
      /*fileMultipart.workWithScalajMultipart({
        multipart =>
          Http.multipart(url, multipart,MultiPart()).
            option(HttpOptions.connTimeout(connectionTimeout_msec)).
            option(HttpOptions.readTimeout(readTimeout_msec)).
            sendBufferSize(16 * 1024).
            responseCode;
      }, {
        (lenWritten, fileLen) =>
          publishProgress(lenWritten.toFloat / fileLen.toFloat);
      });*/

      postMultipart(new URL(url),
        params = Map(
          "identification" -> sendJsonFileRequest.identification,
          "token" -> sendJsonFileRequest.token
        ),
        fileMultipart = fileMultipart,
        connectionTimeoutMsecs = connectionTimeout_msec,
        readTimeoutMsecs = readTimeout_msec
      )(publishProgress(_))
    }

    protected override def onProgressUpdate(percentage: Float): Unit = {
      super.onProgressUpdate(percentage)

      renderProgressWheel(percentage, sendingProgressWheel)
    }

    override def onPostExecute(responseCode: Int): Unit = {
      super.onPostExecute(responseCode)

      log log "the response code is: " + responseCode

      responseCode match {
        case HttpStatus.SC_OK => {
          renderResult(success = true, messageId = R.string.server_sending_success)
        }
        case HttpStatus.SC_BAD_REQUEST => {
          renderResult(success = false, messageId = R.string.bad_request_server_response, Bman.URL)
        }
        case HttpStatus.SC_UNAUTHORIZED => {
          renderResult(success = false, messageId = R.string.unauthorized_server_response)
        }
        case HttpStatus.SC_EXPECTATION_FAILED => {
          //json card file cannot be parsed properly
          renderResult(success = false, messageId = R.string.unexpected_server_response)
        }
        case HttpStatus.SC_INTERNAL_SERVER_ERROR => {
          renderResult(success = false, messageId = R.string.resending_card_failed_please_retry)
        }
        case _ => {
          renderResult(success = false, messageId = R.string.unexpected_server_response)
        }
      }
    }
  }

}