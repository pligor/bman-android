package activities.namelist

import android.content.Context
import generic.PhoneNumberHelper._
import components.MySecurePreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object SendToSmsPreference extends MySecurePreference[String, Option[String]] {
  //remember to have the object as a case object in order for this work correctly
  //val preferenceKey = AutoReplyPreference.toString;
  //better be explicit
  val preferenceKey = "SendToSmsPreference"

  val defaultValue: String = null

  def getValue(implicit context: Context): Option[String] = {
    Option(getInnerValue.asInstanceOf[String])
  }

  def setValue(newValue: Option[String])(implicit context: Context) = {
    val validatedValue = newValue.map {
      value =>
        if (isValidPhoneNumber(value)) {
          value
        } else {
          throw new InvalidPhoneException
        }
    }

    setInnerValue(validatedValue.getOrElse(defaultValue))
  }

  private class InvalidPhoneException extends Exception
}
