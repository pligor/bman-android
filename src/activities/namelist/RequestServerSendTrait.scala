package activities.namelist

import com.pligor.bman.R
import android.content.{Intent, Context}
import components.EmailHelper._
import myandroid.AndroidHelper._
import activities.RequestServerSendActivity
import generic.PhoneNumberHelper._
import myandroid.DialogHelper
import android.text.InputType

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait RequestServerSendTrait {
  def getRequestServerSendMailDialog(token: String, cardId: Long)(implicit context: Context) = {
    DialogHelper.getOneTextDialog(
      titleId = R.string.request_server_send_mail_dialog_title,
      inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS,
      hintId = R.string.request_server_send_mail_dialog_hint,
      buttonTextId = R.string.request_server_send_dialog_button,
      backId = R.drawable.dialog_background,
      prefillEditTextOption = SendToEmailPreference.getValue
    )({
      (dialog, identification) =>
        if (isValidEmailAddress(identification)) {
          SendToEmailPreference.setValue(Some(identification))

          dialog.dismiss()

          proceedToSendingActivity(token, identification, cardId)
        } else {
          showToast(R.string.invalid_mail)
        }
    })
  }

  def getRequestServerSendSmsDialog(token: String, cardId: Long)(implicit context: Context) = {
    DialogHelper.getOneTextDialog(
      titleId = R.string.request_server_send_sms_dialog_title,
      inputType = InputType.TYPE_CLASS_PHONE,
      hintId = R.string.request_server_send_sms_dialog_hint,
      buttonTextId = R.string.request_server_send_dialog_button,
      backId = R.drawable.dialog_background,
      prefillEditTextOption = SendToSmsPreference.getValue
    )({
      (dialog, identification) =>
        if (isValidPhoneNumber(identification)) {
          SendToSmsPreference.setValue(Some(identification))

          dialog.dismiss()

          proceedToSendingActivity(token, identification, cardId)
        } else {
          showToast(R.string.invalid_cellphone)
        }
    })
  }

  private def proceedToSendingActivity(token: String, identification: String, cardId: Long)
                                      (implicit context: Context): Unit = {
    context.startActivity(new Intent(context, classOf[RequestServerSendActivity]).
      putExtra(RequestServerSendActivity.EXTRA.TOKEN.toString, token).
      putExtra(RequestServerSendActivity.EXTRA.IDENTIFICATION.toString, identification).
      putExtra(RequestServerSendActivity.EXTRA.CARD_ID.toString, cardId)
    )
  }
}
