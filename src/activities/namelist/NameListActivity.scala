package activities.namelist

import _root_.myalljoyn.AlljoynTargetWrapper
import android.app.Activity
import com.pligor.bman.{TR, R, TypedViewHolder}
import components._
import android.os.Bundle
import android.view.View
import android.view.View.OnClickListener
import android.widget._
import android.content.Intent
import models.{Card, BluetoothNameListItem}
import myandroid.AndroidHelper._
import myandroid.log
import android.widget.CompoundButton.OnCheckedChangeListener
import myandroid.WifiHelper._
import myauth.AuthActionable
import myandroid.ViewHelper._
import activities.{DownloadOldestCardActivity, AlljoynSendingActivity, TransferringExtras, TransferringActivity}
import myAndroidAPIs.BugsenseActivity
import preferences.HelperPreference
import services.SenderImmutableService

object NameListActivity {

  object EXTRA extends Enumeration {
    val CARD_ID = Value
  }

}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * The alljoyn names will NOT expire after a certain amount of time (this timestamp will get refreshed of course)
 */
class NameListActivity extends Activity with TypedViewHolder
with MyActivity

with AdmobActivity

//with NameListAssertionTrait
with MessageHelperDialogTrait

with RequestServerSendTrait
with AuthActionable
with BluetoothNameOverwriter
with BluetoothList
with AdvertiseNameList
with WifiActivity
with BugsenseActivity {

  protected lazy val advertisedNameListView = findView(TR.advertisedNameListView)

  protected lazy val bluetoothNameListView = findView(TR.bluetoothNameListView)

  private lazy val wifiBehindListImageView = findView(TR.wifiBehindListImageView)

  private lazy val nameListBluetoothToggleButton = findView(TR.nameListBluetoothToggleButton)

  protected lazy val nameListWifiToggleButton: ToggleButton = findView(TR.nameListWifiToggleButton)

  private lazy val repeatScanButton = findView(TR.repeatScanButton)

  private lazy val bluetoothBehindListImageView = findView(TR.bluetoothBehindListImageView)

  private lazy val emailSendButton = findView(TR.emailSendButton)

  private lazy val smsSendButton = findView(TR.smsSendButton)

  private lazy val cardId = getIntent.getLongExtra(NameListActivity.EXTRA.CARD_ID.toString, Card.invalidId)

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_name_list)

    assert(cardId != Card.invalidId)

    repeatScanButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        if (isBluetoothAvail) {
          startBluetoothDiscovery;
        }

        //we have chosen to NOT restart alljoyn
        //one way would be to stop service and start immediately. NOT safe!
        /*if (isWifiAvailable) {
          repeatScanButton.setEnabled(false);
          shutdownAdvertiseNameList(startupAdvertiseNameListSafe());
        }*/
        //second way would be cancel finding the name and start again, but
        // that would have to be a non-immutable service which I do not support right now
      }
    });

    {
      if (isBluetoothSupported) {
        nameListBluetoothToggleButton.setChecked(isBluetoothAvail);
        nameListBluetoothToggleButton.setOnCheckedChangeListener(bluetoothListener);
      } else {
        permanentlyDisableView(
          nameListBluetoothToggleButton,
          backgroundDrawableId = R.drawable.bluetooth_unavailable,
          messageId = R.string.bluetooth_unavailable
        );
      }
      val initBluetoothVisibility = if (isBluetoothAvail) View.VISIBLE else View.GONE;
      Seq(
        bluetoothNameListView,
        bluetoothBehindListImageView
      ) foreach (_.setVisibility(initBluetoothVisibility));
    }

    {
      if (isWifiSupported) {
        nameListWifiToggleButton.setChecked(wifiManager.get.isWifiEnabled);
        nameListWifiToggleButton.setOnCheckedChangeListener(wifiListener);
      } else {
        permanentlyDisableView(
          nameListWifiToggleButton,
          backgroundDrawableId = R.drawable.wifi_unavailable,
          messageId = R.string.wifi_unavailable
        );
      }
      val initWifiVisibility = if (isWifiAvailable) View.VISIBLE else View.GONE;
      Seq(
        advertisedNameListView,
        wifiBehindListImageView
      ) foreach (_.setVisibility(initWifiVisibility));
    }

    emailSendButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        executeAuthAction(token => getRequestServerSendMailDialog(token, cardId).show())
      }
    })

    smsSendButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        executeAuthAction(token => getRequestServerSendSmsDialog(token, cardId).show())
      }
    })
  }

  override def onBackPressed(): Unit = {
    //shutdownAdvertiseNameList(finish());
    //if all happened too quickly the callback is not meant to be executed and the activity cannot finish, and we are stuck
    finish()
  }

  private val wifiListener = new OnCheckedChangeListener {
    def onCheckedChanged(compoundButton: CompoundButton, checked: Boolean) {
      compoundButton.setEnabled(false);
      wifiManager.get.setWifiEnabled(checked);
    }
  }

  private val bluetoothListener = new OnCheckedChangeListener {
    def onCheckedChanged(compoundButton: CompoundButton, checked: Boolean) {
      compoundButton.setEnabled(false); //disable temporarily
      if (checked) {
        requestBluetoothEnable();
      } else {
        val isDisabled = bluetoothDisable();

        if (isDisabled) {
          //enabling button will be taken care from the callbacks
        } else {
          refreshBluetoothStateSafe()

          compoundButton.setEnabled(true)
        }
      }
    }
  }

  protected def onWifiEnabled(): Unit = {
    log log "sender immutable service state (activity): " + SenderImmutableService.getIsStarted;
    refreshWifiStateSafe(forcedState = Some(true));
    nameListWifiToggleButton.setEnabled(true);
    startupAdvertiseNameListSafe();
    wifiBehindListImageView.setVisibility(View.VISIBLE);
  }

  protected def onWifiDisabled(): Unit = {
    log log "sender immutable service state (activity): " + SenderImmutableService.getIsStarted;
    refreshWifiStateSafe(forcedState = Some(false));
    nameListWifiToggleButton.setEnabled(true);
    shutdownAdvertiseNameList();
    wifiBehindListImageView.setVisibility(View.GONE);

    showToast(R.string.sending_alljoyn_off);
  }

  private def refreshWifiStateSafe(forcedState: Option[Boolean] = None): Unit = {
    if (isWifiSupported) {
      nameListWifiToggleButton.setOnCheckedChangeListener(null);
      nameListWifiToggleButton.setChecked(forcedState.getOrElse(isWifiAvailable));
      nameListWifiToggleButton.setOnCheckedChangeListener(wifiListener);
    }
  }

  //no need to do anything else

  private def refreshBluetoothStateSafe(forcedState: Option[Boolean] = None) {
    if (isBluetoothSupported) {
      log log "bluetooth state is refreshed";
      nameListBluetoothToggleButton.setOnCheckedChangeListener(null);
      nameListBluetoothToggleButton.setChecked(forcedState.getOrElse(isBluetoothAvail));
      nameListBluetoothToggleButton.setOnCheckedChangeListener(bluetoothListener);
    }
  }

  override protected def onBluetoothTurnOff() {
    super.onBluetoothTurnOff();

    nameListBluetoothToggleButton.setEnabled(true);
    refreshBluetoothStateSafe(forcedState = Some(false));
    shutdownBluetoothList();
    bluetoothBehindListImageView.setVisibility(View.GONE);

    showToast(R.string.sending_bluetooth_off);
  }

  override protected def onBluetoothTurnOn(success: Boolean) {
    super.onBluetoothTurnOn(success);

    nameListBluetoothToggleButton.setEnabled(true);
    if (success) {
      nameListBluetoothToggleButton.setChecked(true);
      refreshBluetoothStateSafe(forcedState = Some(true));
      startupBluetoothList();
      bluetoothBehindListImageView.setVisibility(View.VISIBLE);

      if (HelperPreference.getValue) {
        getMessageHelperDialog(R.string.sending_bluetooth_on_extended).show();
      } else {
        showToast(R.string.sending_bluetooth_on);
      }

    } else {
      refreshBluetoothStateSafe(forcedState = Some(false));
    }
  }

  protected def onBluetoothDiscoveryStarted() {
    repeatScanButton.setEnabled(false);
  }

  protected def onBluetoothDiscoveryFinished() {
    //if (!isFinding) {
    repeatScanButton.setEnabled(true);
    //}
  }

  /**
   * @deprecated
   */
  protected def onAfterFakeExpiringStartedFinding() {
    /*if (!isDiscovering) {
      repeatScanButton.setEnabled(true);
    }*/
  }

  protected def onBeforeStartedFindingBothAlljoynServices() {
    //repeatScanButton.setEnabled(false);
  }

  protected def onAfterStartedFindingBothAlljoynServices() {
    if (HelperPreference.getValue) {
      getMessageHelperDialog(R.string.sending_alljoyn_on_extended).show();
    } else {
      showToast(R.string.sending_alljoyn_on);
    }
  }

  //going to other activities

  protected def onBluetoothListItemClick(nameListItem: BluetoothNameListItem): Unit = {
    val device = nameListItem.device

    log log "We are going to send card with id: " + cardId + " to device " + device

    startActivity(
      new Intent(activity, classOf[TransferringActivity]).
        putExtra(TransferringExtras.DEVICE.toString, device).
        putExtra(TransferringExtras.CARD_ID.toString, cardId).
        putExtra(TransferringExtras.IS_SENDING.toString, true)
    )
  }

  protected def onAdvertisedListItemClick(targetWrapper: AlljoynTargetWrapper): Unit = {
    //unsetMessengerAdvertiseNameList()

    startActivity({
      new Intent(this, classOf[AlljoynSendingActivity]).putExtra(
        AlljoynSendingActivity.EXTRAS.TARGET_WRAPPER.toString, targetWrapper
      ).putExtra(
        AlljoynSendingActivity.EXTRAS.CARD_ID.toString, cardId
      )
    })
  }
}
