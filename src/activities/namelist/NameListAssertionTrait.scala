package activities.namelist

import java.util.{TimerTask, Timer}
import components.MyActivity
import bluetooth.BluetoothHelper
import BluetoothHelper._
import myandroid.{log, WifiHelper}
import myandroid.WifiHelper._
import scala.Some
import android.app.Activity
import android.widget.ToggleButton
import services.SenderImmutableService

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait NameListAssertionTrait extends MyActivity {
  private var timer: Option[Timer] = None;

  private val periodMsecs = 1000;
  private val initialDelay = 3000;

  implicit private val bluetoothAdapter = getDefaultBluetoothAdapter;
  implicit private lazy val wifiManager = getWifiManager;

  protected def nameListWifiToggleButton: ToggleButton;

  override def onStart() {
    super.onStart();
    timer = Some(new Timer);
    timer.map {
      t =>
        t.scheduleAtFixedRate(new TimerTask {
          def run() {
            val isStarted = SenderImmutableService.getIsStarted;
            val isAvailable = WifiHelper.isWifiAvailable;
            val isChecked = nameListWifiToggleButton.isChecked;

            if ( (isStarted && isAvailable && isChecked) || (!isStarted && !isAvailable && !isChecked) ) {
              val status = if (isStarted) "on" else "off";
              log log ("sender service is " + status + ", wifi is " + status + " and wifi toggle button is " + status);
            }
            else {
              log log ("THE STATUS IS WRONG" + " isStarted " + isStarted + ", isAvailable " + isAvailable
                + " isChecked " + isChecked);
            }
          }
        }, {
          val delayMsecs = initialDelay;
          delayMsecs
        },
        periodMsecs)
    }
  }

  override def onStop() {
    super.onStop();
    timer.map(_.cancel);
    timer = None;
  }
}
