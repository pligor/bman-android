package activities.contactlist

import android.database.Cursor
import com.pligor.bman.R
import models.DatabaseHandler
import android.content.Context
import net.sqlcipher.database.SQLiteDatabase
//import android.database.sqlite.SQLiteDatabase

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object ContactList {
  private val prefixQuery =
    """SELECT card.id AS _id, frontPhotoId, senderName
      |FROM peoplecard, card, photo
      |WHERE cardId = card.id
      |AND photo.id = frontPhotoId
      | """.stripMargin

  private val postfixQuery =
    """
      |ORDER BY orderNumber DESC""".stripMargin

  def getAllContacts(implicit db: SQLiteDatabase): Cursor = {
    db.rawQuery(prefixQuery + postfixQuery, null)
  }

  def getContacts(filterString: String)(implicit db: SQLiteDatabase): Cursor = {
    db.rawQuery(
      prefixQuery +
        """
          |AND senderName LIKE ? COLLATE NOCASE
        """.stripMargin +
        postfixQuery,
      Array[String]("%" + filterString + "%")
    )
  }

  private val mapColumnsToFields = Map[String, Int](
    "frontPhotoId" -> R.id.rowThumbnail,
    "senderName" -> R.id.rowName
  )

  val columnArray = mapColumnsToFields.keys.toArray

  val fieldArray = mapColumnsToFields.values.toArray

  def getContactsIds(filterString: String)(implicit context: Context) = {
    new DatabaseHandler().getScalars[Long](
      """SELECT cardId
        |FROM peoplecard
        |WHERE senderName LIKE ? COLLATE NOCASE
        |ORDER BY orderNumber DESC""".stripMargin,
      Array[String]("%" + filterString + "%")
    )
  }

}
