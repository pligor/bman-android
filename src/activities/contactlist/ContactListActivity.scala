package activities.contactlist

import android.app.Activity
import com.pligor.bman.{R, TR, TypedViewHolder}
import components.{AdmobActivity, ImageSizeDp, MyActivity}
import android.os.Bundle
import android.database.{CursorWrapper, Cursor}
import com.terlici.dragndroplist.{DragNDropCursorAdapter, DragNDropListView}
import myandroid.AndroidHelper._
import models.{Card, PeopleCard, Photo, DatabaseHandler}
import models.DatabaseHandler.{safeCloseCursor, safeCloseDb}
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder
import android.view.View
import android.widget.{Toast, AdapterView}
import views.WrappedImageView
import android.widget.AdapterView.OnItemClickListener
import com.terlici.dragndroplist.DragNDropListView.OnItemDragNDropListener
import myandroid.{DensityPixel, log}
import android.text.{Editable, TextWatcher}
import android.content.Intent
import android.view.View.OnClickListener
import activities.crud.{Crud, CardDetailActivity}
import myAndroidAPIs.BugsenseActivity
import activities.peoplecards.ContactCardsActivity
import net.sqlcipher.database.SQLiteDatabase

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class ContactListActivity extends Activity with TypedViewHolder
with MyActivity

with AdmobActivity

with OnItemClickListener
with OnItemDragNDropListener
with BugsenseActivity {

  private var db: Option[SQLiteDatabase] = None

  private var cursor: Option[Cursor] = None

  private var adapter: Option[DragNDropCursorAdapter] = None

  //private var cursorFunc: Option[(Option[String]) => Cursor] = None;

  //we keep that for performance reasons, since we do not need dynamic here
  private val thumbnailSize = ImageSizeDp(DensityPixel(60), DensityPixel(60))

  private lazy val contactDragNDropListView: DragNDropListView = findView(TR.contactDragNDropListView)

  private lazy val filterEditText = findView(TR.filterEditText)

  private lazy val contactNormalImageButton = findView(TR.contactNormalImageButton)

  private def isFilterEditTextEmpty = filterEditText.getText.toString.trim.length() == 0

  private def getFilterEditText = if (isFilterEditTextEmpty) {
    None
  } else {
    Some(filterEditText.getText.toString.trim)
  }

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_contact_list)

    Toast.makeText(this, R.string.drag_reorder_toast, Toast.LENGTH_SHORT).show()

    contactNormalImageButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        val intent = new Intent(activity, classOf[ContactCardsActivity])

        val curFilterOption = getFilterEditText

        if (curFilterOption.isDefined) {
          val ids = ContactList.getContactsIds(curFilterOption.get)

          log log "ids: " + ids.mkString(", ")

          intent.putExtra(ContactCardsActivity.EXTRA.PEOPLE_CARD_IDS.toString, ids.toArray)
        }

        startActivity(intent)

      }
    })

    contactDragNDropListView.setOnItemClickListener(this)

    contactDragNDropListView.setOnItemDragNDropListener(this)

    filterEditText.addTextChangedListener(new TextWatcher {
      private var wasEmpty = true

      def beforeTextChanged(stringBeforeChange: CharSequence, p2: Int, p3: Int, p4: Int): Unit = {
        wasEmpty = stringBeforeChange.toString.isEmpty
      }

      def onTextChanged(p1: CharSequence, p2: Int, p3: Int, p4: Int): Unit = {
        log log "on text changed";
      }

      def afterTextChanged(editable: Editable): Unit = {
        val finalString = editable.toString

        val isEmpty = finalString.isEmpty

        if (wasEmpty) {
          //set cursor to be filtered
          resetAdapter(setCursor(Some(finalString)), isFiltered = true)
        } else if (!wasEmpty && isEmpty) {
          //set cursor to not be filtered
          resetAdapter(setCursor(filterString = None), isFiltered = false)
        } else {
          //kane apla reset ton cursor na filtrarontai auta pou prepei
          resetList(finalString)
        }

        log log "after text changed: " + editable.toString
      }
    })
  }

  override def onStart(): Unit = {
    super.onStart()

    resetAdapter(setCursor(getFilterEditText), !isFilterEditTextEmpty)
  }


  override def onRestart(): Unit = {
    super.onRestart()

    log log "restarting contact list activity"
    //this is not causing the list to redraw. it is only because the list size is affected by
    //keyboard appearing and disappearing and also because
  }

  private def resetAdapter(curCursor: Cursor, isFiltered: Boolean): Unit = {
    if (new CursorWrapper(curCursor).getCount == 0 && !isFiltered) {
      noItems()
    } else {
      adapter = Some(new DragNDropCursorAdapter(
        context,
        R.layout.list_item_card,
        curCursor,
        ContactList.columnArray, //the columns here, match in order..
        ContactList.fieldArray, //..the fields here
        if (isFiltered) R.id.rowDummy else R.id.rowThumbnail //drag and drop this item
      ))

      adapter.get.setViewBinder(viewBinder)

      contactDragNDropListView.setDragNDropAdapter(adapter.get)
    }
  }

  override def onStop(): Unit = {
    super.onStop()

    safeCloseCursor(cursor)

    safeCloseDb(db)
  }

  private def setCursor(filterString: Option[String]): Cursor = {
    safeCloseCursor(cursor)

    safeCloseDb(db)

    db = Some((new DatabaseHandler).grabReadableDatabase)

    cursor = if (filterString.isDefined) {
      Some(ContactList.getContacts(filterString.get)(db.get))
    } else {
      Some(ContactList.getAllContacts(db.get))
    }

    cursor.get
  }

  private def noItems(): Unit = {
    showToast(R.string.no_cards)

    finish()
  }

  private def resetList(filterString: String): Unit = {
    //adapter().swapCursor(resetCursor()).close();
    adapter.get.changeCursor(setCursor(Some(filterString)))
  }

  def onItemDrag(parent: DragNDropListView, view: View, position: Int, id: Long): Unit = {
    //nop
  }

  def onItemDrop(parent: DragNDropListView,
                 view: View,
                 startPosition: Int,
                 endPosition: Int,
                 id: Long): Unit = {
    val modelStart = PeopleCard.getById(id).get

    val endId = adapter.get.getItemId(endPosition)

    val modelEnd = PeopleCard.getById(endId).get

    val startOrderNumber = modelStart.orderNumber

    val endOrderNumber = modelEnd.orderNumber

    if (startOrderNumber < endOrderNumber) {
      PeopleCard.lowerToHigher(modelStart, modelEnd)
    } else if (startOrderNumber > endOrderNumber) {
      PeopleCard.higherToLower(modelStart, modelEnd)
    } else {
      //nop it has the same order number therefore it is the same model!
    }
  }


  def onItemClick(adapterView: AdapterView[_], view: View, position: Int, id: Long): Unit = {
    startActivity(new Intent(this, classOf[CardDetailActivity]).putExtra(
      Crud.InputExtra.MODEL_ID.toString, id
    ).putExtra(
      Crud.InputExtra.CARD_TYPE.toString, Card.Type.PEOPLE
    ).putExtra(
      Crud.InputExtra.VIEW_ID.toString, view.getId //this is probably a little bit redundant but anyways
    ))
  }

  private lazy val viewBinder = new ViewBinder {
    def setViewValue(view: View, cursor: Cursor, columnIndex: Int): Boolean = {

      //the thing with the bi-color theme is that when you drag an item you drag the color as well!
      /*val parent = view.getParent.asInstanceOf[ViewGroup];
      //this is executed multiple times for the current row but what the heck it wont be a bottleneck
      parent.setBackgroundColor(getResources.getColor(if (cursor.getPosition % 2 == 0) {
        R.color.favListItemEven;
      } else {
        R.color.favListItemOdd;
      }));*/

      view.getId match {
        case R.id.rowThumbnail =>
          val wrappedImageView = view.asInstanceOf[WrappedImageView]

          wrappedImageView.safesetOnceWrapperSize(() => thumbnailSize)

          val frontPhotoId = cursor.getLong(columnIndex)

          val photoOption = Photo.getById(frontPhotoId)

          if (photoOption.isDefined) {
            wrappedImageView.setPhoto(photoOption.get)
          } else {
            //nop we just leave the dummy here
          }

          true

        case _ => false
      }
    }
  }
}
