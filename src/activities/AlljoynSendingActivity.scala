package activities

import android.app.{ProgressDialog, Activity}
import android.os._
import components._
import android.content.{DialogInterface, Intent}
import myalljoyn.{AlljoynTargetWrapper, AlljoynRemoteService}
import com.pligor.bman.{R, TypedViewHolder}
import myandroid.{log, ProgressDialogActivity}
import android.content.DialogInterface.OnCancelListener
import components.ProgressWheelHelper._
import myAndroidAPIs.BugsenseActivity
import services.SenderImmutableService
import models.{Card, MyBugsense}

object AlljoynSendingActivity {

  object EXTRAS extends Enumeration {
    val TARGET_WRAPPER = Value

    val SENDING_FAILURE_STRING = Value

    val PERCENTAGE_SENT = Value

    val CARD_ID = Value
  }

  object MESSAGE extends Enumeration {
    val FILE_SENT_SUCCESSFULLY = Value

    val FILE_SENDING_FAILED = Value

    val WAIT_USER_CONFIRMATION = Value

    val USER_ACCEPTED_FILE = Value

    val USER_REJECTED_FILE = Value

    //val LOST_RAW_SESSION = Value;
    //val LOST_MESSAGE_SESSION = Value;

    val BUFFER_SENT = Value

    val ALLJOYN_SENDER_DESTROYED = Value
  }

}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class AlljoynSendingActivity extends Activity with TypedViewHolder
with ProgressDialogActivity
with MyActivity

with AdmobActivity

with AlljoynRemoteService
with TransferringStatement
with SendingLayout
with BugsenseActivity {

  private var generateTempJsonFileTaskOption: Option[GenerateTempJsonFileTask] = None

  private lazy val cardId = getIntent.getLongExtra(AlljoynSendingActivity.EXTRAS.CARD_ID.toString, Card.invalidId)

  private val progressDialog = new WriteOnce[ProgressDialog]()

  private lazy val targetWrapper = getIntent.getParcelableExtra[AlljoynTargetWrapper](
    AlljoynSendingActivity.EXTRAS.TARGET_WRAPPER.toString
  )

  override def onCreate(bundle: Bundle): Unit = {
    super.onCreate(bundle)

    setContentView(R.layout.activity_sending)

    log log "welcome to alljoyn client sending activity"

    setDestination(LetterConverter.decode(targetWrapper.getName))
  }

  override def onStart(): Unit = {
    super.onStart()

    assert(SenderImmutableService.getIsStarted,
      "make sure to finish this activity, kill the service and go back to list activity to start it there")

    //bind to sender service
    {
      val bounded = bindService(
      new Intent(this, classOf[SenderImmutableService]),
      alljoynServiceConnection, {
        val BIND_NO_AUTO_CREATE = 0

        BIND_NO_AUTO_CREATE
      })

      if (bounded) {
        log log "client activity is bounded"
      } else {
        log log "client activity was stopped forcefully and now restarted but with no service"

        killService() //will NOT finish activity because messenger is not set

        finish()
      }
    }
  }

  override def onStop(): Unit = {
    super.onStop()
    //dont use this assertion, because name list might just as quickly start service back on
    //assert(!SenderImmutableService.getIsStarted);

    generateTempJsonFileTaskOption = None
  }

  override def onBackPressed(): Unit = {
    //super.onBackPressed();
    killAndFinish()
  }

  private def killAndFinish(): Unit = {
    killService()

    if (generateTempJsonFileTaskOption.isDefined) {
      generateTempJsonFileTaskOption.map {
        generateTempJsonFileTask =>
          if (generateTempJsonFileTask.getStatus == AsyncTask.Status.FINISHED) {
            if (generateTempJsonFileTask.isFileGeneratedSuccessfully)
              log log "activity will finish from kill service"
            else
              finish()
          } else {
            generateTempJsonFileTask.cancel({
              val mayInterruptIfRunning = true

              mayInterruptIfRunning
            })

            finish()
          }
      }
    } else {
      finish()
    }
  }

  /**
   * remember that by killing service, this activity is ended
   */
  private def killService(): Unit = {
    safeUnbindAlljoynService()

    stopService(new Intent(this, classOf[SenderImmutableService]))
  }

  private object handler extends Handler {
    override def handleMessage(msg: Message): Unit = {
      super.handleMessage(msg)

      AlljoynSendingActivity.MESSAGE(msg.what) match {
        case AlljoynSendingActivity.MESSAGE.ALLJOYN_SENDER_DESTROYED => {
          finish()
        }
        case AlljoynSendingActivity.MESSAGE.BUFFER_SENT => {
          val bundle = msg.getData

          val percentage = bundle.getFloat(AlljoynSendingActivity.EXTRAS.PERCENTAGE_SENT.toString)

          renderProgressWheel(percentage, sendingProgressWheel)
        }
        case AlljoynSendingActivity.MESSAGE.USER_REJECTED_FILE => {
          assert(progressDialog.isInitialized)

          progressDialog().dismiss()

          renderSendingResult(success = false, messageId = R.string.receiver_rejected_file)
        }
        case AlljoynSendingActivity.MESSAGE.USER_ACCEPTED_FILE => {
          assert(progressDialog.isInitialized)

          progressDialog().dismiss()
        }
        case AlljoynSendingActivity.MESSAGE.WAIT_USER_CONFIRMATION => {

          progressDialog setValue {
            val progDialog = registerProgressDialog {
              val indeterminate = true

              val cancelable = true

              ProgressDialog.show(
                AlljoynSendingActivity.this,
                getResources.getString(R.string.sender_asked_confirm_send_progress_dialog_title),
                getResources.getString(R.string.sender_asked_confirm_send_progress_dialog_message),
                indeterminate,
                cancelable
              )
            }

            progDialog.setOnCancelListener(new OnCancelListener {
              def onCancel(dialog: DialogInterface): Unit = {
                killAndFinish()
              }
            })

            progDialog
          }
        }
        case AlljoynSendingActivity.MESSAGE.FILE_SENT_SUCCESSFULLY => {

          MyBugsense.Event.sendBugsenseEvent(MyBugsense.Event.CARD_SEND_SUCCESS_VIA_ALLJOYN)

          renderSendingResult(success = true, messageId = R.string.alljoyn_send_success)
        }
        case AlljoynSendingActivity.MESSAGE.FILE_SENDING_FAILED => {
          val bundle = msg.getData

          val messageId = bundle.getInt(AlljoynSendingActivity.EXTRAS.SENDING_FAILURE_STRING.toString)

          MyBugsense.Event.sendBugsenseEvent(MyBugsense.Event.CARD_SEND_FAIL_VIA_ALLJOYN)

          renderSendingResult(success = false, messageId = messageId)
        }
      }
    }
  }

  override protected def onAlljoynServiceConnected(): Unit = {
    generateTempJsonFileTaskOption = Some(new GenerateTempJsonFileTask({
      tempJsonFileOption =>

        if (tempJsonFileOption.isDefined) {
          log log "temp json file is ready"

          log log "targetWrapper.isFullyDefined: " + targetWrapper.isFullyDefined

          alljoynServiceMessenger.get.send({
            val message = Message.obtain()

            message.what = SenderImmutableService.MESSAGES.SENDING_ACTIVITY_STARTED.id

            message.setData({
              val bundle = new Bundle

              bundle.putParcelable(SenderImmutableService.EXTRAS.THE_MESSENGER.toString,
                new Messenger(handler))

              bundle.putInt(SenderImmutableService.EXTRAS.SOURCE_ACTIVITY.toString,
                SenderImmutableService.ACTIVITIES.SENDING.id)

              bundle.putString(SenderImmutableService.EXTRAS.FULL_FILEPATH.toString,
                tempJsonFileOption.get.getCanonicalPath)

              bundle.putParcelable(SenderImmutableService.EXTRAS.TARGET_WRAPPER.toString,
                targetWrapper)

              bundle
            })

            message
          })
        } else {
          renderSendingResult(success = false, messageId = R.string.card_json_file_could_not_created)
        }
    }))

    generateTempJsonFileTaskOption.get.execute(cardId)
  }
}
