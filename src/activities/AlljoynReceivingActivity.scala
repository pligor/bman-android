package activities

import android.app.Activity
import android.os._
import android.widget.Toast

import myandroid.{ScalaAsyncTask, log, AndroidHelper}
import AndroidHelper._
import components._

import myalljoyn.AlljoynRemoteService
import android.content.Intent
import com.pligor.bman.{Bman, R, TypedViewHolder}
import models.{Slave, PeopleCard}
import java.io.File
import simple.Simple
import components.ProgressWheelHelper._
import com.todddavies.components.progressbar.ProgressWheel
import myAndroidAPIs.BugsenseActivity
import services.ReceiverImmutableService

object AlljoynReceivingActivity {

  object MESSAGE extends Enumeration {
    val LOST_RAW_SESSION = Value;
    val LOST_MESSAGE_SESSION = Value;
    val FILE_RECEIVED = Value;
    val RAW_SESSION_JOINED = Value;
    val FILE_LEN_INVALID = Value;

    val USER_MANUAL_CONFIRM = Value;
    val USER_AUTO_CONFIRM = Value;

    val ALLJOYN_RECEIVER_DESTROYED = Value;

    val BUFFER_RECEIVED = Value;

    val FAILED_INITIALIZING_INPUT_STREAM = Value;
  }

  object EXTRAS extends Enumeration {
    val TEMP_FILE_CANONICAL_PATH = Value;
    val SENDER_NAME = Value;
    val PERCENTAGE_RECEIVED = Value;
  }

}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class AlljoynReceivingActivity extends Activity with TypedViewHolder
with MyActivity

with AdmobActivity

with AlljoynRemoteService
with TransferringStatement
with ReceivingLayout
with BugsenseActivity {

  override def onCreate(bundle: Bundle): Unit = {
    super.onCreate(bundle);
    setContentView(R.layout.activity_receiving);
    log log "welcome to alljoyn receiving activity";
  }

  override def onStart(): Unit = {
    super.onStart();

    assert(ReceiverImmutableService.getIsStarted,
      "make sure to finish this activity, kill the service and go back to waiting activity to start it there");

    //bind to receiver service
    {
      val intent = new Intent(this, classOf[ReceiverImmutableService]);
      val bounded = bindService(intent, alljoynServiceConnection, {
        val BIND_NO_AUTO_CREATE = 0;
        BIND_NO_AUTO_CREATE;
      });

      if (bounded) {
        log log "server activity is bounded";
      } else {
        log log "server activity was stopped forcefully and now restarted but with no service"
        killService()
      }
    }
  }

  override def onStop(): Unit = {
    super.onStop();
    log log "inside alljoyn receiving activity alljoyn service is stopping";
    //dont use this assertion, because name list might just as quickly start service back on
    //assert(!ReceiverImmutableService.getIsStarted);
  }

  private var isBackPressed: Boolean = false

  override def onBackPressed(): Unit = {
    //super.onBackPressed();

    isBackPressed = true

    if (ReceiverImmutableService.getIsStarted) {
      killService() //wait for service to finish activity
    } else {
      finish()
    }
  }

  /**
   * @return if service is running or not
   */
  private def killService(): Boolean = {
    safeUnbindAlljoynService()

    stopService(new Intent(this, classOf[ReceiverImmutableService]))
  }

  protected def onAlljoynServiceConnected(): Unit = {
    log log "inside alljoyn receiving activity alljoyn service is connected";
    alljoynServiceMessenger.get.send({
      val message = Message.obtain();
      message.what = ReceiverImmutableService.MESSAGES.RECEIVING_ACTIVITY_STARTED.id;
      message.setData({
        val bundle = new Bundle();
        bundle.putParcelable(ReceiverImmutableService.EXTRAS.THE_MESSENGER.toString, new Messenger(handler));
        bundle.putInt(ReceiverImmutableService.EXTRAS.SOURCE_ACTIVITY.toString,
          ReceiverImmutableService.ACTIVITIES.RECEIVING.id);
        bundle;
      });
      message;
    });
  }

  private object handler extends Handler {
    private def displaySenderName(message: Message): String = {
      val senderName = message.getData.getString(AlljoynReceivingActivity.EXTRAS.SENDER_NAME.toString);
      setOrigin(senderName);
      senderName;
    }

    override def handleMessage(msg: Message): Unit = {
      super.handleMessage(msg);
      AlljoynReceivingActivity.MESSAGE(msg.what) match {
        case AlljoynReceivingActivity.MESSAGE.BUFFER_RECEIVED => {
          val bundle = msg.getData;
          val percentage = bundle.getFloat(AlljoynReceivingActivity.EXTRAS.PERCENTAGE_RECEIVED.toString);
          renderProgressWheel(percentage, receivingProgressWheel);
        }
        case AlljoynReceivingActivity.MESSAGE.ALLJOYN_RECEIVER_DESTROYED => {
          if (isBackPressed) {
            finish()

            isBackPressed = false //pleonektika
          } else {
            //nop
          }
        }
        case AlljoynReceivingActivity.MESSAGE.USER_AUTO_CONFIRM => {
          displaySenderName(msg);
        }
        case AlljoynReceivingActivity.MESSAGE.USER_MANUAL_CONFIRM => {

          def answerToService(confirmDecision: Boolean): Unit = {
            alljoynServiceMessenger.get.send({
              val message = Message.obtain();
              message.what = ReceiverImmutableService.MESSAGES.USER_CONFIRM_DECISION.id;
              message.setData({
                val bundle = new Bundle;
                bundle.putBoolean(ReceiverImmutableService.EXTRAS.USER_BOOLEAN_CONFIRM_DECISION.toString, confirmDecision);
                bundle;
              });
              message;
            });
          }

          val dialog = Slave.genConfirmDialog(
            deviceName = displaySenderName(msg),
            acceptReceive = {
              answerToService(confirmDecision = true);
            },
            denyReceive = {
              answerToService(confirmDecision = false)

              log log "we do not display any alerts, because this is user's action. he knows what he is doing"

              onBackPressed()
            },
            timeoutSecs = Bman.DIALOG_CONFIRM_RECEIVE_TIMEOUT_SECS
          );

          dialog.show();
        }
        case AlljoynReceivingActivity.MESSAGE.FILE_LEN_INVALID =>
          renderReceivingResult(success = false, R.string.transferring_slave_sender_tried_invalid_length);

          killService()

        case AlljoynReceivingActivity.MESSAGE.LOST_MESSAGE_SESSION |
             AlljoynReceivingActivity.MESSAGE.LOST_RAW_SESSION =>
          renderReceivingResult(success = false, R.string.alljoyn_receiver_disconnected)

          killService()

        case AlljoynReceivingActivity.MESSAGE.FAILED_INITIALIZING_INPUT_STREAM =>
          renderReceivingResult(success = false, R.string.failed_initializing_input_stream);

          killService()

        case AlljoynReceivingActivity.MESSAGE.RAW_SESSION_JOINED => {
          log log "raw session is joined, starting receiving file";
        }
        case AlljoynReceivingActivity.MESSAGE.FILE_RECEIVED =>
          val bundle = msg.getData;
          val pathOption = Option(bundle.getString(
            AlljoynReceivingActivity.EXTRAS.TEMP_FILE_CANONICAL_PATH.toString
          ));
          if (pathOption.isDefined) {
            new ScalaAsyncTask[String, AnyRef, Unit] {
              protected def doInBackground(path: String): Unit = {
                PeopleCard.create(new File(path))
              }

              override def onPostExecute(result: Unit): Unit = {
                super.onPostExecute(result)
                renderReceivingResult(success = true, R.string.alljoyn_receive_success)

                killService()
              }
            } execute pathOption.get;

          } else {
            log log "saving file is skipped";
            renderReceivingResult(success = false, R.string.alljoyn_receive_failure)

            killService()
          }

      }
    }
  }

}
