package activities

import android.app.Activity
import com.pligor.bman.{TR, R, TypedViewHolder}
import android.os._
import components._
import android.widget._
import android.widget.CompoundButton.OnCheckedChangeListener
import preferences.{HelperPreference, ConfirmReceivePreference, SoundPreference}
import myAndroidAPIs.BugsenseActivity
import myandroid.MyContextWrapper

/**
 * http://stackoverflow.com/questions/12000286/wifi-direct-for-android-version-2-3
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * WifiP2pManager is available only after API 14
 */
class SettingsActivity extends Activity with TypedViewHolder
with MyContextWrapper
with MyActivity

with AdmobActivity

with BugsenseActivity {
  private lazy val soundCheckBox = findView(TR.soundCheckBox)

  private lazy val confirmReceiveCheckBox = findView(TR.confirmReceiveCheckBox)

  private lazy val helperCheckBox = findView(TR.helperCheckBox)

  //http://ydandroid.wordpress.com/2011/05/08/initializing-listview-from-database-using-simplecursoradapter/
  //private var cursor: Option[Cursor] = None;
  /*private lazy val simpleCursorAdapter = new SimpleCursorAdapter(
    this, //context
    R.layout.label_item, //list item id
    cursor.get, //cursor
    Array[String](MetaLabelCols.label.toString), //the columns here, match in order..
    Array[Int](R.id.labelTextView) //..the fields here
  );*/

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_settings)
    //resetCursor();
  }

  override def onStart(): Unit = {
    super.onStart()

    //these two lines before setting the listeners
    soundCheckBox.setChecked(SoundPreference.getValue)

    confirmReceiveCheckBox.setChecked(ConfirmReceivePreference.getValue)

    helperCheckBox.setChecked(HelperPreference.getValue)

    soundCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener {
      def onCheckedChanged(compoundButton: CompoundButton, checked: Boolean): Unit = {
        SoundPreference.setValue(checked)
      }
    })

    confirmReceiveCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener {
      def onCheckedChanged(compoundButton: CompoundButton, checked: Boolean): Unit = {
        ConfirmReceivePreference.setValue(checked)
      }
    })

    helperCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener {
      def onCheckedChanged(compoundButton: CompoundButton, checked: Boolean): Unit = {
        HelperPreference.setValue(checked)
      }
    })
  }

  override def onStop(): Unit = {
    super.onStop()
    //assert(cursor.isDefined && !cursor.get.isClosed);
    //cursor.get.close();
  }

  /*private def resetCursor() {
    cursor = Some(MyMetaLabel.getCursorAll);
  }*/

  /*private def reRenderList() {
    //deprecated way:
    //cursor.requery();
    assert(cursor.isDefined);
    cursor.get.close();
    resetCursor();
    simpleCursorAdapter.changeCursor(cursor.get);
    simpleCursorAdapter.notifyDataSetChanged();
  }*/
}
