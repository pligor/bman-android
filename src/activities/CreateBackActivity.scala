package activities

import android.app.{ProgressDialog, Activity}
import com.pligor.bman.{TR, R, TypedViewHolder}
import components._
import models.{MyCard, Card, RequestCode}
import android.net.Uri
import android.os.Bundle
import android.view.View
import myandroid.AndroidHelper._
import android.content.{DialogInterface, Intent}
import myAndroidAPIs.BugsenseActivity
import android.content.DialogInterface.OnCancelListener
import android.view.View.OnClickListener
import myandroid.{ScalaAsyncTask, ProgressDialogActivity}

object CreateBackActivity {

  object EXTRA extends Enumeration {
    val CARD_ID = Value;
  }

  object OUTPUT_EXTRA extends Enumeration {
    val CARD_ID = Value;
  }

}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class CreateBackActivity extends Activity with TypedViewHolder
with MyActivity

with AdmobActivity

with PhotoImporting
with PhotoCapturing
with ProgressDialogActivity
with BugsenseActivity {
  private var setBackMyCardTaskOption: Option[SetBackMyCardTask] = None;

  private lazy val shootPictureButton = findView(TR.shootPictureButton);
  private lazy val importPictureButton = findView(TR.importPictureButton);
  private lazy val skipBackSideButton = findView(TR.skipBackSideButton);

  protected val photoImportRequestCode: Int = RequestCode.SELECT_IMAGE.id;
  protected val photoCaptureRequestCode: Int = RequestCode.CAMERA_DATA.id;

  private lazy val createBackFrontPreviewWrapper = findView(TR.createBackFrontPreviewWrapper);

  private lazy val createBackFrontPreview = findView(TR.createBackFrontPreview);

  private lazy val myCardId = getIntent.getLongExtra(CreateBackActivity.EXTRA.CARD_ID.toString, Card.invalidId);

  private lazy val myCard = {
    val myCardOption = MyCard.getById(myCardId);
    //this assertion covers also the case where we have not the extra and we have invalid id
    assert(myCardOption.isDefined, "we should not come to this activity without a valid front side on hand");
    myCardOption.get;
  }

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_create_back);

    val wordBack = getResources.getString(R.string.word_back);

    importPictureButton.setText(getResources.getString(R.string.import_picture_button,
      wordBack
    ));

    shootPictureButton.setText(getResources.getString(R.string.shoot_picture_button,
      wordBack
    ));

    val frontPhotoOption = myCard.getFrontPhoto;
    if (frontPhotoOption.isDefined) {
      val frontPhoto = frontPhotoOption.get;
      createBackFrontPreview.safesetOnceWrapperSize(createBackFrontPreviewWrapper)(
        createBackFrontPreview.setPhoto(frontPhoto)
      );
    } else {
      showToast(R.string.error_loading_front_photo);
      onCancel();
    }

    shootPictureButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        shootFromCamera(
          showToast(R.string.image_shot_impossible)
        );
      }
    });

    importPictureButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        importFromGallery()
      }
    });

    skipBackSideButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        proceedToCrudActivity(Some(myCard))
      }
    });
  }

  override def onStop(): Unit = {
    super.onStop()

    setBackMyCardTaskOption.map(_.cancel {
      val mayInterruptIfRunning = true
      mayInterruptIfRunning
    });
  }

  override def onBackPressed(): Unit = {
    onCancel();
    //super.onBackPressed();
  }

  private def onCancel(): Unit = {
    myCard.delete; //if we go back we must delete the already created one
    setResult(Activity.RESULT_CANCELED);
    finish();
  }

  private def proceedToCrudActivity(cardOption: Option[Card]): Unit = {
    if (cardOption.isDefined) {
      val card = cardOption.get;
      assert(card.isOld, "we just set the back side of a card. this card must be old in order to proceed");

      val data = (new Intent).putExtra(CreateBackActivity.OUTPUT_EXTRA.CARD_ID.toString, card.id.get);

      setResult(Activity.RESULT_OK, data)

      finish()
    } else {
      showToast(R.string.retrieving_new_image_error);
    }
  }

  protected def onImportFromGallerySuccess(photoUriOption: Option[Uri]): Unit = {
    if (photoUriOption.isDefined) {
      executeSetBackMyCard(photoUriOption.get)(proceedToCrudActivity);
    } else {
      showToast(R.string.retrieving_new_image_error);
    }
  }

  protected def onShootFromCameraSuccess(photoUri: Uri, onAfterPhotoUriUsage: (Uri) => Unit): Unit = {
    executeSetBackMyCard(photoUri) {
      cardOption =>
        onAfterPhotoUriUsage(photoUri);
        proceedToCrudActivity(cardOption);
    }
  }

  protected def onShootFromCameraFailure(): Unit = {
    showToast(R.string.retrieving_new_image_error);
  }

  protected def onImportFromGalleryFailure(): Unit = {
    showToast(R.string.retrieving_new_image_error);
  }

  private def executeSetBackMyCard(uri: Uri)(op: Option[Card] => Unit): Unit = {
    setBackMyCardTaskOption = Some(new SetBackMyCardTask(op));
    setBackMyCardTaskOption.get.execute(uri);
  }

  private class SetBackMyCardTask(op: Option[Card] => Unit) extends ScalaAsyncTask[Uri, AnyRef, Option[Card]] {

    private val progressDialog = new WriteOnce[ProgressDialog];

    override def onPreExecute(): Unit = {
      super.onPreExecute();

      progressDialog setValue {
        val progDialog = registerProgressDialog {
          val indeterminate = true;
          val cancelable = true;
          ProgressDialog.show(
            CreateBackActivity.this,
            getResources.getString(R.string.create_back_progress_dialog_title),
            getResources.getString(R.string.create_back_progress_dialog_message),
            indeterminate,
            cancelable
          );
        }

        progDialog.setOnCancelListener(new OnCancelListener {
          def onCancel(dialog: DialogInterface): Unit = {
            cancel({
              val mayInterruptIfRunning = true;
              mayInterruptIfRunning;
            });
          }
        });

        progDialog;
      }
    }

    protected def doInBackground(photoUri: Uri): Option[Card] = {
      myCard.setBackUri(photoUri);
    }

    override def onPostExecute(myCardOption: Option[Card]): Unit = {
      super.onPostExecute(myCardOption);
      progressDialog().dismiss();
      op(myCardOption);
    }
  }

}
