package activities

import android.app.Activity
import com.pligor.bman.{TR, R, TypedViewHolder}
import android.os.Bundle
import android.content.pm.ActivityInfo
import models.{Card, Photo}
import asyncbitmap.ImageLoader._
import components.{ImageSizePx, MyActivity}
import myandroid.{Pixel, FullScreenActivity}
import android.widget.RelativeLayout
import android.view.View
import myAndroidAPIs.BugsenseActivity
import android.content.Intent

object ZoomActivity {

  object EXTRA extends Enumeration {
    val PHOTO_ID = Value

    val CARD_ID = Value
  }

  object OUT_EXTRA extends Enumeration {
    val CARD_ID = Value
  }

}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class ZoomActivity extends Activity with TypedViewHolder
with MyActivity
with FullScreenActivity
with BugsenseActivity {

  private lazy val relativeLayout = getContentLayout.asInstanceOf[RelativeLayout];
  private lazy val zoomImageView = findView(TR.zoomImageView);
  private lazy val zoomTextView = findView(TR.zoomTextView);

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_zoom);
  }

  override def onStart() {
    super.onStart();
    zoomTextView.setVisibility(View.VISIBLE);
  }


  override def onBackPressed() {
    val cardId = getIntent.getLongExtra(ZoomActivity.EXTRA.CARD_ID.toString, Card.invalidId);

    if (cardId != Card.invalidId) {
      setResult(Activity.RESULT_OK,
        new Intent().putExtra(ZoomActivity.OUT_EXTRA.CARD_ID.toString, cardId)
      );
    } else {
      //the activity was not called expecting a result
    }

    //super.onBackPressed();
    finish();
  }

  /**
   * We want this function so we can have the sizes
   * we dont want anymore sophisticated methods in this activity like the ones we use in the widget
   */
  override def onWindowFocusChanged(hasFocus: Boolean): Unit = {
    super.onWindowFocusChanged(hasFocus)

    if (hasFocus) {
      val photoId = getIntent.getLongExtra(ZoomActivity.EXTRA.PHOTO_ID.toString, Photo.invalidId);

      val photoOption = Photo.getById(photoId);
      if (photoOption.isDefined) {
        val photo = photoOption.get;

        val largestSquareSide = Pixel(scala.math.max(relativeLayout.getWidth, relativeLayout.getHeight));

        val targetSize = ImageSizePx(
          width = largestSquareSide,
          height = largestSquareSide
        ).toDp;

        loadImage(
          bitmapSource = photo,
          imageView = zoomImageView,
          param = (targetSize, context),
          postOp = {
            (imgView, bitmap) =>
              val orientation = if (bitmap.getHeight < bitmap.getWidth) {
                //typical landscape card
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
              } else {
                //square or portrait card
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
              }

              setRequestedOrientation(orientation);

              imgView.setImageBitmap(bitmap);

              zoomTextView.setVisibility(View.GONE);
          }
        );
      } else {
        finish();
      }
    }
  }
}
