package activities.mycards

import com.pligor.bman.{R, TR, TypedViewHolder}
import components._
import android.os.Bundle
import myandroid.AndroidHelper._
import models.MyCard
import android.content.Intent
import activities._
import android.widget.{ToggleButton, CompoundButton}
import android.widget.CompoundButton.OnCheckedChangeListener
import myandroid.WifiHelper._
import android.bluetooth.BluetoothSocket
import android.view.View
import android.view.View.OnClickListener
import preferences.{HelperPreference, LastMyCardPreference, SavedToggleReceiverStatePreference}
import myandroid.log
import myandroid.ViewHelper._
import myAndroidAPIs.BugsenseActivity
import adapters.page.MyCardPageAdapter
import android.support.v4.app.FragmentActivity
import fragments.MyCardFragment
import android.support.v4.view.ViewPager
import android.support.v4.view.ViewPager.OnPageChangeListener

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class MyCardsActivity extends FragmentActivity with TypedViewHolder
with MyActivity

with AdmobActivity
with SendStatusTrait

//with MyCardsAssertionTrait
with SwipeHelperDialogTrait
with MessageHelperDialogTrait

with WifiActivity
with AlljoynWaitingActivity

with BluetoothListeningActivity
with BluetoothNameOverwriter

with BugsenseActivity {

  private lazy val myCardsWifiToggleButton: ToggleButton = findView(TR.myCardsWifiToggleButton)

  private lazy val myCardsBluetoothToggleButton: ToggleButton = findView(TR.myCardsBluetoothToggleButton)

  private lazy val myCardsReceiverToggleButton: ToggleButton = findView(TR.myCardsReceiverToggleButton)

  private lazy val myCardsHomeButton = findView(TR.myCardsHomeButton)

  private lazy val myCardsNoCardsWrapper = findView(TR.myCardsNoCardsWrapper)

  private lazy val myCardsCreateCardButton = findView(TR.myCardsCreateCardButton)

  private lazy val myCardsViewpager = findView(TR.myCardsViewpager)

  private lazy val myCardPageAdapter = new MyCardPageAdapter(getSupportFragmentManager)

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_my_cards)

    if (isWifiSupported) {
      myCardsWifiToggleButton.setOnCheckedChangeListener(wifiListener)
    } else {
      permanentlyDisableView(
        myCardsWifiToggleButton,
        backgroundDrawableId = R.drawable.wifi_unavailable,
        messageId = R.string.wifi_unavailable
      )
    }

    if (isBluetoothSupported) {
      myCardsBluetoothToggleButton.setOnCheckedChangeListener(bluetoothListener)
    } else {
      permanentlyDisableView(
        myCardsBluetoothToggleButton,
        backgroundDrawableId = R.drawable.bluetooth_unavailable,
        messageId = R.string.bluetooth_unavailable
      )
    }

    if (isWifiSupported || isBluetoothSupported) {
      myCardsReceiverToggleButton.setOnCheckedChangeListener(receiverListener)
    } else {
      myCardsReceiverToggleButton.setEnabled(false); //disable permenantly, actually this should not change because the other buttons should be disabled as well
    }

    myCardsHomeButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        startActivity(new Intent(context, classOf[HomeActivity]).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))

        finish()
      }
    })

    myCardsCreateCardButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        startActivity(new Intent(activity, classOf[CreateFrontActivity]))
      }
    })
  }

  override def onStart(): Unit = {
    super.onStart()

    val lastMyCardOption = MyCard.getById(LastMyCardPreference.getValue)
    //this works correctly either on invalid id or on not found card
    val myCardOption = if (lastMyCardOption.isDefined) {
      lastMyCardOption
    } else {
      MyCard.getTop
    }

    val myCardFound = myCardOption.isDefined

    //make only one of them visible
    {
      myCardsViewpager.setVisibility(if (myCardFound) View.VISIBLE else View.GONE)

      myCardsNoCardsWrapper.setVisibility(if (myCardFound) View.GONE else View.VISIBLE)
    }

    if (myCardFound) {
      /*def renderCurCard() = renderCard(myCardOption.get)

      if (!myCardWidgetLayout.init(renderCurCard())) {
        //render a first card after view is ready
        renderCurCard(); //render the card if the view is already initialized
      }*/

      myCardPageAdapter.setFragments(getFragments)

      myCardsViewpager.setAdapter(myCardPageAdapter)

      myCardPageAdapter.getPosition(myCardOption.get.id.get).map(myCardsViewpager.setCurrentItem)

      myCardsViewpager.setOnPageChangeListener(new OnPageChangeListener {
        def onPageScrollStateChanged(state: Int): Unit = {
          //nop
        }

        def onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int): Unit = {
          //nop
        }

        def onPageSelected(position: Int) = {
          LastMyCardPreference.setValue(
            myCardPageAdapter.getItem(position).asInstanceOf[MyCardFragment].getCardId
          )
        }
      })

    } else {
      //nop
    }

    {
      refreshBluetoothStateSafe()

      refreshWifiStateSafe()

      {
        safeEnableOrDisableReceiver()

        def refreshReceiverStateSafe(forcedState: Option[Boolean] = None): Unit = {
          myCardsReceiverToggleButton.setOnCheckedChangeListener(null)

          val newState = forcedState.getOrElse(isListening || isWaiting)

          myCardsReceiverToggleButton.setChecked(newState)

          myCardsReceiverToggleButton.setOnCheckedChangeListener(receiverListener)
        }

        val savedState = SavedToggleReceiverStatePreference.getValue

        if (myCardsReceiverToggleButton.isEnabled) {
          //as an exception explicitly set the state here because it is executed only when the activity starts
          myCardsReceiverToggleButton.setChecked(savedState)
        } else {
          refreshReceiverStateSafe(Some(savedState))
        }
      }
    }
  }

  /**
   * http://stackoverflow.com/questions/3591784/android-get-width-returns-0
   */
  /*override def onWindowFocusChanged(hasFocus: Boolean) {
    super.onWindowFocusChanged(hasFocus);
  }*/

  override def onStop(): Unit = {
    super.onStop()
  }


  override def onResume(): Unit = {
    super.onResume()
  }

  private def getFragments = {
    MyCard.getAllIds().map(cardId => MyCardFragment.newInstance(cardId, context))
  }

  private object bluetoothListener extends OnCheckedChangeListener {
    def onCheckedChanged(compoundButton: CompoundButton, checked: Boolean): Unit = {
      require(isBluetoothSupported)

      compoundButton.setEnabled(false) //disable temporarily

      myCardsReceiverToggleButton.setEnabled(false) //disable temporarily

      //we comment this line because something weird it might happen with bluetooth
      // it is better to be rarely unsynchronized with the state than crashing the entire application
      //assert(checked != isBluetoothDiscoverable, "the bluetooth state of discoverable must try to change");

      if (checked) {
        requestBluetoothDiscoverable()
      } else {
        val isDisabled = bluetoothDisable()

        log log "bluetooth is disabled is: " + isDisabled
        if (isDisabled) {
          //nop everything will be taken care from the callbacks
        } else {
          refreshBluetoothStateSafe()

          myCardsReceiverToggleButton.setEnabled(isWifiAvailable)

          compoundButton.setEnabled(true)
        }
      }
    }
  }

  private object wifiListener extends OnCheckedChangeListener {
    def onCheckedChanged(compoundButton: CompoundButton, checked: Boolean): Unit = {
      compoundButton.setEnabled(false) //disable temporarily

      myCardsReceiverToggleButton.setEnabled(false) //disable temporarily

      //we comment this line because something weird it might happen with bluetooth
      // it is better to be rarely unsynchronized with the state than crashing the entire application
      //assert(checked != wifiManager.get.isWifiEnabled, "maybe this assertion will fail, use if (?)");

      wifiManager.get.setWifiEnabled(checked)
    }
  }

  private object receiverListener extends OnCheckedChangeListener {
    def onCheckedChanged(compoundButton: CompoundButton, checked: Boolean): Unit = {
      //the extra check with the is listening and is waiting is to avoid the chance that everything
      // is executed too quickly (like synchronously) and then the toggle button will stuck on disabled

      if (checked) {
        assert(isBluetoothDiscoverable || isWifiAvailable)

        val willBeWaiting = startWaitingSafe()

        val willBeListening = startListeningSafe()

        //disable temporarily if necessary
        compoundButton.setEnabled(!(
          (willBeWaiting && !isWaiting) | //will be waiting but didnt get their yet OR
            (willBeListening && !isListening) //will be listening but didnt get their yet
          //then when we get there the receiver button will be enabled, for now set it to false
          )
        )
      } else {
        val willNotBeListening = stopListeningSafe()

        val willNotBeWaiting = stopWaitingSafe()

        //disable temporarily if necessary
        compoundButton.setEnabled(!(
          (willNotBeListening && isListening) || //will stop listening but didnt stopped yet OR
            (willNotBeWaiting && isWaiting) //will stop waiting but didnt stopped yet
          //then when we are going to stop the receiver button will be enabled, for now set it to false
          )
        )
      }

      SavedToggleReceiverStatePreference.setValue(checked)
    }
  }

  protected def onWifiEnabled(): Unit = {
    def setReceiverEnableBasedOnChangingListening(): Unit = {
      val isOnTheProcessOfChangingListening =
        isOnTheProcessOfStartListening || isOnTheProcessOfStopListening

      myCardsReceiverToggleButton.setEnabled(!isOnTheProcessOfChangingListening) //if it is on the process of playing with bluetooth, we must remain in false state
    }

    if (myCardsReceiverToggleButton.isChecked) {
      log log "on wifi enabled the receiver button was found checked";
      val willBeWaiting = startWaitingSafe();

      //we have no control on the broadcast receiver
      //there are cases where onWifiEnabled gets called more than once
      //this is why we will have no assertion in here
      //assert(willBeWaiting, "since wifi was turned off, service should have been stopped");

      //myCardsReceiverToggleButton.setEnabled(!willBeWaiting); //temporarily disable
      if (willBeWaiting) {
        myCardsReceiverToggleButton.setEnabled(false);
      } else {
        setReceiverEnableBasedOnChangingListening();
      }
    } else {
      log log "on wifi enabled the receiver button was found off";
      assert(!isWaiting, "since receiver was off, we must not have been waiting");

      /*if (myCardsReceiverToggleButton.isEnabled) {
        //nop
        //in case we are in the process of disabling or enabling bluetooth listening while on the same time playing with wifi button
      } else {
        assert(safeEnableOrDisableReceiver(), "we just enabled wifi, therefore this must not fail");
      }*/
      setReceiverEnableBasedOnChangingListening();
    }

    refreshWifiStateSafe(Some(true));
    myCardsWifiToggleButton.setEnabled(true);
  }

  protected def onWifiDisabled() {
    val wasWaiting = stopWaitingSafe();

    //the below can be optimized with: !wasWaiting && isBluetoothDiscoverable
    if (wasWaiting) {
      log log "we were waiting so receiver toggle button is disabled now and will be enabled later";
      myCardsReceiverToggleButton.setEnabled(false);
      //the state of the receiver button will be taken care of in the callbacks
    } else {
      val isOnTheProcessOfChangingListening = isOnTheProcessOfStartListening || isOnTheProcessOfStopListening;
      if (isOnTheProcessOfChangingListening) {
        myCardsReceiverToggleButton.setEnabled(false); //re-enabling will be taken care of in bluetooth callbacks
      } else {
        log log "if bluetooth and wifi off, receiver toggle button disabled until re-enabling either bluetooth or wifi"
        myCardsReceiverToggleButton.setEnabled(isBluetoothDiscoverable);
      }
    }

    refreshWifiStateSafe(Some(false));
    myCardsWifiToggleButton.setEnabled(true);
  }


  protected def onBluetoothTurnDiscoverabilityOn(success: Boolean) {
    def setReceiverEnableBasedOnChangingWaiting() {
      val isOnTheProcessOfChangingWaiting = isOnTheProcessOfStartWaiting || isOnTheProcessOfStopWaiting;
      myCardsReceiverToggleButton.setEnabled(!isOnTheProcessOfChangingWaiting); //if it is on the process of playing with wifi, we must remain in false state
    }

    if (success) {
      if (myCardsReceiverToggleButton.isChecked) {
        val willBeListening = startListeningSafe();
        if (willBeListening) {
          myCardsReceiverToggleButton.setEnabled(false); //temporarily disable
        } else {
          setReceiverEnableBasedOnChangingWaiting();
        }
      } else {
        assert(!isListening, "since receiver was off, we must not have been listening");
        //assert(safeEnableOrDisableReceiver(), "we just enabled bluetooth, therefore this must not fail");
        setReceiverEnableBasedOnChangingWaiting();
      }
      log log "bluetooth turned on";
    } else {
      log log "bluetooth failed to turn on";
      safeEnableOrDisableReceiver();
    }

    refreshBluetoothStateSafe(Some(success));
    myCardsBluetoothToggleButton.setEnabled(true);
  }

  protected def onBluetoothTurnDiscoverabilityOff() {
    val willNotBeListening = stopListeningSafe();

    val isOnTheProcessOfChangingWaiting = isOnTheProcessOfStartWaiting || isOnTheProcessOfStopWaiting;

    if (willNotBeListening) {
      //not false because maybe is already not listening  (faster than its shadow :P)
      if (isListening) {
        //on after stop the receiver will be enabled unless wifi is turned off as well
        myCardsReceiverToggleButton.setEnabled(false);
      } else {
        myCardsReceiverToggleButton.setEnabled(!isOnTheProcessOfChangingWaiting && isWifiAvailable);
      }
    } else {
      if (isOnTheProcessOfChangingWaiting) {
        myCardsReceiverToggleButton.setEnabled(false);
        //there are callbacks that will take care of the enabliability
      } else {
        myCardsReceiverToggleButton.setEnabled(isWifiAvailable);
      }
    }
    log log "bluetooth discoverability turned off";

    refreshBluetoothStateSafe(Some(false));
    myCardsBluetoothToggleButton.setEnabled(true);
  }

  //CALLBACKS ON AFTER

  protected def onAlljoynStartWaitingSuccess() {
    val isOnTheProcessOfChangingListening = isOnTheProcessOfStartListening || isOnTheProcessOfStopListening;

    if (isOnTheProcessOfChangingListening) {
      myCardsReceiverToggleButton.setEnabled(false);
    } else {
      safeEnableOrDisableReceiver();
    }

    if (HelperPreference.getValue) {
      getMessageHelperDialog(R.string.receiving_alljoyn_on_extended).show();
    } else {
      showToast(R.string.receiving_alljoyn_on);
    }
  }

  protected def onAlljoynStartWaitingFail(toastId: Int) {
    showToast(toastId);
    //maybe the service has not ended yet, but we are sure that is going to end so
    // we do everything on the callback onAfterDestroyedAlljoynWaiting
  }

  protected def onAfterDestroyedAlljoynWaiting() {
    showToast(R.string.receiving_alljoyn_off);
    val isOnTheProcessOfChangingListening = isOnTheProcessOfStartListening || isOnTheProcessOfStopListening;

    if (isOnTheProcessOfChangingListening) {
      myCardsReceiverToggleButton.setEnabled(false);
    } else {
      safeEnableOrDisableReceiver();
    }
  }


  protected def onStartListening(acceptThreadName: String) {
    val isOnTheProcessOfChangingWaiting = isOnTheProcessOfStartWaiting || isOnTheProcessOfStopWaiting;

    if (isOnTheProcessOfChangingWaiting) {
      myCardsReceiverToggleButton.setEnabled(false);
    } else {
      safeEnableOrDisableReceiver();
    }

    if (HelperPreference.getValue) {
      getMessageHelperDialog(R.string.receiving_bluetooth_on_extended).show();
    } else {
      showToast(R.string.receiving_bluetooth_on);
    }
  }

  protected def onBeforeStopListening() {
    //nop
  }

  protected def onAfterStopListening() {
    //assertion would not work if activity is closing down, because it does not matter
    //if after closing the receiver is enabled or disabled
    showToast(R.string.receiving_bluetooth_off);

    val isOnTheProcessOfChangingWaiting = isOnTheProcessOfStartWaiting || isOnTheProcessOfStopWaiting;

    if (isOnTheProcessOfChangingWaiting) {
      myCardsReceiverToggleButton.setEnabled(false);
    } else {
      safeEnableOrDisableReceiver();
    }
  }


  //OTHER METHODS

  private def safeEnableOrDisableReceiver(): Boolean = {
    log log "safe enabling or disabling receiver";
    val check = isBluetoothDiscoverable || isWifiAvailable
    myCardsReceiverToggleButton.setEnabled(check);
    check;
  }

  /*private def renderCard(myCard: MyCard) {
    myCardWidgetLayout.setCard(myCard)

    LastMyCardPreference.setValue(myCard.id.get)

    myCardWidgetLayout.setOnLeftOnRight(
      onLeft = () => {
        val nextCardOption = myCard.getNext;
        if (nextCardOption.isDefined) {
          renderCard(nextCardOption.get);
        } else {
          showToast(R.string.no_next_card);
        }
      }, onRight = () => {
        val previousCardOption = myCard.getPrev;
        if (previousCardOption.isDefined) {
          renderCard(previousCardOption.get);
        } else {
          showToast(R.string.no_previous_card);
        }
      }
    );
  }*/

  private def refreshBluetoothStateSafe(forcedState: Option[Boolean] = None): Unit = {
    if (isBluetoothSupported) {
      log log "bluetooth state is refreshed"

      myCardsBluetoothToggleButton.setOnCheckedChangeListener(null)

      myCardsBluetoothToggleButton.setChecked(forcedState.getOrElse(isBluetoothDiscoverable))

      myCardsBluetoothToggleButton.setOnCheckedChangeListener(bluetoothListener)
    }
  }

  private def refreshWifiStateSafe(forcedState: Option[Boolean] = None) {
    if (isWifiSupported) {
      myCardsWifiToggleButton.setOnCheckedChangeListener(null);
      myCardsWifiToggleButton.setChecked(forcedState.getOrElse(isWifiAvailable));
      myCardsWifiToggleButton.setOnCheckedChangeListener(wifiListener);
    }
  }

  //going to other activities

  protected def onAlljoynReceivingStarted(): Unit = {
    val intent = new Intent(activity, classOf[AlljoynReceivingActivity])

    startActivity(intent)
  }

  protected def onSocketReceived(socket: BluetoothSocket): Boolean = {
    val readyToReceive: Boolean = TransferringActivity.safeSetAcceptedSocket(socket);

    if (readyToReceive) {
      val intent = new Intent(this, classOf[TransferringActivity]);
      intent.putExtra(TransferringExtras.IS_SENDING.toString, false); //is receiving instead ;)
      startActivity(intent);
      log log "bluetooth connection was accepted";
    }

    readyToReceive;
  }
}

