package activities.mycards

import java.util.{TimerTask, Timer}
import components.BluetoothListeningActivity
import bluetooth.BluetoothHelper
import BluetoothHelper._
import myandroid.{log, WifiHelper}
import myandroid.WifiHelper._
import scala.Some
import android.widget.ToggleButton
import services.ReceiverImmutableService

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MyCardsAssertionTrait extends BluetoothListeningActivity {
  private var timer: Option[Timer] = None;

  private val periodMsecs = 1000;
  private val initialDelay = 3000;

  implicit private val bluetoothAdapter = getDefaultBluetoothAdapter;
  implicit private lazy val wifiManager = getWifiManager;

  override def onStart(): Unit = {
    super.onStart();
    timer = Some(new Timer);
    timer.map {
      t =>
        t.scheduleAtFixedRate(new TimerTask {
          def run(): Unit = {
            val isStarted = ReceiverImmutableService.getIsStarted;
            val isAvailable = WifiHelper.isWifiAvailable;

            log log (
              "receiver started " + isStarted + " is listening " + isListening + " isWaiting "
            );
          }
        }, {
          val delayMsecs = initialDelay;
          delayMsecs
        },
        periodMsecs)
    }
  }

  override def onStop() {
    super.onStop();
    timer.map(_.cancel);
    timer = None;
  }
}
