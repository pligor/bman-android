package activities.crud

import models._
import android.app.Dialog
import com.pligor.bman.R
import android.content.Context
import android.widget.{EditText, RadioGroup, Button}
import android.view._
import android.view.View.OnClickListener
import components.RadioGroupHelper._
import myAndroidSqlite.MySQLiteOpenHelper
import myandroid.AndroidHelper._
import myandroid.DensityPixel

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
private object CardManage {
  def showImageDialog(card: Card)(callback: (Int) => Unit)(implicit context: Context) = {
    val dialogWidth = DensityPixel(260)

    val dialogHeight = DensityPixel(260)

    val dimension = card.currentDimension

    CurrentDimensionPreference.setValue(dimension)
    //Logger("new dimension selected is: " + CurrentDimensionPreference.getValue);

    val dialog = new Dialog(context);

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
    dialog.setCanceledOnTouchOutside(false);

    dialog.setCancelable(true);

    dialog.setContentView(R.layout.create_photo);

    val shootPictureButton = dialog.findViewById(R.id.shootPictureButton).asInstanceOf[Button];
    val importPictureButton = dialog.findViewById(R.id.importPictureButton).asInstanceOf[Button];

    val resourses = context.getResources;

    val word = dimension match {
      case Dimension.frontSide => resourses.getString(R.string.word_front);
      case Dimension.backSide => resourses.getString(R.string.word_back);
    }

    importPictureButton.setText(resourses.getString(R.string.import_picture_button, word));
    shootPictureButton.setText(resourses.getString(R.string.shoot_picture_button, word));

    val listener = new OnClickListener {
      def onClick(view: View) {
        callback(view.getId);
        dialog.dismiss();
      }
    }

    shootPictureButton.setOnClickListener(listener);
    importPictureButton.setOnClickListener(listener);

    dialog.show();
    //according to this: http://stackoverflow.com/questions/2306503/how-to-make-an-alert-dialog-fill-90-of-screen-size
    //and this: http://stackoverflow.com/questions/4406804/how-to-control-the-width-and-height-of-default-alert-dialog-in-android
    //setting the window attributes after show is important

    /*val dialogWindowAttrs = dialog.getWindow.getAttributes;
    dialogWindowAttrs.height = dialogHeight.toPixel.value;
    dialog.getWindow.setAttributes(dialogWindowAttrs);*/
    dialog.getWindow.setLayout(
      /*ViewGroup.LayoutParams.WRAP_CONTENT*/ dialogWidth.toPixel.value,
      dialogHeight.toPixel.value
    );

    dialog;
  }

  def getNewMetaValueDialog(card: Card)(onNewMetaValueCallback: (MetaValue) => Unit)
                           (implicit context: Context): Dialog = {
    val dialog = new Dialog(context);

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background);
    dialog.setCanceledOnTouchOutside(false);

    dialog.setCancelable(true);

    dialog.setContentView(R.layout.dialog_create_meta_value);

    val createNewMetaValueButton = dialog.findViewById(R.id.createNewMetaValueButton).asInstanceOf[Button];

    val myAvailableLabelsRadioGroup = dialog.findViewById(R.id.myAvailableLabelsRadioGroup).asInstanceOf[RadioGroup];

    val newMetaValueEditText = dialog.findViewById(R.id.newMetaValueEditText).asInstanceOf[EditText];
    def getValue = newMetaValueEditText.getText.toString.trim;

    val newLabelEditText = dialog.findViewById(R.id.newLabelEditText).asInstanceOf[EditText];
    def getLabelText = newLabelEditText.getText.toString.trim;

    //these are not working. dont know why. if you solve it remove the if inside on click listener
    /*newLabelEditText.setOnEditorActionListener(new OnEditorActionListener {
      def onEditorAction(p1: TextView, p2: Int, p3: KeyEvent): Boolean = {
        Logger("eimaste mesa sto editor action listener tou new label");
        true;
      }
    });*/
    /*newMetaValueEditText.setOnEditorActionListener(new TextView.OnEditorActionListener {
      def onEditorAction(textView: TextView, actionId: Int, keyEvent: KeyEvent): Boolean = {
        val check = keyEvent.getAction == KeyEvent.ACTION_UP;
        log log "inside on editor action";
        if (check) {
          val check = buttonCheck;
          log log "button check is: " + check;
          createNewMetaValueButton.setEnabled(check);
        }
        true;
      }
    });*/
    //the below is no point to use it if the above does not work
    /*myAvailableLabelsRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener {
      def onCheckedChanged(radioGroup: RadioGroup, checkedId: Int) {
        //createNewMetaValueButton.setEnabled(buttonCheck);

      }
    });*/

    def buttonCheck = {
      ((myAvailableLabelsRadioGroup.getCheckedRadioButtonId != invalidRadioButtonId) || (!getLabelText.isEmpty)) &&
        !getValue.isEmpty
    };

    val labels = card.getLabels(notIncluded = true);
    inflateRadioGroupWithModels(
      myAvailableLabelsRadioGroup,
      labels,
      R.layout.label_radio_button
    );

    createNewMetaValueButton.setOnClickListener(new OnClickListener {
      def onClick(view: View) {
        if (buttonCheck) {
          val cardId = card.id.get;
          val labelText = getLabelText;

          val labelIdOption = if (labelText.isEmpty) {
            val id = getModelFromRadioGroup[MetaLabel](myAvailableLabelsRadioGroup).id.get
            assert(id != MySQLiteOpenHelper.invalidInsertId);
            Some(id);
          } else {
            new MyMetaLabel(label = labelText).safeInsert;
          }

          if (labelIdOption.isDefined) {
            val labelId = labelIdOption.get;
            if (labelId == MySQLiteOpenHelper.invalidInsertId) {

              showToast(R.string.local_database_error);

            } else {
              val metaValue = new MyMetaValue(
                cardId = cardId,
                labelId = labelId,
                dataValue = getValue
              );

              if (metaValue.insert == MySQLiteOpenHelper.invalidInsertId) {

                showToast(R.string.local_database_error);

              } else {
                onNewMetaValueCallback(metaValue);
                dialog.dismiss();
              }
            }
          } else {
            showToast(R.string.meta_label_already_exists);
          }
        } else {
          showToast(R.string.invalid_form);
        }
      }
    });

    dialog;
  }
}
