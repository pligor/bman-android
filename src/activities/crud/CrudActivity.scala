package activities.crud

import android.app.Activity
import com.pligor.bman.{TypedViewHolder, TR, R}
import android.os.Bundle
import components._
import models._
import android.widget.CompoundButton
import android.view.View
import android.net.Uri
import myandroid.log
import myandroid.AndroidHelper._
import adapters.MyMetaDataAdapter
import android.widget.CompoundButton.OnCheckedChangeListener
import myandroid.DialogHelper._
import android.view.View.OnClickListener
import myAndroidAPIs.BugsenseActivity
import views.{ToggleSound, MyFlipImageView}
import views.MyFlipImageView.OnFlipListener

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class CrudActivity extends Activity with TypedViewHolder
with MyActivity

with AdmobActivity

with View.OnClickListener
with OnCheckedChangeListener
with PhotoCapturing
with PhotoImporting
with CrudDetailTrait
with SetCurrentDimensionTrait
with BugsenseActivity {
  protected val photoCaptureRequestCode = RequestCode.CAMERA_DATA.id

  protected val photoImportRequestCode = RequestCode.SELECT_IMAGE.id

  private lazy val crudSwapButton = findView(TR.crudSwapButton)

  private lazy val crudDoneEdittingButton = findView(TR.crudDoneEdittingButton)

  private lazy val crudDeleteTextView = findView(TR.crudDeleteTextView)

  private lazy val crudDeleteCardButton = findView(TR.crudDeleteCardButton)

  private lazy val topPanelTextView = findView(TR.topPanelTextView)

  private lazy val crudImageViewWrapper = findView(TR.crudImageViewWrapper)

  private lazy val crudFlipImageView = new MyFlipImageView(
    findView(TR.crudImageView),
    flipListener = Some(new OnFlipListener {
      def onToggle(newDimension: Dimension.Value): Unit = onChangeState()

      def onFlipEnd(view: MyFlipImageView): Unit = {}

      def onFlipStart(view: MyFlipImageView): Unit = {}
    })
  ) with ToggleSound

  private lazy val addMetaValueButton = findView(TR.addMetaValueButton)

  private lazy val isPublicCheckbox = findView(TR.isPublicCheckbox)

  private lazy val metaListView = findView(TR.metaListView)

  private val metaDataAdapter = new MyMetaDataAdapter

  protected def getCard = crudFlipImageView.getCard

  override def onCreate(bundle: Bundle): Unit = {
    super.onCreate(bundle)

    setContentView(R.layout.activity_crud)

    crudDoneEdittingButton.setOnClickListener(new OnClickListener {
      def onClick(v: View): Unit = {
        onBackPressed()
      }
    })

    crudDeleteCardButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        getOneButtonDialog(
          titleId = R.string.delete_card_confirmation_dialog_title,
          buttonTextId = R.string.delete_card_confirmation_dialog_button,
          backId = R.drawable.dialog_background
        )({
          dialog =>
            dialog.dismiss()

            MyMetaLabel.deleteUnused //we dont really care if everything got deleted

            exitWithResult(cardDeleted = getCard.delete)
        }).show()
      }
    })

    topPanelTextView.setText(UserInfo.getName.get)

    crudFlipImageView.safesetOnceWrapperSize(crudImageViewWrapper)({
      val cardOption = MyCard.getById(cardId)

      assert(cardOption.isDefined,
        "card with id " + cardId + " must exist inside the database, we are not dealing with newborn cards here")

      setCard(cardOption.get)

      metaListView.setAdapter(metaDataAdapter.fillFromCard(getCard))

      isPublicCheckbox.setChecked(getCard.isPublic)

      isPublicCheckbox.setOnCheckedChangeListener(this)
    })

    crudFlipImageView.imageView.setOnClickListener(new View.OnClickListener {
      def onClick(view: View): Unit = {
        log log "double wrapped image view was clicked"

        CardManage.showImageDialog(getCard) {
          case R.id.shootPictureButton => {
            shootFromCamera(
              showToast(R.string.image_shot_impossible)
            )
          }
          case R.id.importPictureButton => {
            importFromGallery()
          }
        }
      }
    })

    addMetaValueButton.setOnClickListener(new View.OnClickListener {
      def onClick(view: View): Unit = {
        val curCard = getCard

        val maxNumber = MyMetaValue.maxMetaValuesPerCard

        val count = curCard.asInstanceOf[MyCard].countMetaValues

        if (count < maxNumber) {
          val dialog = CardManage.getNewMetaValueDialog(curCard) {
            metaValue =>
              metaDataAdapter.addItem(metaValue)
          }
          dialog.show()
        } else {
          showToast(R.string.metadata_limit, maxNumber.toString)
        }
      }
    })
  }

  private def setCard(newCard: Card): Unit = {
    val storedDimension = if(crudFlipImageView.hasCard) getCard.currentDimension else newCard.currentDimension

    crudFlipImageView.setCard(newCard.setDimension(storedDimension))

    onChangeState()
  }

  private def onChangeState(): Unit = {
    val swapCheck = getCard.isTwoSided

    crudSwapButton.setEnabled(swapCheck)

    crudSwapButton.setOnClickListener(if (swapCheck) this else null)

    val deleteCheck = getCard.isTwoSided && (getCard.currentDimension == Dimension.backSide)

    crudDeleteTextView.setEnabled(deleteCheck)

    crudDeleteTextView.setOnClickListener(if (deleteCheck) this else null)
  }

  def onCheckedChanged(checkbox: CompoundButton, checked: Boolean): Unit = {
    assert(checkbox == isPublicCheckbox,
      "there must be only one compound button that reacts on checked change in this activity")

    val myCardOption = getCard.asInstanceOf[MyCard].setIsPublic(checked)

    if (myCardOption.isDefined) {
      setCard(myCardOption.get) //kind of heavy but in order to be consistent..
    } else {
      isPublicCheckbox.setOnCheckedChangeListener(null)

      isPublicCheckbox.setChecked(!checked)

      isPublicCheckbox.setOnCheckedChangeListener(this)

      showToast(R.string.database_error)
    }
  }


  def onClick(view: View): Unit = {
    view.getId match {
      case R.id.crudSwapButton => {
        setCard(getCard.swapSides)
      }
      case R.id.crudDeleteTextView => {
        //delete
        assert(getCard.currentDimension == Dimension.backSide,
          "only on back side must be the delete button to be enabled")

        setCard(getCard.deleteBackCard)
      }
    }
  }

  protected def onShootFromCameraSuccess(photoUri: Uri, onAfterPhotoUriUsage: (Uri) => Unit): Unit = {
    setCurrentDimensionUri(photoUri) {
      newCardOption =>
        if (newCardOption.isDefined) {
          setCard(newCardOption.get)
        } else {
          showToast(R.string.retrieving_new_image_error)
        }

        onAfterPhotoUriUsage(photoUri)
    }
  }

  protected def onImportFromGallerySuccess(photoUriOption: Option[Uri]): Unit = {
    if (photoUriOption.isDefined) {
      val photoUri = photoUriOption.get

      setCurrentDimensionUri(photoUri) {
        newCardOption =>
          if (newCardOption.isDefined) {
            setCard(newCardOption.get)
          } else {
            showToast(R.string.retrieving_new_image_error)
          }
      }
    } else {
      showToast(R.string.retrieving_new_image_error)
    }
  }

  protected def onImportFromGalleryFailure(): Unit = {
    showToast(R.string.retrieving_new_image_error)
  }

  protected def onShootFromCameraFailure(): Unit = {
    showToast(R.string.retrieving_new_image_error)
  }

  override def onBackPressed(): Unit = {
    //do NOT uncomment line below, because we have a race condition and we call finish so it is not necessary anyway
    //super.onBackPressed();
    exitWithResult()
  }

  /*//http://developer.android.com/training/displaying-bitmaps/process-bitmap.html
  private class BitmapWorkerTask(private val imageView: ImageView,
                                 private val overlayView: View)
                                (op: (Option[Uri]) => Option[Long])
    extends ScalaAsyncTask[Option[Uri], AnyRef, Option[Long]] {
    private val imageViewReference = new WeakReference(imageView);

    override def onPreExecute() {
      super.onPreExecute();
      if (Option(imageView).isDefined) {
        overlayView.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.INVISIBLE);
      }
    }

    protected def doInBackground(uriOption: Option[Uri]): Option[Long] = {
      op(uriOption);
    }

    override def onPostExecute(modelIdOption: Option[Long]) {
      super.onPostExecute(modelIdOption);

      def error() {
        showToast(R.string.retrieving_new_image_error);
      }

      if (modelIdOption.isDefined) {
        val imageViewReferenceOption = Option(imageViewReference);
        if (imageViewReferenceOption.isDefined) {
          val imageViewOption: Option[ImageView] = imageViewReference.get;
          if (imageViewOption.isDefined) {
            //we are not actually using the image view
            if (modelIdOption == Some(Card.invalidId)) {
              error()
            } else {
              //Model.setModelId(modelIdOption);
            }
            imageView.setVisibility(View.VISIBLE);
            overlayView.setVisibility(View.INVISIBLE);
          }
        }
      } else {
        error();
      }
    }
  }*/

}
