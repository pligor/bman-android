package activities.crud

import android.app.Activity
import com.pligor.bman.{TypedViewHolder, TR, R}
import android.os.Bundle
import components._
import models._
import android.view.View
import adapters.PeopleMetaDataAdapter
import myandroid.DialogHelper._
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.view.View.OnClickListener
import myandroid.log
import myAndroidAPIs.BugsenseActivity
import android.content.Intent
import activities.ZoomActivity
import activities.namelist.NameListActivity
import views.{ToggleSound, MyFlipImageView}

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
class CardDetailActivity extends Activity with TypedViewHolder
with MyActivity

with AdmobActivity

with CrudDetailTrait
with BugsenseActivity {
  protected val photoCaptureRequestCode = RequestCode.CAMERA_DATA.id

  protected val photoImportRequestCode = RequestCode.SELECT_IMAGE.id

  private lazy val cardDetailImageViewWrapper = findView(TR.cardDetailImageViewWrapper)

  private lazy val cardDetailFlipImageView = new MyFlipImageView(
    findView(TR.cardDetailImageView)
  ) with ToggleSound

  private lazy val cardDetailDoneEdittingButton = findView(TR.cardDetailDoneEdittingButton)

  private lazy val cardDetailDeleteCardButton = findView(TR.cardDetailDeleteCardButton)

  private lazy val topPanelTextView = findView(TR.topPanelTextView)

  private lazy val cardDetailSendButton = findView(TR.cardDetailSendButton)

  private lazy val cardDetailMetaListView = findView(TR.cardDetailMetaListView)

  private val peopleMetaDataAdapter = new PeopleMetaDataAdapter

  override def onCreate(bundle: Bundle): Unit = {
    super.onCreate(bundle)

    setContentView(R.layout.activity_card_detail)

    cardDetailDoneEdittingButton.setOnClickListener(new OnClickListener {
      def onClick(v: View): Unit = {
        onBackPressed()
      }
    })

    cardDetailDeleteCardButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {

        getOneButtonDialog(
          titleId = R.string.delete_card_confirmation_dialog_title,
          buttonTextId = R.string.delete_card_confirmation_dialog_button,
          backId = R.drawable.dialog_background
        )({
          dialog =>
            dialog.dismiss()

            PeopleMetaLabel.deleteUnused //we dont really care if everything got deleted

            exitWithResult(cardDeleted = peopleCard.delete)
        }).show()
      }
    })

    cardDetailFlipImageView.safesetOnceWrapperSize(cardDetailImageViewWrapper)({
      cardDetailFlipImageView.setCard({
        val cardOption = PeopleCard.getById(cardId)

        assert(cardOption.isDefined,
          "card with id " + cardId + " must exist inside the database, we are not dealing with newborn cards here")

        cardOption.get
      })

      topPanelTextView.setText(peopleCard.senderName)

      cardDetailMetaListView.setAdapter(peopleMetaDataAdapter.fillFromCard(peopleCard))

      cardDetailSendButton.setEnabled(peopleCard.isAllowedToBeSent)
    })

    cardDetailMetaListView.setOnItemClickListener(new OnItemClickListener {
      def onItemClick(adapterView: AdapterView[_], view: View, position: Int, id: Long): Unit = {
        val metaValue = peopleMetaDataAdapter.getItem(position)

        val value = metaValue.dataValue

        log log "current metavalue has value: " + value

        MetaDataParser.execute(value)
      }
    })

    cardDetailFlipImageView.imageView.setOnClickListener(new OnClickListener {
      def onClick(v: View): Unit = {
        peopleCard.getCurrentPhoto.map {
          currentPhoto =>
            context.startActivity(new Intent(context, classOf[ZoomActivity]).
              putExtra(ZoomActivity.EXTRA.PHOTO_ID.toString, currentPhoto.id.get)
            )
        }
      }
    })

    if (cardDetailSendButton.isEnabled) {
      //a disabled button cannot be clicked but ok this is a double check
      cardDetailSendButton.setOnClickListener(new OnClickListener {
        def onClick(view: View): Unit = {
          context.startActivity(new Intent(context, classOf[NameListActivity]).putExtra(
            NameListActivity.EXTRA.CARD_ID.toString, peopleCard.id.get
          ))
        }
      })
    }
  }

  override def onBackPressed(): Unit = {
    //do NOT uncomment line below, because we have a race condition and we call finish so it is not necessary anyway
    //super.onBackPressed();
    exitWithResult()
  }

  private def peopleCard = cardDetailFlipImageView.getCard.asInstanceOf[PeopleCard]
}
