package activities.crud

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object Crud {
  object InputExtra extends Enumeration {
    val MODEL_ID = Value;
    val CARD_TYPE = Value;
    val VIEW_ID = Value;
  }

  object OutputExtra extends Enumeration {
    val MODEL_ID = Value;
    val CARD_TYPE = Value;
    val VIEW_ID = Value;
  }
}
