package activities.crud

import models.Card
import android.app.Activity
import android.content.Intent
import views.Views

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait CrudDetailTrait extends Activity {
  protected lazy val cardType = Card.Type(getIntent.getExtras.getInt(Crud.InputExtra.CARD_TYPE.toString));

  protected lazy val cardId = {
    val card_id = getIntent.getExtras.getLong(Crud.InputExtra.MODEL_ID.toString, Card.invalidId);
    assert(card_id != Card.invalidId, "card should be an old card with a valid id");
    card_id;
  }

  protected def exitWithResult(cardDeleted: Boolean = false) {
    val data = new Intent().putExtra(Crud.OutputExtra.MODEL_ID.toString, cardId).
      putExtra(Crud.OutputExtra.VIEW_ID.toString,
      //just passing back the card view id so we can know what to do
      getIntent.getExtras.getInt(Crud.InputExtra.VIEW_ID.toString, Views.invalidViewId)).
      putExtra(Crud.OutputExtra.CARD_TYPE.toString, cardType.id);

    setResult(if (cardDeleted) Activity.RESULT_CANCELED else Activity.RESULT_OK, data);

    finish();
  }
}
