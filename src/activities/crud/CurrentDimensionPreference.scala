package activities.crud

import models.Dimension
import android.content.Context
import components.MySecurePreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
private case object CurrentDimensionPreference extends MySecurePreference[Int, Dimension.Value] {
  //val preferenceKey = CurrentDimensionPreference.toString;
  //for some bizarre reason, above line causes a recursive functions which leads to StackOverflowError
  val preferenceKey = "CurrentDimensionPreference"

  val defaultValue: Int = Dimension.invalidSideId

  def getValue(implicit context: Context): Dimension.Value = {
    Dimension(getInnerValue.asInstanceOf[Int])
  }

  def setValue(newValue: Dimension.Value)(implicit context: Context): Boolean = {
    setInnerValue(newValue.id)
  }
}
