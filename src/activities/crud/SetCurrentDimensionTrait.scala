package activities.crud

import models.{MyCard, Dimension, Card}
import components.{MyActivity, WriteOnce}
import android.net.Uri
import android.app.ProgressDialog
import com.pligor.bman.R
import android.content.DialogInterface.OnCancelListener
import android.content.DialogInterface
import myandroid.{ScalaAsyncTask, ProgressDialogActivity}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait SetCurrentDimensionTrait extends MyActivity with ProgressDialogActivity {
  private var taskOption: Option[SetCurrentDimensionTask] = None

  protected def getCard: Card

  protected def setCurrentDimensionUri(uri: Uri)(onAfterSetting: Option[Card] => Unit): Unit = {
    taskOption = Some(new SetCurrentDimensionTask(onAfterSetting))

    taskOption.get.execute(uri)
  }

  override def onStop(): Unit = {
    super.onStop()

    taskOption.map(_.cancel {
      val mayInterruptIfRunning = true

      mayInterruptIfRunning
    })
  }

  private class SetCurrentDimensionTask(op: Option[Card] => Unit)
    extends ScalaAsyncTask[Uri, AnyRef, Option[Card]] {

    private val progressDialog = new WriteOnce[ProgressDialog]

    override def onPreExecute(): Unit = {
      super.onPreExecute()

      progressDialog setValue {
        val progDialog = registerProgressDialog {
          val indeterminate = true

          val cancelable = true

          ProgressDialog.show(
            SetCurrentDimensionTrait.this,
            getResources.getString(R.string.crud_progress_dialog_title),
            getResources.getString(R.string.crud_progress_dialog_message),
            indeterminate,
            cancelable
          )
        }

        progDialog.setOnCancelListener(new OnCancelListener {
          def onCancel(dialog: DialogInterface): Unit = {
            cancel({
              val mayInterruptIfRunning = true

              mayInterruptIfRunning
            })
          }
        })

        progDialog
      }
    }

    protected def doInBackground(photoUri: Uri): Option[Card] = {
      val myCard = getCard.asInstanceOf[MyCard]

      CurrentDimensionPreference.getValue match {
        case Dimension.frontSide => myCard.setFrontUri(photoUri)
        case Dimension.backSide => myCard.setBackUri(photoUri)
      }
    }

    override def onPostExecute(myCardOption: Option[Card]): Unit = {
      super.onPostExecute(myCardOption)

      progressDialog().dismiss()

      op(myCardOption)
    }
  }
}
