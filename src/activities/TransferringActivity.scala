package activities

import android.app.Activity
import com.pligor.bman.{Bman, CommunicationReason, R, TypedViewHolder}
import components._
import android.os.{Message, Handler, Bundle}
import android.bluetooth.{BluetoothAdapter, BluetoothSocket, BluetoothDevice}
import models._
import bluetooth._
import preferences.ConfirmReceivePreference
import java.io.{IOException, File}
import components.ProgressWheelHelper._
import myAndroidAPIs.BugsenseActivity
import myandroid.{ScalaAsyncTask, MyContextWrapper, log}

object TransferringExtras extends Enumeration {
  val DEVICE = Value
  //bluetooth device (parcelable)
  val CARD_ID = Value
  //long
  val IS_SENDING = Value //boolean
}

object TransferringActivity {
  val acceptedSocket = new ElementaryStack[BluetoothSocket]

  def safeSetAcceptedSocket(socket: BluetoothSocket): Boolean = {
    try {
      TransferringActivity.acceptedSocket.put(socket)

      true
    } catch {
      case e: IllegalArgumentException => try {
        socket.close()

        false
      } catch {
        case e: IOException => false
      }
    }
  }
}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class TransferringActivity extends Activity with TypedViewHolder
with MyContextWrapper
with MyActivity

with AdmobActivity

with SendingLayout
with ReceivingLayout
with BluetoothActivity
with TransferringStatement
with BugsenseActivity {

  /**
   * @return true if bluetooth actually got disabled, false if not
   */
  protected def onBeforeBluetoothTurnOff(disableBluetooth: => Boolean) = disableBluetooth
  //no need to do anything else

  private var generateTempJsonFileTaskOption: Option[GenerateTempJsonFileTask] = None

  private val mutableBuffer = new MutableBuffer

  protected implicit val bluetoothAdapter = Option(BluetoothAdapter.getDefaultAdapter)

  private val deviceWrapper = new WriteOnce[MyBluetoothDeviceWrapper]

  private val jsonFile = new WriteOnce[File]

  //extras
  private lazy val cardId = getIntent.getLongExtra(TransferringExtras.CARD_ID.toString, Card.invalidId);
  private lazy val isSending = getIntent.getBooleanExtra(TransferringExtras.IS_SENDING.toString, false);

  //views
  private lazy val progressWheel = if (isSending) sendingProgressWheel else receivingProgressWheel;

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(
      if (isSending) R.layout.activity_sending
      else R.layout.activity_receiving
    )

    require((isSending && cardId != Card.invalidId) ||
      (!isSending && cardId == Card.invalidId),
      "you are either have a card when you're sending or you don't have a card and you are receiving"
    )

    val device = if (isSending) {
      val remoteDevice = getIntent.getParcelableExtra[BluetoothDevice](TransferringExtras.DEVICE.toString);

      generateTempJsonFileTaskOption = Some(new GenerateTempJsonFileTask({
        tempJsonFileOption =>
          if (tempJsonFileOption.isDefined) {
            jsonFile setValue tempJsonFileOption.get

            //initiate card sequence
            new MyBluetoothDeviceWrapper(
              remoteDevice,
              Some(connectHandler),
              Some(connectedHandler),
              connectThread = None,
              connectedThread = None
            ).createAndStartConnectThread(
              secure = false,
              reason = CommunicationReason.SEND_CARD,
              mutableBuffer
            )
          } else {
            renderSendingResult(success = false, messageId = R.string.card_json_file_could_not_created)
          }
      }))

      generateTempJsonFileTaskOption.get.execute(cardId)

      remoteDevice
    } else {
      val socket = TransferringActivity.acceptedSocket.get;
      val remoteDevice = socket.getRemoteDevice;
      deviceWrapper.setValue(
        new MyBluetoothDeviceWrapper(
          remoteDevice,
          None,
          Some(connectedHandler),
          connectThread = None,
          connectedThread = None
        ).createAndStartConnectedThread(socket, MyBluetoothProtocolState.SLAVE_RECEIVE_REASON, mutableBuffer)
      );

      remoteDevice;
    }

    //render views
    //resultLayout.setVisibility(View.GONE);

    /*connectedNameTextView.setText(getResources.getString(if (isSending) {
      R.string.sending_destination
    } else {
      R.string.receiving_origin
    }).format(device.getName));*/

    val name = device.getName

    if (isSending) {
      setDestination(name)
    } else {
      setOrigin(name)
    }
  }

  override def onStop(): Unit = {
    super.onStop()

    devConnsCanceller()

    if (jsonFile.isInitialized) {
      jsonFile().delete()
    }

    generateTempJsonFileTaskOption.map(_.cancel({
      val mayInterruptIfRunning = true;
      mayInterruptIfRunning;
    }));
  }


  override def onBackPressed(): Unit = {
    super.onBackPressed()

    finish()
  }

  private object devConnsCanceller {
    private val areConnsCancelled = new WriteOnce[Unit];

    def apply(): Unit = {
      if (deviceWrapper.isInitialized && !areConnsCancelled.isInitialized) {
        deviceWrapper().cancelConnThreads();
        areConnsCancelled setValue();
      }
    }
  }

  private def finishWithMessage(messageId: Int, success: Boolean): Unit = {
    if (isSending) {
      MyBugsense.Event.sendBugsenseEvent(
        if (success) MyBugsense.Event.CARD_SEND_SUCCESS_VIA_BLUETOOTH
        else MyBugsense.Event.CARD_SEND_FAIL_VIA_BLUETOOTH
      )


      renderSendingResult(success, messageId);
    } else {
      renderReceivingResult(success, messageId);
    }

    devConnsCanceller()
  }

  protected def onBluetoothTurnOff(): Unit = {
    connectedHandler.finishAbruptly(R.string.bluetooth_adapter_turned_off)
  }

  private object connectedHandler extends Handler {
    private val master = new WriteOnce[Master]

    private val slave = new WriteOnce[Slave]

    def finishAbruptly(toastMessageId: Int): Unit = {
      assert(!(master.isInitialized && slave.isInitialized),
        "we may not have both master and slave being initialized")

      if (master.isInitialized) {
        master().cancel()
      } else if (slave.isInitialized) {
        slave().cancel()
      }

      finishWithMessage(toastMessageId, success = false)
    }

    private def masterFinishSuccessfully(): Unit = {
      finishWithMessage(R.string.card_sent_succesfully, success = true)
    }

    private def slaveFinishSuccessfully(): Unit = {
      finishWithMessage(R.string.transferring_slave_card_received_succesfully, success = true)
    }

    private def slaveFinishGracefully(): Unit = {
      finish()
    }

    override def handleMessage(msg: Message): Unit = {
      super.handleMessage(msg)

      ConnectedThreadMessage(msg.what) match {
        case ConnectedThreadMessage.BYTES_READ => {
          val bytes = msg.obj.asInstanceOf[Array[Byte]]

          val curState = MyBluetoothProtocolState(msg.arg1)

          curState match {
            case MyBluetoothProtocolState.SLAVE_RECEIVE_REASON => {
              log log "The reason of the sender is to send me a card"

              def acceptReceive(): Unit = {
                log log "I accept receiving card"

                deviceWrapper().setConnectedState(MyBluetoothProtocolState.SLAVE_RECEIVE_FILE_LEN)
                  .sendBytes(ByteConv.boolean2arrayByte(value = true))
              }

              if (ConfirmReceivePreference.getValue) {
                Slave.genConfirmDialog(
                  deviceWrapper().bluetoothDevice.getName,
                  acceptReceive = acceptReceive(),
                  denyReceive = {
                    log log "I will NOT receive card"

                    deviceWrapper().setConnectedState(MyBluetoothProtocolState.SLAVE_DENY_FILE)
                      .sendBytes(ByteConv.boolean2arrayByte(value = false))

                    slaveFinishGracefully()
                  },
                  timeoutSecs = Bman.DIALOG_CONFIRM_RECEIVE_TIMEOUT_SECS
                ).show()
              } else {
                log log "I have AUTOMATICALLY accepted to receive this card"

                acceptReceive()
              }
            }
            case MyBluetoothProtocolState.MASTER_CONFIRM_FILE_SEND => {
              master.setValue(new Master(jsonFile(), mutableBuffer))

              val lenOption = master().confirmCardSend(bytes)

              if (lenOption.isDefined) {
                val len = lenOption.get;
                log log "I am sending the file len (number of bytes: " + len +
                  ") and wait for confirmation that len is ok";
                deviceWrapper().setConnectedState(MyBluetoothProtocolState.MASTER_FILE_LEN_ACCEPTANCE)
                  .sendBytes(ByteConv.long2arrayByte(len));
                onConfirmedFileSend();
              } else {
                log log "file is denied";
                finishAbruptly(R.string.transferring_master_card_denied);
              }
            }
            case MyBluetoothProtocolState.SLAVE_RECEIVE_FILE_LEN => {
              slave.setValue(new Slave(mutableBuffer));

              val isLenAcceptable = slave().receiveFileLen(bytes);

              val nextState = if (isLenAcceptable) {
                MyBluetoothProtocolState.SLAVE_RECEIVE_BUFFER_LEN;
              } else {
                MyBluetoothProtocolState.SLAVE_FILE_LEN_INVALID;
              }
              deviceWrapper().setConnectedState(nextState)
                .sendBytes(ByteConv.boolean2arrayByte(value = isLenAcceptable));
            }
            case MyBluetoothProtocolState.MASTER_FILE_LEN_ACCEPTANCE => {

              val bufferLenOption: Option[Int] = master().fileLenAcceptance(bytes);

              if (bufferLenOption.isDefined) {
                val bufferLenOrInvalid = bufferLenOption.get;

                val nextState = if (bufferLenOrInvalid == CardTransaction.INVALID_BUFFER_BYTE_LENGTH) {
                  log log ("This is supposed to be the first buffer and " +
                    "I have already reached the end of file! Error!");
                  MyBluetoothProtocolState.MASTER_ERROR_FILE_EMPTY;
                } else {
                  log log ("I have " + bufferLenOrInvalid + " bytes to send");
                  MyBluetoothProtocolState.MASTER_SEND_BUFFER_DATA;
                }
                deviceWrapper().setConnectedState(nextState)
                  .sendBytes(ByteConv.int2arrayByte(bufferLenOrInvalid));
              } else {
                finishAbruptly(R.string.transferring_master_file_len_invalid);
              }
            }
            case MyBluetoothProtocolState.SLAVE_RECEIVE_BUFFER_LEN => {
              val receiveStatus = slave().receiveBufferLen(bytes);

              renderProgressWheel(slave().getPercentageReceived, progressWheel);

              if (receiveStatus.isComplete) {
                val successfulReceive = receiveStatus.isSuccessful;
                val nextState = if (successfulReceive) {
                  log log "file has been received successfully";
                  MyBluetoothProtocolState.SLAVE_RECEIVE_FILE_COMPLETED;
                } else {
                  log log "invalid file bytes received occassion, (perhaps even none if tried to send empty file";
                  MyBluetoothProtocolState.SLAVE_INVALID_FILE_BYTES_RECEIVED;
                }
                deviceWrapper().setConnectedState(nextState)
                  .sendBytes(ByteConv.boolean2arrayByte(value = successfulReceive));
              } else {
                val bufferLen = ByteConv.arrayByte2int(bytes); //we are re-getting the buffer len here
                deviceWrapper().setConnectedState(MyBluetoothProtocolState.SLAVE_RECEIVE_BUFFER_DATA)
                  .sendBytes(ByteConv.boolean2arrayByte(value = true), Some(bufferLen));
              }
            }
            case MyBluetoothProtocolState.MASTER_SEND_BUFFER_DATA => {
              val buffer = master().sendBufferData(bytes);

              log log ("Including this buffer I will have sent so far " +
                master().fileSender.getSentByteCounter + " bytes from the total of " +
                master().fileSender.fileLen +
                " bytes to send and I wait from slave to tell me that has buffer len");

              renderProgressWheel(master().fileSender.getPercentageSent, progressWheel);

              log log "Next I will wait to get hash";
              deviceWrapper().setConnectedState(MyBluetoothProtocolState.MASTER_GET_BUFFER_HASH)
                .sendBytes(buffer);
            }
            case MyBluetoothProtocolState.SLAVE_RECEIVE_BUFFER_DATA => {
              val hashBytes = slave().receiveBufferData(bytes);

              log log "I am going to wait for master to tell me if the hash of the buffer is ok";

              deviceWrapper().setConnectedState(MyBluetoothProtocolState.SLAVE_RECEIVE_HASH_CONFIRMATION)
                .sendBytes(hashBytes);
            }
            case MyBluetoothProtocolState.MASTER_GET_BUFFER_HASH => {
              val isBufferHashCorrect = master().getBufferHash(bytes);

              val nextState = if (isBufferHashCorrect) {
                log log "Send true to notify that sending succeeded. Wait slave to write files to buffer before proceeding with next buffer's length :)";
                //ConnectedThread.increaseBuffer();
                mutableBuffer.increaseBuffer();
                MyBluetoothProtocolState.MASTER_SEND_BUFFER_LEN;
              } else {
                log log "Send false to notify that sending failed and then wait for slave to ping to resend same data :(";
                //ConnectedThread.decreaseBuffer();
                mutableBuffer.decreaseBuffer();
                MyBluetoothProtocolState.MASTER_SEND_BUFFER_DATA;
              }
              deviceWrapper().setConnectedState(nextState)
                .sendBytes(ByteConv.boolean2arrayByte(value = isBufferHashCorrect));
            }
            case MyBluetoothProtocolState.SLAVE_RECEIVE_HASH_CONFIRMATION => {
              val hashStatus = slave().receiveHashConfirmation(bytes);

              if (hashStatus.isBufferReceivedCorrectly) {
                log log "I am going back to wait for length of next buffer";
                deviceWrapper().setConnectedState(MyBluetoothProtocolState.SLAVE_RECEIVE_BUFFER_LEN)
                  .sendBytes(ByteConv.boolean2arrayByte(value = true));
              } else {
                log log "Now I have to wait for same data again";
                deviceWrapper().setConnectedState(MyBluetoothProtocolState.SLAVE_RECEIVE_BUFFER_DATA)
                  .sendBytes(ByteConv.boolean2arrayByte(value = true), hashStatus.bufferLen);
              }
            }
            case MyBluetoothProtocolState.MASTER_SEND_BUFFER_LEN => {
              val bufferLenOrInvalid = master().sendBufferLen(bytes);

              val nextState = if (bufferLenOrInvalid == CardTransaction.INVALID_BUFFER_BYTE_LENGTH) {
                log log "I have reached the end of file";
                log log "I wait for confirmation of completion from slave and then complete procedure";
                MyBluetoothProtocolState.MASTER_FILE_SENT;
              } else {
                log log ("I have " + bufferLenOrInvalid + " bytes to send");
                MyBluetoothProtocolState.MASTER_SEND_BUFFER_DATA;
              }
              deviceWrapper().setConnectedState(nextState)
                .sendBytes(ByteConv.int2arrayByte(bufferLenOrInvalid));
            }
            case MyBluetoothProtocolState.MASTER_FILE_SENT => {
              val isFileSentSuccessfully = master().fileSent(bytes);

              log log ("isFileSentSuccessfully: " + isFileSentSuccessfully);

              if (isFileSentSuccessfully) {
                deviceWrapper().setConnectedState(MyBluetoothProtocolState.MASTER_SEND_FILE_COMPLETED);
                masterFinishSuccessfully();
              } else {
                finishAbruptly(R.string.transferring_master_card_sent_unsuccesfully);
              }
            }
            case MyBluetoothProtocolState.MASTER_ERROR_FILE_EMPTY => {
              //just assert that we have received false from "receive buffer len" of slave
              master().errorFileEmpty(bytes);

              finishAbruptly(R.string.transferring_master_card_sent_unsuccesfully);
            }
          }
        }
        case ConnectedThreadMessage.CONNECTION_STOPPED => {
          val curState = MyBluetoothProtocolState(msg.arg1);

          curState match {
            case MyBluetoothProtocolState.MASTER_SEND_FILE_COMPLETED => {
              //do nothing more, a message has already been toasted
            }
            case MyBluetoothProtocolState.SLAVE_RECEIVE_FILE_COMPLETED => {
              log log "inside slave receive card completed!";

              new ScalaAsyncTask[Slave, AnyRef, Option[PeopleCard]] {
                protected def doInBackground(slave: Slave): Option[PeopleCard] = {
                  slave.toPeopleCardOption;
                }

                override def onPostExecute(peopleCardOption: Option[PeopleCard]) {
                  super.onPostExecute(peopleCardOption);
                  slave().end();

                  if (peopleCardOption.isDefined) {
                    slaveFinishSuccessfully();
                  } else {
                    finishWithMessage(R.string.transfer_ok_but_card_creation_failed, success = false);
                  }
                }
              } execute slave();
            }
            case MyBluetoothProtocolState.SLAVE_DENY_FILE => {
              slaveFinishGracefully();
            }
            case MyBluetoothProtocolState.SLAVE_FILE_LEN_INVALID => {
              finishAbruptly(R.string.transferring_slave_sender_tried_invalid_length);
            }
            case MyBluetoothProtocolState.SLAVE_INVALID_FILE_BYTES_RECEIVED => {
              finishAbruptly(R.string.transferring_slave_invalid_file_bytes_received);
            }
            case MyBluetoothProtocolState.MASTER_SLAVE_CONNECTION_TIMEOUT => {
              finishAbruptly(R.string.transferring_connection_timeout);
            }
            case MyBluetoothProtocolState.SLAVE_INVALID_REASON => {
              finishAbruptly(R.string.transferring_invalid_reason);
            }
            case _ => {
              log log ("connection lost and the state is: " + curState);
              finishAbruptly(R.string.transferring_connection_lost);
            }
          }
        }
      }
    }
  }

  private object connectHandler extends Handler {
    override def handleMessage(msg: Message): Unit = {
      super.handleMessage(msg)

      ConnectThreadMessage(msg.what) match {
        case ConnectThreadMessage.CONNECTION_FAILED => {
          //one scenario is the other user decided to turn off his/her bman, or even device
          //another scenario is the other device does not have bman installed nor running
          val deviceWrapper = msg.obj.asInstanceOf[MyBluetoothDeviceWrapper];
          log log ("connection has failed for device: " + deviceWrapper.bluetoothDevice);
          finishWithMessage(R.string.transferring_failed, success = false);
        }
        case ConnectThreadMessage.CONNECTION_SUCCEEDED => {
          deviceWrapper.setValue(msg.obj.asInstanceOf[MyBluetoothDeviceWrapper]);
          val reason = CommunicationReason(msg.arg1);

          //first things first! sending the reason
          deviceWrapper().sendBytes(ByteConv.int2arrayByte(reason.id));

          log log ("connection succeeded for device: " + deviceWrapper().bluetoothDevice);
        }
      }
    }
  }

  private def onConfirmedFileSend(): Unit = {
    //nothing yet
  }

  protected def onBluetoothTurnOn(success: Boolean): Unit = {
    //nop yet since there is no control or code that tries to enable bluetooth
  }
}
