package activities

import android.app.Activity
import com.pligor.bman.{TR, R, TypedViewHolder}
import components.{AdmobActivity, TransferringStatement, DownloadOldestCardTrait, MyActivity}
import android.os.Bundle
import android.view.View
import com.todddavies.components.progressbar.ProgressWheel
import components.ProgressWheelHelper.renderProgressWheel
import myAndroidAPIs.BugsenseActivity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object DownloadOldestCardActivity {

  object EXTRA extends Enumeration {
    val TOKEN = Value;
  }

}

class DownloadOldestCardActivity extends Activity with TypedViewHolder
with MyActivity

with AdmobActivity

with DownloadOldestCardTrait
with TransferringStatement
with BugsenseActivity {

  private lazy val token = getIntent.getStringExtra(DownloadOldestCardActivity.EXTRA.TOKEN.toString);

  private lazy val receivingOriginTextView = findView(TR.receivingOriginTextView);
  private lazy val receivingProgressWheel = findViewById(R.id.receivingProgressWheel).asInstanceOf[ProgressWheel];
  private lazy val receivingResultLayout = findView(TR.receivingResultLayout);
  private lazy val receivingResultMessage = findView(TR.receivingResultMessage);
  private lazy val receivingResultIcon = findView(TR.receivingResultIcon);

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_receiving);
  }

  override def onStart() {
    super.onStart();

    receivingResultLayout.setVisibility(View.GONE);

    //just for the brief moment before downloading the info
    renderOriginName(getResources.getString(R.string.online_server));

    executeDownloadCard(token);
  }

  protected def onOldestCardProgress(percentage: Float) {
    renderProgressWheel(percentage, receivingProgressWheel);
  }

  protected def onOldestCardResult(success: Boolean, messageId: Int, args: String*) {
    receivingProgressWheel.setVisibility(View.GONE);
    receivingResultLayout.setVisibility(View.VISIBLE);
    receivingResultMessage.setText(getResources.getString(messageId, args));
    receivingResultIcon.setImageResource(if (success) R.drawable.yes else R.drawable.no);
  }

  protected def onSenderNameReceived(senderName: String) {
    renderOriginName(senderName);
  }

  private def renderOriginName(name: String) {
    receivingOriginTextView.setText(getResources.getString(
      R.string.receiving_origin, name
    ));
  }
}
