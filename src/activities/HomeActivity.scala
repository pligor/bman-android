package activities

import android.app.Activity
import com.pligor.bman.{TypedViewHolder, TR, R}
import components._
import android.os.Bundle
import preferences.HomeScreenOnStartupPreference
import android.widget.CompoundButton
import android.widget.CompoundButton.OnCheckedChangeListener
import android.view.View
import android.content.Intent
import myauth.{SingleToken, AuthActionable}
import activities.account.AccountActivity
import activities.contactlist.ContactListActivity
import activities.mycards.MyCardsActivity
import android.view.View.OnClickListener
import models.{MyTokenPreference, PendingCounter}
import myandroid.log
import myAndroidAPIs.BugsenseActivity
import activities.testing.TestingActivity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class HomeActivity extends Activity with TypedViewHolder
with MyActivity

with AdmobActivity

with DebuggableTrait
with AuthActionable
with CheckPendingCardsTrait
with DisplayNameTrait
with BugsenseActivity {
  private lazy val homeScreenOnStartupCheckBox = findView(TR.homeScreenOnStartupCheckBox)

  private lazy val homeCreateCardButton = findView(TR.homeCreateCardButton)

  private lazy val homeEditAccountDetailsButton = findView(TR.homeEditAccountDetailsButton)

  private lazy val homeContactCardsButton = findView(TR.homeContactCardsButton)

  private lazy val homeMyCardsStandbyButton = findView(TR.homeMyCardsStandbyButton)

  private lazy val homeDisplayNameButton = findView(TR.homeDisplayNameButton)

  private lazy val homeSettingsButton = findView(TR.homeSettingsButton)

  private lazy val homeLogo = findView(TR.homeLogo)

  private lazy val homeHelpImageButton = findView(TR.homeHelpImageButton)

  private lazy val pendingCardsTextView = findView(TR.pendingCardsTextView)

  private lazy val homeRemoteReceiveButton = findView(TR.homeRemoteReceiveButton)

  private val checkPendingCardsCircularPeriod = 30 * 1000

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_home)

    homeLogo.setOnClickListener(if (isDebuggable) new OnClickListener {
      def onClick(view: View): Unit = {
        startActivity(new Intent(context, classOf[TestingActivity]))
      }
    } else null)

    {
      homeScreenOnStartupCheckBox.setChecked(HomeScreenOnStartupPreference.getValue)

      homeScreenOnStartupCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener {
        def onCheckedChanged(compoundButton: CompoundButton, checked: Boolean): Unit = {
          HomeScreenOnStartupPreference.setValue(checked)
        }
      })
    }

    homeEditAccountDetailsButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        executeAuthAction(token =>
        //token will not be passed as an argument
          startActivity(new Intent(activity, classOf[AccountActivity]))
        )
      }
    })

    homeDisplayNameButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        popupDisplayNameDialog(prefilled = true, onNewName = None)
      }
    })

    homeMyCardsStandbyButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        startActivity(new Intent(activity, classOf[MyCardsActivity]))
      }
    })

    homeCreateCardButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        startActivity(new Intent(activity, classOf[CreateFrontActivity]))
      }
    })

    homeContactCardsButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        startActivity(new Intent(activity, classOf[ContactListActivity]))
      }
    })

    homeSettingsButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        startActivity(new Intent(activity, classOf[SettingsActivity]))
      }
    })

    homeRemoteReceiveButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        executeAuthAction(token =>
          startActivity(new Intent(activity, classOf[DownloadOldestCardActivity]).
            putExtra(DownloadOldestCardActivity.EXTRA.TOKEN.toString, token)
          )
        )
      }
    })

    homeHelpImageButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        startActivity(new Intent(activity, classOf[HelpActivity]))
      }
    })
  }


  override def onResume(): Unit = {
    super.onResume()

    log log "home activity onResume"
  }


  override def onPostResume(): Unit = {
    super.onPostResume()

    log log "home activity onPostResume"
  }

  override def onWindowFocusChanged(hasFocus: Boolean): Unit = {
    super.onWindowFocusChanged(hasFocus)

    log log "home activity onWindowFocusChanged"
  }

  override def onStart(): Unit = {
    super.onStart()

    log log "home activity started"

    repeatPeriodicallyCheckPendingCards(getSafePeriod(checkPendingCardsCircularPeriod))
  }

  override def onStop(): Unit = {
    super.onStop()

    log log "home activity stopped"
  }

  protected def getTokenForCheckPendingCards: Option[SingleToken] = {
    val tokenOption = MyTokenPreference.getValue

    if (!tokenOption.isDefined) {
      pendingCardsTextView.setVisibility(View.GONE)
    }

    tokenOption.map(SingleToken.apply)
  }

  protected def renderCheckPendingCardsFailure(messageId: Int, args: String*): Unit = {
    pendingCardsTextView.setVisibility(View.GONE)
  }

  protected def renderCheckPendingCardsSuccess(pendingCounter: PendingCounter): Unit = {
    val emailsAndCellphones = pendingCounter.emailsAndCellphones

    //pendingCardsTextView.setVisibility(if (emailsAndCellphones <= 0) View.GONE else View.VISIBLE);
    pendingCardsTextView.setVisibility(View.VISIBLE)

    pendingCardsTextView.setText(emailsAndCellphones.toString)
  }

}
