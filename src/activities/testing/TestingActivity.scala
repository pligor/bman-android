package activities.testing

import android.os.Bundle
import com.pligor.bman.{TR, TypedViewHolder, R}
import components.MyActivity
import android.view.View
import android.view.View.OnClickListener
import android.support.v4.app._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class TestingActivity extends FragmentActivity with TypedViewHolder
with MyActivity {

  private lazy val testButton = findView(TR.testButton)

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_testing)

    testButton.setOnClickListener(new OnClickListener {
      def onClick(v: View): Unit = {
        //.......
      }
    })
  }
}
