package activities.peoplecards

import android.app.Activity
import com.pligor.bman.{TR, R, TypedViewHolder}
import components._
import android.os.Bundle
import models.{Card, RequestCode, PeopleCard}
import myandroid.AndroidHelper._
import android.view.View
import android.view.View.OnClickListener
import android.content.Intent
import myandroid.log
import myAndroidAPIs.BugsenseActivity
import activities.ZoomActivity
import android.support.v4.app.FragmentActivity
import adapters.page.PeopleCardPageAdapter
import android.support.v4.view.ViewPager.OnPageChangeListener
import fragments.PeopleCardFragment

object ContactCardsActivity {

  object EXTRA extends Enumeration {
    val PEOPLE_CARD_IDS = Value
  }

}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class ContactCardsActivity extends FragmentActivity with TypedViewHolder
with MyActivity

with AdmobActivity
with SendStatusTrait

with SwipeHelperDialogTrait
with BugsenseActivity {
  private lazy val topPanelTextView = findView(TR.topPanelTextView)

  private lazy val contactListImageButton = findView(TR.contactListImageButton)

  //private lazy val cardWidgetLayout = findViewById(R.id.cardWidgetLayout).asInstanceOf[CardWidgetLayout]

  private lazy val peopleCardsViewpager = findView(TR.peopleCardsViewpager)

  private lazy val peopleCardPageAdapter = new PeopleCardPageAdapter(getSupportFragmentManager)

  private lazy val selectedPeopleCardsIdsOption = Option(
    getIntent.getLongArrayExtra(ContactCardsActivity.EXTRA.PEOPLE_CARD_IDS.toString)
  ).map(_.toBuffer)

  /**
   * weird things are valid for the double linked list
   * next if does not exist returns an empty list
   * prev if does not exist returns null
   */
  //private var peopleCardsIdsOption: Option[mutable.DoubleLinkedList[Long]] = None

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_contact_cards_normal)

    contactListImageButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        onBackPressed()
      }
    })

    //resetPeopleCardsIdsDoubleLinkedList()
  }

  private var isStarted = false

  override def onStart(): Unit = {
    super.onStart()

    //because of implementation even if LastPeopleCardPreference is cleared (invalid id) we will
    // still get the initial card
    reload(
      onPeopleCardFound = renderCards,
      forcedIdOption = Some(LastPeopleCardPreference.getValue)
    )

    /*if (!cardWidgetLayout.init(renderCard(peopleCard))) {
          renderCard(peopleCard)
        }*/

    isStarted = true
  }

  override def onBackPressed(): Unit = {
    super.onBackPressed()

    LastPeopleCardPreference.reset
  }

  override def onStop(): Unit = {
    super.onStop()

    isStarted = false
  }

  private def reload(onPeopleCardFound: (PeopleCard) => Unit,
                     forcedIdOption: Option[Long] = None): Unit = {

    def getInitialPeopleCard = if (selectedPeopleCardsIdsOption.isDefined) {
      var cardOption: Option[PeopleCard] = None

      while({
        val headOption = selectedPeopleCardsIdsOption.get.headOption

        if(headOption.isDefined) {
          val head = headOption.get

          cardOption = PeopleCard.getById(head)

          if(cardOption.isDefined) {
            false
          } else {
            selectedPeopleCardsIdsOption.get -= head

            true
          }
        } else {
          false
        }
      }) {}

      cardOption
    } else {
      PeopleCard.getTop
    }

    val peopleCardOption = if (forcedIdOption.isDefined) {
      val cardOption = PeopleCard.getById(forcedIdOption.get)

      if (cardOption.isDefined) {
        /*if(peopleCardsIdsOption.isDefined) {
          repositionPeopleCardsIds(cardOption.get.id.get)
        } else {
          //nop
        }*/

        cardOption
      } else {
        //resetPeopleCardsIdsDoubleLinkedList() //this is safe
        getInitialPeopleCard
      }
    } else {
      getInitialPeopleCard
    }

    if (peopleCardOption.isDefined) {
      onPeopleCardFound(peopleCardOption.get)
    } else {
      showToast(R.string.no_cards)

      finish()
    }
  }

  private def getFragments = {
    val peopleCardIds = selectedPeopleCardsIdsOption.getOrElse(PeopleCard.getAllIds())

    peopleCardIds.map(cardId => PeopleCardFragment.newInstance(cardId))
  }

  /*private def repositionPeopleCardsIds(targetId: Long): Unit = {
    require(peopleCardsIdsOption.isDefined)

    resetPeopleCardsIdsDoubleLinkedList()

    while ( {
      if (peopleCardsIdsOption.get.head == targetId) {
        false
      } else {
        val nextList = peopleCardsIdsOption.get.next

        assert(!nextList.isEmpty,
          "we have iterated through the entire list and still we have not found it")

        peopleCardsIdsOption = Some(nextList)

        true
      }
    }) {}
  }*/

  private def renderCards(initialPeopleCard: PeopleCard): Unit = {
    peopleCardPageAdapter.setFragments(getFragments)

    peopleCardsViewpager.setAdapter(peopleCardPageAdapter)

    peopleCardPageAdapter.getPosition(initialPeopleCard.id.get).map(peopleCardsViewpager.setCurrentItem)

    peopleCardsViewpager.setOnPageChangeListener(new OnPageChangeListener {
      def onPageScrollStateChanged(state: Int): Unit = {
        //nop
      }

      def onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int): Unit = {
        //nop
      }

      def onPageSelected(position: Int) = {
        val selectedPeopleCardId = peopleCardPageAdapter.getItem(position).asInstanceOf[PeopleCardFragment].getCardId

        LastPeopleCardPreference.setValue(selectedPeopleCardId)

        PeopleCard.getById(selectedPeopleCardId).map(
          peopleCard => topPanelTextView.setText(peopleCard.senderName)
        )
      }
    })
  }

  /*private def renderCard(peopleCard: PeopleCard): Unit = {
    cardWidgetLayout.setOnLeftOnRight(
      onLeft = () => {
        val nextCardOption = if (peopleCardsIdsOption.isDefined) {
          //remember a peculiar thing about double linked list:
          // next if does not exist returns an empty list
          assert(!peopleCardsIdsOption.get.isEmpty,
            "cannot be empty since we are asking for next of.. something")

          val nextList = peopleCardsIdsOption.get.next

          if (nextList.isEmpty) {
            None
          } else {
            peopleCardsIdsOption = Some(nextList)

            peopleCardsIdsOption.get.headOption.flatMap(PeopleCard.getById)
          }
        } else {
          peopleCard.getNext
        }

        if (nextCardOption.isDefined) {
          renderCard(nextCardOption.get)
        } else {
          Toast.makeText(this, R.string.no_next_card, Toast.LENGTH_SHORT).show()
        }
      }, onRight = () => {
        val previousCardOption = if (peopleCardsIdsOption.isDefined) {
          //remember another peculiar thing about double linked list:
          // prev if does not exist returns null
          val prevOption = Option(peopleCardsIdsOption.get.prev)

          prevOption.flatMap {
            prevList =>
              peopleCardsIdsOption = prevOption

              PeopleCard.getById(prevList.head)
          }
        } else {
          peopleCard.getPrev
        }

        if (previousCardOption.isDefined) {
          renderCard(previousCardOption.get)
        } else {
          Toast.makeText(this, R.string.no_previous_card, Toast.LENGTH_SHORT).show()
        }
      }
    )
  }*/

  override def onActivityResult(requestCode: Int, resultCode: Int, data: Intent): Unit = {
    super.onActivityResult(requestCode, resultCode, data)

    log log "we got the result from the activity"

    RequestCode(requestCode) match {
      case RequestCode.CRUD =>
        if (resultCode == Activity.RESULT_CANCELED) {
          //this means the card is deleted
          if (isStarted) {
            log log "reloading card"

            reload(renderCards)
          } else {
            //it is about to start so the code will be taken care in there
          }
        } else {
          //no need to do anything because other than deletion no other edit is possible on people cards
        }

      case RequestCode.ZOOM =>
        val cardId = data.getLongExtra(ZoomActivity.OUT_EXTRA.CARD_ID.toString, Card.invalidId)

        log log "after zoom card id is: " + cardId

      /*
      BECAUSE OF LastPeopleCardPreference this is unnecessary
      //reload forced card id as soon as the activity is started
      new ScalaAsyncTask[Int, AnyRef, Unit] {
        protected def doInBackground(period: Int): Unit = {
          while (isStopped) {
            Thread.sleep(period)
          }
        }

        override def onPostExecute(result: Unit): Unit = {
          super.onPostExecute(result)

          reload(renderCard, {
            if (cardId == Card.invalidId) None
            else Some(cardId)
          })
        }
      }.execute(periodSleepAfterZoomMsecs)*/

    }
  }
}
