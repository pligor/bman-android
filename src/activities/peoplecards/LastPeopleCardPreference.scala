package activities.peoplecards

import android.content.Context
import models.PeopleCard
import components.MySecurePreference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case object LastPeopleCardPreference extends MySecurePreference[Long, Long] {
  //val preferenceKey = CurrentDimensionPreference.toString;
  //for some bizarre reason, above line causes a recursive functions which leads to StackOverflowError
  val preferenceKey = "LastPeopleCardPreference"

  val defaultValue: Long = PeopleCard.invalidId

  def getValue(implicit context: Context): Long = getInnerValue.asInstanceOf[Long]

  def setValue(newValue: Long)(implicit context: Context): Boolean = {
    setInnerValue(newValue)
  }
}
