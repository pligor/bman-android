package activities.blank

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait Refresher {
  def doRefresh(): Unit
}
