package activities.blank

import android.app.Activity
import android.os.Bundle
import preferences.HomeScreenOnStartupPreference
import android.content.{DialogInterface, Intent}
import activities.mycards.MyCardsActivity
import myAndroidAPIs.BugsenseActivity
import components.{DisplayNameTrait, MyActivity}
import com.pligor.bman.R
import activities.HomeActivity
import models.DatabaseHandler
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import myandroid.AndroidHelper._
import myauth.AuthActionable
import android.content.DialogInterface.OnCancelListener

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class BlankActivity extends Activity with MyActivity

with DisplayNameTrait
with AuthActionable

with TrialRefresher //this depends on trial refresher of whether is going to be executed or not
with PreTrialRefresher //this is executed

with SubscriptionRefresher //this is going to be executed either way

with BugsenseActivity {
  protected def isProgressDialogShown: Boolean = false

  /**
   * remember: if finish() is inside onCreate, onStart is never called
   */
  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_blank)
  }

  override def onStart(): Unit = {
    super.onStart()

    val dbDidNotExist = !DatabaseHandler.getInstance.getDatabaseFile.exists()

    doRefresh()

    def onNewName(newName: String, onAfter: => Unit): Unit = {
      if (dbDidNotExist) {
        future {
          new DatabaseHandler().populate

          onUI {
            onAfter //actually dismissing the dialog here

            showSignDialogBeforeProceed()
          }
        }
      } else {
        onAfter //actually dismissing the dialog here

        showSignDialogBeforeProceed()
      }

      {} //just to not have this annoying lines from intellij
    }

    if (setDisplayNameIfEmpty(onNewName)) {
      //if was empty will be executed later after new name is set
    } else {
      proceedToNextActivity()
    }
  }

  override def onBackPressed(): Unit = {
    //super.onBackPressed()
  }

  def showSignDialogBeforeProceed(): Unit = {
    val dialog = getSignDialog(token => proceedToNextActivity())

    dialog.setOnCancelListener(new OnCancelListener {
      def onCancel(dialogInterface: DialogInterface): Unit = {
        saveEmailCellphone(dialog)

        proceedToNextActivity()
      }
    })

    dialog.show()
  }

  private def proceedToNextActivity(): Unit = {
    startActivity(new Intent(this,
      if (HomeScreenOnStartupPreference.getValue)
        classOf[HomeActivity]
      else
        classOf[MyCardsActivity]
    ))

    //no finish necessary because we have set no history = true in manifest
  }
}
