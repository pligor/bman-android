package activities.blank

import com.pligor.bman.{Bman, R}
import preferences.{FortumoSubscriptionPreference, FortumoDeviceReceiptPreference, InAppUnmanagedSubscriptionPreference}
import myandroid.{MyContextWrapper, log}
import myInAppGooglePurchase.{InAppBillingTrait, PurchaseState}
import components.SimpleJsonRequestTrait
import spray.json._
import models.{FortumoJsonResponse, SubscriptionProduct}
import org.apache.http.HttpStatus
import com.bugsense.trace.BugSenseHandler
import java.net.URL
import myfortumo.FortumoJsonRequest

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait SubscriptionRefresher extends SimpleJsonRequestTrait
with MyContextWrapper
with InAppBillingTrait
with Refresher {

  private val url = new URL(Bman.SERVER_LINK + "/subscription/getFortumoTimestamp")

  private val connTimeout_msec = 5000

  private val readTimeout_msec = 5000

  protected def onAfterSubscriptionCheck(): Unit = {
    terminateInAppBillingService
  }

  protected def onAfterInAppBillingServiceIsConnected(): Unit = {

    checkInAppPurchase({
      if (InAppUnmanagedSubscriptionPreference.isSubscribed) {
        log log "in app is subscribed"
        onAfterSubscriptionCheck()
      } else {

        def afterConsumeAttempt(): Unit = {
          checkFortumo({
            log log "we did everything we wanted to do so it is ok either if fortumo is subscribed or not"
            onAfterSubscriptionCheck()
          })
        }

        consumePurchase(SubscriptionProduct.productId, SubscriptionProduct.PURCHASE_TYPE)(
          onConsumeSuccess = {
            log log "consume is successful. nothing to show to the user here"
            afterConsumeAttempt()
          },
          onConsumeError = {
            errorCode =>
              log log "consume error: " + errorCode.toString
              afterConsumeAttempt()
          },
          onNoPurchasedItemFound = {
            log log "there is no related item"
            afterConsumeAttempt()
          }
        )

      }
    })

    log log "executed subscription refresher task"
  }


  abstract override def doRefresh(): Unit = {
    super.doRefresh()
    log log "inside subscription refresher"

    bindInAppBillingService(onInAppBillingServiceConnectionFailure = {
      checkFortumo({
        log log "we did everything we wanted to do so it is ok"
        onAfterSubscriptionCheck()
      })
    })
  }

  //we have chosen to let it run in the background until is finished even if it means that
  //next activity will not have catch up
  //private var fortumoTask: Option[SimpleJsonRequestTask] = None

  private class FortumoSubscriptionRefreshException(msg: String) extends Exception(msg)

  private def checkFortumo(op: => Unit): Unit = {

    val fortumoDeviceReceiptOption = FortumoDeviceReceiptPreference.getValue
    //execute only if we have a device receipt in hand, otherwise we cannot do it
    if (fortumoDeviceReceiptOption.isDefined) {

      val fortumoJsonRequest = fortumoDeviceReceiptOption.get.getFortumoJsonRequest(
        fortumoServiceId = SubscriptionProduct.FORTUMO_SERVICE_ID,
        productName = SubscriptionProduct.productId
      )

      val fortumoTask = Some(
        new SimpleJsonRequestTask(
          R.string.server_communication_progress_dialog_title,
          R.string.server_communication_progress_dialog_message
        )({
          jsonResponseOption =>
            if (jsonResponseOption.isDefined) {
              val jsonResponse = jsonResponseOption.get

              jsonResponse.code match {
                case HttpStatus.SC_BAD_REQUEST =>
                  log log "fortumo check bad request"
                  BugSenseHandler.sendException(
                    new FortumoSubscriptionRefreshException("BAD_REQUEST")
                  )

                  op

                case HttpStatus.SC_EXPECTATION_FAILED =>
                  log log "fortumo check expectation failed"
                  BugSenseHandler.sendException(
                    new FortumoSubscriptionRefreshException("EXPECTATION_FAILED")
                  )

                  op

                case HttpStatus.SC_NO_CONTENT =>
                  log log "did not find any fortumo receipt on the server"
                  FortumoSubscriptionPreference.clear

                  op

                case HttpStatus.SC_OK =>
                  log log "the fortumo response body is: " + jsonResponse.body

                  val fortumoJsonResponseOption = try {
                    Some(JsonParser(jsonResponse.body).convertTo[FortumoJsonResponse](FortumoJsonResponse.itsJsonFormat))
                  } catch {
                    case e: org.parboiled.errors.ParsingException => None
                  }

                  if (fortumoJsonResponseOption.isDefined) {
                    log log "got a valid fortumo json receipt"
                    FortumoSubscriptionPreference.setValue(
                      fortumoJsonResponseOption.map(_.fortumo_receipt_received.getTime)
                    )

                    op
                  } else {
                    log log "got an unknown json response, leave everything as it is"
                    BugSenseHandler.sendException(
                      new FortumoSubscriptionRefreshException("UNKNOWN_JSON_RESPONSE")
                    )

                    op
                  }

                case _ =>
                  log log "fortumo check unexpected server response"
                  BugSenseHandler.sendException(
                    new FortumoSubscriptionRefreshException("UNEXPECTED_SERVER_RESPONSE")
                  )

                  op
              }
            } else {
              log log "on server communication error do not do anything"
              op
            }
        })
      )

      fortumoTask.get.execute(
        JsonRequestParam(
          url,
          fortumoJsonRequest.toJson(FortumoJsonRequest.itsJsonFormat),
          connTimeout_msec = connTimeout_msec,
          readTimeout_msec = readTimeout_msec
        )
      )
    } else {
      log log "device receipt is not available"
      op
    }
  }

  /**
   * @param op if none we do not know, else true if purchased or false otherwise
   */
  private def checkInAppPurchase(op: => Unit): Unit = {
    queryPurchasedItems(SubscriptionProduct.PURCHASE_TYPE)(
      onSuccess = {
        (ownedSkus, purchaseDatas) => {
          log log "owned skus: " + (if (ownedSkus.isEmpty) "empty" else ownedSkus.mkString(", "))

          val purchaseDataOption = purchaseDatas.find(pd =>
            pd.productId == SubscriptionProduct.productId && pd.packageName == getPackageName
          )

          if (purchaseDataOption.isDefined) {
            val purchaseData = purchaseDataOption.get

            //find out about that. what does it produce? and also the lines above
            log log "PurchaseState: " + PurchaseState(purchaseData.purchaseState.toInt)

            InAppUnmanagedSubscriptionPreference.setValue(Some(purchaseData.purchaseTime))
          } else {
            //the item id is verified that is not purchased
            //if we have it already cleared then we are in agreement
            //else if it has a value then we must clear it
            //let's clear it either way
            InAppUnmanagedSubscriptionPreference.clear
          }

          op
        }
      },
      onError = {
        responseCode =>
          log log "do nothing if it is empty, leave it empty, if has timestamp, no need to do anything"

          op
      }
    )
  }
}
