package activities.blank

import components.MyActivity
import android.os.Bundle
import preferences.PreTrialPreference
import scalaj.http.{HttpException, HttpOptions, Http}
import myandroid.{MyContextWrapper, ScalaAsyncTask, log}
import java.net.URL
import com.pligor.bman.Bman
import org.apache.http.HttpStatus

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait PreTrialRefresher extends MyContextWrapper with Refresher {
  //concrete

  private val secret = "8Hl65P8TF1i9677mu637OX96wvukESA7zd804t7181K0WSlP1B2I5y824BT3uv8jIx5b51s5Zy404yQa"

  private val connTimeout_msec = 5000

  private val readTimeout_msec = 4000

  private val url = new URL(Bman.SERVER_LINK + "/trial/isPre")

  abstract override def doRefresh(): Unit = {
    log log "inside pre trial refresher"

    if (PreTrialPreference.getValue) {
      //do not let the trial trait execute its do refresh
    } else {
      super.doRefresh()
    }

    //check server to make sure either way
    new isPreTrialTask().execute(secret)
  }

  private class isPreTrialTask extends ScalaAsyncTask[String, AnyRef, Option[Boolean]] {
    protected def doInBackground(param: String): Option[Boolean] = {
      def check(code: Int, body: String): Option[Boolean] = {
        if (code == HttpStatus.SC_OK) Some(body == param)
        else None
      }

      try {
        val request = Http.get(url.toString).
          option(HttpOptions.connTimeout(connTimeout_msec)).
          option(HttpOptions.readTimeout(readTimeout_msec))

        val (responseCode, headersMap, resultString) = request.asHeadersAndParse(Http.readString)

        log log "pre trial response code: " + responseCode

        check(responseCode, resultString)
      } catch {
        case e: HttpException => check(e.code, e.body)
        case e: Exception => {
          log log "an unknown exception in pre trial refresher"
          None
        }
      }
    }

    override def onPostExecute(isPreTrialOption: Option[Boolean]): Unit = {
      super.onPostExecute(isPreTrialOption)

      val previousIsPre = PreTrialPreference.getValue

      if (isPreTrialOption.isDefined) {
        PreTrialPreference.setValue(isPreTrialOption.get)
      } else {
        //dont know, leave it as it is
        log log "do not have a valid response, just leave it as it is"
      }

      //if trial refresher was not executed, must be executed now
      if (previousIsPre && !PreTrialPreference.getValue) {
        PreTrialRefresher.super.doRefresh()
      }
    }
  }

}
