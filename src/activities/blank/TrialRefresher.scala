package activities.blank

import java.net.URL
import com.pligor.bman.{R, Bman}
import preferences.FirstRunTimestampPreference
import myandroid.log
import models.Trial
import spray.json._
import components.{SimpleJsonRequestTrait, MyActivity}
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
protected trait TrialRefresher extends MyActivity with SimpleJsonRequestTrait with Refresher {

  private var task: Option[SimpleJsonRequestTask] = None;

  private val url = new URL(Bman.SERVER_LINK + "/trial/set");

  //we dont want to consume resources for this connection. Either succeed with good connection or fail
  private val connTimeout_msec = 3000;
  private val readTimeout_msec = 3 * connTimeout_msec;


  /*abstract override */ def doRefresh(): Unit = {
    //super.doRefresh()
    log log "inside trial refresher"

    log log "try to finish it in case it is already running";
    task.map(_.cancel({
      val mayInterruptIfRunning = true;
      mayInterruptIfRunning;
    }));

    val isTimestampEmpty = FirstRunTimestampPreference.isEmpty;

    val trialOption = Trial.getOption(FirstRunTimestampPreference.getValue);

    def onNoTimestampFromServer(): Unit = {
      if (isTimestampEmpty) {
        FirstRunTimestampPreference.initialize
      } else {
        log log "do nothing if we already have set a timestamp"
      }
    }

    if (trialOption.isDefined) {
      task = Some(new SimpleJsonRequestTask(
        R.string.server_communication_progress_dialog_title,
        R.string.server_communication_progress_dialog_message
      )({
        jsonResponseOption =>
          if (jsonResponseOption.isDefined) {
            val jsonResponse = jsonResponseOption.get;

            log log "json response: " + jsonResponse.body;

            val timestampFromServerOption = try {
              JsonParser(jsonResponse.body).
                asJsObject.getFields("registered") match {
                case Seq(JsNumber(registered)) => Some(registered.toLongExact);
                case _ => None;
              }
            } catch {
              case e: org.parboiled.errors.ParsingException => None;
            }

            if (timestampFromServerOption.isDefined) {
              FirstRunTimestampPreference.setValue(timestampFromServerOption);
            } else {
              onNoTimestampFromServer();
            }
          } else {
            log log "error connecting with the server when trying to get the timestamp";
            onNoTimestampFromServer();
          }
      }));

      task.get.execute(
        JsonRequestParam(
          url,
          trialOption.get.toJson(Trial.TrialJsonProtocol.itsJsonFormat),
          connTimeout_msec,
          readTimeout_msec
        )
      );
      log log "executed trial refresher task";
    } else {
      log log "we are doomed (at least for now), we have no address, so act as if the device cannot be identified";
      onNoTimestampFromServer();
    }
  }

  //DO NOT STOP THE TASK onStop. BY DEFAULT THE TASK IS LEFT TO RUN ON THE BACKGROUND

}
