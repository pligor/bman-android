package views

import android.content.Context
import android.graphics.{Camera, Matrix}
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator
import android.view.animation.Transformation
import android.widget.ImageView
import android.view.animation.Animation.AnimationListener
import models.{SoundWrapper, Dimension, Card}
import components.{MySwipeListener, ImageSizeDp}
import scala.concurrent.duration._
import myandroid.log
import preferences.SoundPreference

trait ToggleFlipper {
  protected def toggleFlip(animated: Boolean, isRotationReversed: Boolean)(implicit context: Context): Unit
}

trait ToggleSound extends ToggleFlipper {
  //this: MyFlipImageView =>

  abstract override protected def toggleFlip(animated: Boolean, isRotationReversed: Boolean)(implicit context: Context): Unit = {
    super.toggleFlip(animated, isRotationReversed)

    if (SoundPreference.getValue) {
      SoundWrapper().quickSlideSound.play() //this could be synchronous or asynchronous

      log log "sound is playing"
    } else{
      log log "sound is not playing"
    }
  }
}

/**
 * Animation part All credits goes to coomar
 */
class MyFlipAnimator(isRotationXEnabled: Boolean,
                     isRotationYEnabled: Boolean,
                     isRotationZEnabled: Boolean,
                     imageView: ImageView,
                     drawImageView: (BitmapHolder, ImageSizeDp, ImageView) => Unit) extends Animation {

  setFillAfter(true)

  def setToDrawable(bitmapHolder: BitmapHolder,
                    targetSize: ImageSizeDp,
                    rotationReversed: Boolean): Unit = {
    toDrawable = bitmapHolder

    size = targetSize

    isRotationReversed = rotationReversed

    visibilitySwapped = false
  }

  override def initialize(width: Int, height: Int, parentWidth: Int, parentHeight: Int): Unit = {
    super.initialize(width, height, parentWidth, parentHeight)

    camera = new Camera

    this.centerX = width / 2

    this.centerY = height / 2
  }

  protected override def applyTransformation(interpolatedTime: Float, t: Transformation): Unit = {

    val radians: Double = Math.PI * interpolatedTime

    var degrees: Float = (180.0 * radians / Math.PI).asInstanceOf[Float]

    if (isRotationReversed) {
      degrees = -degrees
    }

    if (interpolatedTime >= 0.5f) {
      if (isRotationReversed) {
        degrees += 180.0f
      }
      else {
        degrees -= 180.0f
      }

      if (!visibilitySwapped) {
        drawImageView(toDrawable, size, imageView)

        visibilitySwapped = true
      }
    }

    val matrix: Matrix = t.getMatrix

    camera.save()

    camera.translate(0.0f, 0.0f, (150.0 * Math.sin(radians)).asInstanceOf[Float])

    camera.rotateX(if (isRotationXEnabled) degrees else 0)

    camera.rotateY(if (isRotationYEnabled) degrees else 0)

    camera.rotateZ(if (isRotationZEnabled) degrees else 0)

    camera.getMatrix(matrix)

    camera.restore()

    matrix.preTranslate(-centerX, -centerY)

    matrix.postTranslate(centerX, centerY)
  }

  private var camera: Camera = null

  private var toDrawable: BitmapHolder = null

  private var size: ImageSizeDp = null

  private var centerX: Float = 0.0f

  private var centerY: Float = 0.0f

  private var visibilitySwapped: Boolean = false

  private var isRotationReversed: Boolean = false
}


object MyFlipImageView {

  trait OnFlipListener {
    def onFlipStart(view: MyFlipImageView): Unit

    def onFlipEnd(view: MyFlipImageView): Unit

    def onToggle(newDimension: Dimension.Value): Unit
  }

}

class MyFlipImageView(val imageView: ImageView,
                      val flipListener: Option[MyFlipImageView.OnFlipListener] = None,
                      //duration default is the same as the length of the sound
                      val animationDuration: Duration = 540.milliseconds)
                     (implicit context: Context) extends CardImageView
with ToggleFlipper {

  private final val defaultInterpolator: Interpolator = new DecelerateInterpolator

  private var frontSide: Option[BitmapHolder] = None

  private var backSide: Option[BitmapHolder] = None

  private var isFlipping: Boolean = false

  private def reset(): Unit = {
    frontSide = None

    backSide = None
  }

  def setCard(card: Card)(implicit context: Context): Unit = {
    reset()

    setCard(
      card,
      imageView, afterLoad = {
        imageHolder =>

          getCard.currentDimension match {
            case Dimension.frontSide => frontSide = Some(imageHolder)
            case Dimension.backSide => backSide = Some(imageHolder)
          }
      }
    )

    imageView.setOnTouchListener(mySwipeListener)
  }

  private final val defaultSwipeListener = new MySwipeListener(
    onUp = onSwipeUp,
    onDown = onSwipeDown
  )

  private var mySwipeListener = defaultSwipeListener

  def setOnLeftOnRight(onLeft: Option[() => Unit] = None,
                       onRight: Option[() => Unit] = None) = {
    mySwipeListener = new MySwipeListener(
      onUp = onSwipeUp,
      onDown = onSwipeDown,
      onLeft = onLeft.getOrElse(() => {}),
      onRight = onRight.getOrElse(() => {})
    )

    imageView.setOnTouchListener(mySwipeListener)
  }

  private def onSwipeUp(): Unit = toggleFlip(animated = true, isRotationReversed = false)

  private def onSwipeDown(): Unit = toggleFlip(animated = true, isRotationReversed = true)

  private val flipAnimation: MyFlipAnimator = new MyFlipAnimator(
    isRotationXEnabled = true,
    isRotationYEnabled = false,
    isRotationZEnabled = false,
    imageView = imageView,
    drawImageView = setDrawable
  )

  /**
   * @param isRotationReversed true matches swipe down, false matches swipe up
   */
  protected def toggleFlip(animated: Boolean, isRotationReversed: Boolean)(implicit context: Context): Unit = {
    val targetSize = {
      assert(wrapperSize.isInitialized,
        "wrapper size must be already initialized, before user attemps flipping")

      wrapperSize()()
    }

    val flipFunc = {
      def startAnimation(bitmapHolder: BitmapHolder): Unit = {
        flipAnimation.setToDrawable(
          bitmapHolder = bitmapHolder,
          targetSize = targetSize,
          rotationReversed = isRotationReversed
        )

        imageView.startAnimation(flipAnimation)
      }

      def withoutAnimation(bitmapHolder: BitmapHolder): Unit = {
        setDrawable(bitmapHolder, targetSize, imageView)
      }

      if (animated) startAnimation _ else withoutAnimation _
    }

    swapSides().get match {
      case Dimension.frontSide =>
        if (backSide.isDefined) {
          log log "back side is already loaded"

          flipFunc(backSide.get)
        } else {
          log log "back side is not loaded yet, so show progress bar"

          loadBackPhoto(imageView, afterBack = {
            backImageHolder =>
              backSide = Some(backImageHolder)
          })
        }

      case Dimension.backSide =>
        if (frontSide.isDefined) {
          log log "front side is already loaded"

          flipFunc(frontSide.get)
        } else {
          log log "front side is not loaded yet, so show progress bar"

          loadFrontPhoto(imageView, afterFront = {
            frontImageHolder =>
              frontSide = Some(frontImageHolder)
          })
        }
    }

    flipListener.map(_.onToggle(getCard.currentDimension))
  }

  {
    flipAnimation.setAnimationListener(new AnimationListener {
      def onAnimationStart(animation: Animation): Unit = {
        flipListener.map(_.onFlipStart(MyFlipImageView.this))

        isFlipping = true

        imageView.setOnTouchListener(null)
      }

      def onAnimationEnd(animation: Animation): Unit = {
        flipListener.map(_.onFlipEnd(MyFlipImageView.this))

        isFlipping = false

        imageView.setOnTouchListener(mySwipeListener)
      }

      def onAnimationRepeat(animation: Animation): Unit = {
        //nop
      }
    })

    flipAnimation.setInterpolator(
      /*try {
        val interpolatorResId: Int = typedArray.getResourceId(R.styleable.FlipImageView_flipInterpolator, 0)

        AnimationUtils.loadInterpolator(context, interpolatorResId)
      } catch {
        case e: Resources.NotFoundException => defaultInterpolator
      }*/
      defaultInterpolator)

    flipAnimation.setDuration(/*typedArray.getInt(
      R.styleable.FlipImageView_flipDuration,
      sDefaultDuration
    )*/
      animationDuration.toMillis
    )
  }

}