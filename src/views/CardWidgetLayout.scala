package views

import android.widget._
import android.content.{Intent, Context}
import android.util.AttributeSet
import com.pligor.bman.{TR, TypedViewHolder, Bman, R}
import android.view.{Window, View}
import models.{RequestCode, Card}
import myandroid.AndroidHelper._
import components.MetaDataParser
import android.app.{Activity, Dialog}
import android.view.View.OnClickListener
import adapters.ThinMetaDataAdapter
import activities.crud.{Crud, CardDetailActivity, CrudActivity}
import android.widget.AdapterView.OnItemClickListener
import activities.ZoomActivity
import myandroid.log
import activities.namelist.NameListActivity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * ON CREATE:
 * setInit
 * setOnLeftOnRight
 */
class CardWidgetLayout(context: Context, attrs: AttributeSet, defStyle: Int)
  extends RelativeLayout(context, attrs, defStyle) with TypedViewHolder {

  def this(context: Context, attrs: AttributeSet) = this(
    context,
    attrs,
    Views.defaultViewStyle
  )

  /**
   * http://stackoverflow.com/questions/13824884/how-to-extend-imageview-in-an-android-scala-app/13845719
   */
  def this(context: Context) = this(
    context,
    getAttributeSetFromXml(R.xml.default_card_widget_layout_attrs, "CardWidgetLayout", context.getResources)
  )

  private lazy val cardWidgetImageWrapper = findView(TR.cardWidgetImageWrapper)

  private val limitMetaData = 5

  private lazy val widgetCardOperationsButton = findView(TR.widgetCardOperationsButton)

  private lazy val widgetSendButton = findView(TR.widgetSendButton)

  private lazy val widgetZoomButton = findView(TR.widgetZoomButton)

  private lazy val widgetFlipImageView = new MyFlipImageView(
    findView(TR.widgetImageView)
  )(context) with ToggleSound

  def setOnLeftOnRight(onLeft: () => Unit, onRight: () => Unit): Unit = {
    widgetFlipImageView.setOnLeftOnRight(Some(onLeft), Some(onRight))
  }

  def setCard(card: Card): Unit = {
    val isAllowedToBeSent = card.isAllowedToBeSent && Bman.isPremium(context)

    log log ("setting the send button to be " + (if (isAllowedToBeSent) "enabled" else "disabled"))

    widgetSendButton.setEnabled(isAllowedToBeSent)

    widgetFlipImageView.setCard(card)(context)
  }

  /*override def onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
    super.onSizeChanged(w, h, oldw, oldh);
    if (w > 0 && h > 0 && w != oldw && h != oldh) {
      //on positive change

    }
  }*/

  /**
   * @return whether on after init is about to be executed or not
   */
  def init(onAfterInit: => Unit): Boolean = {
    widgetCardOperationsButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        assert(widgetFlipImageView.hasCard)

        getCardOperationsDialog.show()
      }
    })

    widgetZoomButton.setOnClickListener(new OnClickListener {
      def onClick(p1: View): Unit = {
        assert(widgetFlipImageView.hasCard)

        val card = widgetFlipImageView.getCard

        card.getCurrentPhoto(context).map {
          currentPhoto =>
            context.asInstanceOf[Activity].startActivityForResult(new Intent(context, classOf[ZoomActivity]).
              putExtra(ZoomActivity.EXTRA.PHOTO_ID.toString, currentPhoto.id.get).
              putExtra(ZoomActivity.EXTRA.CARD_ID.toString, card.id.get),
              RequestCode.ZOOM.id
            )
        }
      }
    })

    widgetSendButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        assert(widgetFlipImageView.hasCard)

        val cardId = widgetFlipImageView.getCard.id.get

        context.startActivity(new Intent(context, classOf[NameListActivity]).putExtra(
          NameListActivity.EXTRA.CARD_ID.toString, cardId
        ))
      }
    })

    widgetFlipImageView.safesetOnceWrapperSize(cardWidgetImageWrapper)(onAfterInit)(context)
  }

  private def getCardOperationsDialog = {
    val dialog = new Dialog(context) with TypedViewHolder

    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.getWindow.setBackgroundDrawableResource(R.drawable.dialog_background)
    dialog.setCanceledOnTouchOutside(false)

    dialog.setCancelable(true)

    dialog.setContentView(R.layout.dialog_card_operations)

    val moreCardOperationButton = dialog.findView(TR.moreCardOperationButton)

    moreCardOperationButton.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        val card = widgetFlipImageView.getCard

        val intentData = (new Intent).putExtra(
          Crud.InputExtra.MODEL_ID.toString, card.id.get
        ).putExtra(
          Crud.InputExtra.CARD_TYPE.toString, card.getType.id
        ).putExtra(
          Crud.InputExtra.VIEW_ID.toString, getId
        )

        val targetClass = card.getType match {
          case Card.Type.MINE => classOf[CrudActivity]
          case Card.Type.PEOPLE => classOf[CardDetailActivity]
        }

        val intent = new Intent(context, targetClass).putExtras(intentData)

        log log "we are starting activity for result"

        context.asInstanceOf[Activity].startActivityForResult(intent, RequestCode.CRUD.id)

        dialog.dismiss()
      }
    })

    {
      val metaCardOperationListView = dialog.findView(TR.metaCardOperationListView)

      val card = widgetFlipImageView.getCard

      val thinMetaDataAdapter = new ThinMetaDataAdapter(card.getType)(context).fillFromCard(
        card,
        limitMetaData
      )

      metaCardOperationListView.setAdapter(thinMetaDataAdapter)

      metaCardOperationListView.setOnItemClickListener(new OnItemClickListener {
        def onItemClick(adapterView: AdapterView[_], view: View, position: Int, id: Long): Unit = {
          val metaValue = thinMetaDataAdapter.getItem(position)

          MetaDataParser.execute(metaValue.dataValue)(context)

          dialog.dismiss()
        }
      })
    }

    dialog
  }
}
