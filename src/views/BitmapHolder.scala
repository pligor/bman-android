package views

import components.ImageSizePx
import android.graphics.drawable.BitmapDrawable

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case class BitmapHolder(imageSizePx: ImageSizePx, bitmapDrawable: BitmapDrawable) {

}
