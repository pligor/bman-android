package views

import components.{ImageSizePx, ImageSizeDp, WriteOnce}
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.{ViewGroup, View}
import android.content.Context
import android.graphics.Bitmap
import android.widget.{ProgressBar, ImageView}
import models.Photo
import asyncbitmap.ImageLoader
import myandroid.Pixel

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
//extends ImageView
trait WrappedImage {
  /**
   * plays double role
   * 1) is used to downsample the bitmap in order to not overload memory with large images
   * 2) is used to calculate the actual size of the image view
   */
  protected val wrapperSize = new WriteOnce[() => ImageSizeDp]

  object safesetOnceWrapperSize {
    private val globalLayoutListener = new WriteOnce[OnGlobalLayoutListener]

    def apply(view: View)(onAfterSet: => Unit)(implicit context: Context): Boolean = {
      val check = !globalLayoutListener.isInitialized

      if (check) {
        globalLayoutListener setValue new OnGlobalLayoutListener {
          def onGlobalLayout(): Unit = {
            // Ensure you call it only once :
            view.getViewTreeObserver.removeGlobalOnLayoutListener(this)

            safesetOnceWrapperSize(() => {
              ImageSizeDp(view)(context)
            })

            onAfterSet
          }
        }

        view.getViewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener())
      }

      check
    }
  }

  def safesetOnceWrapperSize(size: () => ImageSizeDp): Unit = {
    if (wrapperSize.isInitialized) {
      //assert(size == wrapperSize(), "you can only set it once, therefore you must set the same size if you want to");
    } else {
      //setOnceWrapperSize(size);
      wrapperSize.setValue(size)
    }
  }

  protected def getImageSizePx(bitmap: Bitmap) = ImageSizePx(width = Pixel(bitmap.getWidth), height = Pixel(bitmap.getHeight))

  protected def setDrawable(bitmapHolder: BitmapHolder,
                            targetSize: ImageSizeDp,
                            imageView: ImageView)(implicit context: Context): Unit = {
    resizeImageView(imageView, targetSize, imageSize = bitmapHolder.imageSizePx).setImageDrawable(bitmapHolder.bitmapDrawable)
  }

  protected def setBitmap(bitmap: Bitmap,
                          targetSize: ImageSizeDp,
                          imageView: ImageView)(implicit context: Context): Unit = {

    resizeImageView(imageView, targetSize, imageSize = getImageSizePx(bitmap)).setImageBitmap(bitmap)
  }

  private def resizeImageView(imageView: ImageView,
                              targetSize: ImageSizeDp,
                              imageSize: ImageSizePx)(implicit context: Context) = {
    //log.log("card view width: " + finalSize.width.value + " and height: " + finalSize.height.value)

    val finalSize = imageSize.fitIfLarger(targetSize.toPx)

    imageView.getLayoutParams.width = finalSize.width.value

    imageView.getLayoutParams.height = finalSize.height.value

    imageView
  }

  /*def setPhoto(photo: Photo, imgView: ImageView)(implicit context: Context) = {
    setPhoto(
      photo,
      imgView,
      preOp = {
        imageView =>
          showProgressBar(imageView)
      },
      postOp = {
        (imageView, bitmap) => {
          hideProgressBar(imageView)

          //set bitmap at last!
          setBitmap(bitmap, wrapperSize()(), imageView)
        }
      }
    )

    this
  }*/

  def setPhoto(photo: Photo,
               imgView: ImageView,
               preOp: ImageView => Unit,
               postOp: (ImageView, Bitmap) => Unit)(implicit context: Context) = {
    require(Option(photo).isDefined)

    require(wrapperSize.isInitialized)

    ImageLoader.loadImage(
      photo,
      imgView,
      param = (wrapperSize()(), context),
      preOp = preOp,
      postOp = postOp
    )

    this
  }

  protected def getProgressBar(view: View): ProgressBar = {
    val viewGroup = view.getParent.asInstanceOf[ViewGroup]

    val childCount = viewGroup.getChildCount

    val indexFound = (0 to childCount).find(index => viewGroup.getChildAt(index).isInstanceOf[ProgressBar])

    assert(indexFound.isDefined)

    viewGroup.getChildAt(indexFound.get).asInstanceOf[ProgressBar]
  }

  protected def showProgressBar(view: ImageView): Unit = {
    //view.requestLayout()

    view.setVisibility(View.GONE)

    getProgressBar(view).setVisibility(View.VISIBLE)
  }


  protected def hideProgressBar(view: ImageView): Unit = {
    getProgressBar(view).setVisibility(View.GONE)

    //view.requestLayout()

    view.setVisibility(View.VISIBLE)
  }
}
