package views

import android.content.Context
import android.util.AttributeSet
import android.widget.{ProgressBar, ImageView}
import myandroid.AndroidHelper._
import com.pligor.bman.R
import components._
import android.graphics.Bitmap
import components.ImageSizePx
import components.ImageSizeDp
import models.Photo
import android.view.{View, ViewGroup}
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import asyncbitmap.ImageLoader
import myandroid.Pixel

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * ON CREATE (before the user actually sees it, before it is being rendered):
 * setOnceWrapperSize
 * setPhoto
 */
class WrappedImageView(context: Context, attrs: AttributeSet, defStyle: Int)
  extends ImageView(context, attrs, defStyle) {

  def this(context: Context, attrs: AttributeSet) = this(
    context,
    attrs,
    Views.defaultViewStyle
  )

  /**
   * http://stackoverflow.com/questions/13824884/how-to-extend-imageview-in-an-android-scala-app/13845719
   */
  def this(context: Context) = this(
    context,
    getAttributeSetFromXml(R.xml.default_wrapped_image_view_attrs, "WrappedImageView", context.getResources)
  )

  object safesetOnceWrapperSize {
    private val globalLayoutListener = new WriteOnce[OnGlobalLayoutListener]

    def apply(view: View)(onAfterSet: => Unit): Boolean = {
      val check = !globalLayoutListener.isInitialized

      if (check) {
        globalLayoutListener setValue new OnGlobalLayoutListener {
          def onGlobalLayout(): Unit = {
            // Ensure you call it only once :
            view.getViewTreeObserver.removeGlobalOnLayoutListener(this)

            safesetOnceWrapperSize(() => {
              ImageSizeDp(view)(context)
            })

            onAfterSet
          }
        }

        view.getViewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener())
      }

      check
    }
  }

  def safesetOnceWrapperSize(size: () => ImageSizeDp): Unit = {
    if (wrapperSize.isInitialized) {
      //assert(size == wrapperSize(), "you can only set it once, therefore you must set the same size if you want to");
    } else {
      //setOnceWrapperSize(size);
      wrapperSize.setValue(size)
    }
  }

  def setPhoto(photo: Photo) = {
    require(Option(photo).isDefined)

    require(wrapperSize.isInitialized)

    ImageLoader.loadImage(
      photo,
      this,
      param = (wrapperSize()(), context),
      preOp = {
        imageView =>
          imageView.setVisibility(View.GONE)
          //curImageView.getParent.asInstanceOf[ViewGroup].addView(progressBarView, getLayoutParams);
          getProgressBar(imageView).setVisibility(View.VISIBLE)
      },
      postOp = {
        (imageView, bitmap) => {
          getProgressBar(imageView).setVisibility(View.GONE)

          //make it visible
          imageView.setVisibility(View.VISIBLE)

          //set bitmap at last!
          setBitmap(bitmap, wrapperSize()(), imageView)
        }
      }
    )(context)

    this
  }

  /**
   * plays double role
   * 1) is used to downsample the bitmap in order to not overload memory with large images
   * 2) is used to calculate the actual size of the image view
   */
  protected val wrapperSize = new WriteOnce[() => ImageSizeDp]

  protected def setBitmap(bitmap: Bitmap, targetSize: ImageSizeDp, imageView: ImageView): Unit = {
    val imageSize = ImageSizePx(width = Pixel(bitmap.getWidth), height = Pixel(bitmap.getHeight))

    val finalSize = imageSize.fitIfLarger(targetSize.toPx(context))

    imageView.getLayoutParams.width = finalSize.width.value

    imageView.getLayoutParams.height = finalSize.height.value

    //log.log("card view width: " + finalSize.width.value + " and height: " + finalSize.height.value)(context);

    imageView.setImageBitmap(bitmap)
  }

  private def getProgressBar(view: View): ProgressBar = {
    val viewGroup = view.getParent.asInstanceOf[ViewGroup]

    val childCount = viewGroup.getChildCount

    val indexFound = (0 to childCount).find(index => viewGroup.getChildAt(index).isInstanceOf[ProgressBar])

    assert(indexFound.isDefined)

    viewGroup.getChildAt(indexFound.get).asInstanceOf[ProgressBar]
    //.findViewById(R.id.cardProgressBar).asInstanceOf[];
  }

}
