package views

import models.{Dimension, Card}
import android.widget.ImageView
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import myandroid.log

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
//http://www.levinotik.com/2012/09/14/scala-abstract-classes-traits-and-self-types/
trait CardImageView extends WrappedImage {
  //this: FlipImageView =>  //to force that only this class can have this trait

  private var cardOption: Option[Card] = None

  def getCard = cardOption.get

  /**
   * check code. If you have a card you also certainly have initialized wrapper size
   */
  def hasCard = cardOption.isDefined

  protected def swapSides() = {
    val prevDimension = cardOption.map(_.currentDimension)

    cardOption = cardOption.map(_.nextDimension())

    prevDimension
  }

  protected def setCard(card: Card,
                        imageView: ImageView,
                        afterLoad: BitmapHolder => Unit)(implicit context: Context) = {
    cardOption = Some(card)

    card.currentDimension match {
      case Dimension.frontSide => loadFrontPhoto(imageView, afterLoad)
      case Dimension.backSide => loadBackPhoto(imageView, afterLoad)
    }

    this
  }

  protected def loadFrontPhoto(imageView: ImageView,
                               afterFront: BitmapHolder => Unit)(implicit context: Context): Unit = {
    setFrontPhoto(
      imageView,
      onAfterSet = {
        frontBitmap =>
          val frontBitmapHolder = BitmapHolder(
            getImageSizePx(frontBitmap),
            new BitmapDrawable(context.getResources, frontBitmap)
          )

          afterFront(frontBitmapHolder)

          //card might have changed in the mean time (remember cardOption is variable)
          if (getCard.currentDimension == Dimension.frontSide) {
            log log "setting front side of the card"

            setDrawable(frontBitmapHolder, wrapperSize()(), imageView)
          } else {
            log log "not setting front side of the card because it was initialized with back dimension"
          }
      }
    )
  }

  protected def loadBackPhoto(imageView: ImageView,
                              afterBack: BitmapHolder => Unit)(implicit context: Context): Unit = {
    setBackPhoto(
      imageView,
      onAfterSet = {
        backBitmap =>
          val backBitmapHolder = BitmapHolder(
            getImageSizePx(backBitmap),
            new BitmapDrawable(context.getResources, backBitmap)
          )

          afterBack(backBitmapHolder)

          //card might have changed in the mean time (remember cardOption is variable)

          if (getCard.currentDimension == Dimension.backSide) {
            log log "setting back drawable, must not have called animation"

            setDrawable(backBitmapHolder, wrapperSize()(), imageView)
          } else {
            log log "not doing anything because we are showing front side"
          }
      }
    )
  }

  private def setFrontPhoto(imageView: ImageView,
                            onAfterSet: Bitmap => Unit)(implicit context: Context): Unit = {
    getCard.getFrontPhoto.fold(ifEmpty = {
      assert(wrapperSize.isInitialized,
        "please initialize wrapper size before setting front photo")

      onAfterSet(getCard.getDummyFrontBitmap(wrapperSize()()))
    })({
      photo =>
        setPhoto(
          photo = photo,
          imgView = imageView,
          preOp = {
            imageView =>
              showProgressBar(imageView)
          }, postOp = {
            (imView, bitmap) =>
              hideProgressBar(imageView)

              onAfterSet(bitmap)
          }
        )
    })
  }

  private def setBackPhoto(imageView: ImageView,
                           onAfterSet: Bitmap => Unit)(implicit context: Context): Unit = {
    getCard.getBackPhoto.fold(ifEmpty = {
      assert(wrapperSize.isInitialized,
        "please initialize wrapper size before setting back photo")

      onAfterSet(getCard.getDummyBackBitmap(wrapperSize()()))
    })({
      photo =>
        setPhoto(
          photo = photo,
          imgView = imageView,
          preOp = {
            imView =>
              showProgressBar(imageView)
          }, postOp = {
            (imView, bitmap) =>
              hideProgressBar(imageView)

              onAfterSet(bitmap)
          }
        )
    })

  }

}
