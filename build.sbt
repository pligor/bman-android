// so we can use keywords from Android, such as 'Android' and 'proguardOptions'
import android.Keys._

import android.Dependencies.AutoLibraryProject

// load the android plugin into the build
android.Plugin.androidBuild

name := "bman"

//to avoid getting out of memory exception
dexMaxHeap in Android := "1408m"

// pick the version of scala you want to use
//scalaVersion := "2.9.2"
scalaVersion := "2.10.3"

//scalacOptions in Compile ++= Seq("-deprecation", "-unchecked")
scalacOptions in Compile ++= Seq("-deprecation", "-unchecked", "-feature", "-language:postfixOps", "-language:implicitConversions")

//for non-ant-based projects, you'll need this for the specific build target:
platformTarget in Android := "android-17"

// call install and run without having to prefix with android:
run <<= run in Android

install <<= install in Android

//PROGUARD CACHES///////////////////////////////////////////////////////////////////////////////////

//these are causing unknown to me issues
//proguardCache in Android += ProguardCache("org.spongycastle") % "com.madgag" % "scpg-jdk15on"
//proguardCache in Android += ProguardCache("android.support.v4") % "android.support.v4"
//proguardCache in Android += ProguardCache("org.apache") % "org.apache"

//proguardCache in Android += ProguardCache("com.google.gson") % "com.google.code.gson"

//proguardCache in Android += ProguardCache("com.google.i18n") % "com.googlecode.libphonenumber"

//proguardCache in Android += ProguardCache("scalaj") % "org.scalaj"

//proguardCache in Android += ProguardCache("spray") % "io.spray"

//proguardCache in Android += ProguardCache("org.parboiled") % "org.parboiled"

//proguardCache in Android += ProguardCache("com.fasterxml.jackson") % "io.backchat.jerkson"

//RESOLVERS/////////////////////////////////////////////////////////////////////////////////////////

//Resolver.file("Local Repository", file("/home/weakwire/.ivy2/cache/"))(Resolver.ivyStylePatterns)
//"Big Bee Consultants" at "http://repo.bigbeeconsultants.co.uk/repo",
resolvers += "spray" at "http://repo.spray.io/"

resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

//LIBRARY DEPENDENCIES//////////////////////////////////////////////////////////////////////////////

libraryDependencies += "org.scaloid" %% "scaloid" % "3.0-8"

libraryDependencies += "com.google.guava" % "guava" % "r09"

libraryDependencies += "commons-io" % "commons-io" % "2.4"

//url
libraryDependencies += "com.github.theon" %% "scala-uri" % "0.3.6"

//encryption
libraryDependencies += "commons-codec" % "commons-codec" % "1.8"

//THERE IS A CONFLICT BETWEEN THE COMPLETE BOUNCYCASTLE AND THE BOUNCYCASTLE EMBEDDED IN ANDROID
//this is why instead of the above we use the below:
//http://search.maven.org/#search|ga|1|g:com.madgag%20AND%20v:1.47.0.2
libraryDependencies += "com.madgag" % "scpg-jdk15on" % "1.47.0.3"

//email
//"javax.mail" % "mail" % "1.4.7", //http://www.oracle.com/technetwork/java/javamail/index-138643.html
//"oro" % "oro" % "2.0.8",
//"commons-validator" % "commons-validator" % "1.3.1",

//phone
libraryDependencies += "com.googlecode.libphonenumber" % "libphonenumber" % "5.9"

libraryDependencies += "com.googlecode.libphonenumber" % "geocoder" % "2.10"

//json
//libraryDependencies += "io.backchat.jerkson" %% "jerkson" % "0.7.0"

libraryDependencies += "com.google.code.gson" % "gson" % "2.2.4"

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.2.2-RC1"

//"net.debasishg" %% "sjson" % "0.19",
//TODO replace spray json with play json
libraryDependencies += "io.spray" %% "spray-json" % "1.2.5" /*cross CrossVersion.full*/

//http clients
libraryDependencies += "commons-httpclient" % "commons-httpclient" % "3.1"

libraryDependencies += "org.scalaj" %% "scalaj-http" % "0.3.12" exclude("junit", "junit") //scalaj 0.3.10 does not play with scala-android

//bee client
//"uk.co.bigbeeconsultants" %% "bee-client" % "0.21.+",
//"org.slf4j" % "slf4j-api" % "1.7.+",
//"ch.qos.logback" % "logback-core" % "1.0.+",
//"ch.qos.logback" % "logback-classic" % "1.0.+",

//test
//"junit" % "junit" % "4.10" % "test",  //DUPLICATE WARNINGS, means is already defined
//"org.scalatest" %% "scalatest" % "1.8" % "test",

//to scope pou einai orismeno sto test mallon simainei oti den tha symperilavoume auto to library sto
//teliko apk. kalo auto. meiwnei to megethos tou apk
//libraryDependencies += "org.specs2" %% "specs2" % "1.12.4.1" % "test" //for scala 2.9
libraryDependencies += "org.specs2" %% "specs2" % "2.3.6" % "test"


